/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <type_traits>

// #include <armarx/control/common/control_law/TaskspaceImpedanceController.h>
#include <armarx/control/common/control_law/aron/TaskspaceImpedanceControllerConfig.aron.generated.h>

namespace armarx
{
    template <typename AronType, typename T>
    T
    fromAron(const ::armarx::aron::data::dto::DictPtr& dto)
    {
        static_assert(
            std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass, AronType>::value,
            "AronType must be an ARON generated type");

        const auto dtoConfig = AronType::FromAron(dto);

        T config;
        fromAron(dtoConfig, config);

        return config;
    }
} // namespace armarx

namespace armarx::control::common::control_law
{

    // TaskspaceImpedanceController

    // void fromAron(const arondto::TaskSpaceImpedanceControllerConfig& dto,
    //               TaskspaceImpedanceController::Config& bo);


} // namespace armarx::control::common::control_law
