#include <boost/algorithm/clamp.hpp>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>
#include "FTSensor.h"


namespace armarx::control::common
{

void FTSensor::initialize(
        const std::string& name,
        const std::string& frame,
        const float filter,
        const float forceDeadZone,
        const float torqueDeadZone,
        const float calibrationTimeLimit,
        const VirtualRobot::RobotNodeSetPtr& rns,
        const RobotUnitPtr& robotUnit)
{
    sensorName = name;
    sensorFrame = frame;

    const SensorValueBase* svlf = robotUnit->getSensorDevice(sensorName)->getSensorValue();
    ARMARX_CHECK_NOT_NULL(rns);
    sensorNode = rns->getNode(sensorFrame);
    forceSensor = svlf->asA<SensorValueForceTorque>();

    timeLimit = calibrationTimeLimit;
    ftFilter = filter;
    deadZoneForce = forceDeadZone;
    deadZoneTorque = torqueDeadZone;

    filteredForce.setZero();
    filteredTorque.setZero();
    filteredForceInRoot.setZero();
    filteredTorqueInRoot.setZero();

    tcpGravityCompensation.setZero();
    reset();
}

void FTSensor::reset()
{
    /// you have to call this method **only** in the first loop of rtRun
    tcpMass = 0.0f;
    tcpCoMInFTSensorFrame.setZero();

    forceOffset.setZero();
    torqueOffset.setZero();
    ft.setZero();

    calibrated.store(false);
    timeForCalibration = 0.0f;

    //    Eigen::Matrix4f forceFrameInRoot = sensorNode->getPoseInRootFrame();
    //    forceFrameInTCP.block<3, 3>(0, 0) = currentPose.block<3, 3>(0, 0).transpose() * forceFrameInRoot.block<3, 3>(0, 0);
    //    tcpCoMInTCPFrame = forceFrameInTCP.block<3, 3>(0, 0) * tcpCoMInFTSensorFrame;
}

bool FTSensor::calibrate(double deltaT)
{
    /// this will calibrate the force torque value in FTSensorFrame until timeLimit is reached
    Eigen::Matrix3f sensorOriInRoot = sensorNode->getOrientationInRootFrame();
    forceOffset  = (1 - ftFilter) * forceOffset  + ftFilter * (forceSensor->force  - sensorOriInRoot.transpose() * tcpGravityCompensation.head(3));
    torqueOffset = (1 - ftFilter) * torqueOffset + ftFilter * (forceSensor->torque - sensorOriInRoot.transpose() * tcpGravityCompensation.tail(3));

    timeForCalibration = timeForCalibration + deltaT;
    if (timeForCalibration > timeLimit)
    {
        calibrated.store(true);
        /// rt is ready only if the FT sensor is calibrated, and the rt2interface buffer is updated (see above)
        ARMARX_IMPORTANT << "FT calibration done, (RT is ready!) \n" << VAROUT(forceOffset) << "\n" << VAROUT(torqueOffset) << "\n" << VAROUT(tcpGravityCompensation);
    }
    return calibrated.load();
}


//void FTSensor::enableGravityCompensation(const float mass, const Eigen::Vector3f& tcpCoMInFTFrame)
//{
//    enableTCPGravityCompensation.store(true);
//    tcpMass = mass;
//    tcpCoMInFTSensorFrame = tcpCoMInFTFrame;
//}

//void FTSensor::disableGravityCompensation()
//{
//    enableTCPGravityCompensation.store(false);
//    tcpMass = 0.0f;
//    tcpCoMInFTSensorFrame.setZero();
//    tcpGravityCompensation.setZero();
//}

Eigen::Vector6f& FTSensor::compensateTCPGravity(const FTBufferData& data)
{
    //    /// update variables from rt buffer
    //    tcpMass = data.tcpMass;
    //    tcpCoMInTCPFrame = forceFrameInTCP.block<3, 3>(0, 0) * data.tcpCoMInFTSensorFrame;

    /// static compensation in root frame
    if (data.enableTCPGravityCompensation)
    {
        Eigen::Vector3f forceVec = data.tcpMass * gravity;
        Eigen::Vector3f torqueVec = (sensorNode->getOrientationInRootFrame() * data.tcpCoMInFTSensorFrame).cross(forceVec);
        tcpGravityCompensation << forceVec, torqueVec;
        //        Eigen::Vector3f localGravity = currentPose.block<3, 3>(0, 0).transpose() * gravity;
        //        Eigen::Vector3f localForceVec = data.tcpMass * localGravity;
        //        Eigen::Vector3f localTorqueVec = tcpCoMInTCPFrame.cross(localForceVec);

        //        tcpGravityCompensation << localForceVec, localTorqueVec;
    }
    else
    {
        tcpGravityCompensation.setZero();
    }
    return tcpGravityCompensation;
}


Eigen::Vector6f& FTSensor::getFilteredForceTorque(const FTBufferData& data)
{
    filteredForce  = (1 - ftFilter) * filteredForce  + ftFilter * forceSensor->force;
    filteredTorque = (1 - ftFilter) * filteredTorque + ftFilter * forceSensor->torque;

    Eigen::Matrix3f forceOriInRoot = sensorNode->getOrientationInRootFrame();
    filteredForceInRoot  = forceOriInRoot * (filteredForce  - forceOffset)  - tcpGravityCompensation.head(3) - data.forceBaseline;
    filteredTorqueInRoot = forceOriInRoot * (filteredTorque - torqueOffset) - tcpGravityCompensation.tail(3) - data.torqueBaseline;

    for (size_t i = 0; i < 3; ++i)
    {
        if (fabs(filteredForceInRoot(i)) > deadZoneForce)
        {
            filteredForceInRoot(i) -= (filteredForceInRoot(i) / fabs(filteredForceInRoot(i))) * deadZoneForce;
        }
        else
        {
            filteredForceInRoot(i) = 0;
        }

        if (fabs(filteredTorqueInRoot(i)) > deadZoneTorque)
        {
            filteredTorqueInRoot(i) -= (filteredTorqueInRoot(i) / fabs(filteredTorqueInRoot(i))) * deadZoneTorque;
        }
        else
        {
            filteredTorqueInRoot(i) = 0;
        }
    }

    ft << filteredForceInRoot, filteredTorqueInRoot;
    return ft;
}
}
