#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

//#include <RobotAPI/libraries/DMPController/TaskSpaceDMPController.h>
//#include <dmp/representation/dmp/umidmp.h>
#include <mplib/factories/VMPFactory.h>
#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/representation/vmp/PrincipalComponentVMP.h>

#include <armarx/control/common/MPPoolInterface.h>

namespace armarx
{

    class MPInterface;
    typedef std::shared_ptr<MPInterface> MPInterfacePtr;

    class MPInterface
    {

    public:
        MPInterface(){};
        ~MPInterface(){};

        void initialize(const std::string& mpType, const std::string &regModel, const int nKernels, const std::string &fdir);
        std::string getMPName();
        // TODO capture the user setting (e.g. type of controllers that can be used in
        // combination with this type of MP

        /// control
        void flow(double deltaT, const Eigen::Matrix4f& currentPose, const Eigen::VectorXf& twist);
        double flow(double canVal, double deltaT, const Eigen::Matrix4f& currentPose, const Eigen::VectorXf& twist);
        void start(const Ice::DoubleSeq& goals);
        void startWithTime(const Ice::DoubleSeq& goals, Ice::Double timeDuration);
        void stop();
        void pause();
        void resume();
        void reset();
        bool isFinished();

        /// setting
        void learnFromCSV(const Ice::StringSeq& fileNames);
        //        void learnFromSampledTraj()

        void setGoal(const Ice::DoubleSeq& goals);
        void setStartAndGoal(const Ice::DoubleSeq& starts, const Ice::DoubleSeq& goals);
        void setViaPoint(Ice::Double u, const Ice::DoubleSeq& viapoint);
        void removeAllViaPoint();
        double getCanVal();

        /// serialze
        void setWeight(const std::vector<std::vector<double>> &weights);
        void setTranslationWeights(const std::vector<std::vector<double>>& weights);
        void setRotationWeights(const std::vector<std::vector<double>>& weights);
        DoubleSeqSeq getWeight();
        std::string serialize();
        Ice::DoubleSeq deserialize(const std::string&);

        ///
        Ice::Double getCanonicalValue();
        void mpRun();

    private:
        std::string mpName;
        std::string regressionModel;
        std::vector<mplib::representation::MPState> currentJointState;

        //        mutable MutexType mpMutex;
        std::shared_ptr<mplib::representation::AbstractMovementPrimitive> vmp;
        //        TaskSpaceDMPControllerConfig config;

        // phaseStop parameters
        double phaseL;
        double phaseK;
        double phaseDist0;
        double phaseDist1;
        double posToOriRatio;

        std::atomic_bool mpRunning = false;
        std::atomic_bool mpFinished = false;
        std::atomic_bool paused = false;
        double canonicalValue = 1.0;
        double motionTimeDuration = 1.0;

    };

    class FirstOrderMP: public MPInterface
    {

    };

    class SecondOrderMP: public MPInterface
    {

    };
}
