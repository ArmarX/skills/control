/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"

namespace armarx::control::common::control_law
{
    void
    fromAron(const arondto::TaskSpaceImpedanceControllerConfig& dto,
             TaskspaceImpedanceController::Config& bo)
    {
        bo = TaskspaceImpedanceController::Config{.kpImpedance = dto.kpImpedance,
                                                  .kdImpedance = dto.kdImpedance,
                                                  .kpNullspace = dto.kpNullspace,
                                                  .kdNullspace = dto.kdNullspace,
                                                  .desiredPose = dto.desiredPose,
                                                  .desiredTwist = dto.desiredTwist,
                                                  .desiredNullspaceJointAngles =
                                                      dto.desiredNullspaceJointAngles,
                                                  .torqueLimit = dto.torqueLimit,
                                                  .qvelFilter = dto.qvelFilter};
    }

    


} // namespace armarx::control::common::control_law
