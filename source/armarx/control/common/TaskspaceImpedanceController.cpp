#include <boost/algorithm/clamp.hpp>

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>
#include "TaskspaceImpedanceController.h"

#include <ArmarXCore/core/logging/Logging.h>


using namespace armarx;

void TaskspaceImpedanceController::initialize(const VirtualRobot::RobotNodeSetPtr& rns)
{
    ARMARX_CHECK_NOT_NULL(rns);
    tcp = rns->getTCP();
    ARMARX_CHECK_NOT_NULL(tcp);
    ik.reset(new VirtualRobot::DifferentialIK(
                 rns, rns->getRobot()->getRootNode(),
                 VirtualRobot::JacobiProvider::eSVDDamped));

    /// joint space variables
    numOfJoints = rns->getSize();
    s.qpos.resize(numOfJoints);
    s.qvel.resize(numOfJoints);
    desiredJointTorques.resize(numOfJoints);

    s.currentTwist.setZero();
    s.forceImpedance.setZero();

    I = Eigen::MatrixXf::Identity(numOfJoints, numOfJoints);
}

void TaskspaceImpedanceController::firstRun()
{
    s.previousTargetPose = s.currentPose;
    s.desiredTCPPose = s.currentPose;
    ARMARX_INFO << VAROUT(s.desiredTCPPose) << VAROUT(s.previousTargetPose);
}

bool TaskspaceImpedanceController::updateControlStatus(
    const Config& cfg,
    const IceUtil::Time& timeSinceLastIteration,
    std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors,
    std::vector<const SensorValue1DoFActuatorVelocity*> velocitySensors,
    std::vector<const SensorValue1DoFActuatorPosition*> positionSensors
)
{
    /// check size and value
    {
        ARMARX_CHECK_EQUAL(numOfJoints, torqueSensors.size());
        ARMARX_CHECK_EQUAL(numOfJoints, velocitySensors.size());
        ARMARX_CHECK_EQUAL(numOfJoints, positionSensors.size());

        ARMARX_CHECK_EQUAL(numOfJoints, cfg.kpNullspace.size());
        ARMARX_CHECK_EQUAL(numOfJoints, cfg.kdNullspace.size());
        ARMARX_CHECK_EQUAL(numOfJoints, cfg.desiredNullspaceJointAngles.size());
        ARMARX_CHECK_GREATER_EQUAL(cfg.torqueLimit, 0);
    }

    /// ----------------------------- get target status from config ---------------------------------------------
    s.kpImpedance = cfg.kpImpedance;
    s.kdImpedance = cfg.kdImpedance;
    s.kpNullspace = cfg.kpNullspace;
    s.kdNullspace = cfg.kdNullspace;

    s.desiredTCPPose = cfg.desiredTCPPose;
    s.desiredNullspaceJointAngles = cfg.desiredNullspaceJointAngles;

    s.torqueLimit = cfg.torqueLimit;
    //    s.qvelFilter  = cfg.qvelFilter;

    /// ----------------------------- Validate control target ---------------------------------------------
    bool valid = false;
    if ((s.previousTargetPose.block<3, 1>(0, 3) - s.desiredTCPPose.block<3, 1>(0, 3)).norm() > 100.0f)
    {
        ARMARX_WARNING << "new target \n\n" << s.desiredTCPPose << "\n\nis too far away from\n\n" << VAROUT(s.previousTargetPose);
        s.desiredTCPPose = s.previousTargetPose;
    }
    else
    {
        s.previousTargetPose = s.desiredTCPPose;
        valid = true;
    }

    /// ----------------------------- get current status of robot ---------------------------------------------
    /// original data in ArmarX use the following units:
    /// position in mm,
    /// joint angles and orientation in radian,
    /// velocity in mm/s and radian/s, etc.
    /// here we convert mm to m, if you use MP from outside, make sure to convert it back to mm

    s.currentPose = tcp->getPoseInRootFrame();
    Eigen::MatrixXf jacobi = ik->getJacobianMatrix(tcp, VirtualRobot::IKSolver::CartesianSelection::All);
    jacobi.block(0, 0, 3, numOfJoints) = 0.001 * jacobi.block(0, 0, 3, numOfJoints);
    ARMARX_CHECK_EQUAL(numOfJoints, jacobi.cols());
    ARMARX_CHECK_EQUAL(6, jacobi.rows());
    s.jacobi = jacobi;

    Eigen::VectorXf qvelRaw(numOfJoints);
    for (size_t i = 0; i < velocitySensors.size(); ++i)
    {
        s.qpos(i)  = positionSensors[i]->position;
        qvelRaw(i) = velocitySensors[i]->velocity;
    }

    s.qvel = (1 - cfg.qvelFilter) * s.qvel + cfg.qvelFilter * qvelRaw;
    s.currentTwist = jacobi * s.qvel;
    s.deltaT = timeSinceLastIteration.toSecondsDouble();
    return valid;
}

const Eigen::VectorXf& TaskspaceImpedanceController::run(bool rtReady)
{
    /// ----------------------------- Impedance control ---------------------------------------------
    /// calculate pose error between target pose and current pose
    /// !!! This is very important: you have to keep postion and orientation both
    ///     with UI unit (meter, radian) to calculate impedance force.

    Eigen::Matrix3f diffMat = s.desiredTCPPose.block<3, 3>(0, 0) * s.currentPose.block<3, 3>(0, 0).transpose();
    Eigen::Vector6f poseErrorImp;
    poseErrorImp.head(3) = 0.001 * (s.desiredTCPPose.block<3, 1>(0, 3) - s.currentPose.block<3, 1>(0, 3));
    poseErrorImp.tail(3) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);
    s.forceImpedance =
            s.kpImpedance.cwiseProduct(poseErrorImp) -
            s.kdImpedance.cwiseProduct(s.currentTwist);

    /// ----------------------------- Nullspace PD Control --------------------------------------------------
    Eigen::VectorXf nullspaceTorque =
            s.kpNullspace.cwiseProduct(s.desiredNullspaceJointAngles - s.qpos) -
            s.kdNullspace.cwiseProduct(s.qvel);

    /// ----------------------------- Map TS target force to JS --------------------------------------------------
    const Eigen::MatrixXf jtpinv = ik->computePseudoInverseJacobianMatrix(s.jacobi.transpose(), lambda);
    desiredJointTorques =
            s.jacobi.transpose() * s.forceImpedance +
            (I - s.jacobi.transpose() * jtpinv) * nullspaceTorque;

    for (int i = 0; i < desiredJointTorques.size(); ++i)
    {
        desiredJointTorques(i) = boost::algorithm::clamp(desiredJointTorques(i), -s.torqueLimit, s.torqueLimit);
    }

    return desiredJointTorques;
}
