#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace armarx
{
    class TaskspaceImpedanceController
    {
    public:
        /// you can set the following values from outside of the rt controller via Ice interfaces
        struct Config
        {
            Eigen::Vector6f kpImpedance;
            Eigen::Vector6f kdImpedance;

            Eigen::VectorXf kpNullspace;
            Eigen::VectorXf kdNullspace;

            Eigen::Matrix4f desiredTCPPose;
            Eigen::VectorXf desiredNullspaceJointAngles;

            float torqueLimit;
            float qvelFilter;
        };

        /// internal status of the controller, containing intermediate variables, mutable targets
        struct Status: Config
        {
            /// joint space variable
            Eigen::VectorXf qpos;
            Eigen::VectorXf qvel;

            /// task spaace variables
            Eigen::Matrix4f previousTargetPose;
            Eigen::Matrix4f currentPose;
            Eigen::Vector6f currentTwist;
            Eigen::Vector6f forceImpedance;

            /// others
            Eigen::MatrixXf jacobi;
            double deltaT;
        };

    private:
        /// joint space variables
        unsigned int numOfJoints;

        Eigen::MatrixXf I;

        VirtualRobot::DifferentialIKPtr ik;
        VirtualRobot::RobotNodePtr tcp;

        const float lambda = 2.0f;
    public:
        Status s;
        Eigen::VectorXf desiredJointTorques;

        void initialize(const VirtualRobot::RobotNodeSetPtr& rns);
        bool updateControlStatus(
            const Config& cfg,
            const IceUtil::Time& timeSinceLastIteration,
            std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors,
            std::vector<const SensorValue1DoFActuatorVelocity*> velocitySensors,
            std::vector<const SensorValue1DoFActuatorPosition*> positionSensors);

        const Eigen::VectorXf& run(bool rtReady);
        void firstRun();
    };
}
