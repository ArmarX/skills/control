#include <boost/algorithm/clamp.hpp>
#include <boost/filesystem.hpp>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>
#include "MP.h"



namespace armarx
{

void MPInterface::initialize(const std::string &mpType, const std::string &regModel, const int nKernels, const std::string &fdir)
{
    mplib::representation::VMPType vmpType = mplib::representation::VMPType::PrincipalComponent;
    if (mpType != "")
    {
        vmpType = mplib::representation::VMPType::_from_string_nocase(mpType.c_str());
    }

    mplib::factories::VMPFactory mpfactory;
    mpfactory.addConfig("kernelSize", nKernels);
    vmp = mpfactory.createMP(vmpType);
    regressionModel = regModel;


}

std::string MPInterface::getMPName()
{
    return mpName;
}

void MPInterface::start(const Ice::DoubleSeq &goals)
{
    //    rtFirstRun.store(true);
    //    while (rtFirstRun.load() || !rtReady.load() || !rt2InterfaceBuffer.updateReadBuffer())
    //    {
    //        usleep(100);
    //    }

    //    Eigen::Matrix4f pose = rt2InterfaceBuffer.getUpToDateReadBuffer().currentTcpPose;

    //    LockGuardType guard {mpMutex};
    //    vmp->->prepareExecution(dmpCtrl->eigen4f2vec(pose), goals);

    //    if (isNullSpaceJointDMPLearned && useNullSpaceJointDMP)
    //    {
    //        ARMARX_INFO << "Using Null Space Joint DMP";
    //    }
    ARMARX_INFO << VAROUT(motionTimeDuration);
    //    ARMARX_INFO << VAROUT(vmp->getViaPoints().size());

    mpRunning.store(true);
    resume();
    ARMARX_INFO << "start mp ...";
}

void MPInterface::startWithTime(const Ice::DoubleSeq& goals, Ice::Double timeDuration)
{
    motionTimeDuration = timeDuration;
    start(goals);
}

void MPInterface::stop()
{
    mpRunning.store(false);
}

void MPInterface::pause()
{
    paused.store(true);
}

void MPInterface::resume()
{
    paused.store(false);
}

void MPInterface::reset()
{
    if (mpRunning.load())
    {
        ARMARX_INFO << "cannot reset a running mp, please stop it first.";
    }
    // TODO, rtFirstRun = true;
}

bool MPInterface::isFinished()
{
    return false;
}

void MPInterface::learnFromCSV(const Ice::StringSeq& fileNames)
{
    std::vector<mplib::core::SampledTrajectory> trajs;
    for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(fileNames[0]), {}))
    {
        mplib::core::SampledTrajectory traj;
        traj.readFromCSVFile(entry.path().string());
        trajs.push_back(traj);
    }

    if (regressionModel == "gpr")
    {
        vmp->setBaseFunctionApproximator(
                    std::make_unique<mplib::math::function_approximation::GaussianProcessRegression<mplib::math::kernel::SquaredExponentialCovarianceFunction>>
                    (0.01, 0.1, 0.000001)
                    );
        for (auto& traj : trajs)
        {
            traj = mplib::core::SampledTrajectory::downSample(traj, 100);
        }
    }

    vmp->learnFromTrajectories(trajs);
}

void MPInterface::setGoal(const Ice::DoubleSeq& goals)
{

}

void MPInterface::setStartAndGoal(const Ice::DoubleSeq& starts, const Ice::DoubleSeq& goals)
{

}

void MPInterface::setViaPoint(Ice::Double u, const Ice::DoubleSeq& viapoint)
{

}

void MPInterface::removeAllViaPoint()
{

}

double MPInterface::getCanonicalValue()
{
    return canonicalValue;
}

/// serialze
void MPInterface::setWeight(const std::vector<std::vector<double>>& weights)
{
    vmp->setWeights(weights);
}

//DoubleSeqSeq MPInterface::getWeight()
//{

//}

//std::string MPInterface::serialize()
//{

//}

//Ice::DoubleSeq MPInterface::deserialize(const std::string&)
//{

//}

/////
//Ice::Double MPInterface::getCanVal()
//{

//}

void MPInterface::mpRun()
{

}

}



