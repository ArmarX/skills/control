#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

namespace armarx::control::common
{

    enum class ControllerType
    {
        TSAdmittance,
        TSImpedance,
        KeypointAdmittance,
        KeypointImpedance,
        TSAdmittanceMP,
        TSImpedanceMP,
        KeypointAdmittanceMP,
        KeypointImpedanceMP,
        KVILImpedanceMP,
        QPWholeBodyImpedance,
        QPWholeBodyVelocity
    };


    inline const simox::meta::EnumNames<ControllerType> ControllerTypeNames{
        {ControllerType::TSAdmittance, "NJointTaskspaceAdmittanceController"},
        {ControllerType::TSImpedance, "NJointTaskspaceImpedanceController"},
        {ControllerType::KeypointAdmittance, "NJointKeypointsAdmittanceController"},
        {ControllerType::KeypointImpedance, "KeypointImpedance"},
        {ControllerType::TSAdmittanceMP, "NJointTSAdmittanceMPController"},
        {ControllerType::TSImpedanceMP, "NJointTSImpedanceMPController"},
        {ControllerType::KeypointAdmittanceMP, "NJointKeypointsAdmittanceMPController"},
        {ControllerType::KeypointImpedanceMP, "NJointKeypointsImpedanceMPController"},
        {ControllerType::KVILImpedanceMP, "NJointKVILImpedanceMPController"},
        {ControllerType::QPWholeBodyImpedance, "NJointQPWholeBodyImpedanceController"},
        {ControllerType::QPWholeBodyVelocity, "NJointQPWholeBodyVelocityController"}};

} // namespace armarx::control::common
