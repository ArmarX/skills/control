#include <boost/algorithm/clamp.hpp>
#include <boost/filesystem.hpp>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>
#include "MPPool.h"



namespace armarx
{

void MPPool::initialize(
        const std::string &mpType,
        const std::string& regModel,
        const int nKernels,
        const std::string& fdir)
{
    mplib::representation::VMPType vmpType = mplib::representation::VMPType::PrincipalComponent;
    if (mpType != "")
    {
        vmpType = mplib::representation::VMPType::_from_string_nocase(mpType.c_str());
    }

    mplib::factories::VMPFactory mpfactory;
    mpfactory.addConfig("kernelSize", nKernels);
    vmp = mpfactory.createMP(vmpType);


    std::vector<mplib::core::SampledTrajectory> trajs;
    for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(fdir), {}))
    {
        mplib::core::SampledTrajectory traj;
        traj.readFromCSVFile(entry.path().string());
        trajs.push_back(traj);
    }

    if (regModel == "gpr")
    {
        vmp->setBaseFunctionApproximator(
                    std::make_unique<mplib::math::function_approximation::GaussianProcessRegression<mplib::math::kernel::SquaredExponentialCovarianceFunction>>
                    (0.01, 0.1, 0.000001)
                    );
        for (auto& traj : trajs)
        {
            traj = mplib::core::SampledTrajectory::downSample(traj, 100);
        }
    }

    vmp->learnFromTrajectories(trajs);

}

void MPPool::start(const Ice::DoubleSeq &goals, const Ice::Current &iceCurrent)
{

}

void MPPool::startWithTime(const Ice::DoubleSeq &goals, Ice::Double timeDuration, const Ice::Current &)
{

}

void MPPool::stop(const Ice::Current &)
{

}

void MPPool::pause(const Ice::Current &)
{

}

void MPPool::resume(const Ice::Current &)
{

}

void MPPool::reset(const Ice::Current &)
{

}

bool MPPool::isFinished(const Ice::Current &)
{
    return false;
}

void MPPool::learnFromCSV(const Ice::StringSeq &fileNames, const Ice::Current &iceCurrent)
{

}

void MPPool::setGoal(const Ice::DoubleSeq &goals, const Ice::Current &iceCurrent)
{

}

void MPPool::setStartAndGoal(const Ice::DoubleSeq &starts, const Ice::DoubleSeq &goals, const Ice::Current &iceCurrent)
{

}

void MPPool::setViaPoint(double u, const Ice::DoubleSeq &viapoint, const Ice::Current &iceCurrent)
{

}

void MPPool::removeAllViaPoint(const Ice::Current &iceCurrent)
{

}

//void MPPool::setWeight(const Ice::DoubleSeqSeq& weights, const Ice::Current &iceCurrent)
//{

//}

std::string MPPool::serialize(const Ice::Current &iceCurrent)
{
    return "";
}

//Ice::DoubleSeq MPPool::deserialize(const std::string &, const Ice::Current &iceCurrent)
//{

//}

double MPPool::getCanVal(const Ice::Current &iceCurrent)
{
    return 0.0;
}


}



