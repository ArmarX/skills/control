/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotControllers::RobotNJointControllersInterface
 * @author     Jianfeng Gao ( jianfeng dot gao at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>


module armarx
{
    interface MPPoolInterface extends NJointControllerInterface
    {
        void start(Ice::DoubleSeq goals);
        void startWithTime(Ice::DoubleSeq goals, double timeDuration);
        void stop();
        void pause();
        void resume();
        void reset();
        bool isFinished();

        /// setting
        void learnFromCSV(Ice::StringSeq fileNames);
        //        void learnFromSampledTraj()

        void setGoal(Ice::DoubleSeq goals);
        void setStartAndGoal(Ice::DoubleSeq starts, Ice::DoubleSeq goals);
        void setViaPoint(double u, Ice::DoubleSeq viapoint);
        void removeAllViaPoint();

        /// serialze
        //        void setWeight(Ice::DoubleSeqSeq weights);
        //        Ice::DoubleSeqSeq getWeight();
        string serialize();
        //        Ice::DoubleSeq deserialize(string);

        /// utilities
        double getCanVal();
    };
   
};
