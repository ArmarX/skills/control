#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

//#include <RobotAPI/libraries/DMPController/TaskSpaceDMPController.h>
//#include <dmp/representation/dmp/umidmp.h>
#include <mplib/factories/VMPFactory.h>
#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/representation/vmp/PrincipalComponentVMP.h>

#include <armarx/control/common/MPPoolInterface.h>
#include "MP.h"


namespace armarx
{
    class MPPool:
            public MPPoolInterface
    {
    public:
        struct MPPoolData
        {
            std::string name;
        };

        void initialize(const std::string& mpType, const std::string &regModel, const int nKernels, const std::string &fdir);
        void mpRun();

    public:
        /// Interface functions
        /// control
        void start(const Ice::DoubleSeq& goals, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void startWithTime(const Ice::DoubleSeq& goals, double timeDuration, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void stop(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void pause(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void resume(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void reset(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        bool isFinished(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;

        /// setting
        void learnFromCSV(const Ice::StringSeq& fileNames, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        //        void learnFromSampledTraj()

        void setGoal(const Ice::DoubleSeq& goals, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void setStartAndGoal(const Ice::DoubleSeq& starts, const Ice::DoubleSeq& goals, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void setViaPoint(double u, const Ice::DoubleSeq& viapoint, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        void removeAllViaPoint(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;

        /// serialze
        //        void setWeight(const Ice::DoubleSeqSeq& weights, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        //        Ice::DoubleSeqSeq getWeight(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        std::string serialize(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;
        //        Ice::DoubleSeq deserialize(const std::string&, const Ice::Current& iceCurrent = Ice::emptyCurrent) override;

        ///
        double getCanVal(const Ice::Current& iceCurrent = Ice::emptyCurrent) override;

    private:
        std::string mpName;
        std::map<std::string, MPInterfacePtr> mps;
        std::shared_ptr<mplib::representation::AbstractMovementPrimitive> vmp;

    };
}
