#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueForceTorque.h>

namespace armarx::control::common
{
    class FTSensor
    {
    public:
        struct FTBufferData
        {
            float tcpMass;
            Eigen::Vector3f tcpCoMInFTSensorFrame;
            bool enableTCPGravityCompensation;
            Eigen::Vector3f forceBaseline;
            Eigen::Vector3f torqueBaseline;
        };

        void initialize(
                const std::string& name,
                const std::string& frame,
                const float filter,
                const float forceDeadZone,
                const float torqueDeadZone,
                const float calibrationTimeLimit,
                const VirtualRobot::RobotNodeSetPtr& rns,
                const RobotUnitPtr& robotUnit);
        void reset();
        bool calibrate(double deltaT);
        Eigen::Vector6f& compensateTCPGravity(const FTBufferData &data);
        Eigen::Vector6f& getFilteredForceTorque(const FTBufferData &data);
        //        void enableGravityCompensation(const float tcpMass, const Eigen::Vector3f& tcpCoMInFTFrame);
        //        void disableGravityCompensation();

    private:
        std::string sensorName;
        std::string sensorFrame;

        const SensorValueForceTorque* forceSensor;
        VirtualRobot::RobotNodePtr sensorNode;

        Eigen::Vector6f tcpGravityCompensation;
        Eigen::Vector6f ft;

        /// for filtering and calibration
        float ftFilter;
        float deadZoneForce;
        float deadZoneTorque;
        Eigen::Vector3f forceOffset;
        Eigen::Vector3f torqueOffset;
        Eigen::Vector3f filteredForce;
        Eigen::Vector3f filteredTorque;
        Eigen::Vector3f filteredForceInRoot;
        Eigen::Vector3f filteredTorqueInRoot;
        float timeForCalibration;
        float timeLimit;
        std::atomic_bool calibrated = false;

        /// for gravity compensation
        //        std::atomic_bool enableTCPGravityCompensation = false;
        float tcpMass = 0.0f;
        Eigen::Vector3f tcpCoMInFTSensorFrame = Eigen::Vector3f::Zero();
        const Eigen::Vector3f gravity { 0.0, 0.0, -9.8 };

        Eigen::Matrix4f forceFrameInTCP = Eigen::Matrix4f::Identity();
        Eigen::Vector3f tcpCoMInTCPFrame;

    };
}
