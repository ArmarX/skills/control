/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    TaskSpaceActiveImpedanceControl::ArmarXObjects::NJointTaskSpaceImpedanceController
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <boost/algorithm/clamp.hpp>

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>

#include "ImpedanceController.h"


namespace armarx::control::njoint_controller::task_space
{
NJointControllerRegistration<NJointTaskspaceImpedanceController> registrationControllerNJointTaskspaceImpedanceController("NJointTaskspaceImpedanceController");

NJointTaskspaceImpedanceController::NJointTaskspaceImpedanceController(const RobotUnitPtr& robotUnit, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
{
    NJointTaskspaceImpedanceControllerConfigPtr cfg = NJointTaskspaceImpedanceControllerConfigPtr::dynamicCast(config);

    ARMARX_CHECK_EXPRESSION(cfg);
    ARMARX_CHECK_EXPRESSION(robotUnit);
    ARMARX_CHECK_EXPRESSION(!cfg->nodeSetName.empty());
    useSynchronizedRtRobot();
    kinematicChainName = cfg->nodeSetName;
    VirtualRobot::RobotNodeSetPtr rns = rtGetRobot()->getRobotNodeSet(cfg->nodeSetName);

    jointNames.clear();
    ARMARX_CHECK_EXPRESSION(rns) << cfg->nodeSetName;
    for (size_t i = 0; i < rns->getSize(); ++i)
    {
        std::string jointName = rns->getNode(i)->getName();
        jointNames.push_back(jointName);
        ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
        ARMARX_CHECK_EXPRESSION(ct);
        const SensorValueBase* sv = useSensorValue(jointName);
        ARMARX_CHECK_EXPRESSION(sv);
        auto casted_ct = ct->asA<ControlTarget1DoFActuatorTorque>();
        ARMARX_CHECK_EXPRESSION(casted_ct);
        targets.push_back(casted_ct);

        const SensorValue1DoFActuatorTorque* torqueSensor = sv->asA<SensorValue1DoFActuatorTorque>();
        const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
        const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
        if (!torqueSensor)
        {
            ARMARX_WARNING << "No Torque sensor available for " << jointName;
        }
        if (!velocitySensor)
        {
            ARMARX_WARNING << "No velocity sensor available for " << jointName;
        }
        if (!positionSensor)
        {
            ARMARX_WARNING << "No position sensor available for " << jointName;
        }

        torqueSensors.push_back(torqueSensor);
        velocitySensors.push_back(velocitySensor);
        positionSensors.push_back(positionSensor);
    };

    controller.initialize(rns);

    TaskspaceImpedanceController::Config configData;
    configData.kpImpedance = cfg->kpImpedance;
    configData.kdImpedance = cfg->kdImpedance;
    configData.kpNullspace = cfg->kpNullspace;
    configData.kdNullspace = cfg->kdNullspace;

    configData.desiredTCPPose = Eigen::Matrix4f::Identity();
    configData.desiredNullspaceJointAngles = cfg->desiredNullspaceJointAngles;

    configData.torqueLimit = cfg->torqueLimit;
    configData.qvelFilter  = cfg->qvelFilter;
    reinitTripleBuffer(configData);


    //    int type_left = 2; // 1 = VMP, 2 = VMPv2
    //    int type_right = 1;
    //    // TODO careful about memory leak
    //    switch (type_left)
    //    {
    //        case 1:
    //            mps["LeftArm"] = new VMP();
    //            break;
    //        case 2:
    //            mps["LeftArm"] = new VMP();
    //    }

    //    mps["LeftArm"]->start();

}

std::string NJointTaskspaceImpedanceController::getClassName(const Ice::Current &) const
{
    return "TaskspaceImpedanceController";
}

std::string NJointTaskspaceImpedanceController::getKinematicChainName(const Ice::Current&)
{
    return kinematicChainName;
}

void NJointTaskspaceImpedanceController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& timeSinceLastIteration)
{
    rtUpdateControlStruct();
    controller.updateControlStatus(rtGetControlStruct(), timeSinceLastIteration, torqueSensors, velocitySensors, positionSensors);

    if (rtFirstRun.load())
    {
        rtFirstRun.store(false);
        rtReady.store(true);
        //        controller.firstRun();
    }

    const auto& desiredJointTorques = controller.run(rtReady.load());
    ARMARX_CHECK_EQUAL(targets.size(), desiredJointTorques.size());

    for (size_t i = 0; i < targets.size(); ++i)
    {
        targets.at(i)->torque = desiredJointTorques(i);
        if (!targets.at(i)->isValid())
        {
            targets.at(i)->torque = 0;
        }
    }

    controlStatusBuffer.getWriteBuffer().kpImpedance    = controller.s.kpImpedance;
    controlStatusBuffer.getWriteBuffer().kdImpedance    = controller.s.kdImpedance;
    controlStatusBuffer.getWriteBuffer().forceImpedance = controller.s.forceImpedance;
    controlStatusBuffer.getWriteBuffer().desiredTCPPose = controller.s.desiredTCPPose;
    controlStatusBuffer.commitWrite();
}

void NJointTaskspaceImpedanceController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
{
    StringVariantBaseMap datafields;
    auto values = debugRTBuffer.getUpToDateReadBuffer().desired_torques;
    for (auto& pair : values)
    {
        datafields[pair.first] = new Variant(pair.second);
    }

    datafields["kpImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(0));
    datafields["kpImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(1));
    datafields["kpImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(2));
    datafields["kpImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(3));
    datafields["kpImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(4));
    datafields["kpImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(5));

    datafields["kdImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(0));
    datafields["kdImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(1));
    datafields["kdImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(2));
    datafields["kdImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(3));
    datafields["kdImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(4));
    datafields["kdImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(5));

    datafields["forceImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(0));
    datafields["forceImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(1));
    datafields["forceImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(2));
    datafields["forceImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(3));
    datafields["forceImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(4));
    datafields["forceImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(5));

    Eigen::Matrix4f desiredTCPPose = controlStatusBuffer.getUpToDateReadBuffer().desiredTCPPose;
    datafields["desiredTCPPose_x"] = new Variant(desiredTCPPose(0, 3));
    datafields["desiredTCPPose_y"] = new Variant(desiredTCPPose(1, 3));
    datafields["desiredTCPPose_z"] = new Variant(desiredTCPPose(2, 3));

    debugObs->setDebugChannel("NJointTaskspaceImpedanceController", datafields);
}

void NJointTaskspaceImpedanceController::setTCPPose(const Eigen::Matrix4f& pose, const Ice::Current&)
{
    LockGuardType guard {controlDataMutex};
    getWriterControlStruct().desiredTCPPose = pose;
    writeControlStruct();
}

void NJointTaskspaceImpedanceController::setNullspaceJointAngles(const Eigen::VectorXf& jointAngles, const Ice::Current &)
{
    LockGuardType guard {controlDataMutex};
    getWriterControlStruct().desiredNullspaceJointAngles = jointAngles;
    writeControlStruct();
}

void NJointTaskspaceImpedanceController::setControlParameters(const std::string& name, const Eigen::VectorXf& value, const Ice::Current &)
{
    LockGuardType guard {controlDataMutex};
    if (name == "kpImpedance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kpImpedance = value;
    }
    else if (name == "kdImpedance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kdImpedance = value;
    }
    else if (name == "kpNullspace")
    {
        ARMARX_CHECK_EQUAL(value.size(), targets.size());
        getWriterControlStruct().kpNullspace = value;
    }
    else if (name == "kdNullspace")
    {
        ARMARX_CHECK_EQUAL(value.size(), targets.size());
        getWriterControlStruct().kdNullspace = value;
    }
    else
    {
        ARMARX_ERROR << name << " is not supported by TaskSpaceImpedanceController";
    }
    writeControlStruct();
}

void NJointTaskspaceImpedanceController::rtPreActivateController()
{
    VirtualRobot::RobotNodeSetPtr rns = rtGetRobot()->getRobotNodeSet(kinematicChainName);
    Eigen::Matrix4f currentPose = rns->getTCP()->getPoseInRootFrame();
    ARMARX_IMPORTANT << "rt preactivate controller with target pose\n\n" << currentPose;
    controller.s.desiredTCPPose = currentPose;
    controller.s.previousTargetPose = currentPose;
    getWriterControlStruct().desiredTCPPose = currentPose;
    writeControlStruct();
}
}
