/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ...
 * @author     Jianfeng Gao ( jianfeng dot gao at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

#include <armarx/control/common/FTSensor.h>
#include <armarx/control/common/TaskspaceAdmittanceController.h>
#include <armarx/control/njoint_controller/ControllerInterface.h>


namespace armarx::control::njoint_controller::task_space
{
    namespace common = armarx::control::common;
    /**
    * @defgroup Library-NJointTaskspaceAdmittanceController NJointTaskspaceAdmittanceController
    * @ingroup Library-RobotUnit-NJointControllers
    * A description of the library NJointTaskspaceAdmittanceController.
    *
    * @class NJointTaskspaceAdmittanceController
    * @ingroup Library-NJointTaskspaceAdmittanceController
    * @brief Brief description of class NJointTaskspaceAdmittanceController.
    *
    * Detailed description of class NJointTaskspaceAdmittanceController.
    */
    class NJointTaskspaceAdmittanceController :
        public NJointControllerWithTripleBuffer<TaskspaceAdmittanceController::Config>,
        public NJointTaskspaceAdmittanceControllerInterface
    {
    public:
        using ConfigPtrT = NJointTaskspaceAdmittanceControllerConfigPtr;

        NJointTaskspaceAdmittanceController(const RobotUnitPtr& robotUnit, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);
        std::string getClassName(const Ice::Current&) const override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        // NJointController interface
        /// set control target
        void setTCPPose(const Eigen::Matrix4f&, const Ice::Current&) override;
        void setNullspaceJointAngles(const Eigen::VectorXf&, const Ice::Current&) override;

        /// set control parameter
        std::string getKinematicChainName(const Ice::Current&) override;
        void setControlParameters(const std::string&, const Eigen::VectorXf&, const Ice::Current&) override;
        void setForceTorqueBaseline(const Eigen::Vector3f&, const Eigen::Vector3f&, const Ice::Current&) override;

        /// ft sensor
        void toggleGravityCompensation(const bool toggle, const Ice::Current &);
        void setTCPMass(Ice::Float, const Ice::Current&) override;
        void setTCPCoMInFTFrame(const Eigen::Vector3f&, const Ice::Current&) override;
        void calibrateFTSensor(const Ice::Current&) override;


    protected:
        void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&) override;

    private:
        /// devices
        std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors;
        std::vector<const SensorValue1DoFActuatorVelocity*> velocitySensors;
        std::vector<const SensorValue1DoFActuatorPosition*> positionSensors;
        std::vector<ControlTarget1DoFActuatorTorque*> targets;

        /// set buffers
        struct debugRTInfo
        {
            StringFloatDictionary desired_torques;
        };
        TripleBuffer<debugRTInfo> debugRTBuffer;
        TripleBuffer<TaskspaceAdmittanceController::Status> controlStatusBuffer;
        TripleBuffer<common::FTSensor::FTBufferData> ftSensorBuffer;

        /// variables
        std::string kinematicChainName;
        std::vector<std::string> jointNames;
        TaskspaceAdmittanceController controller;
        common::FTSensor ftsensor;

        std::atomic_bool rtFirstRun = true;
        std::atomic_bool rtReady = false;

        //        Eigen::Matrix4f previousTargetPose;

        // NJointController interface
    protected:
        void rtPreActivateController() override;
    };
}
