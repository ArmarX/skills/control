/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ...
 * @author     Jianfeng Gao ( jianfeng dot gao at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

#include <armarx/control/common/TaskspaceImpedanceController.h>
#include <armarx/control/njoint_controller/ControllerInterface.h>


namespace armarx::control::njoint_controller::task_space
{
    //    class MPInterface
    //    {
    //    public:
    //        void start() {}
    //    };

    //    class VMP: public MPInterface
    //    {

    //    };

    /**
    * @defgroup Library-NJointTaskspaceImpedanceController NJointTaskspaceImpedanceController
    * @ingroup Library-RobotUnit-NJointControllers
    * A description of the library NJointTaskspaceImpedanceController.
    *
    * @class NJointTaskspaceImpedanceController
    * @ingroup Library-NJointTaskspaceImpedanceController
    * @brief Brief description of class NJointTaskspaceImpedanceController.
    *
    * Detailed description of class NJointTaskspaceImpedanceController.
    */
    class NJointTaskspaceImpedanceController :
        public NJointControllerWithTripleBuffer<TaskspaceImpedanceController::Config>,
        public NJointTaskspaceImpedanceControllerInterface
    {
    public:
        using ConfigPtrT = NJointTaskspaceImpedanceControllerConfigPtr;

        NJointTaskspaceImpedanceController(const RobotUnitPtr& robotUnit, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);
        std::string getClassName(const Ice::Current&) const override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        // NJointController interface
        std::string getKinematicChainName(const Ice::Current&) override;
        void setTCPPose(const Eigen::Matrix4f&, const Ice::Current&) override;
        void setNullspaceJointAngles(const Eigen::VectorXf&, const Ice::Current&) override;

        void setControlParameters(const std::string&, const Eigen::VectorXf&, const Ice::Current&);

    protected:
        void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&) override;

    private:
        std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors;
        std::vector<const SensorValue1DoFActuatorVelocity*> velocitySensors;
        std::vector<const SensorValue1DoFActuatorPosition*> positionSensors;
        std::vector<ControlTarget1DoFActuatorTorque*> targets;

        struct debugRTInfo
        {
            StringFloatDictionary desired_torques;
        };
        TripleBuffer<debugRTInfo> debugRTBuffer;
        TripleBuffer<TaskspaceImpedanceController::Status> controlStatusBuffer;

        std::string kinematicChainName;
        std::vector<std::string> jointNames;
        TaskspaceImpedanceController controller;

        std::atomic_bool rtFirstRun = true;
        std::atomic_bool rtReady = false;

        Eigen::Matrix4f previousTargetPose;

        //        std::map<std::string, MPInterface*> mps; // use shared_ptr?

        // NJointController interface
    protected:
        void rtPreActivateController() override;
    };
}
