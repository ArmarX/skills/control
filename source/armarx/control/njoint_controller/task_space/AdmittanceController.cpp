/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    TaskSpaceActiveImpedanceControl::ArmarXObjects::NJointTaskSpaceImpedanceController
 * @author     zhou ( you dot zhou at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <boost/algorithm/clamp.hpp>

#include <SimoxUtility/math/convert/mat4f_to_pos.h>
#include <SimoxUtility/math/convert/mat4f_to_quat.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <VirtualRobot/MathTools.h>

#include "AdmittanceController.h"

namespace armarx::control::njoint_controller::task_space
{
NJointControllerRegistration<NJointTaskspaceAdmittanceController> registrationControllerNJointTaskspaceAdmittanceController("NJointTaskspaceAdmittanceController");

NJointTaskspaceAdmittanceController::NJointTaskspaceAdmittanceController(
        const RobotUnitPtr& robotUnit,
        const NJointControllerConfigPtr& config,
        const VirtualRobot::RobotPtr&)
{
    ARMARX_INFO << "creating task-space admittance controller";
    NJointTaskspaceAdmittanceControllerConfigPtr cfg = NJointTaskspaceAdmittanceControllerConfigPtr::dynamicCast(config);

    ARMARX_CHECK_EXPRESSION(cfg);
    ARMARX_CHECK_EXPRESSION(robotUnit);
    ARMARX_CHECK_EXPRESSION(!cfg->nodeSetName.empty());
    useSynchronizedRtRobot();
    kinematicChainName = cfg->nodeSetName;

    VirtualRobot::RobotNodeSetPtr rns = rtGetRobot()->getRobotNodeSet(cfg->nodeSetName);
    ARMARX_CHECK_EXPRESSION(rns) << cfg->nodeSetName;

    jointNames.clear();
    for (size_t i = 0; i < rns->getSize(); ++i)
    {
        std::string jointName = rns->getNode(i)->getName();
        jointNames.push_back(jointName);
        ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
        ARMARX_CHECK_EXPRESSION(ct);
        const SensorValueBase* sv = useSensorValue(jointName);
        ARMARX_CHECK_EXPRESSION(sv);
        auto casted_ct = ct->asA<ControlTarget1DoFActuatorTorque>();
        ARMARX_CHECK_EXPRESSION(casted_ct);
        targets.push_back(casted_ct);

        const SensorValue1DoFActuatorTorque* torqueSensor = sv->asA<SensorValue1DoFActuatorTorque>();
        const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
        const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
        if (!torqueSensor)
        {
            ARMARX_WARNING << "No Torque sensor available for " << jointName;
        }
        if (!velocitySensor)
        {
            ARMARX_WARNING << "No velocity sensor available for " << jointName;
        }
        if (!positionSensor)
        {
            ARMARX_WARNING << "No position sensor available for " << jointName;
        }

        torqueSensors.push_back(torqueSensor);
        velocitySensors.push_back(velocitySensor);
        positionSensors.push_back(positionSensor);
    };
    controller.initialize(rns);
    ftsensor.initialize(
                cfg->ftSensorName,
                cfg->ftFrameName,
                cfg->ftFilter,
                cfg->forceDeadZone,
                cfg->torqueDeadZone,
                cfg->waitTimeForCalibration,
                rns, robotUnit
                );

    common::FTSensor::FTBufferData ftData;
    ftData.forceBaseline.setZero();
    ftData.torqueBaseline.setZero();
    ftData.enableTCPGravityCompensation = cfg->enableTCPGravityCompensation;
    ftData.tcpMass = cfg->tcpMass;
    ftData.tcpCoMInFTSensorFrame = cfg->tcpCoMInForceSensorFrame;
    ftSensorBuffer.reinitAllBuffers(ftData);

    TaskspaceAdmittanceController::Config configData;
    configData.kpImpedance  = cfg->kpImpedance;
    configData.kdImpedance  = cfg->kdImpedance;
    configData.kpAdmittance = cfg->kpAdmittance;
    configData.kdAdmittance = cfg->kdAdmittance;
    configData.kmAdmittance = cfg->kmAdmittance;

    configData.kpNullspace  = cfg->kpNullspace;
    configData.kdNullspace  = cfg->kdNullspace;

    configData.currentForceTorque.setZero();
    configData.desiredTCPPose = Eigen::Matrix4f::Identity();
    configData.desiredTCPTwist.setZero();
    configData.desiredNullspaceJointAngles = cfg->desiredNullspaceJointAngles;

    configData.torqueLimit  = cfg->torqueLimit;
    configData.qvelFilter   = cfg->qvelFilter;
    reinitTripleBuffer(configData);
}

std::string NJointTaskspaceAdmittanceController::getClassName(const Ice::Current &) const
{
    return "TaskspaceAdmittanceController";
}

std::string NJointTaskspaceAdmittanceController::getKinematicChainName(const Ice::Current &)
{
    return kinematicChainName;
}

void NJointTaskspaceAdmittanceController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& timeSinceLastIteration)
{
    rtUpdateControlStruct();
    bool valid = controller.updateControlStatus(
                rtGetControlStruct(),
                timeSinceLastIteration,
                torqueSensors,
                velocitySensors,
                positionSensors);

    /// for mp controllers you can commit the status buffer now
    {
        controlStatusBuffer.getWriteBuffer().currentPose = controller.s.currentPose;
        controlStatusBuffer.getWriteBuffer().currentTwist = controller.s.currentTwist * 1000.0f;
        controlStatusBuffer.getWriteBuffer().deltaT = controller.s.deltaT;
        controlStatusBuffer.commitWrite();
    }

    if (rtFirstRun.load())
    {
        rtFirstRun.store(false);
        rtReady.store(false);

        ftsensor.reset();
        controller.firstRun();
        // ARMARX_IMPORTANT << "admittance control first run with\n" << VAROUT(previousTargetPose);
    }
    else
    {
        ftsensor.compensateTCPGravity(ftSensorBuffer.getReadBuffer());
        if (!rtReady.load())
        {
            if(ftsensor.calibrate(controller.s.deltaT))
            {
                rtReady.store(true);
            }
        }
        else
        {
            if (valid)
            {
                controller.s.currentForceTorque = ftsensor.getFilteredForceTorque(ftSensorBuffer.getReadBuffer());
            }
        }
    }

    const auto& desiredJointTorques = controller.run(rtReady.load());
    ARMARX_CHECK_EQUAL(targets.size(), desiredJointTorques.size());

    /// write torque target to joint device
    for (size_t i = 0; i < targets.size(); ++i)
    {
        targets.at(i)->torque = desiredJointTorques(i);
        if (!targets.at(i)->isValid())
        {
            targets.at(i)->torque = 0;
        }
    }

    /// for debug output
    {
        controlStatusBuffer.getWriteBuffer().kpImpedance        = controller.s.kpImpedance;
        controlStatusBuffer.getWriteBuffer().kdImpedance        = controller.s.kdImpedance;
        controlStatusBuffer.getWriteBuffer().forceImpedance     = controller.s.forceImpedance;
        controlStatusBuffer.getWriteBuffer().currentForceTorque = controller.s.currentForceTorque;

        controlStatusBuffer.getWriteBuffer().virtualPose        = controller.s.virtualPose;
        controlStatusBuffer.getWriteBuffer().desiredTCPPose     = controller.s.desiredTCPPose;
        controlStatusBuffer.getWriteBuffer().virtualVel         = controller.s.virtualVel;
        controlStatusBuffer.getWriteBuffer().virtualAcc         = controller.s.virtualAcc;
        controlStatusBuffer.commitWrite();
    }
}

void NJointTaskspaceAdmittanceController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
{
    StringVariantBaseMap datafields;
    auto values = debugRTBuffer.getUpToDateReadBuffer().desired_torques;
    for (auto& pair : values)
    {
        datafields[pair.first] = new Variant(pair.second);
    }

    Eigen::Matrix4f virtualPose = controlStatusBuffer.getUpToDateReadBuffer().virtualPose;
    Eigen::Vector3f rpy = VirtualRobot::MathTools::eigen4f2rpy(virtualPose);
    datafields["virtualPose_x"]  = new Variant(virtualPose(0, 3));
    datafields["virtualPose_y"]  = new Variant(virtualPose(1, 3));
    datafields["virtualPose_z"]  = new Variant(virtualPose(2, 3));
    datafields["virtualPose_rx"] = new Variant(rpy(0));
    datafields["virtualPose_ry"] = new Variant(rpy(1));
    datafields["virtualPose_rz"] = new Variant(rpy(2));

    Eigen::Matrix4f desiredPose = controlStatusBuffer.getUpToDateReadBuffer().desiredTCPPose;
    Eigen::Vector3f rpyDesired = VirtualRobot::MathTools::eigen4f2rpy(desiredPose);
    datafields["desiredPose_x"]  = new Variant(desiredPose(0, 3));
    datafields["desiredPose_y"]  = new Variant(desiredPose(1, 3));
    datafields["desiredPose_z"]  = new Variant(desiredPose(2, 3));
    datafields["desiredPose_rx"] = new Variant(rpyDesired(0));
    datafields["desiredPose_ry"] = new Variant(rpyDesired(1));
    datafields["desiredPose_rz"] = new Variant(rpyDesired(2));

    datafields["virtualVel_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(0));
    datafields["virtualVel_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(1));
    datafields["virtualVel_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(2));
    datafields["virtualVel_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(3));
    datafields["virtualVel_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(4));
    datafields["virtualVel_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualVel(5));

    datafields["virtualAcc_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(0));
    datafields["virtualAcc_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(1));
    datafields["virtualAcc_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(2));
    datafields["virtualAcc_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(3));
    datafields["virtualAcc_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(4));
    datafields["virtualAcc_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().virtualAcc(5));

    datafields["kpImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(0));
    datafields["kpImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(1));
    datafields["kpImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(2));
    datafields["kpImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(3));
    datafields["kpImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(4));
    datafields["kpImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kpImpedance(5));

    datafields["kdImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(0));
    datafields["kdImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(1));
    datafields["kdImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(2));
    datafields["kdImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(3));
    datafields["kdImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(4));
    datafields["kdImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().kdImpedance(5));

    datafields["forceImpedance_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(0));
    datafields["forceImpedance_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(1));
    datafields["forceImpedance_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(2));
    datafields["forceImpedance_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(3));
    datafields["forceImpedance_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(4));
    datafields["forceImpedance_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().forceImpedance(5));

    datafields["currentForceTorque_x"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(0));
    datafields["currentForceTorque_y"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(1));
    datafields["currentForceTorque_z"]  = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(2));
    datafields["currentForceTorque_rx"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(3));
    datafields["currentForceTorque_ry"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(4));
    datafields["currentForceTorque_rz"] = new Variant(controlStatusBuffer.getUpToDateReadBuffer().currentForceTorque(5));

    debugObs->setDebugChannel("NJointTaskspaceAdmittanceController", datafields);
}

void NJointTaskspaceAdmittanceController::setTCPPose(const Eigen::Matrix4f& pose, const Ice::Current&)
{
    LockGuardType guard {controlDataMutex};
    getWriterControlStruct().desiredTCPPose = pose;
    writeControlStruct();
}

void NJointTaskspaceAdmittanceController::setNullspaceJointAngles(const Eigen::VectorXf& jointAngles, const Ice::Current &)
{
    LockGuardType guard {controlDataMutex};
    getWriterControlStruct().desiredNullspaceJointAngles = jointAngles;
    writeControlStruct();
}

void NJointTaskspaceAdmittanceController::setControlParameters(const std::string& name, const Eigen::VectorXf& value, const Ice::Current &)
{
    LockGuardType guard {controlDataMutex};
    if (name == "kpImpedance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kpImpedance = value;
    }
    else if (name == "kdImpedance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kdImpedance = value;
    }
    else if (name == "kpAdmittance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kpAdmittance = value;
    }
    else if (name == "kdAdmittance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kdAdmittance = value;
    }
    else if (name == "kmAdmittance")
    {
        ARMARX_CHECK_EQUAL(value.size(), 6);
        getWriterControlStruct().kmAdmittance = value;
    }
    else if (name == "kpNullspace")
    {
        ARMARX_CHECK_EQUAL(value.size(), targets.size());
        getWriterControlStruct().kpNullspace = value;
    }
    else if (name == "kdNullspace")
    {
        ARMARX_CHECK_EQUAL(value.size(), targets.size());
        getWriterControlStruct().kdNullspace = value;
    }
    else
    {
        ARMARX_ERROR << name << " is not supported by TaskSpaceAdmittanceController";
    }
    writeControlStruct();
}

void NJointTaskspaceAdmittanceController::setForceTorqueBaseline(const Eigen::Vector3f& forceBaseline, const Eigen::Vector3f& torqueBaseline, const Ice::Current &)
{
    ftSensorBuffer.getWriteBuffer().forceBaseline = forceBaseline;
    ftSensorBuffer.getWriteBuffer().torqueBaseline = torqueBaseline;
    ftSensorBuffer.commitWrite();
}

void NJointTaskspaceAdmittanceController::setTCPMass(Ice::Float mass, const Ice::Current &)
{
    ftSensorBuffer.getWriteBuffer().enableTCPGravityCompensation = true;
    ftSensorBuffer.getWriteBuffer().tcpMass = mass;
    ftSensorBuffer.commitWrite();
}

void NJointTaskspaceAdmittanceController::setTCPCoMInFTFrame(const Eigen::Vector3f& tcpCoMInFTSensorFrame, const Ice::Current &)
{
    ftSensorBuffer.getWriteBuffer().enableTCPGravityCompensation = true;
    ftSensorBuffer.getWriteBuffer().tcpCoMInFTSensorFrame = tcpCoMInFTSensorFrame;
    ftSensorBuffer.commitWrite();
}

void NJointTaskspaceAdmittanceController::calibrateFTSensor(const Ice::Current &)
{
    rtReady.store(false);
}

void NJointTaskspaceAdmittanceController::toggleGravityCompensation(const bool toggle, const Ice::Current &)
{
    ftSensorBuffer.getWriteBuffer().enableTCPGravityCompensation = toggle;
    ftSensorBuffer.commitWrite();
    rtReady.store(false);  /// you also need to re-calibrate ft sensor
}

void NJointTaskspaceAdmittanceController::rtPreActivateController()
{
    VirtualRobot::RobotNodeSetPtr rns = rtGetRobot()->getRobotNodeSet(kinematicChainName);
    Eigen::Matrix4f currentPose = rns->getTCP()->getPoseInRootFrame();
    ARMARX_IMPORTANT << "rt preactivate controller with target pose\n\n" << currentPose;
    controller.s.desiredTCPPose = currentPose;
    controller.s.previousTargetPose = currentPose;
    controller.s.virtualPose = currentPose;
    controller.s.virtualVel.setZero();
    controller.s.virtualAcc.setZero();
    getWriterControlStruct().desiredTCPPose = currentPose;
    writeControlStruct();
}
}
