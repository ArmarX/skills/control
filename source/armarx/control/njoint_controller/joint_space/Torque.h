#pragma once


#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <armarx/control/njoint_controller/ControllerInterface.h>


namespace armarx::control::njoint_controller::joint_space
{
    struct NJointTorqueControllerTarget
    {
        Ice::FloatSeq targetTorques;
    };

    TYPEDEF_PTRS_HANDLE(NJointTorqueController);

    class NJointTorqueController :
        public NJointControllerWithTripleBuffer<NJointTorqueControllerTarget>,
        public NJointTorqueControllerInterface
    {
    public:
        using ConfigPtrT = NJointTorqueControllerConfigPtr;
        NJointTorqueController(RobotUnitPtr prov, const NJointTorqueControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        //        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        //        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        std::vector<ControlTarget1DoFActuatorTorque*> targets;
        float maxTorque = std::numeric_limits<float>::max();

        // NJointTorqueControllerInterface interface
    public:
        void setControllerTarget(const Ice::FloatSeq&, const Ice::Current&) override;
    };

} // namespace armarx
