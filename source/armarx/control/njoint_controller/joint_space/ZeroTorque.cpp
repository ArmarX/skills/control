#include "ZeroTorque.h"

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Robot.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>


namespace armarx::control::njoint_controller::joint_space
{

    NJointControllerRegistration<NJointZeroTorqueController> registrationControllerNJointZeroTorqueController("NJointZeroTorqueController");

    std::string NJointZeroTorqueController::getClassName(const Ice::Current&) const
    {
        return "NJointZeroTorqueController";
    }

    NJointZeroTorqueController::NJointZeroTorqueController(RobotUnitPtr prov, const NJointZeroTorqueControllerConfigPtr& config, const VirtualRobot::RobotPtr& r)
    {
        ARMARX_CHECK_EXPRESSION(prov);
        RobotUnitPtr robotUnit = RobotUnitPtr::dynamicCast(prov);
        ARMARX_CHECK_NOT_NULL(robotUnit);
        ARMARX_CHECK_NOT_NULL(config);
        ARMARX_CHECK_EXPRESSION(!config->jointNames.empty());
        for (auto& jointName : config->jointNames)
        {
            auto node = r->getRobotNode(jointName);
            ARMARX_CHECK_EXPRESSION(node) << jointName;

            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::ZeroTorque1DoF);
            ARMARX_CHECK_NOT_NULL(ct);
            targets.push_back(ct->asA<ControlTarget1DoFActuatorZeroTorque>());
        }
    }

    void NJointZeroTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        for (size_t i = 0; i < targets.size(); ++i)
        {
            targets.at(i)->torque = 0;
        }
    }

    WidgetDescription::WidgetPtr NJointZeroTorqueController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        ::armarx::WidgetDescription::WidgetSeq widgets;

        LabelPtr label = new Label;
        label->text = "joint names";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;
        box->multiSelect = true;

        for (auto& c : controlDevices)
        {
            if (c.second->hasJointController(ControlModes::ZeroTorque1DoF))
            {
                box->options.push_back(c.first);
            }
        }

        box->name = "jointNames";
        layout->children.emplace_back(box);

        layout->children.insert(layout->children.end(),
                                widgets.begin(),
                                widgets.end());

        return layout;
    }

    NJointZeroTorqueControllerConfigPtr NJointZeroTorqueController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        auto var = VariantPtr::dynamicCast(values.at("jointNames"));
        ARMARX_CHECK_EXPRESSION(var) << "jointNames";
        return new NJointZeroTorqueControllerConfig {var->get<SingleTypeVariantList>()->toStdVector<std::string>()};
    }

} // namespace armarx
