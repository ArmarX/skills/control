#pragma once


#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <armarx/control/njoint_controller/ControllerInterface.h>


namespace armarx::control::njoint_controller::joint_space
{

    TYPEDEF_PTRS_HANDLE(NJointZeroTorqueController);

    class NJointZeroTorqueController :
        public NJointController,
        public NJointZeroTorqueControllerInterface
    {
    public:
        using ConfigPtrT = NJointZeroTorqueControllerConfigPtr;
        NJointZeroTorqueController(RobotUnitPtr prov, const NJointZeroTorqueControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        //        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        //        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointZeroTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);

    protected:
        std::vector<ControlTarget1DoFActuatorZeroTorque*> targets;

    };

} // namespace armarx
