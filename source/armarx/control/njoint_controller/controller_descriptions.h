/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/control/client/ControllerDescription.h>
#include <armarx/control/common/type.h>

#include <armarx/control/common/control_law/aron/TaskspaceImpedanceControllerConfig.aron.generated.h>


namespace armarx::control::client
{
    // template <>
    // struct ControllerDescription<armarx::control::common::ControllerType::TSAdmittance>
    // {
    //     using AronDTO = armarx::control::arondto::TaskSpaceAdmittanceControllerConfig;

    //     constexpr static const char* name = "NJointTSAdmittance";
    // };

     template <>
    struct ControllerDescription<armarx::control::common::ControllerType::TSImpedance>
    {
        using AronDTO = armarx::control::common::control_law::arondto::TaskSpaceImpedanceControllerConfig;

        constexpr static const char* name = "NJointTSImpedance";
    };


} // namespace armarx::control::client
