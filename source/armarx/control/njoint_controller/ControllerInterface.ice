/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

#include <ArmarXCore/interface/serialization/Eigen.ice>

module armarx
{
    //    module RTController
    //    {
    //        class ControllerInfo
    //        {
    //            string kinematic_chain;
    //        };
    //    }


    /// ------------------------------ impedance controller --------------------------------------
    class NJointTaskspaceImpedanceControllerConfig extends NJointControllerConfig
    {
        string nodeSetName;

        /// control parameter ts
        Eigen::Vector6f kpImpedance;
        Eigen::Vector6f kdImpedance;

        /// nullspace config
        Eigen::VectorXf kpNullspace;
        Eigen::VectorXf kdNullspace;
        Eigen::VectorXf desiredNullspaceJointAngles;

        float torqueLimit;
        float qvelFilter = 0.9f;
    };

    interface NJointTaskspaceImpedanceControllerInterface extends NJointControllerInterface
    {
        /// set control target
        void setTCPPose(Eigen::Matrix4f pose);
        void setNullspaceJointAngles(Eigen::VectorXf jointAngles);

        /// set control parameter
        string getKinematicChainName();
        void setControlParameters(string name, Eigen::VectorXf value);
    };




    /// ------------------------------ admittance controller --------------------------------------
    class NJointTaskspaceAdmittanceControllerConfig extends NJointControllerConfig
    {
        string nodeSetName;

        /// control parameter ts
        Eigen::Vector6f kpImpedance;
        Eigen::Vector6f kdImpedance;
        Eigen::Vector6f kpAdmittance;
        Eigen::Vector6f kdAdmittance;
        Eigen::Vector6f kmAdmittance;

        /// nullspace config
        Eigen::VectorXf kpNullspace;
        Eigen::VectorXf kdNullspace;
        Eigen::VectorXf desiredNullspaceJointAngles;

        float torqueLimit;
        float qvelFilter = 0.9f;

        /// force torque sensor
        string ftSensorName;
        string ftFrameName = "ArmR8_Wri2";
        float ftFilter;
        float forceDeadZone;
        float torqueDeadZone;
        float waitTimeForCalibration;

        bool enableTCPGravityCompensation;
        float tcpMass = 0.0f;
        Eigen::Vector3f tcpCoMInForceSensorFrame;
    };

    interface NJointTaskspaceAdmittanceControllerInterface extends NJointControllerInterface
    {
        /// set control target
        void setTCPPose(Eigen::Matrix4f pose);
        void setNullspaceJointAngles(Eigen::VectorXf jointAngles);

        /// set control parameter
        string getKinematicChainName();
        void setControlParameters(string name, Eigen::VectorXf value);
        void setForceTorqueBaseline(Eigen::Vector3f force, Eigen::Vector3f torque);

        /// ft sensor
        void toggleGravityCompensation(bool toggle);
        void setTCPMass(float mass);
        void setTCPCoMInFTFrame(Eigen::Vector3f com);
        void calibrateFTSensor();
    };


    /// ------------------------------ torque controller --------------------------------------
    class NJointTorqueControllerConfig extends NJointControllerConfig
    {
        Ice::StringSeq jointNames;
        float maxTorqueNm = 10.0f;
    };


    interface NJointTorqueControllerInterface extends NJointControllerInterface
    {
        void setControllerTarget(Ice::FloatSeq targetTorques);
    };


    /// ------------------------------ zero torque controller --------------------------------------
    class NJointZeroTorqueControllerConfig extends NJointControllerConfig
    {
        Ice::StringSeq jointNames;
    };


    interface NJointZeroTorqueControllerInterface extends NJointControllerInterface
    {

    };

};

