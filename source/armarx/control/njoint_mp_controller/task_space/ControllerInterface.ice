/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotControllers::RobotNJointControllersInterface
 * @author     Jianfeng Gao ( jianfeng dot gao at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>
// #include <RobotControllers/interface/RobotNJointControllers/NJointTaskspaceControllerInterface.ice>
#include <armarx/control/common/MPPoolInterface.ice>
#include <armarx/control/njoint_controller/ControllerInterface.ice>


module armarx
{
    module NJointMultiMPControllerMode
    {
        enum CartesianSelection
        {
            ePosition = 7,
            eOrientation = 8,
            eAll = 15
        };
    };


    interface NJointTaskspaceImpedanceMPControllerInterface extends NJointTaskspaceImpedanceControllerInterface, MPPoolInterface
    {

    }

    class NJointMultiMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        string dmpMode = "MinimumJerk";
        string dmpStyle = "Discrete";
        double dmpAmplitude = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double timeDuration = 10;
        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        NJointMultiMPControllerMode::CartesianSelection mode = NJointMultiMPControllerMode::eAll;

        double maxLinearVel;
        double maxAngularVel;
        double maxJointVelocity;
        string debugName;

        float Kp_LinearVel;
        float Kd_LinearVel;
        float Kp_AngularVel;
        float Kd_AngularVel;

        float vel_filter;

        int jsDMPKernelSize = 100;
        int jsDMPBaseMode = 1;
        double jsDMPPhaseL = 10;
        double jsDMPPhaseK = 10;
        double jsDMPPhaseDist0 = 0.3;
        double jsDMPPhaseDist1 = 0.1;
        double jsDMPPhaseKp = 1;
        bool isPhaseStop = false;
        Ice::StringSeq jointNamesJSDMP;
    };


    interface NJointMultiMPControllerInterface extends NJointControllerInterface
    {
        void learnTSDMPFromFiles(Ice::StringSeq trajfiles);
        void learnJSDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goalsTS, Ice::DoubleSeq goalsJS, double times);
        void runDMPWithTime(Ice::DoubleSeq goals, double time);

        void setSpeed(double times);
        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setViaPointsJS(double canVal, double point);
        void removeAllViaPoints();
        void setGoalsTS(Ice::DoubleSeq goals);
        void setGoalsJS(Ice::DoubleSeq goals);

        double getCanVal();
        void resetDMP();
        void stopDMP();
        void pauseDMP();
        void resumeDMP();

        void setControllerTarget(float avoidJointLimitsKp, NJointMultiMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
        string getTSDMPAsString();
        Ice::DoubleSeq createTSDMPFromString(string dmpString);
        string getJSDMPAsString();
        Ice::DoubleSeq createJSDMPFromString(string dmpString);

    };
};
