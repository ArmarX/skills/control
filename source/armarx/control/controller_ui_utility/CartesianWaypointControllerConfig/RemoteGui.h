/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ControllerUIUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/RemoteGui/RemoteGui.h>

#include <RobotAPI/interface/core/CartesianWaypointControllerConfig.h>

//make
namespace armarx::RemoteGui
{
    detail::GroupBoxBuilder makeConfigGui(const std::string& name,
                                          const CartesianWaypointControllerConfig& val);

    void getValueFromMap(CartesianWaypointControllerConfig& cfg,
                         RemoteGui::ValueMap const& values,
                         std::string const& name);
} // namespace armarx::RemoteGui
