/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ControllerUIUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RemoteGui.h"

namespace armarx::RemoteGui
{
    detail::GroupBoxBuilder
    makeConfigGui(const std::string& name, const CartesianWaypointControllerConfig& val)
    {
        return RemoteGui::makeGroupBox(name).addChild(
            RemoteGui::makeSimpleGridLayout()
                .cols(10)

                .addChild(RemoteGui::makeTextLabel("Max accelerations:"))
                .addChild(RemoteGui::makeTextLabel("Pos:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_maxAcc_Pos")
                              .min(0)
                              .max(10000)
                              .value(val.maxPositionAcceleration)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Ori:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_maxAcc_Ori")
                              .min(0)
                              .max(10)
                              .value(val.maxOrientationAcceleration)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Nullspace:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_maxAcc_Null")
                              .min(0)
                              .max(10)
                              .value(val.maxNullspaceAcceleration)
                              .decimals(3))
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::HSpacer)

                .addChild(RemoteGui::makeTextLabel("JointLimitAvoidance:"))
                .addChild(RemoteGui::makeTextLabel("KP:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_JointLimitAvoidance_KP")
                              .min(0)
                              .max(10)
                              .value(val.kpJointLimitAvoidance)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Scale:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_JointLimitAvoidance_Scale")
                              .min(0)
                              .max(10)
                              .value(val.jointLimitAvoidanceScale)
                              .decimals(3))
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::Widget())
                .addChild(new RemoteGui::HSpacer)

                .addChild(RemoteGui::makeTextLabel("Position:"))
                .addChild(RemoteGui::makeTextLabel("Near:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Thresholds_Pos_Near")
                              .min(0)
                              .max(1000)
                              .value(val.thresholdPositionNear)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Reached:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Thresholds_Pos_Reached")
                              .min(0)
                              .max(1000)
                              .value(val.thresholdPositionReached)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Max vel:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Max_vel_pos")
                              .min(0)
                              .max(1000)
                              .value(val.maxPosVel)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("KP:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_KP_pos")
                              .min(0)
                              .max(10)
                              .value(val.kpPos)
                              .decimals(3))
                .addChild(new RemoteGui::HSpacer)

                .addChild(RemoteGui::makeTextLabel("Orientation:"))
                .addChild(RemoteGui::makeTextLabel("Near:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Thresholds_Ori_Near")
                              .min(0)
                              .max(3.14f)
                              .value(val.thresholdOrientationNear)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Reached:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Thresholds_Ori_Reached")
                              .min(0)
                              .max(3.14f)
                              .value(val.thresholdOrientationReached)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("Max vel:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_Max_vel_ori")
                              .min(0)
                              .max(31.4f)
                              .value(val.maxOriVel)
                              .decimals(3))
                .addChild(RemoteGui::makeTextLabel("KP:"))
                .addChild(RemoteGui::makeFloatSpinBox(name + "_KP_ori")
                              .min(0)
                              .max(10)
                              .value(val.kpOri)
                              .decimals(3))
                .addChild(new RemoteGui::HSpacer));
    }

    void
    getValueFromMap(CartesianWaypointControllerConfig& cfg,
                    RemoteGui::ValueMap const& values,
                    std::string const& name)
    {
        getValueFromMap(cfg.maxPositionAcceleration, values, name + "_maxAcc_Pos");
        getValueFromMap(cfg.maxOrientationAcceleration, values, name + "_maxAcc_Ori");
        getValueFromMap(cfg.maxNullspaceAcceleration, values, name + "_maxAcc_Null");

        getValueFromMap(cfg.kpJointLimitAvoidance, values, name + "_JointLimitAvoidance_KP");
        getValueFromMap(cfg.jointLimitAvoidanceScale, values, name + "_JointLimitAvoidance_Scale");

        getValueFromMap(cfg.thresholdOrientationNear, values, name + "_Thresholds_Ori_Near");
        getValueFromMap(cfg.thresholdOrientationReached, values, name + "_Thresholds_Ori_Reached");
        getValueFromMap(cfg.thresholdPositionNear, values, name + "_Thresholds_Pos_Near");
        getValueFromMap(cfg.thresholdPositionReached, values, name + "_Thresholds_Pos_Reached");

        getValueFromMap(cfg.maxOriVel, values, name + "_Max_vel_ori");
        getValueFromMap(cfg.maxPosVel, values, name + "_Max_vel_pos");
        getValueFromMap(cfg.kpOri, values, name + "_KP_ori");
        getValueFromMap(cfg.kpPos, values, name + "_KP_pos");
    }
} // namespace armarx::RemoteGui
