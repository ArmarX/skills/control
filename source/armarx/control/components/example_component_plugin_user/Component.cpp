/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    control::ArmarXObjects::example_component_plugin_user
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"
#include <thread>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <armarx/control/njoint_controller/controller_descriptions.h>

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>


namespace armarx::control::components::example_component_plugin_user
{

    const std::string Component::defaultName = "example_component_plugin_user";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        // def->optional(
        //     properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        // def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {

        // Load the controllers that we are using here.
        auto robotUnit = getControlComponentPlugin().getRobotUnitPlugin().getRobotUnit();
        robotUnit->loadLibFromPackage("armarx_control", "libarmarx_control_njoint_controller.so");
        robotUnit->loadLibFromPackage("armarx_control", "libarmarx_control_njoint_qp_controller.so");

        //
        //  Example:
        //
        //      use of controller builder with defaults
        //
        {
            ARMARX_INFO << "Example 1";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

            auto controller = builder.withDefaultConfig()
                                  .withNodeSet("RightArm")
                                  .create("MyTaskSpaceImpedanceController1");
            ARMARX_CHECK_NOT_NULL(controller);

            std::this_thread::sleep_for(std::chrono::seconds(1));


            controller->activate();
            std::this_thread::sleep_for(std::chrono::seconds(2));
            
        }


        //
        //  Example:
        //
        //      use of controller builder with defaults (short version)
        //
        {
            ARMARX_INFO << "Example 2";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

            auto controller =
                builder.withNodeSet("RightArm")
                    .create("MyTaskSpaceImpedanceController2"); // implicit withDefaultConfig()
            ARMARX_CHECK_NOT_NULL(controller);

            controller->activate();

            std::this_thread::sleep_for(std::chrono::seconds(2));
        }


        //
        //  Example:
        //
        //      use of controller builder with config from file
        //
        {
            ARMARX_INFO << "Example 3";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();


            auto controller = builder.withDefaultConfig()
                                  .withConfig("file provided by Jeff")
                                  .withNodeSet("RightArm")
                                  .create("MyTaskSpaceImpedanceController3");
            ARMARX_CHECK_NOT_NULL(controller);
            
            std::this_thread::sleep_for(std::chrono::seconds(2));

            controller->updateConfig();

            std::this_thread::sleep_for(std::chrono::seconds(2));

        }


        //
        //  Example:
        //
        //      use of controller builder with config from memory
        //
        // {
        //     ARMARX_INFO << "Example 4";

        //     auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

        //     auto controller = builder.withNodeSet("RightArm")
        //                           .withConfig(armarx::armem::MemoryID("memorylink", "to", "config"))
        //                           .create("MyTaskSpaceImpedanceController4");
        //     ARMARX_CHECK_NOT_NULL(controller);
        // }


        //
        //  Example:
        //
        //      use of controller builder with config update
        //
        {
            ARMARX_INFO << "Example 5";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

            auto controller =
                builder.withNodeSet("RightArm").create("MyTaskSpaceImpedanceController5");
            ARMARX_CHECK_NOT_NULL(controller);

            // update some parameters
            controller->config.kdImpedance *= 2;

            // send new config to robot unit / the controller
            controller->updateConfig();

            // again, update some parameters
            controller->config.kdImpedance.setOnes();
            controller->config.kdImpedance *= 42;

            controller->updateConfig();


            std::this_thread::sleep_for(std::chrono::seconds(2));

            controller->activate();

            std::this_thread::sleep_for(std::chrono::seconds(2));

            // send new config to robot unit / the controller
            controller->updateConfig();

            std::this_thread::sleep_for(std::chrono::seconds(2));

            controller->deactivate();
        }

        //
        //  Example:
        //
        //      use of controller builder with config access before creation
        //
        {
            ARMARX_INFO << "Example 6";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

            // access config before creation
            builder.config().kdImpedance *= 3;

            auto controller =
                builder.withNodeSet("RightArm").create("MyTaskSpaceImpedanceController6");
            ARMARX_CHECK_NOT_NULL(controller);
            
            std::this_thread::sleep_for(std::chrono::seconds(2));
            
            controller->activate();

            std::this_thread::sleep_for(std::chrono::seconds(2));

            controller->deactivate();
            
        }

        

        //
        //  Example:
        //
        //      use of controller builder with config access before creation
        //
        {
            ARMARX_INFO << "Example 7";

            auto builder = controllerBuilder<common::ControllerType::TSImpedance>();

            // access config before creation
            builder.config().kdImpedance *= 3;
 
            auto controller =
                builder.withNodeSet("RightArm").create("MyTaskSpaceImpedanceController7");
            ARMARX_CHECK_NOT_NULL(controller);

            // controller->daemonize();

            std::this_thread::sleep_for(std::chrono::seconds(2));

        }


        ARMARX_INFO << "done.";
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::control::components::example_component_plugin_user
