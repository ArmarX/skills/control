/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Navigation::ArmarXObjects::ControlMemory
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/server/plugins.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>
// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <mutex>


namespace armarx::control::components::control_memory
{

    /**
     * @defgroup Component-ControlMemory ControlMemory
     * @ingroup armarx_navigation-Components
     * A description of the component ControlMemory.
     *
     * @class ControlMemory
     * @ingroup Component-ControlMemory
     * @brief Brief description of class ControlMemory.
     *
     * Detailed description of class ControlMemory.
     */
    class Component :
        virtual public armarx::Component
        // , virtual public armarx::DebugObserverComponentPluginUser
        ,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::armem::server::ReadWritePluginUser
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:
        void visuRun();

        void loadDefaultConfigs();


    private:
        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string snapshotToLoad = "";

            struct LocationGraph
            {
                bool visuLocations = true;
                bool visuGraphEdges = true;
                float visuFrequency = 2;
            };
            LocationGraph locationGraph;
        };
        Properties properties;
        std::mutex propertiesMutex;


        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            struct LocationGraph
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::CheckBox visuLocations;
                armarx::RemoteGui::Client::CheckBox visuGraphEdges;

                void setup(Component& owner);
                void update(Component& owner);
            };
            LocationGraph locationGraph;
        };
        RemoteGuiTab tab;


        struct Tasks
        {
            SimpleRunningTask<>::pointer_type visuTask;
        };
        Tasks tasks;
    };

} // namespace armarx::control
