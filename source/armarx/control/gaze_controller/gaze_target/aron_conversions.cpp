/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    gaze_controller::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"
#include <ArmarXCore/core/time.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>

namespace armarx::control::gaze_controller::gaze_target
{

    void toAron(arondto::FramedPosition& dto, const armarx::FramedPositionPtr& bo)
    {
        dto.position = Eigen::Vector3f(bo->x, bo->y, bo->z);
        dto.frame = bo->frame;
        dto.agent = bo->agent;
    }

    void fromAron(const arondto::FramedPosition& dto, armarx::FramedPositionPtr& bo)
    {
        bo = new FramedPosition(dto.position, dto.frame, dto.agent);
    }


    void toAron(arondto::GazeTarget& dto, const GazeTarget& bo)
    {
        toAron(dto.targetPoint, bo.targetPoint);
        dto.requestTimestamp = bo.requestTimestamp;
        dto.reachedTimestamp = bo.reachedTimestamp;
        dto.releasedTimestamp = bo.releasedTimestamp;
        dto.abortedTimestamp = bo.abortedTimestamp;
    }


    void fromAron(const arondto::GazeTarget& dto, GazeTarget& bo)
    {
        fromAron(dto.targetPoint, bo.targetPoint);
        bo.requestTimestamp = dto.requestTimestamp;
        bo.reachedTimestamp = dto.reachedTimestamp;
        bo.releasedTimestamp = dto.releasedTimestamp;
        bo.abortedTimestamp = dto.abortedTimestamp;
    }

}
