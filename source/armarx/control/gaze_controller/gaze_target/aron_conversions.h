/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    memory_tutorial::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <armarx/control/gaze_controller/gaze_target/GazeTarget.h>
#include <armarx/control/gaze_controller/gaze_target/aron/GazeTarget.aron.generated.h>
#include <armarx/control/gaze_controller/gaze_target/aron/FramedPosition.aron.generated.h>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/armarx.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>


namespace armarx::control::gaze_controller::gaze_target
{
    void toAron(arondto::FramedPosition& dto, const armarx::FramedPositionPtr& bo);
    void fromAron(const arondto::FramedPosition& dto, armarx::FramedPositionPtr& bo);

    void toAron(arondto::GazeTarget& dto, const GazeTarget& bo);
    void fromAron(const arondto::GazeTarget& dto, GazeTarget& bo);
}
