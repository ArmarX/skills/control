/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_control::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time.h>
#include <RobotAPI/libraries/core/FramedPose.h>


namespace armarx::control::gaze_controller::gaze_target
{
    /**
     * @brief Business Object (BO) class of GazeTarget.
     */
    class GazeTarget
    {
    public:

        armarx::DateTime requestTimestamp  = armarx::DateTime(0);
        armarx::DateTime reachedTimestamp  = armarx::DateTime(0);
        armarx::DateTime releasedTimestamp = armarx::DateTime(0);
        armarx::DateTime abortedTimestamp  = armarx::DateTime(0);
        armarx::FramedPositionPtr targetPoint = nullptr;

        GazeTarget();
        explicit GazeTarget(armarx::DateTime requestTimestamp,
                armarx::DateTime reachedTimestamp,
                armarx::DateTime releasedTimestamp,
                armarx::DateTime abortedTimestamp,
                armarx::FramedPositionPtr targetPoint);

        bool isReached() const
        {
            return reachedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }
        bool isReleased() const
        {
            return releasedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }
        bool isAborted() const
        {
            return  abortedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }

    };

}

