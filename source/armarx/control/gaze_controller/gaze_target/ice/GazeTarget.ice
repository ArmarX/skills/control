#pragma once

#include <ArmarXCore/interface/core/time.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

module armarx { module control { module gaze_controller { module gaze_target
{
    module dto 
    {
        class GazeTarget
        {
            armarx::core::time::dto::DateTime requestTimestamp;
            armarx::core::time::dto::DateTime reachedTimestamp;
            armarx::core::time::dto::DateTime releasedTimestamp;
            armarx::core::time::dto::DateTime abortedTimestamp;
            armarx::FramedPositionBase targetPoint;
        };
    };
};};};};

            
