/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_control::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2nullptr22
 * @copyright  http://www.gnu.org/licenses/gpl-2.nullptr.txt
 *             GNU General Public License
 */

#include "GazeTarget.h"

namespace armarx::control::gaze_controller::gaze_target
{
    GazeTarget::GazeTarget():
        requestTimestamp(0),
        reachedTimestamp(0),
        releasedTimestamp(0),
        abortedTimestamp(0),
        targetPoint(nullptr)
    {}

    GazeTarget::GazeTarget(armarx::DateTime requestTimestamp,
                           armarx::DateTime reachedTimestamp,
                           armarx::DateTime releasedTimestamp,
                           armarx::DateTime abortedTimestamp,
                           armarx::FramedPositionPtr targetPoint):
        requestTimestamp(requestTimestamp),
        reachedTimestamp(reachedTimestamp),
        releasedTimestamp(releasedTimestamp),
        abortedTimestamp(abortedTimestamp),
        targetPoint(targetPoint)
    {}
}
