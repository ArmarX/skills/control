#pragma once
#include "GazeTarget.h"
#include <armarx/control/gaze_controller/gaze_target/ice/GazeTarget.h>

namespace armarx::control::gaze_controller::gaze_target
{
    void fromIce(const dto::GazeTargetPtr& dto, GazeTarget& bo);
    void toIce(dto::GazeTargetPtr& dto, const GazeTarget& bo);
}
