#include "ice_conversions.h"
#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/time.h>

namespace armarx::control::gaze_controller::gaze_target
{
    void fromIce(const dto::GazeTargetPtr& dto, GazeTarget& bo)
    {
        fromIce(dto->requestTimestamp, bo.requestTimestamp);
        fromIce(dto->reachedTimestamp, bo.reachedTimestamp);
        fromIce(dto->releasedTimestamp, bo.releasedTimestamp);
        fromIce(dto->abortedTimestamp, bo.abortedTimestamp);
        bo.targetPoint = FramedPositionPtr::dynamicCast(dto->targetPoint);
    }

    void toIce(dto::GazeTargetPtr& dto, const GazeTarget& bo)
    {
        toIce(dto->requestTimestamp, bo.requestTimestamp);
        toIce(dto->reachedTimestamp, bo.reachedTimestamp);
        toIce(dto->releasedTimestamp, bo.releasedTimestamp);
        toIce(dto->abortedTimestamp, bo.abortedTimestamp);
        dto->targetPoint = bo.targetPoint;
    }

}

