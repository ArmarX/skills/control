
#include "GazeController.h"
#include "RobotAPI/components/units/RobotUnit/ControlModes.h"
#include "RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h"

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <armarx/control/gaze_controller/gaze_target.h>

namespace armarx::control::gaze_controller
{

    armarx::NJointControllerRegistration<GazeController> registrationControllerGazeController("GazeController");

    GazeController::GazeController(armarx::RobotUnitPtr robotUnit, const GazeControllerConfigPtr& config, const VirtualRobot::RobotPtr&):
        _config(config)
    {
        ARMARX_CHECK_NOT_NULL(_config);
        //robot
        {
            _rtRobot = useSynchronizedRtRobot();
            ARMARX_CHECK_NOT_NULL(_rtRobot);
            //std::vector<VirtualRobot::RobotNodePtr> nodes = _rtRobot->getRobotNodes();
            _rtCameraNode = _rtRobot->getRobotNode(_config->cameraNodeName);
            _rtPitchNode = _rtRobot->getRobotNode(_config->pitchNodeName);
            _rtYawNode = _rtRobot->getRobotNode(_config->yawNodeName);
            _rtTorsoNode = _rtRobot->getRobotNode(_config->torsoNodeName);
            ARMARX_CHECK_NOT_NULL(_rtCameraNode);
            ARMARX_CHECK_NOT_NULL(_rtPitchNode);
            ARMARX_CHECK_NOT_NULL(_rtYawNode);
            ARMARX_CHECK_NOT_NULL(_rtTorsoNode);
            //Joint velocities to be controlled
            armarx::ControlTarget1DoFActuatorPosition* pitchCtrlTarget =
                useControlTarget<armarx::ControlTarget1DoFActuatorPosition>(_config->pitchNodeName, ControlModes::Position1DoF);
            ARMARX_CHECK_NOT_NULL(pitchCtrlTarget);
            _rtPitchCtrlPos = &(pitchCtrlTarget->position);
            armarx::ControlTarget1DoFActuatorPosition* yawCtrlTarget =
                useControlTarget<armarx::ControlTarget1DoFActuatorPosition>(_config->yawNodeName, ControlModes::Position1DoF);
            ARMARX_CHECK_NOT_NULL(yawCtrlTarget);
            _rtYawCtrlPos = &(yawCtrlTarget->position);
        }

        // _pid.reset(new MultiDimPIDControllerTemplate<2>(_config->Kp, _config->Ki, _config->Kd, _config->maxControlValue, _config->maxDerivation, true));
    }

    GazeController::~GazeController()
    {
    }

    void GazeController::onInitNJointController()
    {
        offeringTopic(topicName);
    }

    void GazeController::onConnectNJointController()
    {
        topic = getTopic<GazeControllerListenerPrx>(topicName);
    }

    std::string GazeController::getClassName(const Ice::Current&) const
    {
        return "GazeController";
    }

    void GazeController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        auto now = armarx::core::time::DateTime::Now();
        if (_tripBufTarget.updateReadBuffer())
        {
            auto& targetBuf = _tripBufTarget._getNonConstReadBuffer();
            if (targetBuf.stopRequest)
            {
                targetBuf.stopRequest = false;
                if (_controlActive)
                {
                    _currentTarget.releasedTimestamp = now;
                    _tripRt2NonRt.getWriteBuffer() = _currentTarget;
                    _tripRt2NonRt.commitWrite();
                }
                _controlActive = false;
            }
            if (targetBuf.targetRequest)
            {
                targetBuf.targetRequest = false;
                if (_controlActive)
                {
                    _currentTarget.releasedTimestamp = now;
                    _tripRt2NonRt.getWriteBuffer() = _currentTarget;
                    _tripRt2NonRt.commitWrite();
                }
                _currentTarget = targetBuf.controlTarget;
                _controlActive = true;
            }
        }

        if (_controlActive)
        {
            const Eigen::Vector2f currentJointAngles(_rtYawNode->getJointValue(), _rtPitchNode->getJointValue());
            const Eigen::Vector3f targetPoint = _currentTarget.targetPoint->toRootEigen(_rtRobot);
            const float h = _rtCameraNode->getPositionInRootFrame().z();
            float yaw = -std::atan2(targetPoint.x(), targetPoint.y());
            float pitch = std::atan2(h - targetPoint.z(), targetPoint.y());
            const bool targetReachable = _rtYawNode->checkJointLimits(yaw)
                                         && _rtPitchNode->checkJointLimits(pitch);
            if (!targetReachable)
            {
                //limit joint ranges to avoid damage
                yaw = std::clamp(yaw, _rtYawNode->getJointLimitLo(), _rtYawNode->getJointLimitHi());
                pitch = std::clamp(pitch, _rtPitchNode->getJointLimitLo(), _rtPitchNode->getJointLimitHi());
            }
            const Eigen::Vector2f targetJointAngles(yaw, pitch);

            // _pid->update(timeSinceLastIteration.toSecondsDouble(), currentJointAngles, targetJointAngles);
            // Eigen::Vector2f ctrlVel = _pid->getControlValue();

            if (!targetReachable && _config->abortIfUnreachable)
            {
                //abort the target
                _currentTarget.abortedTimestamp = now;
                _tripRt2NonRt.getWriteBuffer() = _currentTarget;
                _tripRt2NonRt.commitWrite();
                // ctrlVel.setZero();
                // _pid->reset();
                _controlActive = false;
            }
            const Eigen::Vector2f angleDiff = targetJointAngles - currentJointAngles;
            const bool targetReached = std::abs(angleDiff.x()) < _config->yawAngleTolerance &&
                                       std::abs(angleDiff.y()) < _config->pitchAngleTolerance;
            if (targetReached)
            {
                if (targetReachable && !_currentTarget.isReached())
                {
                    _currentTarget.reachedTimestamp = now;
                    _tripRt2NonRt.getWriteBuffer() = _currentTarget;
                    _tripRt2NonRt.commitWrite();
                }
                //ctrlVel.setZero();
                //_pid->reset();
                //keep tracking
            }
            //report debugging variables
            _publishCurrentYawAngle = currentJointAngles.x();
            _publishCurrentPitchAngle = currentJointAngles.y();
            _publishTargetYawAngle = targetJointAngles.x();
            _publishTargetPitchAngle = targetJointAngles.y();
            _publishYawControlPos = 0; //ctrlVel.x();
            _publishPitchControlPos = 0;// ctrlVel.y();
            //apply velocities
            *_rtYawCtrlPos = targetJointAngles.x();
            *_rtPitchCtrlPos = targetJointAngles.y();
        }
        else
        {
            *_rtYawCtrlPos = 0;
            *_rtPitchCtrlPos = 0;
        }
    }

    void GazeController::rtPreActivateController()
    {}

    void GazeController::rtPostDeactivateController()
    {}

    void GazeController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObserver)
    {

        const float currentPitchAngle = _publishCurrentPitchAngle;
        const float currentYawAngle = _publishCurrentYawAngle;
        const float targetPitchAngle = _publishTargetPitchAngle;
        const float targetYawAngle = _publishTargetYawAngle;
        const float pitchControlVel = _publishPitchControlPos;
        const float yawControlVel = _publishYawControlPos;

        StringVariantBaseMap datafields;
        datafields["currentPitchAngle"] = new Variant(currentPitchAngle);
        datafields["currentYawAngle"] = new Variant(currentYawAngle);
        datafields["targetPitchAngle"] = new Variant(targetPitchAngle);
        datafields["targetYawAngle"] = new Variant(targetYawAngle);
        datafields["pitchControlVel"] = new Variant(pitchControlVel);
        datafields["yawControlVel"] = new Variant(yawControlVel);
        if (_tripRt2NonRt.updateReadBuffer())
        {
            gaze_target::GazeTarget target = _tripRt2NonRt.getReadBuffer();
            publishTarget(target);
        }
        debugObserver->setDebugChannel(getInstanceName(), datafields);
    }

    void GazeController::publishTarget(const gaze_target::GazeTarget& bo)
    {
        gaze_target::dto::GazeTargetPtr dto = new gaze_target::dto::GazeTarget();
        toIce(dto, bo);
        topic->reportGazeTarget(dto);
    }

    void GazeController::submitTarget(const FramedPositionBasePtr& targetPoint, const armarx::core::time::dto::DateTime& requestTimestamp, const Ice::Current&)
    {
        FramedPositionPtr point = FramedPositionPtr::dynamicCast(targetPoint);
        armarx::core::time::DateTime timestamp;
        armarx::core::time::fromIce(requestTimestamp, timestamp);
        auto target = gaze_target::GazeTarget(timestamp, 0, 0, 0, point);

        publishTarget(target);
        ARMARX_INFO << "setting new target: (" << point->x << ", " << point->y << ", " << point->z << ")";

        std::lock_guard g{_tripBufTargetMutex};
        auto& w = _tripBufTarget.getWriteBuffer();
        w.stopRequest = false;
        w.targetRequest = true;
        w.controlTarget = target;
        _tripBufTarget.commitWrite();
    }

    void GazeController::removeTargetAfter(const armarx::core::time::dto::Duration& dto, const Ice::Current&)
    {
        armarx::core::time::Duration bo;
        armarx::core::time::fromIce(dto, bo);
        std::async(std::launch::async, [](auto duration, GazeController * self)
        {
            armarx::core::time::Clock::WaitFor(duration);
            self->removeTarget();
        }, bo, this);
    }

    void GazeController::removeTarget()
    {
        ARMARX_INFO << "Stop request received";
        std::lock_guard g(_tripBufTargetMutex);
        auto& w = _tripBufTarget.getWriteBuffer();
        w.stopRequest = true;
        w.targetRequest = false;
        w.controlTarget = gaze_target::GazeTarget(0, 0, 0, 0, nullptr);
        _tripBufTarget.commitWrite();
    }
}
