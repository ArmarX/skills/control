
/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Raphael Grimm
* @copyright  2019 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/time.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <armarx/control/gaze_controller/gaze_target/ice/GazeTarget.ice>

module armarx
{
    module control
    {
        module gaze_controller
        {

            class GazeControllerConfig extends NJointControllerConfig
            {
                string cameraFrameName = "DepthCamera";
                string yawNodeName = "Neck_1_Yaw";
                string pitchNodeName = "Neck_2_Pitch";
                string cameraNodeName = "DepthCamera";
                string torsoNodeName = "TorsoJoint";

                float Kp = 1.9f;
                float Ki = 0.0f;
                float Kd = 0.0f;
                double maxControlValue = 1.0;
                double maxDerivation = 0.5;

                float yawAngleTolerance = 0.005;
                float pitchAngleTolerance = 0.005;
                bool abortIfUnreachable = false;
            };

            interface GazeControllerInterface extends
                NJointControllerInterface
            {
                void submitTarget(FramedPositionBase target, armarx::core::time::dto::DateTime requestTimestamp);
                void removeTargetAfter(armarx::core::time::dto::Duration duration);
            };

            interface GazeControllerListener
            {
                void reportGazeTarget(gaze_target::dto::GazeTarget target);
            };

        };
    };
};
