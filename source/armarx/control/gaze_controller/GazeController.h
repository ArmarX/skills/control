
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection
 * @author     Johann Mantel
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <future>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/control/gaze_controller/gaze_target/GazeTarget.h>
#include <armarx/control/gaze_controller/GazeControllerInterface.h>

namespace armarx::control::gaze_controller
{
    TYPEDEF_PTRS_HANDLE(GazeController);
    /**
     * @class GazeControlPropertyDefinitions
     * @brief Property definitions of `GazeControl`.
     */
    class GazeControlPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GazeControlPropertyDefinitions(std::string prefix);
    };

    /**
     * @defgroup Component-GazeControl GazeControl
     * @ingroup ActiveVision-Components
     * A description of the component GazeControl.
     *
     * @class GazeControl
     * @ingroup Component-GazeControl
     * @brief Brief description of class GazeControl.
     *
     * Detailed description of class GazeControl.
     */
    class GazeController :
        public NJointController
        , public GazeControllerInterface
    {
    public:
        using ConfigPtrT = GazeControllerConfigPtr;

        GazeController(RobotUnitPtr robotUnit, const GazeControllerConfigPtr& config, const VirtualRobot::RobotPtr& robot);

        ~GazeController();

        // NJointControllerInterface interface
        void onInitNJointController() override;

        // NJointControllerInterface interface
        void onConnectNJointController() override;

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        // NJointController interface
        void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&) override;

        //GazeController interface
        void submitTarget(const FramedPositionBasePtr& target, const armarx::core::time::dto::DateTime& requestTimestamp, const Ice::Current& c = ::Ice::Current()) override;

        //GazeController interface
        void removeTargetAfter(const armarx::core::time::dto::Duration& duration, const Ice::Current& c = ::Ice::Current()) override;

    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:
        void publishTarget(const gaze_target::GazeTarget& target);
        void removeTarget();

        struct ControlData
        {
            bool stopRequest = false;
            bool targetRequest = false;
            gaze_target::GazeTarget controlTarget;
        };

        //rt variables
        const GazeControllerConfigPtr _config;

        VirtualRobot::RobotPtr _rtRobot;
        VirtualRobot::RobotNodePtr _rtYawNode;
        VirtualRobot::RobotNodePtr _rtPitchNode;
        VirtualRobot::RobotNodePtr _rtCameraNode;
        VirtualRobot::RobotNodePtr _rtTorsoNode;

        float* _rtPitchCtrlPos;
        float* _rtYawCtrlPos;

        // std::unique_ptr<MultiDimPIDControllerTemplate<2>> _pid;
        gaze_target::GazeTarget _currentTarget;
        bool _controlActive = false;

        //commit buffer
        mutable std::recursive_mutex           _tripBufTargetMutex;
        WriteBufferedTripleBuffer<ControlData> _tripBufTarget;

        //publishing
        mutable std::recursive_mutex           _tripRt2NonRtMutex;
        TripleBuffer<gaze_target::GazeTarget>  _tripRt2NonRt;

        std::atomic<float> _publishCurrentPitchAngle;
        std::atomic<float> _publishCurrentYawAngle;
        std::atomic<float> _publishTargetPitchAngle;
        std::atomic<float> _publishTargetYawAngle;
        std::atomic<float> _publishPitchControlPos;
        std::atomic<float> _publishYawControlPos;

        std::string topicName = "GazeTargets";
        GazeControllerListenerPrx topic;
    };
}
