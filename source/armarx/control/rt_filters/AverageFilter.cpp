#include "AverageFilter.h"

namespace armarx::control::rt_filters
{
    RtAverageFilter::RtAverageFilter(size_t windowSize)
    {
        dataHistory.set_capacity(windowSize);
    }

    float
    RtAverageFilter::update(float value)
    {
        dataHistory.push_back(value);
        float result = 0;
        for (auto& value : dataHistory)
        {
            result += value;
        }
        return result / static_cast<float>(dataHistory.size());
    }
} // namespace armarx::control::rt_filters
