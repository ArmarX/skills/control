#pragma once


// TODO: Should not depend on the whole ethercat library.
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>


namespace armarx::control::rt_filters
{
    template <typename T>
    class MedianFilteredLinearConvertedValue
    {
    public:
        MedianFilteredLinearConvertedValue() = default;
        MedianFilteredLinearConvertedValue(MedianFilteredLinearConvertedValue&&) = default;
        MedianFilteredLinearConvertedValue(const MedianFilteredLinearConvertedValue&) = default;
        MedianFilteredLinearConvertedValue&
        operator=(MedianFilteredLinearConvertedValue&&) = default;
        MedianFilteredLinearConvertedValue&
        operator=(const MedianFilteredLinearConvertedValue&) = default;

        void
        init(T* raw,
             const DefaultRapidXmlReaderNode& node,
             float defaultValue = std::nan("1"),
             bool offsetBeforeFactor = true,
             const char* nameForDebugging = "")
        {
            lcv.init(raw, node, defaultValue, offsetBeforeFactor, nameForDebugging);
            median = RtMedianFilter{node.attribute_as<std::size_t>("median_filter_width")};
        }

        void
        read()
        {
            lcv.read();
            value = median.update(lcv.value);
        }

        ethercat::LinearConvertedValue<T> lcv;
        RtMedianFilter median;
        float value = 0;
    };

} // namespace armarx::control::rt_filters
