#pragma once


// Boost
#include <boost/circular_buffer.hpp>


namespace armarx::control::rt_filters
{

    using AverageFilterPtr = std::shared_ptr<class RtAverageFilter>;

    class RtAverageFilter
    {
    public:
        RtAverageFilter(size_t windowSize = 50);


        float update(float value);

    private:
        boost::circular_buffer<float> dataHistory;
    };

} // namespace armarx::control::rt_filters
