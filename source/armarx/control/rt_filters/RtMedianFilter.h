#pragma once


// STD/STL
#include <vector>

// Boost
#include <boost/circular_buffer.hpp>


namespace armarx::control::rt_filters
{

    class RtMedianFilter
    {
    public:
        RtMedianFilter(size_t windowSize = 10);

        RtMedianFilter(RtMedianFilter&&) = default;
        RtMedianFilter(const RtMedianFilter&) = default;
        RtMedianFilter& operator=(RtMedianFilter&&) = default;
        RtMedianFilter& operator=(const RtMedianFilter&) = default;

        float update(float value);

        std::size_t getWindowSize() const;

    private:
        boost::circular_buffer<float> dataHistory;
        size_t windowSize;
        std::vector<float> medianBuffer;
    };

} // namespace armarx::control::rt_filters
