/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FirFilter.h"

namespace armarx::control::rt_filters
{
    FirFilter::FirFilter(const std::vector<float>& impulseResponse) :
        impulseResponse(impulseResponse),
        values(impulseResponse.size(), 0.0f),
        index(0),
        size(impulseResponse.size())
    {
    }

    void
    FirFilter::setImpulseResponse(const std::vector<float>& impulseResponse)
    {
        this->impulseResponse = impulseResponse;
        this->index = 0;
        this->size = impulseResponse.size();
        this->values = std::vector<float>(size, 0);
    }

    float
    FirFilter::update(float value)
    {
        index = (index + size - 1) % size;
        values.at(index) = value;
        float response = 0;
        for (size_t i = 0; i < size; i++)
        {
            response += values.at((i + index) % size) * impulseResponse.at(i);
        }
        return response;
    }
} // namespace armarx::control::rt_filters
