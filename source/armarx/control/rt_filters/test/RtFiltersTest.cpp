/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::RtFilters
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE Armar6RT::ArmarXLibraries::RtFilters

#define ARMARX_BOOST_TEST

#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/control/Test.h>
#include <armarx/control/rt_filters/FirFilter.h>

using namespace armarx::control::rt_filters;

BOOST_AUTO_TEST_CASE(testFirFilter1)
{

    BOOST_CHECK_EQUAL(true, true);

    FirFilter filter({1, -1});

    std::vector<float> inputSequence({1, 2, 3, 5, 4});
    std::vector<float> expectedResponse({1, 1, 1, 2, -1});

    for (size_t i = 0; i < expectedResponse.size(); i++)
    {
        float actual = filter.update(inputSequence.at(i));
        float expected = expectedResponse.at(i);

        BOOST_CHECK_CLOSE(expected, actual, 0.001f);
    }
}

BOOST_AUTO_TEST_CASE(testFirFilter2)
{

    BOOST_CHECK_EQUAL(true, true);

    std::vector<float> impulseResponse;
    float v = 1;
    for (size_t i = 0; i < 10; i++)
    {
        impulseResponse.push_back(v);
        v /= 2;
    }
    FirFilter filter(impulseResponse);

    std::vector<float> inputSequence({1, 2, 3, 4, 5, 10, 0, -10, 2, 5, 7, 10, 5, 0, 13, 20});
    float expected = 0;

    for (float i : inputSequence)
    {
        float actual = filter.update(i);
        expected = i + expected / 2;
        ARMARX_INFO << VAROUT(i) << VAROUT(actual) << VAROUT(expected);

        BOOST_CHECK_CLOSE(expected, actual, 0.1f);
    }
}
