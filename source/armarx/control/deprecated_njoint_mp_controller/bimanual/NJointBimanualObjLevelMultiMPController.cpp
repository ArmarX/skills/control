#include "NJointBimanualObjLevelMultiMPController.h"

#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/time/CycleUtil.h>

namespace armarx::control::deprecated_njoint_mp_controller::bimanual
{
    NJointControllerRegistration<NJointBimanualObjLevelMultiMPController>
        registrationControllerNJointBimanualObjLevelMultiMPController(
            "NJointBimanualObjLevelMultiMPController");

    NJointBimanualObjLevelMultiMPController::NJointBimanualObjLevelMultiMPController(
        const RobotUnitPtr& robUnit,
        const armarx::NJointControllerConfigPtr& config,
        const VirtualRobot::RobotPtr&)
    {
        ARMARX_INFO << "Initializing Bimanual Object Level Controller";

        // initialize robot kinematic chain and sensors
        useSynchronizedRtRobot();
        cfg = NJointBimanualObjLevelMultiMPControllerConfigPtr::dynamicCast(config);

        leftRNS = rtGetRobot()->getRobotNodeSet("LeftArm");

        for (size_t i = 0; i < leftRNS->getSize(); ++i)
        {
            std::string jointName = leftRNS->getNode(i)->getName();
            leftJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            leftTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());
            const SensorValue1DoFActuatorVelocity* velocitySensor =
                sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor =
                sv->asA<SensorValue1DoFActuatorPosition>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }


            leftVelocitySensors.push_back(velocitySensor);
            leftPositionSensors.push_back(positionSensor);
        };

        rightRNS = rtGetRobot()->getRobotNodeSet("RightArm");

        for (size_t i = 0; i < rightRNS->getSize(); ++i)
        {
            std::string jointName = rightRNS->getNode(i)->getName();
            rightJointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            rightTargets.push_back(ct->asA<ControlTarget1DoFActuatorTorque>());

            const SensorValue1DoFActuatorVelocity* velocitySensor =
                sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFActuatorPosition* positionSensor =
                sv->asA<SensorValue1DoFActuatorPosition>();

            if (!velocitySensor)
            {
                ARMARX_WARNING << "No velocitySensor available for " << jointName;
            }
            if (!positionSensor)
            {
                ARMARX_WARNING << "No positionSensor available for " << jointName;
            }

            rightVelocitySensors.push_back(velocitySensor);
            rightPositionSensors.push_back(positionSensor);
        };


        const SensorValueBase* svlf = robUnit->getSensorDevice("FT L")->getSensorValue();
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        const SensorValueBase* svrf = robUnit->getSensorDevice("FT R")->getSensorValue();
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        leftIK.reset(new VirtualRobot::DifferentialIK(
            leftRNS, leftRNS->getRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        rightIK.reset(new VirtualRobot::DifferentialIK(rightRNS,
                                                       rightRNS->getRobot()->getRootNode(),
                                                       VirtualRobot::JacobiProvider::eSVDDamped));

        tcpLeft = leftRNS->getTCP();
        tcpRight = rightRNS->getTCP();

        /* ----------------- initialize DMP ----------------- */
        tsvmp::TaskSpaceDMPControllerConfig taskSpaceDMPConfig;
        taskSpaceDMPConfig.DMPKernelSize = cfg->kernelSize;
        taskSpaceDMPConfig.DMPMode = cfg->dmpMode;
        taskSpaceDMPConfig.DMPAmplitude = cfg->dmpAmplitude;
        taskSpaceDMPConfig.phaseStopParas.goDist = cfg->phaseDist0;
        taskSpaceDMPConfig.phaseStopParas.backDist = cfg->phaseDist1;
        taskSpaceDMPConfig.phaseStopParas.Kpos = cfg->phaseKpPos;
        taskSpaceDMPConfig.phaseStopParas.Dpos = 0;
        taskSpaceDMPConfig.phaseStopParas.Kori = cfg->phaseKpOri;
        taskSpaceDMPConfig.phaseStopParas.Dori = 0;
        taskSpaceDMPConfig.phaseStopParas.mm2radi = cfg->posToOriRatio;
        taskSpaceDMPConfig.phaseStopParas.maxValue = cfg->phaseL;
        taskSpaceDMPConfig.phaseStopParas.slop = cfg->phaseK;
        taskSpaceDMPConfig.motionTimeDuration = cfg->timeDuration;

        taskSpaceDMPConfig.DMPStyle = cfg->dmpObjType;
        objectDMP.reset(new tsvmp::TaskSpaceDMPController("objDMP", taskSpaceDMPConfig, false));
        taskSpaceDMPConfig.DMPStyle = cfg->dmpLeftType;
        leftTCPInObjDMP.reset(new tsvmp::TaskSpaceDMPController("leftDMP", taskSpaceDMPConfig, false));
        taskSpaceDMPConfig.DMPStyle = cfg->dmpRightType;
        rightTCPInObjDMP.reset(new tsvmp::TaskSpaceDMPController("rightDMP", taskSpaceDMPConfig, false));
        ARMARX_IMPORTANT << "dmps initialized";

        /* ----------------- initialize control parameters ----------------- */
        auto setParamVec = [&](Eigen::VectorXf& param, ::Ice::FloatSeq& cfgParam, const size_t n)
        {
            param.setZero(n);
            ARMARX_CHECK_EQUAL(cfgParam.size(), n);

            for (size_t i = 0; i < n; ++i)
            {
                param(i) = cfgParam.at(i);
            }
        };

        setParamVec(KpImpedance, cfg->KpImpedance, 12);
        setParamVec(KdImpedance, cfg->KdImpedance, 12);
        setParamVec(KpAdmittance, cfg->KpAdmittance, 6);
        setParamVec(KdAdmittance, cfg->KdAdmittance, 6);
        setParamVec(KmAdmittance, cfg->KmAdmittance, 6);
        setParamVec(KmPID, cfg->KmPID, 6);

        ARMARX_INFO << "got controller params";

        /* ------------- initialize default joint values ------------------- */
        leftDesiredJointValues.resize(leftTargets.size());
        ARMARX_CHECK_EQUAL(cfg->leftDesiredJointValues.size(), leftTargets.size());

        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            leftDesiredJointValues(i) = cfg->leftDesiredJointValues.at(i);
        }

        rightDesiredJointValues.resize(rightTargets.size());
        ARMARX_CHECK_EQUAL(cfg->rightDesiredJointValues.size(), rightTargets.size());

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            rightDesiredJointValues(i) = cfg->rightDesiredJointValues.at(i);
        }

        /* ------------- obtain object initial poses ------------------- */
        // object pose (position in meter)
        objInitialPose = VirtualRobot::MathTools::quat2eigen4f(cfg->objInitialPose[4],
                                                               cfg->objInitialPose[5],
                                                               cfg->objInitialPose[6],
                                                               cfg->objInitialPose[3]);
        for (int i = 0; i < 3; ++i)
        {
            objInitialPose(i, 3) = cfg->objInitialPose[i];
        }

        virtualAcc.setZero(6);
        virtualVel.setZero(6);
        virtualPose = objInitialPose;

        objTransMatrixInAnchor = Eigen::Matrix4f::Identity();

        // obtain left & right initial pose in the object frame (in meter)
        leftInitialPose = Eigen::Matrix4f::Identity();
        rightInitialPose = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f leftPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f rightPose = tcpRight->getPoseInRootFrame();
        leftInitialPose.block<3, 3>(0, 0) =
            objInitialPose.block<3, 3>(0, 0).transpose() * leftPose.block<3, 3>(0, 0);
        leftInitialPose.block<3, 1>(0, 3) =
            objInitialPose.block<3, 3>(0, 0).transpose() *
            (leftPose.block<3, 1>(0, 3) * 0.001 - objInitialPose.block<3, 1>(0, 3));
        rightInitialPose.block<3, 3>(0, 0) =
            objInitialPose.block<3, 3>(0, 0).transpose() * rightPose.block<3, 3>(0, 0);
        rightInitialPose.block<3, 1>(0, 3) =
            objInitialPose.block<3, 3>(0, 0).transpose() *
            (rightPose.block<3, 1>(0, 3) * 0.001 - objInitialPose.block<3, 1>(0, 3));

        integratedPoseObj = virtualPose;
        integratedPoseLeft = leftInitialPose; // need to be represented in the object local frame.
        integratedPoseRight = rightInitialPose;
        /* ------------- initialize buffers ------------------- */
        // interface to RT
        Inferface2rtData interface2rtData;
        interface2rtData.KpImpedance = KpImpedance;
        interface2rtData.KdImpedance = KdImpedance;
        interface2rtData.KmAdmittance = KmAdmittance;
        interface2rtData.KpAdmittance = KpAdmittance;
        interface2rtData.KdAdmittance = KdAdmittance;
        interface2rtBuffer.reinitAllBuffers(interface2rtData);

        // RT to DMP
        RT2ControlData rt2ControlData;
        rt2ControlData.deltaT = 0;
        rt2ControlData.currentTime = 0;
        rt2ControlData.currentPoseObj = objInitialPose;
        rt2ControlData.currentTwistObj.setZero();
        rt2ControlData.currentPoseObj = leftInitialPose;
        rt2ControlData.currentTwistObj.setZero();
        rt2ControlData.currentPoseObj = rightInitialPose;
        rt2ControlData.currentTwistObj.setZero();
        rt2ControlBuffer.reinitAllBuffers(rt2ControlData);

        // DMP (or interface) to RT Target
        NJointBimanualObjLevelMultiMPControlData initData;
        initData.objTargetPose = objInitialPose;
        initData.objTargetTwist.setZero(6);

        initData.leftTargetPoseInObj = leftInitialPose;
        initData.leftTargetTwistInObj.setZero(6);

        initData.rightTargetPoseInObj = rightInitialPose;
        initData.rightTargetTwistInObj.setZero(6);
        reinitTripleBuffer(initData);

        // Control interface: RT to interface
        RT2InterfaceData rt2InterfaceData;
        rt2InterfaceData.currentLeftPoseInObjFrame = leftInitialPose;
        rt2InterfaceData.currentRightPoseInObjFrame = rightInitialPose;
        rt2InterfaceData.currentObjPose = objInitialPose;
        rt2InterfaceData.currentObjForce.setZero();
        rt2InterfaceData.currentObjVel.setZero();
        rt2InterfaceBuffer.reinitAllBuffers(rt2InterfaceData);


        /* ------------- initialize PID force controller ------------------- */
        forcePIDControllers.resize(12);
        for (size_t i = 0; i < 6; i++)
        {
            forcePIDControllers.at(i).reset(new PIDController(
                cfg->forceP[i], cfg->forceI[i], cfg->forceD[i], cfg->forcePIDLimits[i]));
            forcePIDControllers.at(i + 6).reset(new PIDController(
                cfg->forceP[i], cfg->forceI[i], cfg->forceD[i], cfg->forcePIDLimits[i]));
            forcePIDControllers.at(i)->reset();
            forcePIDControllers.at(i + 6)->reset();
        }

        /* ------------- initialize force torque sensor related ------------------- */
        // sensor frame to tcp frame transformation
        sensorFrame2TcpFrameLeft.setZero();
        sensorFrame2TcpFrameRight.setZero();

        // filter
        filterCoeff = cfg->filterCoeff;
        filteredOldValue.setZero(12);

        // first loop calibrate
        ftcalibrationTimer = 0;
        ftOffset.setZero(12);

        // target wrench
        targetWrench.setZero(cfg->targetWrench.size());
        for (size_t i = 0; i < cfg->targetWrench.size(); ++i)
        {
            targetWrench(i) = cfg->targetWrench[i];
        }

        /* ------------- static compensation ------------------- */
        massLeft = cfg->massLeft;
        CoMVecLeft << cfg->CoMVecLeft[0], cfg->CoMVecLeft[1], cfg->CoMVecLeft[2];
        forceOffsetLeft << cfg->forceOffsetLeft[0], cfg->forceOffsetLeft[1],
            cfg->forceOffsetLeft[2];
        torqueOffsetLeft << cfg->torqueOffsetLeft[0], cfg->torqueOffsetLeft[1],
            cfg->torqueOffsetLeft[2];

        massRight = cfg->massRight;
        CoMVecRight << cfg->CoMVecRight[0], cfg->CoMVecRight[1], cfg->CoMVecRight[2];
        forceOffsetRight << cfg->forceOffsetRight[0], cfg->forceOffsetRight[1],
            cfg->forceOffsetRight[2];
        torqueOffsetRight << cfg->torqueOffsetRight[0], cfg->torqueOffsetRight[1],
            cfg->torqueOffsetRight[2];

        /* ------------- Pose Offset ------------------- */
        fixedLeftRightRotOffset = Eigen::Matrix3f::Identity();
        objCom2TCPLeftInObjFrame.setZero();
        objCom2TCPRightInObjFrame.setZero();

        /* ------------- setup flags ------------------- */
        firstLoop = true;
        dmpStarted = false;
        ARMARX_IMPORTANT << "finished construction!";
    }


    void
    NJointBimanualObjLevelMultiMPController::rtPreActivateController()
    {
    }

    std::string
    NJointBimanualObjLevelMultiMPController::getClassName(const Ice::Current&) const
    {
        return "NJointBimanualObjLevelMultiMPController";
    }

    void
    NJointBimanualObjLevelMultiMPController::controllerRun()
    {
        if (!rt2ControlBuffer.updateReadBuffer() || !dmpStarted)
        {
            return;
        }

        // get status from RT loop
        double deltaT = rt2ControlBuffer.getReadBuffer().deltaT;
        Eigen::Matrix4f currentPoseObj = rt2ControlBuffer.getReadBuffer().currentPoseObj;
        Eigen::VectorXf currentTwistObj = rt2ControlBuffer.getReadBuffer().currentTwistObj;
        Eigen::Matrix4f currentPoseLeftInObj =
            rt2ControlBuffer.getReadBuffer().currentPoseLeftInObj;
        Eigen::VectorXf currentTwistLeftInObj =
            rt2ControlBuffer.getReadBuffer().currentTwistLeftInObj;
        Eigen::Matrix4f currentPoseRightInObj =
            rt2ControlBuffer.getReadBuffer().currentPoseRightInObj;
        Eigen::VectorXf currentTwistRightInObj =
            rt2ControlBuffer.getReadBuffer().currentTwistRightInObj;

        // set can val from outside
        rightTCPInObjDMP->flow(deltaT, currentPoseRightInObj, currentTwistRightInObj);

        // DMP flow control
        bool objectDMPFinished = false;
        bool leftTCPInObjDMPFinished = false;
        bool rightTCPInObjDMPFinished = false;

        if (objectDMP->canVal > 1e-8 || objectDMP->config.DMPStyle == "Periodic")
        {
            objectDMP->flow(deltaT, currentPoseObj, currentTwistObj);
            getWriterControlStruct().objTargetPose = objectDMP->getTargetPoseMat();
            getWriterControlStruct().objTargetTwist = objectDMP->getTargetVelocity();
        }
        else
        {
            objectDMPFinished = true;
        }


        if (leftTCPInObjDMP->canVal > 1e-8 || leftTCPInObjDMP->config.DMPStyle == "Periodic")
        {
            leftTCPInObjDMP->flow(deltaT, currentPoseLeftInObj, currentTwistLeftInObj);
            getWriterControlStruct().leftTargetPoseInObj = leftTCPInObjDMP->getTargetPoseMat();
            getWriterControlStruct().leftTargetTwistInObj = leftTCPInObjDMP->getTargetVelocity();
        }
        else
        {
            leftTCPInObjDMPFinished = true;
        }

        if (rightTCPInObjDMP->canVal > 1e-8 || rightTCPInObjDMP->config.DMPStyle == "Periodic")
        {
            rightTCPInObjDMP->flow(deltaT, currentPoseRightInObj, currentTwistRightInObj);
            getWriterControlStruct().rightTargetPoseInObj = rightTCPInObjDMP->getTargetPoseMat();
            getWriterControlStruct().rightTargetTwistInObj = rightTCPInObjDMP->getTargetVelocity();
        }
        else
        {
            rightTCPInObjDMPFinished = true;
        }


        if (objectDMPFinished && leftTCPInObjDMPFinished && rightTCPInObjDMPFinished)
        {
            finished = true;
            dmpStarted = false;
        }

        // set target to RT loop
        LockGuardType guard{controlDataMutex};
        writeControlStruct();
    }

    void
    NJointBimanualObjLevelMultiMPController::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                                                   const IceUtil::Time& timeSinceLastIteration)
    {
        // ------------------------------------------- current tcp pose -------------------------------------------
        Eigen::Matrix4f currentLeftPose = tcpLeft->getPoseInRootFrame();
        Eigen::Matrix4f currentRightPose = tcpRight->getPoseInRootFrame();

        currentLeftPose.block<3, 1>(0, 3) = 0.001 * currentLeftPose.block<3, 1>(0, 3);
        currentRightPose.block<3, 1>(0, 3) = 0.001 * currentRightPose.block<3, 1>(0, 3);

        double deltaT = timeSinceLastIteration.toSecondsDouble();
        ftcalibrationTimer += deltaT;

        if (firstLoop)
        {
            Eigen::Matrix4f leftPose = currentLeftPose;
            Eigen::Matrix4f rightPose = currentRightPose;

            Eigen::Vector3f objCom2TCPLeftInGlobal =
                leftPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3);
            Eigen::Vector3f objCom2TCPRightInGlobal =
                rightPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3);

            // Temporary (until we could detect and set object pose by perception)
            // T_obj = T_{leftHand} * T_{objTransMatrixInAnchor}
            objTransMatrixInAnchor.block<3, 3>(0, 0) =
                leftPose.block<3, 3>(0, 0).transpose() * objInitialPose.block<3, 3>(0, 0);
            objTransMatrixInAnchor.block<3, 1>(0, 3) =
                leftPose.block<3, 3>(0, 0).transpose() *
                (objInitialPose.block<3, 1>(0, 3) - leftPose.block<3, 1>(0, 3));

            objCom2TCPLeftInObjFrame =
                virtualPose.block<3, 3>(0, 0).transpose() * objCom2TCPLeftInGlobal;
            objCom2TCPRightInObjFrame =
                virtualPose.block<3, 3>(0, 0).transpose() * objCom2TCPRightInGlobal;


            Eigen::Matrix4f leftSensorFrame =
                leftRNS->getRobot()->getRobotNode("ArmL8_Wri2")->getPoseInRootFrame();
            Eigen::Matrix4f rightSensorFrame =
                rightRNS->getRobot()->getRobotNode("ArmR8_Wri2")->getPoseInRootFrame();
            leftSensorFrame.block<3, 1>(0, 3) = leftSensorFrame.block<3, 1>(0, 3) * 0.001;
            rightSensorFrame.block<3, 1>(0, 3) = rightSensorFrame.block<3, 1>(0, 3) * 0.001;

            // sensor frame 2 tcp frame transformation
            sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) =
                leftPose.block<3, 3>(0, 0).transpose() * leftSensorFrame.block<3, 3>(0, 0);
            sensorFrame2TcpFrameRight.block<3, 3>(0, 0) =
                rightPose.block<3, 3>(0, 0).transpose() * rightSensorFrame.block<3, 3>(0, 0);
            CoMVecLeft = sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * CoMVecLeft;
            CoMVecRight = sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * CoMVecRight;
            firstLoop = false;
        }

        Eigen::Matrix4f currentLeftPoseInObjFrame = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f currentRightPoseInObjFrame = Eigen::Matrix4f::Identity();
        currentLeftPoseInObjFrame.block<3, 3>(0, 0) =
            virtualPose.block<3, 3>(0, 0).transpose() * currentLeftPose.block<3, 3>(0, 0);
        currentLeftPoseInObjFrame.block<3, 1>(0, 3) =
            virtualPose.block<3, 3>(0, 0).transpose() *
            (currentLeftPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3));
        currentRightPoseInObjFrame.block<3, 3>(0, 0) =
            virtualPose.block<3, 3>(0, 0).transpose() * currentRightPose.block<3, 3>(0, 0);
        currentRightPoseInObjFrame.block<3, 1>(0, 3) =
            virtualPose.block<3, 3>(0, 0).transpose() *
            (currentRightPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3));
        rt2InterfaceBuffer.getWriteBuffer().currentLeftPoseInObjFrame = currentLeftPoseInObjFrame;
        rt2InterfaceBuffer.getWriteBuffer().currentRightPoseInObjFrame = currentRightPoseInObjFrame;


        // --------------------------------------------- get control parameters ---------------------------------------
        KpImpedance = interface2rtBuffer.getUpToDateReadBuffer().KpImpedance;
        KdImpedance = interface2rtBuffer.getUpToDateReadBuffer().KdImpedance;
        KmAdmittance = interface2rtBuffer.getUpToDateReadBuffer().KmAdmittance;
        KpAdmittance = interface2rtBuffer.getUpToDateReadBuffer().KpAdmittance;
        KdAdmittance = interface2rtBuffer.getUpToDateReadBuffer().KdAdmittance;

        if (ftcalibrationTimer < cfg->ftCalibrationTime)
        {
            ftOffset.block<3, 1>(0, 0) =
                0.5 * ftOffset.block<3, 1>(0, 0) + 0.5 * leftForceTorque->force;
            ftOffset.block<3, 1>(3, 0) =
                0.5 * ftOffset.block<3, 1>(3, 0) + 0.5 * leftForceTorque->torque;
            ftOffset.block<3, 1>(6, 0) =
                0.5 * ftOffset.block<3, 1>(6, 0) + 0.5 * rightForceTorque->force;
            ftOffset.block<3, 1>(9, 0) =
                0.5 * ftOffset.block<3, 1>(9, 0) + 0.5 * rightForceTorque->torque;

            for (int i = 0; i < KmAdmittance.size(); ++i)
            {
                KmAdmittance(i) = 0;
            }
        }
        else
        {
            for (int i = 0; i < KmAdmittance.size(); ++i)
            {
                KmAdmittance(i) = cfg->KmAdmittance.at(i);
            }
        }

        // -------------------------------------------- target wrench ---------------------------------------------
        Eigen::VectorXf deltaPoseForWrenchControl;
        deltaPoseForWrenchControl.setZero(12);
        for (size_t i = 0; i < 12; ++i)
        {
            if (KpImpedance(i) == 0)
            {
                deltaPoseForWrenchControl(i) = 0;
            }
            else
            {
                deltaPoseForWrenchControl(i) = targetWrench(i) / KpImpedance(i);
                if (deltaPoseForWrenchControl(i) > 0.1)
                {
                    deltaPoseForWrenchControl(i) = 0.1;
                }
                else if (deltaPoseForWrenchControl(i) < -0.1)
                {
                    deltaPoseForWrenchControl(i) = -0.1;
                }
                //            deltaPoseForWrenchControl(i + 6) = targetWrench(i + 6) / KpImpedance(i);
            }
        }


        // --------------------------------------------- grasp matrix ---------------------------------------------

        Eigen::Vector3f objCom2TCPLeftInGlobal =
            currentLeftPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3);
        Eigen::Vector3f objCom2TCPRightInGlobal =
            currentRightPose.block<3, 1>(0, 3) - virtualPose.block<3, 1>(0, 3);

        Eigen::MatrixXf graspMatrix;
        graspMatrix.setZero(6, 12);
        graspMatrix.block<3, 3>(0, 0) = Eigen::MatrixXf::Identity(3, 3);
        graspMatrix.block<3, 3>(0, 6) = Eigen::MatrixXf::Identity(3, 3);
        //        graspMatrix.block<6, 6>(0, 0) = Eigen::MatrixXf::Identity(6, 6);
        //        graspMatrix.block<6, 6>(0, 6) = Eigen::MatrixXf::Identity(6, 6);

        //        Eigen::Vector3f rvec = virtualPose.block<3, 3>(0, 0) * objCom2TCPLeftInObjFrame;
        graspMatrix.block<3, 3>(3, 0) = skew(objCom2TCPLeftInGlobal);

        //        rvec = virtualPose.block<3, 3>(0, 0) * objCom2TCPRightInObjFrame;
        graspMatrix.block<3, 3>(3, 6) = skew(objCom2TCPRightInGlobal);

        // // projection of grasp matrix
        // Eigen::MatrixXf pinvG = leftIK->computePseudoInverseJacobianMatrix(graspMatrix, 0);
        // Eigen::MatrixXf G_range = pinvG * graspMatrix;
        // Eigen::MatrixXf PG = Eigen::MatrixXf::Identity(12, 12) - G_range;
        float lambda = 1;
        Eigen::MatrixXf pinvGraspMatrixT =
            leftIK->computePseudoInverseJacobianMatrix(graspMatrix.transpose(), lambda);

        // ---------------------------------------------- object pose ----------------------------------------------
        Eigen::Matrix4f objCurrentPose = currentLeftPose * objTransMatrixInAnchor;
        Eigen::VectorXf objCurrentTwist;
        //        getObjStatus(objCurrentPose, objCurrentTwist);
        objCurrentTwist.setZero(6);

        // -------------------------------------- get Jacobian matrix and qpos -------------------------------------
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(leftTargets.size(), leftTargets.size());
        // Jacobian matrices
        Eigen::MatrixXf jacobiL =
            leftIK->getJacobianMatrix(tcpLeft, VirtualRobot::IKSolver::CartesianSelection::All);
        Eigen::MatrixXf jacobiR =
            rightIK->getJacobianMatrix(tcpRight, VirtualRobot::IKSolver::CartesianSelection::All);
        jacobiL.block<3, 8>(0, 0) = 0.001 * jacobiL.block<3, 8>(0, 0);
        jacobiR.block<3, 8>(0, 0) = 0.001 * jacobiR.block<3, 8>(0, 0);

        // qpos, qvel
        Eigen::VectorXf leftqpos;
        Eigen::VectorXf leftqvel;
        leftqpos.resize(leftPositionSensors.size());
        leftqvel.resize(leftVelocitySensors.size());
        for (size_t i = 0; i < leftVelocitySensors.size(); ++i)
        {
            leftqpos(i) = leftPositionSensors[i]->position;
            leftqvel(i) = leftVelocitySensors[i]->velocity;
        }

        Eigen::VectorXf rightqpos;
        Eigen::VectorXf rightqvel;
        rightqpos.resize(rightPositionSensors.size());
        rightqvel.resize(rightVelocitySensors.size());

        for (size_t i = 0; i < rightVelocitySensors.size(); ++i)
        {
            rightqpos(i) = rightPositionSensors[i]->position;
            rightqvel(i) = rightVelocitySensors[i]->velocity;
        }

        // -------------------------------------- compute TCP and object velocity -------------------------------------
        Eigen::VectorXf currentLeftTwist = jacobiL * leftqvel;
        Eigen::VectorXf currentRightTwist = jacobiR * rightqvel;

        Eigen::VectorXf currentTwist(12);
        currentTwist << currentLeftTwist, currentRightTwist;
        objCurrentTwist = pinvGraspMatrixT * currentTwist;

        rt2ControlBuffer.getWriteBuffer().currentPoseObj = objCurrentPose;
        rt2ControlBuffer.getWriteBuffer().currentTwistObj = objCurrentTwist;
        rt2ControlBuffer.getWriteBuffer().deltaT = deltaT;
        rt2ControlBuffer.getWriteBuffer().currentTime += deltaT;
        rt2ControlBuffer.commitWrite();

        // --------------------------------------------- get ft sensor ---------------------------------------------
        // static compensation
        Eigen::Vector3f gravity;
        gravity << 0.0, 0.0, -9.8;
        Eigen::Vector3f localGravityLeft = currentLeftPose.block<3, 3>(0, 0).transpose() * gravity;
        Eigen::Vector3f localForceVecLeft = massLeft * localGravityLeft;
        Eigen::Vector3f localTorqueVecLeft = CoMVecLeft.cross(localForceVecLeft);

        Eigen::Vector3f localGravityRight =
            currentRightPose.block<3, 3>(0, 0).transpose() * gravity;
        Eigen::Vector3f localForceVecRight = massRight * localGravityRight;
        Eigen::Vector3f localTorqueVecRight = CoMVecRight.cross(localForceVecRight);

        // mapping of measured wrenches
        Eigen::VectorXf wrenchesMeasured(12);
        wrenchesMeasured << leftForceTorque->force - forceOffsetLeft,
            leftForceTorque->torque - torqueOffsetLeft, rightForceTorque->force - forceOffsetRight,
            rightForceTorque->torque - torqueOffsetRight;
        for (size_t i = 0; i < 12; i++)
        {
            wrenchesMeasured(i) =
                (1 - filterCoeff) * wrenchesMeasured(i) + filterCoeff * filteredOldValue(i);
        }
        filteredOldValue = wrenchesMeasured;
        wrenchesMeasured.block<3, 1>(0, 0) =
            sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(0, 0) -
            localForceVecLeft;
        wrenchesMeasured.block<3, 1>(3, 0) =
            sensorFrame2TcpFrameLeft.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(3, 0) -
            localTorqueVecLeft;
        wrenchesMeasured.block<3, 1>(6, 0) =
            sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(6, 0) -
            localForceVecRight;
        wrenchesMeasured.block<3, 1>(9, 0) =
            sensorFrame2TcpFrameRight.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(9, 0) -
            localTorqueVecRight;
        //        if (wrenchesMeasured.norm() < cfg->forceThreshold)
        //        {
        //            wrenchesMeasured.setZero();
        //        }

        Eigen::VectorXf forceTorqueMeasurementInRoot(12);
        forceTorqueMeasurementInRoot.block<3, 1>(0, 0) =
            currentLeftPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(0, 0);
        forceTorqueMeasurementInRoot.block<3, 1>(3, 0) =
            currentLeftPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(3, 0);
        forceTorqueMeasurementInRoot.block<3, 1>(6, 0) =
            currentRightPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(6, 0);
        forceTorqueMeasurementInRoot.block<3, 1>(9, 0) =
            currentRightPose.block<3, 3>(0, 0) * wrenchesMeasured.block<3, 1>(9, 0);


        // map to object
        Eigen::VectorXf objFTValue = graspMatrix * forceTorqueMeasurementInRoot;
        for (size_t i = 0; i < 6; i++)
        {
            if (fabs(objFTValue(i)) < cfg->forceThreshold.at(i))
            {
                objFTValue(i) = 0;
            }
            else
            {
                objFTValue(i) -= cfg->forceThreshold.at(i) * objFTValue(i) / fabs(objFTValue(i));
            }
        }

        // pass sensor value to statechart
        rt2InterfaceBuffer.getWriteBuffer().currentObjPose = objCurrentPose;
        rt2InterfaceBuffer.getWriteBuffer().currentObjVel = objCurrentTwist.head(3);
        rt2InterfaceBuffer.getWriteBuffer().currentObjForce = objFTValue.head(3);
        rt2InterfaceBuffer.commitWrite();


        // --------------------------------------------- get MP target ---------------------------------------------
        Eigen::Matrix4f objTargetPose = rtGetControlStruct().objTargetPose;
        Eigen::VectorXf objTargetTwist = rtGetControlStruct().objTargetTwist;

        Eigen::Matrix4f leftPoseInObj = rtGetControlStruct().leftTargetPoseInObj;
        Eigen::VectorXf leftTwistInObj = rtGetControlStruct().leftTargetTwistInObj;

        Eigen::Matrix4f rightPoseInObj = rtGetControlStruct().rightTargetPoseInObj;
        Eigen::VectorXf rightTwistInObj = rtGetControlStruct().rightTargetTwistInObj;

        // integrate mp target
        integrateVel2Pose(deltaT, objTargetTwist, integratedPoseObj);
        integrateVel2Pose(deltaT, leftTwistInObj, integratedPoseLeft);
        integrateVel2Pose(deltaT, rightTwistInObj, integratedPoseRight);

        // --------------------------------------------- obj admittance control ---------------------------------------------
        // do admittance control on the object, calculate the virtual pose as attractor for the impedance controller
        Eigen::VectorXf objPoseError(6);
        objPoseError.head(3) = virtualPose.block<3, 1>(0, 3) - objTargetPose.block<3, 1>(0, 3);
        Eigen::Matrix3f objDiffMat =
            virtualPose.block<3, 3>(0, 0) * objTargetPose.block<3, 3>(0, 0).transpose();
        objPoseError.tail(3) = VirtualRobot::MathTools::eigen3f2rpy(objDiffMat);


        Eigen::VectorXf objAcc;
        Eigen::VectorXf objVel;
        objAcc.setZero(6);
        objVel.setZero(6);
        for (size_t i = 0; i < 6; i++)
        {
            // objAcc(i) = KmAdmittance(i) * objFTValue(i) - KpAdmittance(i) * objPoseError(i) - KdAdmittance(i) * (virtualVel(i) - boxTwist(i));
            objAcc(i) = KmAdmittance(i) * objFTValue(i) - KpAdmittance(i) * objPoseError(i) -
                        KdAdmittance(i) * virtualVel(i);
        }
        objVel = virtualVel + 0.5 * deltaT * (objAcc + virtualAcc);
        Eigen::VectorXf deltaObjPose = 0.5 * deltaT * (objVel + virtualVel);
        virtualAcc = objAcc;
        virtualVel = objVel;
        virtualPose.block<3, 1>(0, 3) += deltaObjPose.head(3);
        virtualPose.block<3, 3>(0, 0) = VirtualRobot::MathTools::rpy2eigen3f(
                                            deltaObjPose(3), deltaObjPose(4), deltaObjPose(5)) *
                                        virtualPose.block<3, 3>(0, 0);

        // --------------------------------------------- convert to tcp pose ---------------------------------------------
        // [R_v, P_v // 0, 1] * [R_l P_l// 0, 1] = [R_v*R_l R_v * P_l + P_v]
        Eigen::Matrix4f tcpTargetPoseLeft = virtualPose * leftPoseInObj;
        Eigen::Matrix4f tcpTargetPoseRight = virtualPose * rightPoseInObj;
        //        tcpTargetPoseLeft.block<3, 1>(0, 3) += virtualPose.block<3, 3>(0, 0) * (objCom2TCPLeftInObjFrame - deltaPoseForWrenchControl.block<3, 1>(0, 0));
        //        tcpTargetPoseRight.block<3, 1>(0, 3) += virtualPose.block<3, 3>(0, 0) * (objCom2TCPRightInObjFrame - deltaPoseForWrenchControl.block<3, 1>(6, 0));


        // --------------------------------------------- Impedance control ---------------------------------------------
        Eigen::VectorXf poseError(12);
        Eigen::Matrix3f diffMat =
            tcpTargetPoseLeft.block<3, 3>(0, 0) * currentLeftPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(0, 0) =
            tcpTargetPoseLeft.block<3, 1>(0, 3) - currentLeftPose.block<3, 1>(0, 3);
        poseError.block<3, 1>(3, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        diffMat =
            tcpTargetPoseRight.block<3, 3>(0, 0) * currentRightPose.block<3, 3>(0, 0).transpose();
        poseError.block<3, 1>(6, 0) =
            tcpTargetPoseRight.block<3, 1>(0, 3) - currentRightPose.block<3, 1>(0, 3);
        poseError.block<3, 1>(9, 0) = VirtualRobot::MathTools::eigen3f2rpy(diffMat);

        Eigen::VectorXf forceImpedance(12);
        for (size_t i = 0; i < 12; i++)
        {
            forceImpedance(i) = KpImpedance(i) * poseError(i) - KdImpedance(i) * currentTwist(i);
        }

        // --------------------------------------------- Nullspace control ---------------------------------------------
        Eigen::VectorXf leftNullspaceTorque =
            cfg->knull * (leftDesiredJointValues - leftqpos) - cfg->dnull * leftqvel;
        Eigen::VectorXf rightNullspaceTorque =
            cfg->knull * (rightDesiredJointValues - rightqpos) - cfg->dnull * rightqvel;

        // --------------------------------------------- Set Torque Control Command ---------------------------------------------
        //        float lambda = 1;
        Eigen::MatrixXf jtpinvL =
            leftIK->computePseudoInverseJacobianMatrix(jacobiL.transpose(), lambda);
        Eigen::MatrixXf jtpinvR =
            rightIK->computePseudoInverseJacobianMatrix(jacobiR.transpose(), lambda);
        Eigen::VectorXf leftJointDesiredTorques =
            jacobiL.transpose() * forceImpedance.head(6) +
            (I - jacobiL.transpose() * jtpinvL) * leftNullspaceTorque;
        Eigen::VectorXf rightJointDesiredTorques =
            jacobiR.transpose() * forceImpedance.tail(6) +
            (I - jacobiR.transpose() * jtpinvR) * rightNullspaceTorque;

        // torque limit
        for (size_t i = 0; i < leftTargets.size(); ++i)
        {
            float desiredTorque = leftJointDesiredTorques(i);
            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }
            desiredTorque = (desiredTorque > cfg->torqueLimit) ? cfg->torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -cfg->torqueLimit) ? -cfg->torqueLimit : desiredTorque;
            rt2DebugBuffer.getWriteBuffer().desired_torques[leftJointNames[i]] =
                leftJointDesiredTorques(i);
            leftTargets.at(i)->torque = desiredTorque;
        }

        for (size_t i = 0; i < rightTargets.size(); ++i)
        {
            float desiredTorque = rightJointDesiredTorques(i);
            if (isnan(desiredTorque))
            {
                desiredTorque = 0;
            }
            desiredTorque = (desiredTorque > cfg->torqueLimit) ? cfg->torqueLimit : desiredTorque;
            desiredTorque = (desiredTorque < -cfg->torqueLimit) ? -cfg->torqueLimit : desiredTorque;
            rt2DebugBuffer.getWriteBuffer().desired_torques[rightJointNames[i]] =
                rightJointDesiredTorques(i);
            rightTargets.at(i)->torque = desiredTorque;
        }

        // --------------------------------------------- debug output ---------------------------------------------
        // impedance control, target TS force
        rt2DebugBuffer.getWriteBuffer().forceImpedance = forceImpedance;
        rt2DebugBuffer.getWriteBuffer().poseError = poseError;
        rt2DebugBuffer.getWriteBuffer().forceTorqueMeasurementInRoot = forceTorqueMeasurementInRoot;

        // object current pose and current twist
        rt2DebugBuffer.getWriteBuffer().objPoseVec = eigenPose2EigenVec(objCurrentPose);
        rt2DebugBuffer.getWriteBuffer().objCurrentTwist = objCurrentTwist;

        // object force and torque
        rt2DebugBuffer.getWriteBuffer().objForceTorque = objFTValue;

        // virtual pose, vel and acc
        rt2DebugBuffer.getWriteBuffer().virtualPoseVec = eigenPose2EigenVec(virtualPose);
        rt2DebugBuffer.getWriteBuffer().virtualVel = virtualVel;
        rt2DebugBuffer.getWriteBuffer().virtualAcc = virtualAcc;

        // integrated pose
        rt2DebugBuffer.getWriteBuffer().integratedPoseObjVec =
            eigenPose2EigenVec(integratedPoseObj);
        rt2DebugBuffer.getWriteBuffer().integratedPoseLeftVec =
            eigenPose2EigenVec(integratedPoseLeft);
        rt2DebugBuffer.getWriteBuffer().integratedPoseRightVec =
            eigenPose2EigenVec(integratedPoseRight);


        // hand poses
        rt2DebugBuffer.getWriteBuffer().targetHandPoseInRootLeft =
            eigenPose2EigenVec(tcpTargetPoseLeft);
        rt2DebugBuffer.getWriteBuffer().targetHandPoseInRootRight =
            eigenPose2EigenVec(tcpTargetPoseRight);
        rt2DebugBuffer.getWriteBuffer().currentHandPoseInRootLeft =
            eigenPose2EigenVec(currentLeftPose);
        rt2DebugBuffer.getWriteBuffer().currentHandPoseInRootRight =
            eigenPose2EigenVec(currentRightPose);

        // dmp targets
        rt2DebugBuffer.getWriteBuffer().objTargetPoseVec = eigenPose2EigenVec(objTargetPose);
        rt2DebugBuffer.getWriteBuffer().leftPoseVecInObj = eigenPose2EigenVec(leftPoseInObj);
        rt2DebugBuffer.getWriteBuffer().rightPoseVecInObj = eigenPose2EigenVec(rightPoseInObj);
        rt2DebugBuffer.getWriteBuffer().objTargetTwist = objTargetTwist;

        // parameters
        rt2DebugBuffer.getWriteBuffer().KpImpedance = KpImpedance;
        rt2DebugBuffer.getWriteBuffer().KdImpedance = KdImpedance;
        rt2DebugBuffer.getWriteBuffer().KpAdmittance = KpAdmittance;
        rt2DebugBuffer.getWriteBuffer().KdAdmittance = KdAdmittance;
        rt2DebugBuffer.getWriteBuffer().KmAdmittance = KmAdmittance;
        rt2DebugBuffer.getWriteBuffer().KmPID = KmPID;

        rt2DebugBuffer.commitWrite();
    }

    void
    NJointBimanualObjLevelMultiMPController::getObjStatus(Eigen::Matrix4f& pose,
                                                          Eigen::VectorXf& twist)
    {
        // this is a temporary function to get status of the object,
        // in fact, this should be set via the interface function.
        Eigen::Matrix4f leftPose = tcpLeft->getPoseInRootFrame();
        leftPose.block<3, 1>(0, 3) *= 0.001;
        pose = leftPose * objTransMatrixInAnchor;
        twist.setZero(6);
    }

    // TODO
    //    void NJointBimanualObjLevelMultiMPController::setObjStatus()
    //    {
    //        // ice interface function, create a buffer for this
    //    }


    void
    NJointBimanualObjLevelMultiMPController::learnDMPFromFiles(const Ice::StringSeq& fileNames,
                                                               const Ice::Current&)
    {
        objectDMP->learnDMPFromFiles(fileNames);
    }

    void
    NJointBimanualObjLevelMultiMPController::learnMultiDMPFromFiles(
        const Ice::StringSeq& objFileNames,
        const Ice::StringSeq& leftFileNames,
        const Ice::StringSeq& rightFileNames,
        const Ice::Current&)
    {
        objectDMP->learnDMPFromFiles(objFileNames);
        leftTCPInObjDMP->learnDMPFromFiles(leftFileNames);
        rightTCPInObjDMP->learnDMPFromFiles(rightFileNames);
    }

    void
    NJointBimanualObjLevelMultiMPController::setGoals(const Ice::DoubleSeq& goals,
                                                      const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        objectDMP->setGoalPoseVec(goals);
    }

    void
    NJointBimanualObjLevelMultiMPController::setMultiMPGoals(const Ice::DoubleSeq& goalObj,
                                                             const Ice::DoubleSeq& goalLeft,
                                                             const Ice::DoubleSeq& goalRight,
                                                             const Ice::Current& ice)
    {
        LockGuardType guard(controllerMutex);
        objectDMP->setGoalPoseVec(goalObj);
        leftTCPInObjDMP->setGoalPoseVec(goalLeft);
        rightTCPInObjDMP->setGoalPoseVec(goalRight);
    }

    void
    NJointBimanualObjLevelMultiMPController::integrateVel2Pose(const double deltaT,
                                                               Eigen::VectorXf& vel,
                                                               Eigen::Matrix4f& pose)
    {
        Eigen::Matrix3f deltaRot =
            VirtualRobot::MathTools::rpy2eigen3f(vel.tail(3) * static_cast<float>(deltaT));
        pose.block<3, 3>(0, 0) = deltaRot * pose.block<3, 3>(0, 0);
        pose.block<3, 1>(0, 3) += vel.head(3) * static_cast<float>(deltaT);
    }

    std::vector<double>
    NJointBimanualObjLevelMultiMPController::eigenPose2Vec(const Eigen::Matrix4f& pose)
    {
        VirtualRobot::MathTools::Quaternion ori = VirtualRobot::MathTools::eigen4f2quat(pose);
        std::vector<double> poseVec{pose(0, 3), pose(1, 3), pose(2, 3), ori.w, ori.x, ori.y, ori.z};
        return poseVec;
    }

    Eigen::VectorXf
    NJointBimanualObjLevelMultiMPController::eigenPose2EigenVec(const Eigen::Matrix4f& pose)
    {
        VirtualRobot::MathTools::Quaternion ori = VirtualRobot::MathTools::eigen4f2quat(pose);
        Eigen::VectorXf poseVec;
        poseVec.setZero(7);
        poseVec(0) = pose(0, 3);
        poseVec(1) = pose(1, 3);
        poseVec(2) = pose(2, 3);
        poseVec(3) = ori.w;
        poseVec(4) = ori.x;
        poseVec(5) = ori.y;
        poseVec(6) = ori.z;
        return poseVec;
    }

    void
    NJointBimanualObjLevelMultiMPController::runDMP(const Ice::DoubleSeq& goalObj,
                                                    const Ice::DoubleSeq& goalLeft,
                                                    const Ice::DoubleSeq& goalRight,
                                                    Ice::Double timeDuration,
                                                    const Ice::Current&)
    {
        while (!rt2InterfaceBuffer.updateReadBuffer())
        {
            usleep(1000);
        }

        Eigen::Matrix4f leftInitPose =
            rt2InterfaceBuffer.getUpToDateReadBuffer().currentLeftPoseInObjFrame;
        Eigen::Matrix4f rightInitPose =
            rt2InterfaceBuffer.getUpToDateReadBuffer().currentRightPoseInObjFrame;
        Eigen::Matrix4f objInitPose = rt2InterfaceBuffer.getUpToDateReadBuffer().currentObjPose;
        std::vector<double> objInitPoseVec = eigenPose2Vec(objInitPose);
        std::vector<double> leftInitPoseVec = eigenPose2Vec(leftInitPose);
        std::vector<double> rightInitPoseVec = eigenPose2Vec(rightInitPose);
        ARMARX_INFO << "get init poses: \n"
                    << VAROUT(objInitPoseVec) << "\n"
                    << VAROUT(leftInitPoseVec) << "\n"
                    << VAROUT(rightInitPoseVec);

        // set the integrated left/right pose when start to run dmp
        integratedPoseObj = objInitPose;
        integratedPoseLeft = leftInitPose;
        integratedPoseRight = rightInitPose;

        objectDMP->prepareExecution(objInitPoseVec, goalObj);
        objectDMP->canVal = timeDuration;
        objectDMP->config.motionTimeDuration = timeDuration;

        leftTCPInObjDMP->prepareExecution(leftInitPose, getLocalPose(goalObj, goalLeft));
        leftTCPInObjDMP->canVal = timeDuration;
        leftTCPInObjDMP->config.motionTimeDuration = timeDuration;

        rightTCPInObjDMP->prepareExecution(rightInitPose, getLocalPose(goalObj, goalRight));
        rightTCPInObjDMP->canVal = timeDuration;
        rightTCPInObjDMP->config.motionTimeDuration = timeDuration;
        ARMARX_INFO << "DMP prepare execution is done";

        finished = false;
        dmpStarted = true;
    }


    Eigen::Matrix4f
    NJointBimanualObjLevelMultiMPController::getLocalPose(const Eigen::Matrix4f& newCoordinate,
                                                          const Eigen::Matrix4f& globalTargetPose)
    {
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();

        localPose.block<3, 3>(0, 0) =
            newCoordinate.block<3, 3>(0, 0).inverse() * globalTargetPose.block<3, 3>(0, 0);
        localPose.block<3, 1>(0, 3) =
            newCoordinate.block<3, 3>(0, 0).inverse() *
            (globalTargetPose.block<3, 1>(0, 3) - newCoordinate.block<3, 1>(0, 3));


        return localPose;
    }

    Eigen::Matrix4f
    NJointBimanualObjLevelMultiMPController::getLocalPose(
        const std::vector<double>& newCoordinateVec,
        const std::vector<double>& globalTargetPoseVec)
    {
        Eigen::Matrix4f newCoordinate =
            VirtualRobot::MathTools::quat2eigen4f(newCoordinateVec.at(4),
                                                  newCoordinateVec.at(5),
                                                  newCoordinateVec.at(6),
                                                  newCoordinateVec.at(3));
        newCoordinate(0, 3) = newCoordinateVec.at(0);
        newCoordinate(1, 3) = newCoordinateVec.at(1);
        newCoordinate(2, 3) = newCoordinateVec.at(2);

        Eigen::Matrix4f globalTargetPose =
            VirtualRobot::MathTools::quat2eigen4f(globalTargetPoseVec.at(4),
                                                  globalTargetPoseVec.at(5),
                                                  globalTargetPoseVec.at(6),
                                                  globalTargetPoseVec.at(3));
        globalTargetPose(0, 3) = globalTargetPoseVec.at(0);
        globalTargetPose(1, 3) = globalTargetPoseVec.at(1);
        globalTargetPose(2, 3) = globalTargetPoseVec.at(2);

        return getLocalPose(newCoordinate, globalTargetPose);
    }

    void
    NJointBimanualObjLevelMultiMPController::runDMPWithVirtualStart(const Ice::DoubleSeq& starts,
                                                                    const Ice::DoubleSeq& goals,
                                                                    Ice::Double timeDuration,
                                                                    const Ice::Current&)
    {
        ARMARX_ERROR << "implementation not complete!!!";
        while (!rt2InterfaceBuffer.updateReadBuffer())
        {
            usleep(1000);
        }
        ARMARX_IMPORTANT << "obj level control: setup dmp ...";
        objectDMP->prepareExecution(starts, goals);
        objectDMP->canVal = timeDuration;
        objectDMP->config.motionTimeDuration = timeDuration;

        finished = false;
        dmpStarted = true;

        ARMARX_IMPORTANT << "obj level control: run dmp with virtual start.";
    }

    void
    NJointBimanualObjLevelMultiMPController::setViaPoints(Ice::Double u,
                                                          const Ice::DoubleSeq& viapoint,
                                                          const Ice::Current&)
    {
        //        LockGuardType guard(controllerMutex);
        ARMARX_INFO << "setting via points ";
        objectDMP->setViaPose(u, viapoint);
    }

    void
    NJointBimanualObjLevelMultiMPController::removeAllViaPoints(const Ice::Current&)
    {
        objectDMP->removeAllViaPoints();
    }

    void
    NJointBimanualObjLevelMultiMPController::setKpImpedance(const Ice::FloatSeq& value,
                                                            const Ice::Current&)
    {

        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 12);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard{interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KpImpedance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    void
    NJointBimanualObjLevelMultiMPController::setAmplitude(Ice::Double amp, const Ice::Current&)
    {
        LockGuardType guard{controllerMutex};
        if (objectDMP->config.DMPStyle == "Periodic")
        {
            objectDMP->setAmplitude(amp);
        }

        if (leftTCPInObjDMP->config.DMPStyle == "Periodic")
        {
            leftTCPInObjDMP->setAmplitude(amp);
        }

        if (rightTCPInObjDMP->config.DMPStyle == "Periodic")
        {
            rightTCPInObjDMP->setAmplitude(amp);
        }
    }

    void
    NJointBimanualObjLevelMultiMPController::setKdImpedance(const Ice::FloatSeq& value,
                                                            const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 12);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard{interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KdImpedance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    void
    NJointBimanualObjLevelMultiMPController::setKpAdmittance(const Ice::FloatSeq& value,
                                                             const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 6);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard{interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KpAdmittance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    void
    NJointBimanualObjLevelMultiMPController::setKdAdmittance(const Ice::FloatSeq& value,
                                                             const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 6);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard{interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KdAdmittance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    void
    NJointBimanualObjLevelMultiMPController::setKmAdmittance(const Ice::FloatSeq& value,
                                                             const Ice::Current&)
    {
        Eigen::VectorXf setpoint;
        setpoint.setZero(value.size());
        ARMARX_CHECK_EQUAL(value.size(), 6);

        for (size_t i = 0; i < value.size(); ++i)
        {
            setpoint(i) = value.at(i);
        }

        LockGuardType guard{interfaceDataMutex};
        interface2rtBuffer.getWriteBuffer().KmAdmittance = setpoint;
        interface2rtBuffer.commitWrite();
    }

    std::vector<float>
    NJointBimanualObjLevelMultiMPController::getCurrentObjVel(const Ice::Current&)
    {
        Eigen::Vector3f tvel = rt2InterfaceBuffer.getUpToDateReadBuffer().currentObjVel;
        std::vector<float> tvelvec = {tvel(0), tvel(1), tvel(2)};
        return tvelvec;
    }

    std::vector<float>
    NJointBimanualObjLevelMultiMPController::getCurrentObjForce(const Ice::Current&)
    {
        Eigen::Vector3f fvel = rt2InterfaceBuffer.getUpToDateReadBuffer().currentObjForce;
        std::vector<float> fvelvec = {fvel(0), fvel(1), fvel(2)};
        return fvelvec;
    }

    void
    NJointBimanualObjLevelMultiMPController::publishVec(const Eigen::VectorXf& bufferVec,
                                                        const std::string name,
                                                        StringVariantBaseMap& datafields)
    {
        for (int i = 0; i < bufferVec.rows(); ++i)
        {
            std::string data_name = name + "_" + std::to_string(i);
            datafields[data_name] = new Variant(bufferVec(i));
        }
    }

    void
    NJointBimanualObjLevelMultiMPController::onPublish(const SensorAndControl&,
                                                       const DebugDrawerInterfacePrx&,
                                                       const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = rt2DebugBuffer.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        publishVec(
            rt2DebugBuffer.getUpToDateReadBuffer().forceImpedance, "forceImpedance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().forcePID, "forcePID", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().poseError, "poseError", datafields);

        // ------------------ force torque measurement of hands ------------------
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().forceTorqueMeasurementInRoot,
                   "forceTorqueMeasurementInRoot",
                   datafields);

        // ------------------ object force torques ------------------
        publishVec(
            rt2DebugBuffer.getUpToDateReadBuffer().objForceTorque, "objForceTorque", datafields);

        // ------------------ virtual pose, vel and acc ------------------
        publishVec(
            rt2DebugBuffer.getUpToDateReadBuffer().virtualPoseVec, "virtualPoseVec", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().virtualVel, "virtualVel", datafields);

        // ------------------ object pose vec, vel ------------------
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().objPoseVec, "objPoseVec", datafields);
        publishVec(
            rt2DebugBuffer.getUpToDateReadBuffer().objCurrentTwist, "objCurrentTwist", datafields);

        // ------------------ hand poses ------------------
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().targetHandPoseInRootLeft,
                   "targetHandPoseInRootLeft",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().targetHandPoseInRootRight,
                   "targetHandPoseInRootRight",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().currentHandPoseInRootLeft,
                   "currentHandPoseInRootLeft",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().currentHandPoseInRootRight,
                   "currentHandPoseInRootRight",
                   datafields);

        // ------------------ dmp targets ------------------
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().objTargetPoseVec,
                   "objTargetPoseVec",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().leftPoseVecInObj,
                   "leftPoseVecInObj",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().rightPoseVecInObj,
                   "rightPoseVecInObj",
                   datafields);
        publishVec(
            rt2DebugBuffer.getUpToDateReadBuffer().objTargetTwist, "objTargetTwist", datafields);

        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().integratedPoseObjVec,
                   "integratedPoseObjVec",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().integratedPoseLeftVec,
                   "integratedPoseLeftVec",
                   datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().integratedPoseRightVec,
                   "integratedPoseRightVec",
                   datafields);

        // ------------------ controller parameters ------------------
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KpImpedance, "KpImpedance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KdImpedance, "KdImpedance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KpAdmittance, "KpAdmittance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KdAdmittance, "KdAdmittance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KmAdmittance, "KmAdmittance", datafields);
        publishVec(rt2DebugBuffer.getUpToDateReadBuffer().KmPID, "KmPID", datafields);

        //        Eigen::VectorXf forceImpedance = rt2DebugBuffer.getUpToDateReadBuffer().forceImpedance;
        //        for (int i = 0; i < forceImpedance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "forceImpedance_" + ss.str();
        //            datafields[data_name] = new Variant(forceImpedance(i));
        //        }

        //        Eigen::VectorXf forcePID = rt2DebugBuffer.getUpToDateReadBuffer().forcePID;
        //        for (int i = 0; i < forcePID.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "forcePID_" + ss.str();
        //            datafields[data_name] = new Variant(forcePID(i));
        //        }


        //        Eigen::VectorXf poseError = rt2DebugBuffer.getUpToDateReadBuffer().poseError;
        //        for (int i = 0; i < poseError.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "poseError_" + ss.str();
        //            datafields[data_name] = new Variant(poseError(i));
        //        }

        //        // ------------------ force torque measurement of hands ------------------
        //        Eigen::VectorXf forceTorqueMeasurementInRoot = rt2DebugBuffer.getUpToDateReadBuffer().forceTorqueMeasurementInRoot;
        //        for (int i = 0; i < forceTorqueMeasurementInRoot.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "forceTorqueMeasurementInRoot_" + ss.str();
        //            datafields[data_name] = new Variant(forceTorqueMeasurementInRoot(i));
        //        }

        // ------------------ object force torques ------------------
        //        Eigen::VectorXf objForceTorque = rt2DebugBuffer.getUpToDateReadBuffer().objForceTorque;
        //        for (int i = 0; i < objForceTorque.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "objForceTorque_" + ss.str();
        //            datafields[data_name] = new Variant(objForceTorque(i));
        //        }

        // ------------------ virtual pose, vel and acc ------------------
        //        Eigen::VectorXf virtualPoseVec = rt2DebugBuffer.getUpToDateReadBuffer().virtualPoseVec;
        //        for (int i = 0; i < virtualPoseVec.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "virtualPoseVec_" + ss.str();
        //            datafields[data_name] = new Variant(virtualPoseVec(i));
        //        }

        //        Eigen::VectorXf virtualVel = rt2DebugBuffer.getUpToDateReadBuffer().virtualVel;
        //        for (int i = 0; i < virtualVel.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "virtualVel_" + ss.str();
        //            datafields[data_name] = new Variant(virtualVel(i));
        //        }

        // ------------------ object pose vec, vel ------------------
        //        Eigen::VectorXf objPoseVec = rt2DebugBuffer.getUpToDateReadBuffer().objPoseVec;
        //        for (int i = 0; i < objPoseVec.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "objPoseVec_" + ss.str();
        //            datafields[data_name] = new Variant(objPoseVec(i));
        //        }

        //        Eigen::VectorXf objCurrentTwist = rt2DebugBuffer.getUpToDateReadBuffer().objCurrentTwist;
        //        for (int i = 0; i < objCurrentTwist.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "objCurrentTwist_" + ss.str();
        //            datafields[data_name] = new Variant(objCurrentTwist(i));
        //        }

        // ------------------ hand poses ------------------
        //        Eigen::VectorXf targetHandPoseInRootLeft = rt2DebugBuffer.getUpToDateReadBuffer().targetHandPoseInRootLeft;
        //        for (int i = 0; i < targetHandPoseInRootLeft.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "targetHandPoseInRootLeft_" + ss.str();
        //            datafields[data_name] = new Variant(targetHandPoseInRootLeft(i));
        //        }
        //        Eigen::VectorXf targetHandPoseInRootRight = rt2DebugBuffer.getUpToDateReadBuffer().targetHandPoseInRootRight;
        //        for (int i = 0; i < targetHandPoseInRootRight.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "targetHandPoseInRootRight_" + ss.str();
        //            datafields[data_name] = new Variant(targetHandPoseInRootRight(i));
        //        }
        //        Eigen::VectorXf currentHandPoseInRootLeft = rt2DebugBuffer.getUpToDateReadBuffer().currentHandPoseInRootLeft;
        //        for (int i = 0; i < currentHandPoseInRootLeft.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "currentHandPoseInRootLeft_" + ss.str();
        //            datafields[data_name] = new Variant(currentHandPoseInRootLeft(i));
        //        }
        //        Eigen::VectorXf currentHandPoseInRootRight = rt2DebugBuffer.getUpToDateReadBuffer().currentHandPoseInRootRight;
        //        for (int i = 0; i < currentHandPoseInRootRight.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "currentHandPoseInRootRight_" + ss.str();
        //            datafields[data_name] = new Variant(currentHandPoseInRootRight(i));
        //        }
        // ------------------ dmp targets ------------------
        //        Eigen::VectorXf objTargetPoseVec = rt2DebugBuffer.getUpToDateReadBuffer().objTargetPoseVec;
        //        for (int i = 0; i < objTargetPoseVec.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "objTargetPoseVec_" + ss.str();
        //            datafields[data_name] = new Variant(objTargetPoseVec(i));
        //        }
        //        Eigen::VectorXf leftPoseVecInObj = rt2DebugBuffer.getUpToDateReadBuffer().leftPoseVecInObj;
        //        for (int i = 0; i < leftPoseVecInObj.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "leftPoseVecInObj_" + ss.str();
        //            datafields[data_name] = new Variant(leftPoseVecInObj(i));
        //        }
        //        Eigen::VectorXf rightPoseVecInObj = rt2DebugBuffer.getUpToDateReadBuffer().rightPoseVecInObj;
        //        for (int i = 0; i < rightPoseVecInObj.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "rightPoseVecInObj_" + ss.str();
        //            datafields[data_name] = new Variant(rightPoseVecInObj(i));
        //        }
        //        Eigen::VectorXf objTargetTwist = rt2DebugBuffer.getUpToDateReadBuffer().objTargetTwist;
        //        for (int i = 0; i < objTargetTwist.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "objTargetTwist_" + ss.str();
        //            datafields[data_name] = new Variant(objTargetTwist(i));
        //        }

        // parameters
        //        Eigen::VectorXf KpImpedance = rt2DebugBuffer.getUpToDateReadBuffer().KpImpedance;
        //        for (int i = 0; i < KpImpedance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KpImpedance_" + ss.str();
        //            datafields[data_name] = new Variant(KpImpedance(i));
        //        }
        //        Eigen::VectorXf KdImpedance = rt2DebugBuffer.getUpToDateReadBuffer().KdImpedance;
        //        for (int i = 0; i < KdImpedance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KdImpedance_" + ss.str();
        //            datafields[data_name] = new Variant(KdImpedance(i));
        //        }
        //        Eigen::VectorXf KpAdmittance = rt2DebugBuffer.getUpToDateReadBuffer().KpAdmittance;
        //        for (int i = 0; i < KpAdmittance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KpAdmittance_" + ss.str();
        //            datafields[data_name] = new Variant(KpAdmittance(i));
        //        }
        //        Eigen::VectorXf KdAdmittance = rt2DebugBuffer.getUpToDateReadBuffer().KdAdmittance;
        //        for (int i = 0; i < KdAdmittance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KdAdmittance_" + ss.str();
        //            datafields[data_name] = new Variant(KdAdmittance(i));
        //        }
        //        Eigen::VectorXf KmAdmittance = rt2DebugBuffer.getUpToDateReadBuffer().KmAdmittance;
        //        for (int i = 0; i < KmAdmittance.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KmAdmittance_" + ss.str();
        //            datafields[data_name] = new Variant(KmAdmittance(i));
        //        }
        //        Eigen::VectorXf KmPID = rt2DebugBuffer.getUpToDateReadBuffer().KmPID;
        //        for (int i = 0; i < KmPID.rows(); ++i)
        //        {
        //            std::stringstream ss;
        //            ss << i;
        //            std::string data_name = "KmPID_" + ss.str();
        //            datafields[data_name] = new Variant(KmPID(i));
        //        }

        debugObs->setDebugChannel("BimanualForceController", datafields);
    }

    void
    NJointBimanualObjLevelMultiMPController::onInitNJointController()
    {
        ARMARX_INFO << "====== init bimanual multi mp controller ======";
        runTask("NJointBimanualObjLevelMultiMPController",
                [&]
                {
                    CycleUtil c(1);
                    getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
                    while (getState() == eManagedIceObjectStarted)
                    {
                        if (isControllerActive())
                        {
                            controllerRun();
                        }
                        c.waitForCycleDuration();
                    }
                });
    }

    void
    NJointBimanualObjLevelMultiMPController::onDisconnectNJointController()
    {
        ARMARX_INFO << "====== stop bimanual multi mp controller ======";
    }
} // namespace armarx::ctrl::njoint_ctrl::bimanual_force
