/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
    class NJointBimanualForceControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = 10;

        Ice::DoubleSeq boxInitialPose;
        float boxWidth;
        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

        // impedance, admittance, object motion parameters
        Ice::FloatSeq KpImpedance;
        Ice::FloatSeq KdImpedance;
        Ice::FloatSeq KpAdmittance;
        Ice::FloatSeq KdAdmittance;
        Ice::FloatSeq KmAdmittance;
        Ice::FloatSeq KmPID;
        //        Ice::FloatSeq objectKp;
        //        Ice::FloatSeq objectKd;

        // pid force controller parameters
        Ice::FloatSeq targetWrench;
        Ice::FloatSeq forceP;
        Ice::FloatSeq forceI;
        Ice::FloatSeq forceD;
        Ice::FloatSeq forcePIDLimits;

        float filterCoeff;


        float massLeft;
        Ice::FloatSeq CoMVecLeft;
        Ice::FloatSeq forceOffsetLeft;
        Ice::FloatSeq torqueOffsetLeft;

        float massRight;
        Ice::FloatSeq CoMVecRight;
        Ice::FloatSeq forceOffsetRight;
        Ice::FloatSeq torqueOffsetRight;

        //        // flags for testing variants
        //        int dmpFeedType;    // 0: no dmp, 1: frame, 2: vel, 3: acc, 4: force
        //        int method;         // 1,2,3,4 four diffrent method w.r.t inertia trick; default (0): normal method
        //        int jcMethod;       // 0: lin's paper, default: Jc = J
        //        int pdotMethod;     // 0: numerical differential, 1: paper, Tau - Tau.Transpose, 2 test, default: zero matrix
        //        int graspingType;   // 0: fixed to obj, 1, fixed to hand, 2, dynamically from hand pose, defualt: identity

        float knull;
        float dnull;

        float torqueLimit;

        float forceThreshold;

    };

    interface NJointBimanualForceControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals);

        //        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);
        void setViaPoints(double u, Ice::DoubleSeq viapoint);
        double getVirtualTime();


    };

    class NJointBimanualForceMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpStyle = "Discrete";
        double dmpAmplitude = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double timeDuration = 10;
        double posToOriRatio = 100;

        double maxLinearVel;
        double maxAngularVel;

        // velocity control
        float Kp_LinearVel;
        float Kd_LinearVel;
        float Kp_AngularVel;
        float Kd_AngularVel;

        // force control
        float forceP;
        float forceI;
        float forceD;

        float filterCoeff;
        float KpJointLimitAvoidanceScale;

        float targetSupportForce;
        Ice::FloatSeq leftForceOffset;
        Ice::FloatSeq rightForceOffset;

        string debugName;

    };


    interface NJointBimanualForceMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(string whichMP, Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq leftGoals, Ice::DoubleSeq rightGoals);
        double getCanVal();

        void setViaPoints(string whichDMP, double canVal, Ice::DoubleSeq viaPoint);
    };
};

