/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

module armarx
{
    module detail
    {
        module NJBmanCartAdmCtrl
        {
            struct Box
            {
                //                Eigen::Matrix4f initialpose;
                //                Eigen::Vector3f velXYZ;
                //                Eigen::Vector3f velRPY;
                float width;
            };

            struct Nullspace
            {
                Ice::FloatSeq desiredJointValuesLeft;
                Ice::FloatSeq desiredJointValuesRight;
                float k;
                float d;
            };
            struct Impedance
            {
                Eigen::Vector3f KpXYZ;
                Eigen::Vector3f KpRPY;
                Eigen::Vector3f KdXYZ;
                Eigen::Vector3f KdRPY;
            };
            struct Admittance
            {
                Eigen::Vector3f KpXYZ;
                Eigen::Vector3f KpRPY;
                Eigen::Vector3f KdXYZ;
                Eigen::Vector3f KdRPY;
                Eigen::Vector3f KmXYZ;
                Eigen::Vector3f KmRPY;
            };
            struct Force
            {
                Eigen::Vector3f wrenchXYZ;
                Eigen::Vector3f wrenchRPY;

                // static compensation
                float mass;
                Eigen::Vector3f com;
                Eigen::Vector3f offsetForce;
                Eigen::Vector3f offsetTorque;

                Eigen::Vector3f forceThreshold;
            };
        };
    };

    class NJointBimanualCartesianAdmittanceControllerConfig extends NJointControllerConfig
    {
        detail::NJBmanCartAdmCtrl::Box box;
        detail::NJBmanCartAdmCtrl::Nullspace nullspace;
        detail::NJBmanCartAdmCtrl::Impedance impedanceLeft;
        detail::NJBmanCartAdmCtrl::Impedance impedanceRight;
        detail::NJBmanCartAdmCtrl::Admittance admittanceObject;
        detail::NJBmanCartAdmCtrl::Force forceLeft;
        detail::NJBmanCartAdmCtrl::Force forceRight;

        float filterCoeff;
        float torqueLimit;
        double ftCalibrationTime;

        string kinematicChainRight;
        string kinematicChainLeft;
        string ftSensorRight;
        string ftSensorLeft;
        string ftSensorRightFrame;
        string ftSensorLeftFrame;
    };

    interface NJointBimanualCartesianAdmittanceControllerInterface extends NJointControllerInterface
    {
        //        bool isAtGoal();
        //        void runDMP(Ice::DoubleSeq goals, double timeDuration);
        //        void runDMPWithVirtualStart(Ice::DoubleSeq starts, Ice::DoubleSeq goals, double timeDuration);

        //        //        void setViaPoints(double canVal, Ice::DoubleSeq point);
        //        void setGoals(Ice::DoubleSeq goals);
        //        void setViaPoints(double u, Ice::DoubleSeq viapoint);
        void setConfig(NJointBimanualCartesianAdmittanceControllerConfig cfg);
        void setDesiredJointValuesLeft(Ice::FloatSeq cfg);
        void setDesiredJointValuesRight(Ice::FloatSeq cfg);
        void setNullspaceConfig(detail::NJBmanCartAdmCtrl::Nullspace nullspace);
        void setAdmittanceConfig(detail::NJBmanCartAdmCtrl::Admittance admittanceObject);
        void setForceConfig(detail::NJBmanCartAdmCtrl::Force left, detail::NJBmanCartAdmCtrl::Force right);
        void setImpedanceConfig(detail::NJBmanCartAdmCtrl::Impedance left, detail::NJBmanCartAdmCtrl::Impedance right);

        ["cpp:const"] Eigen::Matrix4f getBoxPose();
        void setBoxPose(Eigen::Matrix4f pose);
        void setBoxWidth(float w);
        void setBoxVelocity(Eigen::Vector3f velXYZ, Eigen::Vector3f velRPY);
        void setBoxPoseAndVelocity(Eigen::Matrix4f pose,
                                   Eigen::Vector3f velXYZ,
                                   Eigen::Vector3f velRPY);
        void moveBoxPose(Eigen::Matrix4f pose);
        void moveBoxPosition(Eigen::Vector3f pos);

        /*
        Eigen::Matrix4f initialpose;
        //                Eigen::Vector3f velXYZ;
        //                Eigen::Vector3f velRPY;
        */
    };



};

