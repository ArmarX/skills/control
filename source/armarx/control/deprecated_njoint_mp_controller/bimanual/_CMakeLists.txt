
armarx_build_if(DMP_FOUND "DMP not available")

armarx_add_library(
    LIB_NAME
        NJointController_BimanualForce
    SOURCES
        NJointBimanualCartesianAdmittanceController.cpp
        NJointBimanualForceController.cpp
        NJointBimanualObjLevelController.cpp
        NJointBimanualObjLevelMultiMPController.cpp
        NJointBimanualObjLevelVelController.cpp
    HEADERS
        NJointBimanualCartesianAdmittanceController.h
        NJointBimanualForceController.h
        NJointBimanualObjLevelController.h
        NJointBimanualObjLevelMultiMPController.h
        NJointBimanualObjLevelVelController.h
    LIBS
        ArmarXCoreInterfaces
        ArmarXCore
        RobotAPIInterfaces
        RobotAPICore
        RobotAPIUnits
        ArmarXCoreObservers
        ArmarXCoreStatechart
        ArmarXCoreEigen3Variants
        VirtualRobot
        Saba
        SimDynamics
        RobotUnit
        BimanualForceControlInterfaces
        ${DMP_LIBRARIES}
        DMPController
        Eigen3::Eigen
)

if (DMP_FOUND)
    target_include_directories(armarx_control::NJointController_BimanualForce PUBLIC ${DMP_INCLUDE_DIRS})
endif()
