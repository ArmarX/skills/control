/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>


module armarx
{
    class NJointBimanualObjLevelControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = 10;

        Ice::DoubleSeq boxInitialPose;
        float boxWidth;
        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

        // impedance, admittance, object motion parameters
        Ice::FloatSeq KpImpedance;
        Ice::FloatSeq KdImpedance;
        Ice::FloatSeq KpAdmittance;
        Ice::FloatSeq KdAdmittance;
        Ice::FloatSeq KmAdmittance;
        Ice::FloatSeq KmPID;
        //        Ice::FloatSeq objectKp;
        //        Ice::FloatSeq objectKd;

        // pid force controller parameters
        Ice::FloatSeq targetWrench;
        Ice::FloatSeq forceP;
        Ice::FloatSeq forceI;
        Ice::FloatSeq forceD;
        Ice::FloatSeq forcePIDLimits;

        float filterCoeff;


        float massLeft;
        Ice::FloatSeq CoMVecLeft;
        Ice::FloatSeq forceOffsetLeft;
        Ice::FloatSeq torqueOffsetLeft;

        float massRight;
        Ice::FloatSeq CoMVecRight;
        Ice::FloatSeq forceOffsetRight;
        Ice::FloatSeq torqueOffsetRight;

        //        // flags for testing variants
        //        int dmpFeedType;    // 0: no dmp, 1: frame, 2: vel, 3: acc, 4: force
        //        int method;         // 1,2,3,4 four diffrent method w.r.t inertia trick; default (0): normal method
        //        int jcMethod;       // 0: lin's paper, default: Jc = J
        //        int pdotMethod;     // 0: numerical differential, 1: paper, Tau - Tau.Transpose, 2 test, default: zero matrix
        //        int graspingType;   // 0: fixed to obj, 1, fixed to hand, 2, dynamically from hand pose, defualt: identity

        float knull;
        float dnull;

        float torqueLimit;

        Ice::FloatSeq forceThreshold;

        double ftCalibrationTime;

    };

    interface NJointBimanualObjLevelControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double timeDuration);
        void runDMPWithVirtualStart(Ice::DoubleSeq starts, Ice::DoubleSeq goals, double timeDuration);

        //        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);
        void setViaPoints(double u, Ice::DoubleSeq viapoint);
        void removeAllViaPoints();
        double getVirtualTime();

        void setKpImpedance(Ice::FloatSeq value);
        void setKdImpedance(Ice::FloatSeq value);
        void setKmAdmittance(Ice::FloatSeq value);
        void setKpAdmittance(Ice::FloatSeq value);
        void setKdAdmittance(Ice::FloatSeq value);

        Ice::FloatSeq getCurrentObjVel();
        Ice::FloatSeq getCurrentObjForce();

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();
        void setMPRotWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPRotWeights();
    };

    class NJointBimanualObjLevelVelControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 10;
        string dmpMode = "Linear";
        string dmpType = "Discrete";
        double timeDuration = 10;

        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

        Ice::FloatSeq KpImpedance;
        Ice::FloatSeq KdImpedance;

        float knull;
        float dnull;

        float jointVelLimit;
        float jointLimitAvoidanceKp;

    };

    interface NJointBimanualObjLevelVelControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double timeDuration);
        void runDMPWithVirtualStart(Ice::DoubleSeq starts, Ice::DoubleSeq goals, double timeDuration);

        void setGoals(Ice::DoubleSeq goals);
        void setViaPoints(double u, Ice::DoubleSeq viapoint);
        void removeAllViaPoints();
        double getVirtualTime();

        void setKpImpedance(Ice::FloatSeq value);
        void setKdImpedance(Ice::FloatSeq value);

        Ice::FloatSeq getCurrentObjVel();

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();
        void setMPRotWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPRotWeights();
    };

    class NJointBimanualObjLevelMultiMPControllerConfig extends NJointControllerConfig
    {
        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpObjType = "Discrete";
        string dmpLeftType = "Discrete";
        string dmpRightType = "Discrete";

        float dmpAmplitude = 1.0;

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 10;
        double timeDuration = 10;

        Ice::DoubleSeq objInitialPose;
        Ice::FloatSeq leftDesiredJointValues;
        Ice::FloatSeq rightDesiredJointValues;

        // impedance, admittance, object motion parameters
        Ice::FloatSeq KpImpedance;
        Ice::FloatSeq KdImpedance;
        Ice::FloatSeq KpAdmittance;
        Ice::FloatSeq KdAdmittance;
        Ice::FloatSeq KmAdmittance;
        Ice::FloatSeq KmPID;

        // pid force controller parameters
        Ice::FloatSeq targetWrench;
        Ice::FloatSeq forceP;
        Ice::FloatSeq forceI;
        Ice::FloatSeq forceD;
        Ice::FloatSeq forcePIDLimits;

        float filterCoeff;


        float massLeft;
        Ice::FloatSeq CoMVecLeft;
        Ice::FloatSeq forceOffsetLeft;
        Ice::FloatSeq torqueOffsetLeft;

        float massRight;
        Ice::FloatSeq CoMVecRight;
        Ice::FloatSeq forceOffsetRight;
        Ice::FloatSeq torqueOffsetRight;

        float knull;
        float dnull;

        float torqueLimit;

        Ice::FloatSeq forceThreshold;

        double ftCalibrationTime;

    };

    interface NJointBimanualObjLevelMultiMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnMultiDMPFromFiles(Ice::StringSeq objFileNames, Ice::StringSeq leftFileNames, Ice::StringSeq rightFileNames);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goalObj, Ice::DoubleSeq goalLeft, Ice::DoubleSeq goalRight, double timeDuration);
        void runDMPWithVirtualStart(Ice::DoubleSeq starts, Ice::DoubleSeq goals, double timeDuration);

        //        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);
        void setMultiMPGoals(Ice::DoubleSeq goalObj, Ice::DoubleSeq goalLeft, Ice::DoubleSeq goalRight);

        void setViaPoints(double u, Ice::DoubleSeq viapoint);
        void removeAllViaPoints();

        double getVirtualTime();

        void setKpImpedance(Ice::FloatSeq value);
        void setKdImpedance(Ice::FloatSeq value);
        void setKmAdmittance(Ice::FloatSeq value);
        void setKpAdmittance(Ice::FloatSeq value);
        void setKdAdmittance(Ice::FloatSeq value);
        void setAmplitude(double amp);

        Ice::FloatSeq getCurrentObjVel();
        Ice::FloatSeq getCurrentObjForce();
    };

};

