/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

#include <ArmarXCore/interface/serialization/Eigen.ice>

module armarx
{
    module NJointTaskSpaceDMPControllerMode
    {
        enum CartesianSelection
        {
            ePosition = 7,
            eOrientation = 8,
            eAll = 15
        };
    };


    class NJointTaskSpaceDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        string dmpMode = "MinimumJerk";
        string dmpStyle = "Discrete";
        double dmpAmplitude = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double timeDuration = 10;
        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        string frameName = "";
        NJointTaskSpaceDMPControllerMode::CartesianSelection mode = NJointTaskSpaceDMPControllerMode::CartesianSelection::eAll;

        double maxLinearVel;
        double maxAngularVel;
        double maxJointVelocity;
        string debugName;

        float Kp_LinearVel;
        float Kd_LinearVel;
        float Kp_AngularVel;
        float Kd_AngularVel;

        float pos_filter;
        float vel_filter;
    };


    interface NJointTaskSpaceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);
        void runDMPWithTime(Ice::DoubleSeq goals, double time);

        void setSpeed(double times);
        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void removeAllViaPoints();
        void setGoals(Ice::DoubleSeq goals);

        double getCanVal();
        void resetDMP();
        void stopDMP();
        void pauseDMP();
        void resumeDMP();

        void setControllerTarget(float avoidJointLimitsKp, NJointTaskSpaceDMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
        string getDMPAsString();
        Ice::DoubleSeq createDMPFromString(string dmpString);

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();

        void setLinearVelocityKd(float kd);
        void setLinearVelocityKp(float kp);
        void setAngularVelocityKd(float kd);
        void setAngularVelocityKp(float kp);

    };

    class NJointCCDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        int dmpMode = 1;
        int dmpNum = 2;

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;


        // misc
        Ice::DoubleSeq timeDurations;
        Ice::StringSeq dmpTypes;
        Ice::DoubleSeq amplitudes;


        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        NJointTaskSpaceDMPControllerMode::CartesianSelection mode = NJointTaskSpaceDMPControllerMode::CartesianSelection::eAll;

    };

    interface NJointCCDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(int dmpId, Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP();

        void setTemporalFactor(int dmpId, double tau);
        void setViaPoints(int dmpId, double canVal, Ice::DoubleSeq point);
        void setGoals(int dmpId, Ice::DoubleSeq goals);

        void setControllerTarget(float avoidJointLimitsKp, NJointTaskSpaceDMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
    };


    class NJointPeriodicTSDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        double maxLinearVel;
        double maxAngularVel;
        float maxJointVel;
        float avoidJointLimitsKp = 1;

        float Kpos;
        float Kori;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;
    };


    interface NJointPeriodicTSDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);

        double getCanVal();
    };


    class NJointPeriodicTSDMPCompliantControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq desiredNullSpaceJointValues;
        float Knull;
        float Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;

        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        bool ignoreWSLimitChecks = false;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;

        bool isManipulability = false;
        Ice::DoubleSeq kmani;
        Ice::DoubleSeq positionManipulability;
        Ice::DoubleSeq maniWeight;

    };


    interface NJointPeriodicTSDMPCompliantControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();
    };

    class DeprecatedNJointTaskSpaceDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        float DMPKd = 20;
        int kernelSize = 100;
        double tau = 1;
        string dmpMode = "MinimumJerk";
        string dmpStyle = "Discrete";
        double dmpAmplitude = 1;

        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double timeDuration = 10;
        double posToOriRatio = 100;

        // velocity controller configuration
        string nodeSetName = "";
        string tcpName = "";
        string frameName = "";
        NJointTaskSpaceDMPControllerMode::CartesianSelection mode = NJointTaskSpaceDMPControllerMode::CartesianSelection::eAll;

        double maxLinearVel;
        double maxAngularVel;
        double maxJointVelocity;
        string debugName;

        float Kp_LinearVel;
        float Kd_LinearVel;
        float Kp_AngularVel;
        float Kd_AngularVel;

        float pos_filter;
        float vel_filter;
    };


    interface DeprecatedNJointTaskSpaceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);
        void runDMPWithTime(Ice::DoubleSeq goals, double time);

        void setSpeed(double times);
        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void removeAllViaPoints();
        void setGoals(Ice::DoubleSeq goals);

        double getCanVal();
        void resetDMP();
        void stopDMP();
        void pauseDMP();
        void resumeDMP();

        void setControllerTarget(float avoidJointLimitsKp, NJointTaskSpaceDMPControllerMode::CartesianSelection mode);
        void setTorqueKp(StringFloatDictionary torqueKp);
        void setNullspaceJointVelocities(StringFloatDictionary nullspaceJointVelocities);
        string getDMPAsString();
        Ice::DoubleSeq createDMPFromString(string dmpString);

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();

        void setLinearVelocityKd(float kd);
        void setLinearVelocityKp(float kp);
        void setAngularVelocityKd(float kd);
        void setAngularVelocityKp(float kp);

    };

    class DeprecatedNJointTaskSpaceImpedanceDMPControllerConfig extends NJointControllerConfig
       {

           // dmp configuration
           int kernelSize = 100;
           string dmpMode = "MinimumJerk";
           string dmpType = "Discrete";
           double timeDuration;
           string nodeSetName;

           // phaseStop technique
           double phaseL = 10;
           double phaseK = 10;
           double phaseDist0 = 50;
           double phaseDist1 = 10;
           double posToOriRatio = 100;


           Ice::FloatSeq Kpos;
           Ice::FloatSeq Dpos;
           Ice::FloatSeq Kori;
           Ice::FloatSeq Dori;

           Ice::FloatSeq Knull;
           Ice::FloatSeq Dnull;

           bool useNullSpaceJointDMP;
           Ice::FloatSeq defaultNullSpaceJointValues;

           float torqueLimit;


           string forceSensorName;
           float waitTimeForCalibration;
           float forceFilter;
           float forceDeadZone;
           Eigen::Vector3f forceThreshold;
           string forceFrameName = "ArmR8_Wri2";
       };

       interface DeprecatedNJointTaskSpaceImpedanceDMPControllerInterface extends NJointControllerInterface
       {
           void learnDMPFromFiles(Ice::StringSeq trajfiles);
           void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);
           void setUseNullSpaceJointDMP(bool enable);

           bool isFinished();
           bool isDMPRunning();
           void runDMP(Ice::DoubleSeq goals);
           void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);

           void setViaPoints(double canVal, Ice::DoubleSeq point);
           void setGoals(Ice::DoubleSeq goals);
           void setDefaultNullSpaceJointValues(Eigen::VectorXf jointValues);

           double getVirtualTime();

           void resetDMP();
           void stopDMP();
           void resumeDMP();
           void removeAllViaPoints();

           void setMPWeights(DoubleSeqSeq weights);
           DoubleSeqSeq getMPWeights();

           void setLinearVelocityKd(Eigen::Vector3f kd);
           void setLinearVelocityKp(Eigen::Vector3f kp);
           void setAngularVelocityKd(Eigen::Vector3f kd);
           void setAngularVelocityKp(Eigen::Vector3f kp);
           void setNullspaceVelocityKd(Eigen::VectorXf jointValues);
           void setNullspaceVelocityKp(Eigen::VectorXf jointValues);

           void enableForceStop();
           void disableForceStop();
           void setForceThreshold(Eigen::Vector3f forceThreshold);

           string getDMPAsString();
           Ice::DoubleSeq createDMPFromString(string dmpString);

           double getCanVal();
       };


       class DeprecatedNJointTaskSpaceAdmittanceDMPControllerConfig extends NJointControllerConfig
       {
           // dmp configuration
           int kernelSize = 100;
           string dmpMode = "MinimumJerk";
           string dmpType = "Discrete";
           double timeDuration;
           string nodeSetName;

           // phaseStop technique
           double phaseL = 10;
           double phaseK = 10;
           double phaseDist0 = 50;
           double phaseDist1 = 10;
           double posToOriRatio = 100;

           // control parameter ts
           Eigen::Vector6f kpImpedance;
           Eigen::Vector6f kdImpedance;
           Eigen::Vector6f kpAdmittance;
           Eigen::Vector6f kdAdmittance;
           Eigen::Vector6f kmAdmittance;

           // nullspace config
           Eigen::VectorXf Knull;
           Eigen::VectorXf Dnull;
           bool useNullSpaceJointDMP;
           Eigen::VectorXf targetNullSpaceJointValues;

           float jointTorqueLimit;
           float qvelFilter;

           /// force torque sensor
           string forceSensorName;
           string forceFrameName = "ArmR8_Wri2";
           float forceFilter;
           float forceDeadZone;
           float torqueDeadZone;
           float waitTimeForCalibration;

           bool enableTCPGravityCompensation;
           float tcpMass = 0.0f;
           Eigen::Vector3f tcpCoMInForceSensorFrame;
           //        Eigen::Vector3f forceOffsetExternal;
           //        Eigen::Vector3f torqueOffsetExternal;
       };

       interface DeprecatedNJointTaskSpaceAdmittanceDMPControllerInterface extends NJointControllerInterface
       {
           void setTargetNullSpaceJointValues(Eigen::VectorXf jointValues);

           // MP related
           void learnDMPFromFiles(Ice::StringSeq trajfiles);
           void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);
           void setUseNullSpaceJointDMP(bool enable);

           void setViaPoints(double canVal, Ice::DoubleSeq point);
           void setGoals(Ice::DoubleSeq goals);
           void setStartAndGoals(Ice::DoubleSeq starts, Ice::DoubleSeq goals);
           bool isFinished();
           void runDMP(Ice::DoubleSeq goals);
           void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);
           double getVirtualTime();
           void resetDMP();
           void stopDMP();
           void resumeDMP();
           void pauseDMP();
           void removeAllViaPoints();
           void setMPWeights(DoubleSeqSeq weights);
           DoubleSeqSeq getMPWeights();
           string getDMPAsString();
           Ice::DoubleSeq createDMPFromString(string dmpString);
           double getCanVal();

           void setKpImpedance(Eigen::Vector6f kpImpedance);
           void setKdImpedance(Eigen::Vector6f kdImpedance);

           void setKpAdmittance(Eigen::Vector6f kpAdmittance);
           void setKdAdmittance(Eigen::Vector6f kdAdmittance);
           void setKmAdmittance(Eigen::Vector6f kmAdmittance);

           void setTCPMass(float mass);
           void setTCPCoMInFTFrame(Eigen::Vector3f com);

           void setNullspaceVelocityKd(Eigen::VectorXf jointValues);
           void setNullspaceVelocityKp(Eigen::VectorXf jointValues);

           //        RTController::ControllerInfo getControllerInfo();
           string getKinematicChainName();

           //        Eigen::Vector6f getFilteredForceTorque();
           void setPotentialForceBaseline(Eigen::Vector3f force, Eigen::Vector3f torque);
       };

       class DeprecatedNJointPeriodicTSDMPCompliantControllerConfig extends NJointControllerConfig
          {
              // dmp configuration
              int kernelSize = 100;
              double dmpAmplitude = 1;
              double timeDuration = 10;
              string nodeSetName = "";

              double phaseL = 10;
              double phaseK = 10;
              float phaseDist0 = 50;
              float phaseDist1 = 10;
              double phaseKpPos = 1;
              double phaseKpOri = 0.1;
              double posToOriRatio = 100;

              // controller gains
              Eigen::Vector6f kpImpedance;
              Eigen::Vector6f kdImpedance;
              Eigen::Vector4f pidForce;

              Eigen::VectorXf Knull;
              Eigen::VectorXf Dnull;
              Eigen::VectorXf targetNullSpaceJointValues;

              float jointTorqueLimit;
              float qvelFilter;

              //        float minimumReactForce = 0;
              //        float maxLinearVel;
              //        float maxAngularVel;

              /// adaptive settings
              Ice::FloatSeq ws_x;
              Ice::FloatSeq ws_y;
              Ice::FloatSeq ws_z;
              bool ignoreWSLimitChecks = false;
              float changePositionTolerance;
              float changeTimerThreshold;

              /// force torque sensor
              string forceSensorName = "FT R";
              string forceFrameName = "ArmR8_Wri2";
              float forceFilter = 0.8;
              float forceDeadZone;
              float torqueDeadZone;
              float waitTimeForCalibration = 0.1;

              bool enableTCPGravityCompensation;
              float tcpMass = 0.0f;
              Eigen::Vector3f tcpCoMInForceSensorFrame;

              /// tool setup (Effective frame in root)
              Eigen::Matrix4f toolFrameInRoot;
          };


          interface DeprecatedNJointPeriodicTSDMPCompliantControllerInterface extends NJointControllerInterface
          {
              void learnDMPFromFiles(Ice::StringSeq trajfiles);
              void learnDMPFromTrajectory(TrajectoryBase trajectory);

              bool isFinished();
              void runDMP(Ice::DoubleSeq goals, double tau);

              void resetDMP();
              void stopDMP();
              void resumeDMP();
              void pauseDMP();

              void setSpeed(double times);
              void setGoals(Ice::DoubleSeq goals);
              void setAmplitude(double amplitude);
              void setTargetForceInRootFrame(float force);

              void setKpImpedance(Eigen::Vector6f kpImpedance);
              void setKdImpedance(Eigen::Vector6f kdImpedance);

              double getCanVal();

              string getKinematicChainName();
          };
};

