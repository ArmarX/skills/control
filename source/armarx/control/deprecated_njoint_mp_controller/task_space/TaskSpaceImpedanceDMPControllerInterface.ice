/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

#include <ArmarXCore/interface/serialization/Eigen.ice>

module armarx
{
    class NJointTaskSpaceImpedanceDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";
        double timeDuration;
        string nodeSetName;

        // phaseStop technique
        double phaseL = 10;
        double phaseK = 10;
        double phaseDist0 = 50;
        double phaseDist1 = 10;
        double posToOriRatio = 100;


        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        bool useNullSpaceJointDMP;
        Ice::FloatSeq defaultNullSpaceJointValues;

        float torqueLimit;


        string forceSensorName;
        float waitTimeForCalibration;
        float forceFilter;
        float forceDeadZone;
        Eigen::Vector3f forceThreshold;
        string forceFrameName = "ArmR8_Wri2";
    };

    interface NJointTaskSpaceImpedanceDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);
        void setUseNullSpaceJointDMP(bool enable);

        bool isFinished();
        bool isDMPRunning();
        void runDMP(Ice::DoubleSeq goals);
        void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);
        void setDefaultNullSpaceJointValues(Eigen::VectorXf jointValues);

        double getVirtualTime();

        void resetDMP();
        void stopDMP();
        void resumeDMP();
        void removeAllViaPoints();

        void setMPWeights(DoubleSeqSeq weights);
        DoubleSeqSeq getMPWeights();

        void setLinearVelocityKd(Eigen::Vector3f kd);
        void setLinearVelocityKp(Eigen::Vector3f kp);
        void setAngularVelocityKd(Eigen::Vector3f kd);
        void setAngularVelocityKp(Eigen::Vector3f kp);
        void setNullspaceVelocityKd(Eigen::VectorXf jointValues);
        void setNullspaceVelocityKp(Eigen::VectorXf jointValues);

        void enableForceStop();
        void disableForceStop();
        void setForceThreshold(Eigen::Vector3f forceThreshold);
    };
}
