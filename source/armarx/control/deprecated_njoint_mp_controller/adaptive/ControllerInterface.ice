/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::control::deprecated_njoint_mp_controller::adaptive::ControllerInterface
 * @author     jianfeng gao ( jianfeng dot gao at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/Trajectory.ice>

#include <ArmarXCore/interface/serialization/Eigen.ice>

module armarx
{
    class NJointTaskSpaceAdaptiveDMPControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        string dmpMode = "MinimumJerk";
        string dmpType = "Discrete";
        double timeDuration;
        string nodeSetName;

        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        bool useNullSpaceJointDMP;
        Ice::FloatSeq defaultNullSpaceJointValues;

        float torqueLimit;
        string forceSensorName;
        float filterCoeff;
    };

    interface NJointTaskSpaceAdaptiveDMPControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnJointDMPFromFiles(string jointTrajFile, Ice::FloatSeq currentJVS);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals);
        void runDMPWithTime(Ice::DoubleSeq goals, double timeDuration);

        void setViaPoints(double canVal, Ice::DoubleSeq point);
        void setGoals(Ice::DoubleSeq goals);

        double getVirtualTime();
        void setCanVal(double canVal);

        void resetDMP();
        void stopDMP();
        void resumeDMP();

        void setKdImpedance(Ice::FloatSeq dampings);
        void setKpImpedance(Ice::FloatSeq stiffness);
        void setKpNull(Ice::FloatSeq knull);
        void setKdNull(Ice::FloatSeq dnull);
        Ice::FloatSeq getForce();
        Ice::FloatSeq getVelocityInMM();
        void removeAllViaPoints();

        void setUseNullSpaceJointDMP(bool useJointDMP);
        void setDefaultJointValues(Ice::FloatSeq desiredJointVals);
    };


    class NJointAdaptiveWipingControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;

        Ice::FloatSeq desiredNullSpaceJointValues;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;
        float Kpf;

        float minimumReactForce = 0;

        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;

        Ice::FloatSeq ftOffset;
        Ice::FloatSeq handCOM;
        float handMass;
        float angularKp;
        float frictionCone;
    };


    interface NJointAdaptiveWipingControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();
    };

    class NJointAnomalyDetectionAdaptiveWipingControllerConfig extends NJointControllerConfig
    {

        // dmp configuration
        int kernelSize = 100;
        double dmpAmplitude = 1;
        double timeDuration = 10;

        double phaseL = 10;
        double phaseK = 10;
        float phaseDist0 = 50;
        float phaseDist1 = 10;
        double phaseKpPos = 1;
        double phaseKpOri = 0.1;
        double posToOriRatio = 100;


        // velocity controller configuration
        string nodeSetName = "";

        float maxJointTorque;
        Ice::FloatSeq desiredNullSpaceJointValues;

        Ice::FloatSeq Kpos;
        Ice::FloatSeq Dpos;
        Ice::FloatSeq Kori;
        Ice::FloatSeq Dori;
        Ice::FloatSeq Knull;
        Ice::FloatSeq Dnull;

        string forceSensorName = "FT R";
        string forceFrameName = "ArmR8_Wri2";
        float forceFilter = 0.8;
        float waitTimeForCalibration = 0.1;

        // anomaly detection and friction estimation
        int velocityHorizon = 100;
        int frictionHorizon = 100;

        // pid params
        bool isForceCtrlInForceDir;
        bool isForceControlEnabled;
        bool isRotControlEnabled;
        bool isTorqueControlEnabled;
        bool isLCRControlEnabled;
        Ice::FloatSeq pidForce;
        Ice::FloatSeq pidRot;
        Ice::FloatSeq pidTorque;
        Ice::FloatSeq pidLCR;

        float minimumReactForce = 0;
        float forceDeadZone;
        float velFilter;

        float maxLinearVel;
        float maxAngularVel;

        Ice::FloatSeq ws_x;
        Ice::FloatSeq ws_y;
        Ice::FloatSeq ws_z;

        float adaptCoeff;
        float reactThreshold;
        float dragForceDeadZone;
        float adaptForceCoeff;
        float changePositionTolerance;
        float changeTimerThreshold;

        Ice::FloatSeq ftOffset;
        Ice::FloatSeq handCOM;
        float handMass;

        float ftCommandFilter;
        float frictionCone;
        float fricEstiFilter;
        float velNormThreshold;
        float maxInteractionForce;

        float increaseKpForceCoeff;
        float increaseKpRotCoeff;
        float decreaseKpForceCoeff;
        float decreaseKpRotCoeff;

        float adaptRateIncrease;
        float adaptRateDecrease;
        float adaptRateDecreaseRot;
        float adaptCoeffKdImpIncrease;

        float increaseKpOriCoeff;
        float increaseKdOriCoeff;
        float increaseKpNullCoeff;
        float increaseKdNullCoeff;

        bool isAdaptOriImpEnabled;
        float loseContactForceIntThreshold;
        bool loseContactDetectionEnabled;
        int loseContactCounterMax;
        float rotAngleSigmoid;
        bool useDMPInGlobalFrame;
    };


    interface NJointAnomalyDetectionAdaptiveWipingControllerInterface extends NJointControllerInterface
    {
        void learnDMPFromFiles(Ice::StringSeq trajfiles);
        void learnDMPFromTrajectory(TrajectoryBase trajectory);

        bool isFinished();
        void runDMP(Ice::DoubleSeq goals, double tau);

        void setSpeed(double times);
        void setGoals(Ice::DoubleSeq goals);
        void setAmplitude(double amplitude);
        void setTargetForceInRootFrame(float force);

        double getCanVal();

        Ice::FloatSeq getAnomalyInput();
        Ice::FloatSeq getAnomalyOutput();

        void setTrigerAbnormalEvent(bool abnormal);

        void pauseDMP();
        void resumeDMP();
    };
};

