
#pragma once

#include <ArmarXCore/core/time/CycleUtil.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/IK/DifferentialIK.h>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <VirtualRobot/Robot.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueForceTorque.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/Trajectory.h>
#include <RobotAPI/libraries/core/PIDController.h>

// control
#include <armarx/control/deprecated_njoint_mp_controller/adaptive/ControllerInterface.h>
#include <armarx/control/deprecated_njoint_mp_controller/TaskSpaceVMP.h>


namespace armarx::control::deprecated_njoint_mp_controller::adaptive
{
    namespace tsvmp = armarx::control::deprecated_njoint_mp_controller::tsvmp;

    TYPEDEF_PTRS_HANDLE(NJointAdaptiveWipingController);
    TYPEDEF_PTRS_HANDLE(NJointAdaptiveWipingControllerControlData);

    class NJointAdaptiveWipingControllerControlData
    {
    public:
        Eigen::VectorXf targetTSVel;
    };

    /**
     * @brief The NJointAdaptiveWipingController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointAdaptiveWipingController :
        public NJointControllerWithTripleBuffer<NJointAdaptiveWipingControllerControlData>,
        public NJointAdaptiveWipingControllerInterface
    {
    public:
        using ConfigPtrT = NJointAdaptiveWipingControllerConfigPtr;
        NJointAdaptiveWipingController(const RobotUnitPtr&, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const;

        // NJointController interface

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration);

        // NJointAdaptiveWipingControllerInterface interface
        void learnDMPFromFiles(const Ice::StringSeq& fileNames, const Ice::Current&);
        void learnDMPFromTrajectory(const TrajectoryBasePtr& trajectory, const Ice::Current&);
        bool isFinished(const Ice::Current&)
        {
            return false;
        }

        void setSpeed(Ice::Double times, const Ice::Current&);
        void setGoals(const Ice::DoubleSeq& goals, const Ice::Current&);
        void setAmplitude(Ice::Double amp, const Ice::Current&);
        void runDMP(const Ice::DoubleSeq& goals, Ice::Double tau, const Ice::Current&);
        void setTargetForceInRootFrame(Ice::Float force, const Ice::Current&);
        double getCanVal(const Ice::Current&)
        {
            return dmpCtrl->canVal;
        }

    protected:
        virtual void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&);

        void onInitNJointController();
        void onDisconnectNJointController();
        void controllerRun();

    private:
        struct DebugBufferData
        {
            StringFloatDictionary latestTargetVelocities;
            StringFloatDictionary currentPose;
            double currentCanVal;
            double mpcFactor;
            double error;
            double phaseStop;
            double posError;
            double oriError;
            double deltaT;
        };

        TripleBuffer<DebugBufferData> debugOutputData;


        struct DebugRTData
        {
            Eigen::Matrix4f targetPose;
            Eigen::Vector3f filteredForce;
            Eigen::Vector3f filteredForceInRoot;
            Eigen::Vector3f filteredTorque;

            Eigen::Vector3f rotationAxis;

            Eigen::Vector3f reactForce;
            Eigen::Vector3f adaptK;
            Eigen::VectorXf targetVel;
            Eigen::Matrix4f currentPose;
            bool isPhaseStop;

            Eigen::Matrix4f globalPose;
            Eigen::Vector3f globalFilteredForce;
            Eigen::Vector3f currentToolDir;
            Eigen::VectorXf currentTwist;

            float rotAngle;
        };
        TripleBuffer<DebugRTData> debugRT;




        struct RTToControllerData
        {
            double currentTime;
            double deltaT;
            Eigen::Matrix4f currentPose;
            Eigen::VectorXf currentTwist;
            bool isPhaseStop;
        };
        TripleBuffer<RTToControllerData> rt2CtrlData;

        struct RTToUserData
        {
            Eigen::Matrix4f currentTcpPose;
            float waitTimeForCalibration;
        };
        TripleBuffer<RTToUserData> rt2UserData;

        struct UserToRTData
        {
            float targetForce;
        };
        TripleBuffer<UserToRTData> user2rtData;


        tsvmp::TaskSpaceDMPControllerPtr dmpCtrl;

        std::vector<const SensorValue1DoFActuatorVelocity*> velocitySensors;
        std::vector<const SensorValue1DoFActuatorPosition*> positionSensors;
        std::vector<ControlTarget1DoFActuatorTorque*> targets;

        // velocity ik controller parameters
        std::string nodeSetName;

        bool started;
        bool firstRun;
        bool dmpRunning;

        VirtualRobot::DifferentialIKPtr ik;
        VirtualRobot::RobotNodePtr tcp;

        NJointAdaptiveWipingControllerConfigPtr cfg;
        mutable MutexType controllerMutex;
        PeriodicTask<NJointAdaptiveWipingController>::pointer_type controllerTask;
        Eigen::Matrix4f targetPose;

        Eigen::Vector3f kpos;
        Eigen::Vector3f dpos;
        Eigen::Vector3f kori;
        Eigen::Vector3f dori;
        Eigen::VectorXf knull;
        Eigen::VectorXf dnull;
        float kpf;

        Eigen::VectorXf nullSpaceJointsVec;
        const SensorValueForceTorque* forceSensor;

        PIDControllerPtr forcePID;

        Eigen::Vector3f filteredForce;
        Eigen::Vector3f filteredTorque;
        Eigen::Vector3f forceOffset;
        Eigen::Vector3f currentForceOffset;

        Eigen::Vector3f torqueOffset;
        Eigen::Vector3f currentTorqueOffset;
        float handMass;
        Eigen::Vector3f handCOM;
        Eigen::Vector3f gravityInRoot;

        Eigen::Vector3f filteredForceInRoot;
        Eigen::Vector3f filteredTorqueInRoot;

        Eigen::Matrix3f toolTransform;
        Eigen::Vector3f oriToolDir;
        Eigen::Matrix3f origHandOri;
        Eigen::VectorXf qvel_filtered;

        Eigen::Vector3f adaptK;
        float lastDiff;
        Eigen::Vector2f lastPosition;
        double changeTimer;

    };

} // namespace armarx

