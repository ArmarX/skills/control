/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    DSController::ArmarXObjects::DSRTBimanualController
 * @author     Mahdi Khoramshahi ( m80 dot khoramshahi at gmail dot com )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DSRTBimanualController.h"

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

namespace armarx::control::ds_controller
{
    NJointControllerRegistration<DSRTBimanualController> registrationControllerDSRTBimanualController("DSRTBimanualController");

    void DSRTBimanualController::onInitNJointController()
    {
        ARMARX_INFO << "init ...";
        controllerStopRequested = false;
        controllerRunFinished = false;
        runTask("DSRTBimanualControllerTask", [&]
        {
            CycleUtil c(1);
            getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
            while (getState() == eManagedIceObjectStarted && !controllerStopRequested)
            {
                ARMARX_VERBOSE << deactivateSpam(1) << "control fct";
                if (isControllerActive())
                {
                    controllerRun();
                }
                c.waitForCycleDuration();
            }
            controllerRunFinished = true;
            ARMARX_INFO << "Control Fct finished";
        });


    }

    void DSRTBimanualController::onDisconnectNJointController()
    {
        ARMARX_INFO << "disconnect";
        controllerStopRequested = true;
        while (!controllerRunFinished)
        {
            ARMARX_INFO << deactivateSpam(1) << "waiting for run function";
            usleep(10000);
        }
    }


    DSRTBimanualController::DSRTBimanualController(const RobotUnitPtr& robotUnit, const armarx::NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&)
    {
        cfg = DSRTBimanualControllerConfigPtr::dynamicCast(config);
        useSynchronizedRtRobot();

        VirtualRobot::RobotNodeSetPtr left_ns = rtGetRobot()->getRobotNodeSet("LeftArm");
        VirtualRobot::RobotNodeSetPtr right_ns = rtGetRobot()->getRobotNodeSet("RightArm");

        left_jointNames.clear();
        right_jointNames.clear();

        ARMARX_CHECK_EXPRESSION(left_ns) << "LeftArm";
        ARMARX_CHECK_EXPRESSION(right_ns) << "RightArm";

        // for left arm
        for (size_t i = 0; i < left_ns->getSize(); ++i)
        {
            std::string jointName = left_ns->getNode(i)->getName();
            left_jointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF); // ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Velocity1DoF);
            ARMARX_CHECK_EXPRESSION(ct);
            const SensorValueBase* sv = useSensorValue(jointName);
            ARMARX_CHECK_EXPRESSION(sv);
            auto casted_ct = ct->asA<ControlTarget1DoFActuatorTorque>(); // auto casted_ct = ct->asA<ControlTarget1DoFActuatorVelocity>();
            ARMARX_CHECK_EXPRESSION(casted_ct);
            left_torque_targets.push_back(casted_ct);

            const SensorValue1DoFActuatorTorque* torqueSensor = sv->asA<SensorValue1DoFActuatorTorque>();
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFGravityTorque* gravityTorqueSensor = sv->asA<SensorValue1DoFGravityTorque>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            if (!torqueSensor)
            {
                ARMARX_WARNING << "No Torque sensor available for " << jointName;
            }
            if (!gravityTorqueSensor)
            {
                ARMARX_WARNING << "No Gravity Torque sensor available for " << jointName;
            }

            left_torqueSensors.push_back(torqueSensor);
            left_gravityTorqueSensors.push_back(gravityTorqueSensor);
            left_velocitySensors.push_back(velocitySensor);
            left_positionSensors.push_back(positionSensor);
        };

        // for right arm
        for (size_t i = 0; i < right_ns->getSize(); ++i)
        {
            std::string jointName = right_ns->getNode(i)->getName();
            right_jointNames.push_back(jointName);
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Torque1DoF); // ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Velocity1DoF);
            ARMARX_CHECK_EXPRESSION(ct);
            const SensorValueBase* sv = useSensorValue(jointName);
            ARMARX_CHECK_EXPRESSION(sv);
            auto casted_ct = ct->asA<ControlTarget1DoFActuatorTorque>(); // auto casted_ct = ct->asA<ControlTarget1DoFActuatorVelocity>();
            ARMARX_CHECK_EXPRESSION(casted_ct);
            right_torque_targets.push_back(casted_ct);

            const SensorValue1DoFActuatorTorque* torqueSensor = sv->asA<SensorValue1DoFActuatorTorque>();
            const SensorValue1DoFActuatorVelocity* velocitySensor = sv->asA<SensorValue1DoFActuatorVelocity>();
            const SensorValue1DoFGravityTorque* gravityTorqueSensor = sv->asA<SensorValue1DoFGravityTorque>();
            const SensorValue1DoFActuatorPosition* positionSensor = sv->asA<SensorValue1DoFActuatorPosition>();
            if (!torqueSensor)
            {
                ARMARX_WARNING << "No Torque sensor available for " << jointName;
            }
            if (!gravityTorqueSensor)
            {
                ARMARX_WARNING << "No Gravity Torque sensor available for " << jointName;
            }

            right_torqueSensors.push_back(torqueSensor);
            right_gravityTorqueSensors.push_back(gravityTorqueSensor);
            right_velocitySensors.push_back(velocitySensor);
            right_positionSensors.push_back(positionSensor);
        };


        const SensorValueBase* svlf = useSensorValue("FT L");
        leftForceTorque = svlf->asA<SensorValueForceTorque>();
        const SensorValueBase* svrf = useSensorValue("FT R");
        rightForceTorque = svrf->asA<SensorValueForceTorque>();

        ARMARX_INFO << "Initialized " << left_torque_targets.size() << " targets for the left arm";
        ARMARX_INFO << "Initialized " << right_torque_targets.size() << " targets for the right arm";

        left_arm_tcp =  left_ns->getTCP();
        right_arm_tcp =  right_ns->getTCP();

        left_sensor_frame = left_ns->getRobot()->getRobotNode("ArmL8_Wri2");
        right_sensor_frame  = right_ns->getRobot()->getRobotNode("ArmR8_Wri2");

        left_ik.reset(new VirtualRobot::DifferentialIK(left_ns, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));
        right_ik.reset(new VirtualRobot::DifferentialIK(right_ns, rtGetRobot()->getRootNode(), VirtualRobot::JacobiProvider::eSVDDamped));


        // ?? not sure about this
        DSRTBimanualControllerSensorData initSensorData;

        initSensorData.left_tcpPose = left_arm_tcp->getPoseInRootFrame();
        initSensorData.currentTime = 0;
        controllerSensorData.reinitAllBuffers(initSensorData);

        initSensorData.right_tcpPose = right_arm_tcp->getPoseInRootFrame();
        initSensorData.currentTime = 0;
        controllerSensorData.reinitAllBuffers(initSensorData);


        left_oldPosition = left_arm_tcp->getPositionInRootFrame();
        left_oldOrientation = left_arm_tcp->getPoseInRootFrame().block<3, 3>(0, 0);

        right_oldPosition = right_arm_tcp->getPositionInRootFrame();
        right_oldOrientation = right_arm_tcp->getPoseInRootFrame().block<3, 3>(0, 0);


        std::vector<float> left_desiredQuaternionVec = cfg->left_desiredQuaternion;
        ARMARX_CHECK_EQUAL(left_desiredQuaternionVec.size(), 4);

        std::vector<float> right_desiredQuaternionVec = cfg->right_desiredQuaternion;
        ARMARX_CHECK_EQUAL(right_desiredQuaternionVec.size(), 4);

        left_desiredQuaternion.w() = left_desiredQuaternionVec.at(0);
        left_desiredQuaternion.x() = left_desiredQuaternionVec.at(1);
        left_desiredQuaternion.y() = left_desiredQuaternionVec.at(2);
        left_desiredQuaternion.z() = left_desiredQuaternionVec.at(3);

        right_desiredQuaternion.w() = right_desiredQuaternionVec.at(0);
        right_desiredQuaternion.x() = right_desiredQuaternionVec.at(1);
        right_desiredQuaternion.y() = right_desiredQuaternionVec.at(2);
        right_desiredQuaternion.z() = right_desiredQuaternionVec.at(3);


        // set initial joint velocities filter

        left_jointVelocity_filtered.resize(left_torque_targets.size());
        left_jointVelocity_filtered.setZero();
        right_jointVelocity_filtered.resize(left_torque_targets.size());
        right_jointVelocity_filtered.setZero();

        // do we need to duplicate this?
        DSRTBimanualControllerControlData initData;
        for (size_t i = 0; i < 3; ++i)
        {
            initData.left_tcpDesiredLinearVelocity(i) = 0;
            initData.right_tcpDesiredLinearVelocity(i) = 0;

        }

        for (size_t i = 0; i < 3; ++i)
        {
            initData.left_tcpDesiredAngularError(i) = 0;
            initData.right_tcpDesiredAngularError(i) = 0;

        }
        reinitTripleBuffer(initData);


        // initial filter
        left_desiredTorques_filtered.resize(left_torque_targets.size());
        left_desiredTorques_filtered.setZero();
        right_desiredTorques_filtered.resize(right_torque_targets.size());
        right_desiredTorques_filtered.setZero();


        left_currentTCPLinearVelocity_filtered.setZero();
        right_currentTCPLinearVelocity_filtered.setZero();

        left_currentTCPAngularVelocity_filtered.setZero();
        right_currentTCPAngularVelocity_filtered.setZero();

        left_tcpDesiredTorque_filtered.setZero();
        right_tcpDesiredTorque_filtered.setZero();


        smooth_startup = 0;


        filterTimeConstant = cfg->filterTimeConstant;
        posiKp = cfg->posiKp;
        v_max = cfg->v_max;
        Damping = cfg->posiDamping;
        Coupling_stiffness = cfg->couplingStiffness;
        Coupling_force_limit = cfg->couplingForceLimit;
        forward_gain = cfg->forwardGain;
        torqueLimit = cfg->torqueLimit;
        null_torqueLimit = cfg->NullTorqueLimit;
        oriKp = cfg->oriKp;
        oriDamping  = cfg->oriDamping;
        contactForce = cfg->contactForce;

        left_oriUp.w() =  cfg->left_oriUp[0];
        left_oriUp.x() =  cfg->left_oriUp[1];
        left_oriUp.y() =  cfg->left_oriUp[2];
        left_oriUp.z() =  cfg->left_oriUp[3];

        left_oriDown.w() = cfg->left_oriDown[0];
        left_oriDown.x() = cfg->left_oriDown[1];
        left_oriDown.y() = cfg->left_oriDown[2];
        left_oriDown.z() = cfg->left_oriDown[3];

        right_oriUp.w() = cfg->right_oriUp[0];
        right_oriUp.x() = cfg->right_oriUp[1];
        right_oriUp.y() = cfg->right_oriUp[2];
        right_oriUp.z() = cfg->right_oriUp[3];

        right_oriDown.w() = cfg->right_oriDown[0];
        right_oriDown.x() = cfg->right_oriDown[1];
        right_oriDown.y() = cfg->right_oriDown[2];
        right_oriDown.z() = cfg->right_oriDown[3];


        guardTargetZUp = cfg->guardTargetZUp;
        guardTargetZDown = cfg->guardTargetZDown;
        guardDesiredZ = guardTargetZUp;
        guard_mounting_correction_z = 0;

        guardXYHighFrequency = 0;
        left_force_old.setZero();
        right_force_old.setZero();

        left_contactForceZ_correction = 0;
        right_contactForceZ_correction = 0;


        std::vector<float> leftarm_qnullspaceVec = cfg->leftarm_qnullspaceVec;
        std::vector<float> rightarm_qnullspaceVec = cfg->rightarm_qnullspaceVec;

        left_qnullspace.resize(leftarm_qnullspaceVec.size());
        right_qnullspace.resize(rightarm_qnullspaceVec.size());

        for (size_t i = 0; i < leftarm_qnullspaceVec.size(); ++i)
        {
            left_qnullspace(i) = leftarm_qnullspaceVec[i];
            right_qnullspace(i) = rightarm_qnullspaceVec[i];
        }

        nullspaceKp = cfg->nullspaceKp;
        nullspaceDamping = cfg->nullspaceDamping;


        //set GMM parameters ...
        if (cfg->gmmParamsFile == "")
        {
            ARMARX_ERROR << "gmm params file cannot be empty string ...";
        }
        gmmMotionGenerator.reset(new BimanualGMMMotionGen(cfg->gmmParamsFile));
        positionErrorTolerance = cfg->positionErrorTolerance;
        forceFilterCoeff = cfg->forceFilterCoeff;
        for (size_t i = 0 ; i < 3; ++i)
        {
            leftForceOffset[i] = cfg->forceLeftOffset.at(i);
            rightForceOffset[i] = cfg->forceRightOffset.at(i);
        }
        isDefaultTarget = false;
        ARMARX_INFO << "Initialization done";
    }

    void DSRTBimanualController::controllerRun()
    {
        if (!controllerSensorData.updateReadBuffer())
        {
            return;
        }


        // receive the measurements
        Eigen::Matrix4f left_currentTCPPose = controllerSensorData.getReadBuffer().left_tcpPose;
        Eigen::Matrix4f right_currentTCPPose = controllerSensorData.getReadBuffer().right_tcpPose;

        Eigen::Vector3f left_force = controllerSensorData.getReadBuffer().left_force;
        Eigen::Vector3f right_force = controllerSensorData.getReadBuffer().right_force;
        Eigen::Vector3f both_arm_force_ave = - 0.5 * (left_force + right_force); // negative sign for the direction


        //    float left_force_z = left_force(2);
        //    float right_force_z = right_force(2);

        Eigen::Vector3f left_currentTCPPositionInMeter;
        Eigen::Vector3f right_currentTCPPositionInMeter;

        left_currentTCPPositionInMeter << left_currentTCPPose(0, 3), left_currentTCPPose(1, 3), left_currentTCPPose(2, 3);
        right_currentTCPPositionInMeter << right_currentTCPPose(0, 3), right_currentTCPPose(1, 3), right_currentTCPPose(2, 3);

        left_currentTCPPositionInMeter = 0.001 * left_currentTCPPositionInMeter;
        right_currentTCPPositionInMeter = 0.001 * right_currentTCPPositionInMeter;


        // updating the desired height for the guard based on the interaction forces
        float both_arm_height_z_ave =  0.5 * (left_currentTCPPositionInMeter(2) + right_currentTCPPositionInMeter(2));


        float adaptive_ratio = 1;
        float force_error_z = 0;
        float guard_mounting_correction_x = 0;
        float guard_mounting_correction_y = 0;


        if (cfg->guardDesiredDirection)
        {
            adaptive_ratio = 1;
            force_error_z = both_arm_force_ave(2) - adaptive_ratio  * cfg->liftingForce;

            // meausures the high-frequency components of the interaction forces
            float diff_norm = (left_force - left_force_old).squaredNorm() + (right_force - right_force_old).squaredNorm();
            // diff_norm = deadzone(diff_norm,0.1,2);
            guardXYHighFrequency = cfg->highPassFilterFactor * guardXYHighFrequency + 0.1 * diff_norm;

            guardXYHighFrequency = (guardXYHighFrequency > 10) ? 10 : guardXYHighFrequency;

            left_force_old = left_force;
            right_force_old = right_force;

            if (fabs(both_arm_height_z_ave - guardTargetZUp) < cfg->mountingDistanceTolerance)
            {
                guard_mounting_correction_z = (1 - cfg->mountingCorrectionFilterFactor) * guard_mounting_correction_z + cfg->mountingCorrectionFilterFactor * (- 0.1 * (guardXYHighFrequency - 8));
                guard_mounting_correction_z = deadzone(guard_mounting_correction_z, 0, 0.1);


                guard_mounting_correction_x = guard_mounting_correction_x - cfg->forceErrorZGain * deadzone(both_arm_force_ave(0), 1, 3);
                guard_mounting_correction_y = guard_mounting_correction_y - cfg->forceErrorZGain * deadzone(both_arm_force_ave(1), 1, 3);

                guard_mounting_correction_x = deadzone(guard_mounting_correction_x, 0, 0.1);
                guard_mounting_correction_y = deadzone(guard_mounting_correction_y, 0, 0.1);


            }

        }
        else
        {
            adaptive_ratio = (0.5 * (both_arm_height_z_ave - guardTargetZDown) / (guardTargetZUp - guardTargetZDown) + 0.5);
            force_error_z = both_arm_force_ave(2) - adaptive_ratio  * cfg->loweringForce;

        }



        force_error_z = deadzone(force_error_z, cfg->forceErrorZDisturbance, cfg->forceErrorZThreshold);
        guardDesiredZ = guardDesiredZ - cfg->forceErrorZGain * (force_error_z);

        guardDesiredZ = (guardDesiredZ > guardTargetZUp) ?  guardTargetZUp : guardDesiredZ;
        guardDesiredZ = (guardDesiredZ < guardTargetZDown) ?  guardTargetZDown : guardDesiredZ;

        if (isDefaultTarget)
        {
            guardDesiredZ = guardTargetZDown;
        }

        if (!cfg->guardDesiredDirection && guardDesiredZ < 1.5)
        {
            guard_mounting_correction_y = -0.1;
        }

        // update the desired velocity
        gmmMotionGenerator->updateDesiredVelocity(
            left_currentTCPPositionInMeter,
            right_currentTCPPositionInMeter,
            positionErrorTolerance,
            guardDesiredZ,
            guard_mounting_correction_x,
            guard_mounting_correction_y,
            guard_mounting_correction_z);

        //    ARMARX_INFO << "tcpDesiredLinearVelocity: " << gmmMotionGenerator->left_DS_DesiredVelocity;
        //    ARMARX_INFO << "tcpDesiredLinearVelocity: " << gmmMotionGenerator->right_DS_DesiredVelocity;


        Eigen::Vector3f left_tcpDesiredAngularError;
        Eigen::Vector3f right_tcpDesiredAngularError;

        left_tcpDesiredAngularError << 0, 0, 0;
        right_tcpDesiredAngularError << 0, 0, 0;



        Eigen::Matrix3f left_currentRotMat = left_currentTCPPose.block<3, 3>(0, 0);
        Eigen::Matrix3f right_currentRotMat = right_currentTCPPose.block<3, 3>(0, 0);


        float lqratio = (left_currentTCPPositionInMeter(2) - guardTargetZDown) / (guardTargetZUp - guardTargetZDown);
        float rqratio = (right_currentTCPPositionInMeter(2) - guardTargetZDown) / (guardTargetZUp - guardTargetZDown);

        lqratio = (lqratio > 1) ?  1 : lqratio;
        lqratio = (lqratio < 0) ?  0 : lqratio;
        rqratio = (rqratio > 1) ?  1 : rqratio;
        rqratio = (rqratio < 0) ?  0 : rqratio;


        Eigen::Quaternionf left_desiredQuaternion = quatSlerp(lqratio, left_oriDown, left_oriUp);
        Eigen::Quaternionf right_desiredQuaternion = quatSlerp(rqratio, right_oriDown, right_oriUp);


        Eigen::Matrix3f left_desiredRotMat = left_desiredQuaternion.normalized().toRotationMatrix();
        Eigen::Matrix3f right_desiredRotMat = right_desiredQuaternion.normalized().toRotationMatrix();

        Eigen::Matrix3f left_orientationError = left_currentRotMat * left_desiredRotMat.inverse();
        Eigen::Matrix3f right_orientationError = right_currentRotMat * right_desiredRotMat.inverse();

        Eigen::Quaternionf left_diffQuaternion(left_orientationError);
        Eigen::Quaternionf right_diffQuaternion(right_orientationError);

        Eigen::AngleAxisf left_angleAxis(left_diffQuaternion);
        Eigen::AngleAxisf right_angleAxis(right_diffQuaternion);

        left_tcpDesiredAngularError = left_angleAxis.angle() * left_angleAxis.axis();
        right_tcpDesiredAngularError = right_angleAxis.angle() * right_angleAxis.axis();


        // writing to the buffer
        getWriterControlStruct().left_tcpDesiredLinearVelocity = gmmMotionGenerator->left_DS_DesiredVelocity;
        getWriterControlStruct().right_tcpDesiredLinearVelocity = gmmMotionGenerator->right_DS_DesiredVelocity;
        getWriterControlStruct().left_right_distanceInMeter = gmmMotionGenerator->left_right_position_errorInMeter;

        getWriterControlStruct().left_tcpDesiredAngularError = left_tcpDesiredAngularError;
        getWriterControlStruct().right_tcpDesiredAngularError = right_tcpDesiredAngularError;

        writeControlStruct();
        debugCtrlDataInfo.getWriteBuffer().desredZ = guardDesiredZ;
        debugCtrlDataInfo.getWriteBuffer().force_error_z = force_error_z;
        debugCtrlDataInfo.getWriteBuffer().guardXYHighFrequency = guardXYHighFrequency;
        debugCtrlDataInfo.getWriteBuffer().guard_mounting_correction_x = guard_mounting_correction_x;
        debugCtrlDataInfo.getWriteBuffer().guard_mounting_correction_y = guard_mounting_correction_y;
        debugCtrlDataInfo.getWriteBuffer().guard_mounting_correction_z = guard_mounting_correction_z;
        debugCtrlDataInfo.commitWrite();

    }


    void DSRTBimanualController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        // reading the control objectives from the other threads (desired velocities)
        Eigen::Vector3f left_tcpDesiredLinearVelocity = rtGetControlStruct().left_tcpDesiredLinearVelocity;
        Eigen::Vector3f right_tcpDesiredLinearVelocity = rtGetControlStruct().right_tcpDesiredLinearVelocity;
        Eigen::Vector3f left_tcpDesiredAngularError = rtGetControlStruct().left_tcpDesiredAngularError;
        Eigen::Vector3f right_tcpDesiredAngularError = rtGetControlStruct().right_tcpDesiredAngularError;
        Eigen::Vector3f left_right_distanceInMeter = rtGetControlStruct().left_right_distanceInMeter;

        double deltaT = timeSinceLastIteration.toSecondsDouble();


        // measure the sesor data for the other threads

        Eigen::Matrix4f leftSensorFrame = left_sensor_frame->getPoseInRootFrame();
        Eigen::Matrix4f rightSensorFrame = right_sensor_frame->getPoseInRootFrame();

        //    ARMARX_IMPORTANT << "left force offset: " << leftForceOffset;
        //    ARMARX_IMPORTANT << "right force offset: " << rightForceOffset;

        Eigen::Vector3f left_forceInRoot = leftSensorFrame.block<3, 3>(0, 0) * (leftForceTorque->force - leftForceOffset);
        Eigen::Vector3f right_forceInRoot = rightSensorFrame.block<3, 3>(0, 0) * (rightForceTorque->force - rightForceOffset);
        //    Eigen::Vector3f left_torqueInRoot = leftSensorFrame.block<3, 3>(0, 0) * leftForceTorque->torque;
        //    Eigen::Vector3f right_torqueInRoot = rightSensorFrame.block<3, 3>(0, 0) * rightForceTorque->torque;
        left_forceInRoot(2) = left_forceInRoot(2) + cfg->leftForceOffsetZ; // - 4 + 8.5;
        right_forceInRoot(2) = right_forceInRoot(2) + cfg->rightForceOffsetZ; // + 30 - 5.2;


        Eigen::Matrix4f left_currentTCPPose = left_arm_tcp->getPoseInRootFrame();
        Eigen::Matrix4f right_currentTCPPose = right_arm_tcp->getPoseInRootFrame();


        Eigen::MatrixXf left_jacobi = left_ik->getJacobianMatrix(left_arm_tcp, VirtualRobot::IKSolver::CartesianSelection::All);
        Eigen::MatrixXf right_jacobi = right_ik->getJacobianMatrix(right_arm_tcp, VirtualRobot::IKSolver::CartesianSelection::All);

        Eigen::VectorXf left_qpos;
        Eigen::VectorXf left_qvel;

        Eigen::VectorXf right_qpos;
        Eigen::VectorXf right_qvel;

        left_qpos.resize(left_positionSensors.size());
        left_qvel.resize(left_velocitySensors.size());

        right_qpos.resize(right_positionSensors.size());
        right_qvel.resize(right_velocitySensors.size());

        int jointDim = left_positionSensors.size();

        for (size_t i = 0; i < left_velocitySensors.size(); ++i)
        {
            left_qpos(i) = left_positionSensors[i]->position;
            left_qvel(i) = left_velocitySensors[i]->velocity;

            right_qpos(i) = right_positionSensors[i]->position;
            right_qvel(i) = right_velocitySensors[i]->velocity;
        }

        Eigen::VectorXf left_tcptwist = left_jacobi * left_qvel;
        Eigen::VectorXf right_tcptwist = right_jacobi * right_qvel;

        Eigen::Vector3f left_currentTCPLinearVelocity;
        Eigen::Vector3f right_currentTCPLinearVelocity;
        Eigen::Vector3f left_currentTCPAngularVelocity;
        Eigen::Vector3f right_currentTCPAngularVelocity;
        left_currentTCPLinearVelocity << 0.001 * left_tcptwist(0),  0.001 * left_tcptwist(1), 0.001 * left_tcptwist(2);
        right_currentTCPLinearVelocity << 0.001 * right_tcptwist(0),  0.001 * right_tcptwist(1), 0.001 * right_tcptwist(2);
        left_currentTCPAngularVelocity << left_tcptwist(3), left_tcptwist(4), left_tcptwist(5);
        right_currentTCPAngularVelocity << right_tcptwist(3), right_tcptwist(4), right_tcptwist(5);
        double filterFactor = deltaT / (filterTimeConstant + deltaT);
        left_currentTCPLinearVelocity_filtered  = (1 - filterFactor) * left_currentTCPLinearVelocity_filtered  + filterFactor * left_currentTCPLinearVelocity;
        right_currentTCPLinearVelocity_filtered = (1 - filterFactor) * right_currentTCPLinearVelocity_filtered + filterFactor * right_currentTCPLinearVelocity;
        left_currentTCPAngularVelocity_filtered = (1 - filterFactor) * left_currentTCPAngularVelocity_filtered + left_currentTCPAngularVelocity;
        right_currentTCPAngularVelocity_filtered = (1 - filterFactor) * right_currentTCPAngularVelocity_filtered + right_currentTCPAngularVelocity;
        left_jointVelocity_filtered  = (1 - filterFactor) * left_jointVelocity_filtered  + filterFactor * left_qvel;
        right_jointVelocity_filtered  = (1 - filterFactor) * right_jointVelocity_filtered  + filterFactor * right_qvel;

        // writing into the bufffer for other threads
        controllerSensorData.getWriteBuffer().left_tcpPose = left_currentTCPPose;
        controllerSensorData.getWriteBuffer().right_tcpPose = right_currentTCPPose;
        controllerSensorData.getWriteBuffer().left_force = left_forceInRoot;
        controllerSensorData.getWriteBuffer().right_force = right_forceInRoot;
        controllerSensorData.getWriteBuffer().currentTime += deltaT;
        controllerSensorData.commitWrite();




        // inverse dynamics:


        // reading the tcp orientation




        // computing the task-specific forces
        Eigen::Vector3f left_DS_force = -(1.2 * left_currentTCPLinearVelocity_filtered - forward_gain * left_tcpDesiredLinearVelocity);
        Eigen::Vector3f right_DS_force = -(1.2 * right_currentTCPLinearVelocity_filtered - forward_gain * right_tcpDesiredLinearVelocity);
        for (int i = 0; i < 3; ++i)
        {
            left_DS_force(i) = left_DS_force(i) * Damping[i];
            right_DS_force(i) = right_DS_force(i) * Damping[i];

            left_DS_force(i)  = deadzone(left_DS_force(i), 0.1, 100);
            right_DS_force(i)  = deadzone(right_DS_force(i), 0.1, 100);

        }

        // computing coupling forces
        Eigen::Vector3f coupling_force = - Coupling_stiffness * left_right_distanceInMeter;
        float coupling_force_norm = coupling_force.norm();

        if (coupling_force_norm > Coupling_force_limit)
        {
            coupling_force = (Coupling_force_limit / coupling_force_norm) * coupling_force;
        }

        coupling_force(0)  = deadzone(coupling_force(0), 0.1, 100);
        coupling_force(1)  = deadzone(coupling_force(1), 0.1, 100);
        coupling_force(2)  = deadzone(coupling_force(2), 0.1, 100);




        double v_both = left_currentTCPLinearVelocity_filtered.norm() + right_currentTCPLinearVelocity_filtered.norm();
        float force_contact_switch = 0;
        float left_height = left_currentTCPPose(2, 3) * 0.001;
        float right_height = right_currentTCPPose(2, 3) * 0.001;

        float disTolerance = cfg->contactDistanceTolerance;
        bool isUp =  fabs(left_height - guardTargetZUp) <  disTolerance && fabs(right_height - guardTargetZUp) < disTolerance;
        if (v_both < disTolerance && isUp)
        {
            force_contact_switch = 1;
        }
        else if (v_both < 0.05 &&  isUp)
        {
            force_contact_switch = (0.05 - v_both) / (0.05 - disTolerance);
        }
        else if (v_both >= 0.05)
        {
            force_contact_switch = 0;
        }

        // computing for contact forces
        float left_force_error = force_contact_switch * (-left_forceInRoot(2) -  contactForce);
        float right_force_error = force_contact_switch * (-right_forceInRoot(2) -  contactForce);

        //    float left_force_error_limited = left_force_error;
        //    float right_force_error_limited = right_force_error;

        //    left_force_error_limited = (left_force_error_limited >  2) ? 2 : left_force_error_limited;
        //    left_force_error_limited = (left_force_error_limited < -2) ? -2 : left_force_error_limited;

        //    right_force_error_limited = (right_force_error_limited >  2) ? 2 : right_force_error_limited;
        //    right_force_error_limited = (right_force_error_limited < -2) ? -2 : right_force_error_limited;


        left_force_error  = deadzone(left_force_error, 0.5, 2);
        right_force_error  = deadzone(right_force_error, 0.5, 2);

        left_contactForceZ_correction = left_contactForceZ_correction -  forceFilterCoeff * left_force_error;
        right_contactForceZ_correction = right_contactForceZ_correction -  forceFilterCoeff * right_force_error;

        left_contactForceZ_correction = (left_contactForceZ_correction > 30) ? 30 : left_contactForceZ_correction;
        right_contactForceZ_correction = (right_contactForceZ_correction > 30) ? 30 : right_contactForceZ_correction;

        left_contactForceZ_correction = (left_contactForceZ_correction < -30) ? -contactForce : left_contactForceZ_correction;
        right_contactForceZ_correction = (right_contactForceZ_correction < -30) ? -contactForce : right_contactForceZ_correction;

        Eigen::Vector3f left_tcpDesiredForce = left_DS_force + coupling_force;
        Eigen::Vector3f right_tcpDesiredForce = right_DS_force - coupling_force;

        left_tcpDesiredForce(2) += force_contact_switch * (contactForce + left_contactForceZ_correction);
        right_tcpDesiredForce(2) += force_contact_switch * (contactForce + right_contactForceZ_correction);



        Eigen::Vector3f left_tcpDesiredTorque = - oriKp * left_tcpDesiredAngularError - oriDamping * left_currentTCPAngularVelocity_filtered;
        Eigen::Vector3f right_tcpDesiredTorque = - oriKp * right_tcpDesiredAngularError - oriDamping * right_currentTCPAngularVelocity_filtered;

        //    for (int i = 0; i < left_tcpDesiredTorque.size(); ++i)
        //    {
        //        left_tcpDesiredTorque(i) = deadzone(left_tcpDesiredTorque(i), )

        //    }


        left_tcpDesiredTorque_filtered = (1 - filterFactor) * left_tcpDesiredTorque_filtered  + filterFactor * left_tcpDesiredTorque;
        right_tcpDesiredTorque_filtered = (1 - filterFactor) * right_tcpDesiredTorque_filtered  + filterFactor * right_tcpDesiredTorque;


        Eigen::Vector6f left_tcpDesiredWrench;
        Eigen::Vector6f right_tcpDesiredWrench;

        left_tcpDesiredWrench << 0.001 * left_tcpDesiredForce, left_tcpDesiredTorque_filtered;
        right_tcpDesiredWrench << 0.001 * right_tcpDesiredForce, right_tcpDesiredTorque_filtered;


        // calculate the null-spcae torque
        Eigen::MatrixXf I = Eigen::MatrixXf::Identity(jointDim, jointDim);

        float lambda = 0.2;
        Eigen::MatrixXf left_jtpinv = left_ik->computePseudoInverseJacobianMatrix(left_jacobi.transpose(), lambda);
        Eigen::MatrixXf right_jtpinv = right_ik->computePseudoInverseJacobianMatrix(right_jacobi.transpose(), lambda);


        Eigen::VectorXf left_nullqerror = left_qpos - left_qnullspace;
        Eigen::VectorXf right_nullqerror = right_qpos - right_qnullspace;

        for (int i = 0; i < left_nullqerror.size(); ++i)
        {
            if (fabs(left_nullqerror(i)) < 0.09)
            {
                left_nullqerror(i) = 0;
            }

            if (fabs(right_nullqerror(i)) < 0.09)
            {
                right_nullqerror(i) = 0;
            }
        }


        Eigen::VectorXf left_nullspaceTorque = - nullspaceKp * left_nullqerror - nullspaceDamping * left_jointVelocity_filtered;
        Eigen::VectorXf right_nullspaceTorque = - nullspaceKp * right_nullqerror - nullspaceDamping * right_jointVelocity_filtered;

        Eigen::VectorXf left_projectedNullTorque = (I - left_jacobi.transpose() * left_jtpinv) * left_nullspaceTorque;
        Eigen::VectorXf right_projectedNullTorque = (I - right_jacobi.transpose() * right_jtpinv) * right_nullspaceTorque;

        float left_nulltorque_norm = left_projectedNullTorque.norm();
        float right_nulltorque_norm = right_projectedNullTorque.norm();

        if (left_nulltorque_norm > null_torqueLimit)
        {
            left_projectedNullTorque = (null_torqueLimit / left_nulltorque_norm) * left_projectedNullTorque;
        }

        if (right_nulltorque_norm > null_torqueLimit)
        {
            right_projectedNullTorque = (null_torqueLimit / right_nulltorque_norm) * right_projectedNullTorque;
        }

        Eigen::VectorXf left_jointDesiredTorques = left_jacobi.transpose() * left_tcpDesiredWrench + left_projectedNullTorque;
        Eigen::VectorXf right_jointDesiredTorques = right_jacobi.transpose() * right_tcpDesiredWrench + right_projectedNullTorque;


        right_desiredTorques_filtered = (1 - cfg->TorqueFilterConstant) * right_desiredTorques_filtered + cfg->TorqueFilterConstant * right_jointDesiredTorques;


        for (size_t i = 0; i < left_torque_targets.size(); ++i)
        {
            float desiredTorque = smooth_startup * left_jointDesiredTorques(i);
            desiredTorque = deadzone(desiredTorque, cfg->desiredTorqueDisturbance, torqueLimit);
            left_desiredTorques_filtered(i) = (1 - cfg->TorqueFilterConstant) * left_desiredTorques_filtered(i) + cfg->TorqueFilterConstant * desiredTorque;

            std::string names = left_jointNames[i] + "_desiredTorques";
            debugDataInfo.getWriteBuffer().desired_torques[names] = desiredTorque;
            names = names + "_filtered";
            debugDataInfo.getWriteBuffer().desired_torques[names] = left_desiredTorques_filtered(i);

            if (fabs(left_desiredTorques_filtered(i)) > torqueLimit)
            {
                left_torque_targets.at(i)->torque = 0;
            }
            else
            {
                left_torque_targets.at(i)->torque = left_desiredTorques_filtered(i); // targets.at(i)->velocity = desiredVelocity;
            }
        }

        for (size_t i = 0; i < right_torque_targets.size(); ++i)
        {
            float desiredTorque = smooth_startup * right_jointDesiredTorques(i);
            desiredTorque = deadzone(desiredTorque, cfg->desiredTorqueDisturbance, torqueLimit);
            right_desiredTorques_filtered(i) = (1 - cfg->TorqueFilterConstant) * right_desiredTorques_filtered(i) + cfg->TorqueFilterConstant * desiredTorque;

            std::string names = right_jointNames[i] + "_desiredTorques";
            debugDataInfo.getWriteBuffer().desired_torques[names] = desiredTorque;
            names = names + "_filtered";
            debugDataInfo.getWriteBuffer().desired_torques[names] = right_desiredTorques_filtered(i);

            if (fabs(right_desiredTorques_filtered(i)) > torqueLimit)
            {
                right_torque_targets.at(i)->torque = 0;
            }
            else
            {
                right_torque_targets.at(i)->torque = right_desiredTorques_filtered(i); // targets.at(i)->velocity = desiredVelocity;
            }
        }

        smooth_startup = smooth_startup + 0.001 * (1.1  - smooth_startup);
        smooth_startup = (smooth_startup > 1) ? 1 : smooth_startup;
        smooth_startup = (smooth_startup < 0) ?  0 : smooth_startup;



        debugDataInfo.getWriteBuffer().left_realForce_x = left_forceInRoot(0);
        debugDataInfo.getWriteBuffer().left_realForce_y = left_forceInRoot(1);
        debugDataInfo.getWriteBuffer().left_realForce_z = left_forceInRoot(2);
        debugDataInfo.getWriteBuffer().right_realForce_x = right_forceInRoot(0);
        debugDataInfo.getWriteBuffer().right_realForce_y = right_forceInRoot(1);
        debugDataInfo.getWriteBuffer().right_realForce_z = right_forceInRoot(2);

        debugDataInfo.getWriteBuffer().left_force_error = left_force_error;
        debugDataInfo.getWriteBuffer().right_force_error = right_force_error;


        debugDataInfo.getWriteBuffer().left_tcpDesiredTorque_x = left_tcpDesiredTorque_filtered(0);
        debugDataInfo.getWriteBuffer().left_tcpDesiredTorque_y = left_tcpDesiredTorque_filtered(1);
        debugDataInfo.getWriteBuffer().left_tcpDesiredTorque_z = left_tcpDesiredTorque_filtered(2);
        debugDataInfo.getWriteBuffer().right_tcpDesiredTorque_x = right_tcpDesiredTorque_filtered(0);
        debugDataInfo.getWriteBuffer().right_tcpDesiredTorque_y = right_tcpDesiredTorque_filtered(1);
        debugDataInfo.getWriteBuffer().right_tcpDesiredTorque_z = right_tcpDesiredTorque_filtered(2);
        //        debugDataInfo.getWriteBuffer().desiredForce_x = tcpDesiredForce(0);
        //        debugDataInfo.getWriteBuffer().desiredForce_y = tcpDesiredForce(1);
        //        debugDataInfo.getWriteBuffer().desiredForce_z = tcpDesiredForce(2);


        //        debugDataInfo.getWriteBuffer().tcpDesiredTorque_x = tcpDesiredTorque(0);
        //        debugDataInfo.getWriteBuffer().tcpDesiredTorque_y = tcpDesiredTorque(1);
        //        debugDataInfo.getWriteBuffer().tcpDesiredTorque_z = tcpDesiredTorque(2);

        //        debugDataInfo.getWriteBuffer().tcpDesiredAngularError_x = tcpDesiredAngularError(0);
        //        debugDataInfo.getWriteBuffer().tcpDesiredAngularError_y = tcpDesiredAngularError(1);
        //        debugDataInfo.getWriteBuffer().tcpDesiredAngularError_z = tcpDesiredAngularError(2);

        //        debugDataInfo.getWriteBuffer().currentTCPAngularVelocity_x = currentTCPAngularVelocity(0);
        //        debugDataInfo.getWriteBuffer().currentTCPAngularVelocity_y = currentTCPAngularVelocity(1);
        //        debugDataInfo.getWriteBuffer().currentTCPAngularVelocity_z = currentTCPAngularVelocity(2);

        //        debugDataInfo.getWriteBuffer().currentTCPLinearVelocity_x = currentTCPLinearVelocity_filtered(0);
        //        debugDataInfo.getWriteBuffer().currentTCPLinearVelocity_y = currentTCPLinearVelocity_filtered(1);
        //        debugDataInfo.getWriteBuffer().currentTCPLinearVelocity_z = currentTCPLinearVelocity_filtered(2);

        debugDataInfo.commitWrite();

        //    }
        //    else
        //    {
        //        for (size_t i = 0; i < targets.size(); ++i)
        //        {
        //            targets.at(i)->torque = 0;

        //        }
        //    }



    }

    float DSRTBimanualController::deadzone(float input, float disturbance, float threshold)
    {
        if (input > 0)
        {
            input = input - disturbance;
            input = (input < 0) ? 0 : input;
            input = (input > threshold) ? threshold : input;
        }
        else if (input < 0)
        {
            input = input + disturbance;
            input = (input > 0) ? 0 : input;
            input = (input < -threshold) ? -threshold : input;
        }


        return input;
    }

    Eigen::Quaternionf DSRTBimanualController::quatSlerp(double t, const Eigen::Quaternionf& q0, const Eigen::Quaternionf& q1)
    {
        double cosHalfTheta = q0.w() * q1.w() + q0.x() * q1.x() + q0.y() * q1.y() + q0.z() * q1.z();


        Eigen::Quaternionf q1x = q1;
        if (cosHalfTheta < 0)
        {
            q1x.w() = -q1.w();
            q1x.x() = -q1.x();
            q1x.y() = -q1.y();
            q1x.z() = -q1.z();
        }


        if (fabs(cosHalfTheta) >= 1.0)
        {
            return q0;
        }

        double halfTheta = acos(cosHalfTheta);
        double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);


        Eigen::Quaternionf result;
        if (fabs(sinHalfTheta) < 0.001)
        {
            result.w() = (1 - t) * q0.w() + t * q1x.w();
            result.x() = (1 - t) * q0.x() + t * q1x.x();
            result.y() = (1 - t) * q0.y() + t * q1x.y();
            result.z() = (1 - t) * q0.z() + t * q1x.z();

        }
        else
        {
            double ratioA = sin((1 - t) * halfTheta) / sinHalfTheta;
            double ratioB = sin(t * halfTheta) / sinHalfTheta;

            result.w() = ratioA * q0.w() + ratioB * q1x.w();
            result.x() = ratioA * q0.x() + ratioB * q1x.x();
            result.y() = ratioA * q0.y() + ratioB * q1x.y();
            result.z() = ratioA * q0.z() + ratioB * q1x.z();
        }

        return result;
    }

    void DSRTBimanualController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& debugObs)
    {

        StringVariantBaseMap datafields;
        auto values = debugDataInfo.getUpToDateReadBuffer().desired_torques;
        for (auto& pair : values)
        {
            datafields[pair.first] = new Variant(pair.second);
        }

        //    std::string nameJacobi = "jacobiori";
        //    std::string nameJacobipos = "jacobipos";

        //    std::vector<float> jacobiVec = debugDataInfo.getUpToDateReadBuffer().jacobiVec;
        //    std::vector<float> jacobiPos = debugDataInfo.getUpToDateReadBuffer().jacobiPos;

        //    for (size_t i = 0; i < jacobiVec.size(); ++i)
        //    {
        //        std::stringstream ss;
        //        ss << i;
        //        std::string name = nameJacobi + ss.str();
        //        datafields[name] = new Variant(jacobiVec[i]);
        //        std::string namepos = nameJacobipos + ss.str();
        //        datafields[namepos] = new Variant(jacobiPos[i]);

        //    }


        datafields["left_tcpDesiredTorque_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_tcpDesiredTorque_x);
        datafields["left_tcpDesiredTorque_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_tcpDesiredTorque_y);
        datafields["left_tcpDesiredTorque_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_tcpDesiredTorque_z);
        datafields["right_tcpDesiredTorque_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_tcpDesiredTorque_x);
        datafields["right_tcpDesiredTorque_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_tcpDesiredTorque_y);
        datafields["right_tcpDesiredTorque_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_tcpDesiredTorque_z);

        datafields["left_realForce_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_realForce_x);
        datafields["left_realForce_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_realForce_y);
        datafields["left_realForce_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_realForce_z);
        datafields["right_realForce_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_realForce_x);
        datafields["right_realForce_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_realForce_y);
        datafields["right_realForce_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_realForce_z);

        datafields["left_force_error"] = new Variant(debugDataInfo.getUpToDateReadBuffer().left_force_error);
        datafields["right_force_error"] = new Variant(debugDataInfo.getUpToDateReadBuffer().right_force_error);


        datafields["Desired_Guard_Z"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().desredZ);
        datafields["Force_Error_Z"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().force_error_z);
        datafields["guardXYHighFrequency"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().guardXYHighFrequency);
        datafields["guard_mounting_correction_x"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().guard_mounting_correction_x);
        datafields["guard_mounting_correction_y"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().guard_mounting_correction_y);
        datafields["guard_mounting_correction_z"] = new Variant(debugCtrlDataInfo.getUpToDateReadBuffer().guard_mounting_correction_z);



        //    datafields["desiredForce_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().desiredForce_x);
        //    datafields["desiredForce_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().desiredForce_y);
        //    datafields["desiredForce_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().desiredForce_z);

        //    datafields["tcpDesiredTorque_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredTorque_x);
        //    datafields["tcpDesiredTorque_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredTorque_y);
        //    datafields["tcpDesiredTorque_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredTorque_z);

        //    datafields["tcpDesiredAngularError_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredAngularError_x);
        //    datafields["tcpDesiredAngularError_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredAngularError_y);
        //    datafields["tcpDesiredAngularError_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().tcpDesiredAngularError_z);

        //    datafields["currentTCPAngularVelocity_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPAngularVelocity_x);
        //    datafields["currentTCPAngularVelocity_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPAngularVelocity_y);
        //    datafields["currentTCPAngularVelocity_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPAngularVelocity_z);


        //    datafields["currentTCPLinearVelocity_x"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPLinearVelocity_x);
        //    datafields["currentTCPLinearVelocity_y"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPLinearVelocity_y);
        //    datafields["currentTCPLinearVelocity_z"] = new Variant(debugDataInfo.getUpToDateReadBuffer().currentTCPLinearVelocity_z);


        debugObs->setDebugChannel("DSBimanualControllerOutput", datafields);

    }

    void DSRTBimanualController::rtPreActivateController()
    {

    }

    void DSRTBimanualController::rtPostDeactivateController()
    {

    }

    void DSRTBimanualController::setToDefaultTarget(const Ice::Current&)
    {
        isDefaultTarget = true;
    }
}
