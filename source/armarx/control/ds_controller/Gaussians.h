/*
 * Gaussians.h
 *
 *  Created on: Nov 19, 2011
 *      Author: Seungsu KIM
 */

#ifndef __GAUSSIANSM_H__
#define __GAUSSIANSM_H__

#include "MathLib.h"

#define GAUSSIAN_MAXIMUM_NUMBER 50

struct GMMState
{
    MathLib::Vector Mu;
    MathLib::Matrix Sigma;
    double Prio;
};

struct GMMStateP
{
    MathLib::Vector MuI;
    MathLib::Matrix SigmaII;
    MathLib::Matrix SigmaIIInv;
    double detSigmaII;

    // for GMR
    MathLib::Vector muO;
    MathLib::Matrix SigmaIO;
    MathLib::Matrix SigmaIOInv;
};

struct GMMs
{
    unsigned int nbStates;
    unsigned int nbDim;

    GMMState States[GAUSSIAN_MAXIMUM_NUMBER];
};

class Gaussians
{
private:
    GMMStateP gmmpinv[GAUSSIAN_MAXIMUM_NUMBER];

public:
    GMMs model;

    Gaussians(const char* f_mu, const char* f_sigma, const char* f_prio);
    Gaussians(int nbStates, int nbDim, const char* f_mu, const char* f_sigma, const char* f_prio);
    Gaussians(const int nbStates,
              const int nbDim,
              const vector<double> pri_vec,
              const vector<double> mu_vec,
              const vector<double> sig_vec);
    Gaussians(GMMs* model);

    void setGMMs(GMMs* model);

    // For fast computation of GaussianPDF
    MathLib::Vector gfDiff, gfDiffp;
    MathLib::Vector gDer;
    MathLib::Vector gPdf;
    int nbDimI;


    void InitFastGaussians(int first_inindex, int last_inindex);
    double GaussianPDFFast(int state, MathLib::Vector x);
    double GaussianProbFast(MathLib::Vector x);
    MathLib::Vector GaussianDerProbFast(MathLib::Vector x);

    void InitFastGMR(int first_inindex, int last_inindex, int first_outindex, int last_outindex);
    void
    Regression(const MathLib::Vector& indata, MathLib::Vector& outdata, MathLib::Matrix& derGMR);
    void Regression(const MathLib::Vector& indata, MathLib::Vector& outdata);
    MathLib::Vector Regression(const MathLib::Vector& indata);
};
/*
void GaussianMux(GMMs *modelK, GMMs *modelL, GMMs *modelOut);
void GaussianRotate(GMMs *model, Vector P, Matrix R, GMMs *modelOut);
*/

#endif //__GAUSSIANS_H__
