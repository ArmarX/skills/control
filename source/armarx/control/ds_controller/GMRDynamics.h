/*
 * GMRDynamics.h
 *
 *  Created on: Nov 20, 2011
 *      Author: Seungsu KIM
 */

#ifndef __GMRDYNAMICS_H__
#define __GMRDYNAMICS_H__

#include "Gaussians.h"
#include <boost/shared_ptr.hpp>

#define GMR_ERROR_TOLERANCE 0.001
#define INTEGRATION_L 5
#define REACHING_ITERATION_MAX 500
#define REAL_MIN (1e-30)

// GMR Dynamics
class GMRDynamics
{
private:
    Gaussians* GMM;

    double delta_t;
    double target_t;
    double current_t;

    MathLib::Vector gXi;
    MathLib::Vector target;
    unsigned int gDim;

public:
    GMRDynamics(int nStates, int nVar, double delta_t, const char* f_mu, const char* f_sigma, const char* f_prio);
    GMRDynamics(int nStates, int nVar, double delta_t, const vector<double> pri_vec, const vector<double> mu_vec, const vector<double> sig_vec);

    void initGMR(int first_inindex, int last_inindex, int first_outindex, int last_outindex);

    void   setStateTarget(MathLib::Vector state, MathLib::Vector target);
    void   setTarget(MathLib::Vector target, double target_t = -1.0);
    MathLib::Vector getTarget(void);
    double getTargetT(void);
    void   setState(MathLib::Vector state);
    MathLib::Vector getState(void);
    void   setCurrentTime(double current_t);
    double getCurrentTime(void);

    MathLib::Vector getVelocity(MathLib::Vector x);

    MathLib::Vector getNextState(void);
    MathLib::Vector getNextState(double lamda);
    double getReachingTime(double lamda);
};

typedef boost::shared_ptr<GMRDynamics> GMMPtr;

#endif //__GMRDYNAMICS_H__
