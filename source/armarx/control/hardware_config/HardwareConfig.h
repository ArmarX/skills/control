#pragma once

#include <map>

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace armarx::control::hardware_config
{
    class DeviceConfig;

    /**
     * @brief Root of the config structure.
     */
    class HardwareConfig
    {

    public:
        HardwareConfig();

        DeviceConfig& getDeviceConfig(std::string name);

        /**
         * @brief Print a representation of the entire structure
         * @param stream
         * @param rhs   the HardwareConfig structure to be printed
         * @return
         */
        friend std::ostream&
        operator<<(std::ostream& stream, const HardwareConfig& rhs)
        {
            std::stringstream s;
            for (auto& dev : rhs.deviceConfigs)
            {
                dev.second->print(s, 0);
            }
            stream << s.str();
            return stream;
        }

        /**
         * @brief This method is called when the config is completely read form the HardwareConfig file.
         * It starts the tracking of which item is read from this Config object.
         */
        void onParsingFinished();

        /**
         * @brief Add a DeviceConfig
         * @param name      the DeviceConfig name
         * @param config    the DeviceConfig object
         */
        void addDeviceConfig(const std::string name, std::shared_ptr<DeviceConfig> config);

        /**
         * @brief This method is called when the config is completely read form the HardwareConfig file.
         * It starts the tracking of which item is read from this Config object.
         */
        bool checkAllItemsRead(std::vector<std::string>& errors) const;

        std::map<std::string, std::shared_ptr<DeviceConfig>> deviceConfigs;
    };
} // namespace armarx::control::hardware_config
