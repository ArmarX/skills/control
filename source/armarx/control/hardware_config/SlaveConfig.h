#pragma once

#include <armarx/control/hardware_config/Config.h>
#include <armarx/control/hardware_config/SlaveIdentifierConfig.h>

namespace armarx::control::hardware_config
{
    class SlaveProfile : public Config
    {
        friend class SlaveConfig;

    public:
        SlaveProfile(SlaveIdentifierConfig identifier, std::string type, std::string name) :
            type(type), name(name), identifier(identifier)
        {
        }
        std::string getName();
        std::string getType();
        SlaveIdentifierConfig& getIdentifier();

    private:
        std::string type;
        std::string name;
        SlaveIdentifierConfig identifier;
    };

    class SlaveConfig : public Config
    {
    public:
        SlaveConfig(SlaveIdentifierConfig identifier,
                    std::optional<std::string> type,
                    std::optional<std::string> name);

        SlaveConfig(SlaveIdentifierConfig identifier,
                    std::optional<std::string> type,
                    std::optional<std::string> name,
                    std::shared_ptr<SlaveProfile> profile);

        virtual ~SlaveConfig() = default;

        SlaveIdentifierConfig& getIdentifier();

        std::optional<std::string> getType() const;

        std::optional<std::string> getName() const;

        bool checkAllItemsRead(std::vector<std::string>& errors) const override;

        void print(std::stringstream& s, int indention) const override;

        void onParsingFinished() override;

    private:
        SlaveIdentifierConfig identifier;
        std::optional<std::string> type;
        std::optional<std::string> name;
        bool identifierRead = false;
    };

    enum SlaveType
    {
        WithType,
        WithoutType
    };
} // namespace armarx::control::hardware_config
