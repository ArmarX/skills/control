#include "Config.h"

namespace armarx::control::hardware_config
{
    Config::Config()
    {
    }

    bool
    Config::getBool(const std::string name)
    {
        return get<bool>(name);
    }

    float
    Config::getFloat(const std::string name)
    {
        return get<float>(name);
    }

    std::uint32_t
    Config::getUint(const std::string name)
    {
        return get<std::uint32_t>(name);
    }

    std::int32_t
    Config::getInt(const std::string name)
    {
        return get<std::int32_t>(name);
    }

    types::LinearConfig
    Config::getLinearConfig(const std::string name)
    {
        return get<types::LinearConfig>(name);
    }

    types::ModularConvertedValueConfig
    Config::getModularConvertedValueConfig(const std::string& name)
    {
        return get<types::ModularConvertedValueConfig>(name);
    }

    std::string
    Config::getString(const std::string name)
    {
        return get<std::string>(name);
    }

    std::list<bool>
    Config::getBoolList(const std::string name)
    {
        return get<std::list<bool>>(name);
    }

    std::list<float>
    Config::getFloatList(const std::string name)
    {
        return get<std::list<float>>(name);
    }

    std::list<std::uint32_t>
    Config::getUintList(const std::string name)
    {
        return get<std::list<std::uint32_t>>(name);
    }

    std::list<std::int32_t>
    Config::getIntList(const std::string name)
    {
        return get<std::list<std::int32_t>>(name);
    }

    bool
    Config::checkAllItemsRead(std::vector<std::string>& errors) const
    {
        bool succ = true;
        for (auto& i : items)
        {
            if (!i.second.isRead)
            {
                errors.push_back(createErrorMessageWithContext("Configuration element " + i.first +
                   " was not expected by the Device code but defined in HardwareConfig.", *this));
            }
        }
        return succ;
    }

    // helper type for the visitor #4
    template <class... Ts>
    struct overloaded : Ts...
    {
        using Ts::operator()...;
    };
    // explicit deduction guide (not needed as of C++20)
    template <class... Ts>
    overloaded(Ts...) -> overloaded<Ts...>;

    template <class T>
    void
    printList(std::stringstream& s, std::list<T> list)
    {
        s << "{";
        unsigned int i = 0;
        for (auto& l : list)
        {
            i++;
            s << l;
            if (i != list.size())
            {
                s << ", ";
            }
        }
        s << "}" << std::endl;
    }

    void
    Config::print(std::stringstream& s, int indention) const
    {
        for (auto& c : items)
        {
            for (int i = 0; i < indention; i++)
            {
                s << "\t";
            }
            s << c.first << " (";
            std::visit(
                overloaded{[&s](bool arg)
                           {
                               s << "Bool): ";
                               s << arg << std::endl;
                           },
                           [&s](float arg)
                           {
                               s << "Float): ";
                               s << arg << std::endl;
                           },
                           [&s](std::uint32_t arg)
                           {
                               s << "UInt): ";
                               s << arg << std::endl;
                           },
                           [&s](std::int32_t arg)
                           {
                               s << "Int): ";
                               s << arg << std::endl;
                           },
                           [&s](auto arg)
                           {
                               s << "?): ";
                               s << arg << std::endl;
                           },
                           [&s](types::LinearConfig arg)
                           {
                               s << "LinearConvertedValue): factor=" << arg.getFactor()
                                 << " offset=" << arg.getOffset() << std::endl;
                           },
                           [&s](types::ModularConvertedValueConfig arg)
                           {
                               s << "ModularConvertedValueConfig): factor=" << arg.getFactor()
                                 << " zeroOffset=" << arg.getZeroOffset() << std::endl;
                           },
                           [&s](std::string arg)
                           {
                               s << "String): ";
                               s << arg << std::endl;
                           },
                           [&s](std::list<bool> arg)
                           {
                               s << "BoolList): ";
                               printList(s, arg);
                           },
                           [&s](std::list<float> arg)
                           {
                               s << "FloatList): ";
                               printList(s, arg);
                           },
                           [&s](std::list<std::uint32_t> arg)
                           {
                               s << "UIntList): ";
                               printList(s, arg);
                           },
                           [&s](std::list<std::int32_t> arg)
                           {
                               s << "IntList): ";
                               printList(s, arg);
                           },
                           [&s](Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> arg) {
                               s << "Matrix<Float>):" << std::endl << arg << std::endl;
                           },
                           [&s](Eigen::Matrix<std::uint32_t, Eigen::Dynamic, Eigen::Dynamic> arg) {
                               s << "Matrix<UInt>):" << std::endl << arg << std::endl;
                           },
                           [&s](Eigen::Matrix<std::int32_t, Eigen::Dynamic, Eigen::Dynamic> arg) {
                               s << "Matrix<Int>):" << std::endl << arg << std::endl;
                           }},
                c.second.data);
        } // namespace armarx::control::hardware_config
    }

    void
    Config::setBool(const std::string name, bool value, ConfigTag tag)
    {
        set<bool>(name, value, tag);
    }

    void
    Config::setFloat(const std::string name, float value, ConfigTag tag)
    {
        set<float>(name, value, tag);
    }

    void
    Config::setUint(const std::string name, std::uint32_t value, ConfigTag tag)
    {
        set<std::uint32_t>(name, value, tag);
    }

    void
    Config::setInt(const std::string name, std::int32_t value, ConfigTag tag)
    {
        set<std::int32_t>(name, value, tag);
    }

    void
    Config::setLinearConfig(const std::string name, types::LinearConfig&& value, ConfigTag tag)
    {
        set<types::LinearConfig>(name, value, tag);
    }

    void
    Config::setModularConvertedValueConfig(const std::string name, types::ModularConvertedValueConfig&& value, ConfigTag tag)
    {
        set<types::ModularConvertedValueConfig>(name, value, tag);
    }

    void
    Config::setString(const std::string name, std::string value, ConfigTag tag)
    {
        set<std::string>(name, value, tag);
    }

    void
    Config::setBoolList(const std::string name, std::list<bool> value, ConfigTag tag)
    {
        set<std::list<bool>>(name, value, tag);
    }

    void
    Config::setFloatList(const std::string name, std::list<float> value, ConfigTag tag)
    {
        set<std::list<float>>(name, value, tag);
    }

    void
    Config::setUintList(const std::string name, std::list<std::uint32_t> value, ConfigTag tag)
    {
        set<std::list<std::uint32_t>>(name, value, tag);
    }

    void
    Config::setIntList(const std::string name, std::list<std::int32_t> value, ConfigTag tag)
    {
        set<std::list<std::int32_t>>(name, value, tag);
    }


    Config&
    ControllerConfig::getControllerConfig(const std::string controllerName)
    {
        auto it = controllerConfigs.find(controllerName);
        if (it != controllerConfigs.end())
        {
            return *it->second;
        }
        throw ConfigElementNotFoundError("Controller with name: " + controllerName);
    }

    void
    ControllerConfig::addControllerConfig(const std::string name,
                                          std::shared_ptr<Config> controllerConfig)
    {
        controllerConfigs[name] = controllerConfig;
    }

    ControllerConfig::ControllerConfig(const ControllerConfig& orig)
    {
        for (auto& pair : orig.controllerConfigs)
        {
            // Work around private copy constructor
            Config* copied = new Config(*pair.second);
            this->addControllerConfig(pair.first, std::shared_ptr<Config>(copied));
        }
    }

    std::string
    tagName(ConfigTag tag)
    {
        switch (tag)
        {
            case TagProfile:
                return "Profile";
            case TagCustom:
                return "Custom";
            default:
                return "";
        }
    }

    void
    Config::onParsingFinished()
    {
        // Reset isRead tags
        for (auto& i : items)
        {
            i.second.isRead = false;
        }
    }

    std::string createErrorMessageWithContext(std::string message, const Config& context)
    {
        std::stringstream ss;
        ss << message
           << std::endl << "Context:" << std::endl;
        context.print(ss, 0);
        return ss.str();
    }
} // namespace armarx::control::hardware_config
