#define BOOST_TEST_MODULE Armarx::Control::Hardwareconfig

#define ARMARX_BOOST_TEST

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/control/Test.h>
#include <armarx/control/hardware_config/ConfigParser.h>

using namespace armarx::control::hardware_config;

BOOST_AUTO_TEST_CASE(formatTest)
{

    BOOST_CHECK_EQUAL(true, true);

    std::string sampleXml = R""""(
<Profiles>
    <Slave type="Elmo" profilename="s1">
        <Config>
            <Float name="loLimit">-1.8</Float>
            <Float name="hiLimit">1.8</Float>
            <Bool name="limitless">false</bool>
        </Config>
    </Slave>
</Profiles>
<Device type="KITHandV2" name="RightHand">
    <Slave>
        <Identifier>
            <VendorID>0x7BC</VendorID>
            <ProductID>0x403</ProductID>
            <Serial>0</Serial>
        </Identifier>
        <Config>
            <LinearConvertedValue name="motorTemperature" factor="1" offset="0"/>
            <LinearConvertedValue name="relativeEncoderFingers" factor="0.0000035" offset="0"/>
            <LinearConvertedValue name="relativeEncoderThumb" factor="0.0000070" offset="0"/>
            <LinearConvertedValue name="motorCurrentFingers" factor="1" offset="0"/>
            <LinearConvertedValue name="motorCurrentThumb" factor="1" offset="0"/>
        </Config>
    </Slave>
    <Subdevice name="Fingers" type="finger">
        <Config>
            <Int name="maxPwm">82</Int>
            <Float name="Kp">20</Float>
        </Config>
    </Subdevice>
    <Subdevice name="Thumb" type="finger">
        <Config>
            <Int name="maxPwm">82</Int>
            <Float name="Kp">10</Float>
        </Config>
    </Subdevice>
</Device>
<Device type="Joint" name="ArmL1_Cla1">
    <Slave type="Elmo" profile="s1">
        <Identifier>
            <VendorID>0x9A</VendorID>
            <ProductID>0x30924</ProductID>
            <Serial>16282079</Serial>
        </Identifier>
        <Config>
            <LinearConvertedValue name="velocity" factor="-0.000006818" offset="0.0"/>
            <Float name="loLimit">-1.39</Float>
        </Config>
    </Slave>
    <Slave type="SensorBoard">
        <Identifier>
            <VendorID>0x7BC</VendorID>
            <ProductID>0x103</ProductID>
            <Serial>0</Serial>
        </Identifier>
        <Config>
            <LinearConvertedValue name="position" factor="-0.000011984" offset="-119882.179"/>
            <LinearConvertedValue name="actualTorque" factor="0.00000050414692322706" offset="-119882.179"/>
            <IntList name="IMUCalibration">65525 65508 9 0 0 0 0 0 0 1000 0</IntList>
        </Config>
    </Slave>
    <Controller type="TorqueController">
        <Float name="inertia">0.0</Float>
        <Float name="scalePI">0.3</Float>
        <Float name="scaleP">0.0</Float>
        <Float name="Kd">3</Float>
        <Float name="maxTorque">8</Float>
        <Float name="actuatorType">1</Float>
    </Controller>
    <Controller type="ZeroTorqueController">
        <Int name="actuatorType">1</Int>
        <Float name="pid_proportional_gain">0.1</Float>
        <Float name="pid_Integral_gain">4</Float>
        <Float name="deadZone">3</Float>
        <Float name="decay">0.003</Float>
        <Float name="maxAcceleration">5</Float>
    </Controller>
    <Controller type="PositionContoller"/>
    <Controller type="VelocityController"/>
</Device>
    )"""";

    armarx::RapidXmlReaderPtr reader = armarx::RapidXmlReader::FromXmlString(sampleXml);

    armarx::RapidXmlReaderNode documentNode = reader->getDocument();

    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    std::vector<std::string> errors;
    config.checkAllItemsRead(errors);
}

BOOST_AUTO_TEST_CASE(slaveProfileTest)
{
    std::string sampleXml = R""""(
<Profiles>
    <Slave type="Elmo" profilename="s1">
        <Identifier>
            <VendorID>0x9A</VendorID>
            <ProductID>0x30924</ProductID>
        </Identifier>
        <Config>
            <Float name="loLimit">-1.8</Float>
            <Float name="hiLimit">1.8</Float>
            <Bool name="limitless">false</bool>
        </Config>
    </Slave>
</Profiles>
<Device type="Joint" name="ArmL1_Cla1">
    <Slave type="Elmo" profile="s1">
        <Identifier>
            <Serial>16282079</Serial>
        </Identifier>
        <Config>
            <LinearConvertedValue name="velocity" factor="-0.000006818" offset="0.0"/>
            <Float name="loLimit">-1.39</Float>
        </Config>
    </Slave>
</Device>
<Device type="Joint" name="ArmL1_Cla2">
    <Slave type="Elmo">
        <Identifier>
            <VendorID>0x9A</VendorID>
            <ProductID>0x30924</ProductID>
            <Serial>16282080</Serial>
        </Identifier>
        <Config>
            <LinearConvertedValue name="position" factor="-0.000011984" offset="-119882.179"/>
            <LinearConvertedValue name="actualTorque" factor="0.00000050414692322706" offset="-119882.179"/>
            <IntList name="IMUCalibration">65525 65508 9 0 0 0 0 0 0 1000 0</IntList>
        </Config>
    </Slave>
</Device>
        )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("ArmL1_Cla1");
    auto& slaveConfig = deviceConfig.getSlaveConfig("Elmo");

    // Check if profile has been sucessfully used and the entries are properly overwritten
    BOOST_CHECK_CLOSE(slaveConfig.getFloat("loLimit"), -1.39, .001);
    BOOST_CHECK_CLOSE(slaveConfig.getFloat("hiLimit"), 1.8, .001);
    BOOST_CHECK_EQUAL(slaveConfig.getBool("limitless"), false);

    auto& deviceConfig2 = config.getDeviceConfig("ArmL1_Cla2");
    auto& slaveConfig2 = deviceConfig2.getSlaveConfig("Elmo");
    // Check other "Elmo" slave does not have the profile applied
    BOOST_CHECK_EXCEPTION(slaveConfig2.getFloat("loLimit"),
                          ConfigElementNotFoundError,
                          [](ConfigElementNotFoundError) { return true; });

    // Check identifier in profile
    auto identifier = slaveConfig.getIdentifier();
    BOOST_CHECK_EQUAL(identifier.getVendorID(), 0x9a);
    BOOST_CHECK_EQUAL(identifier.getProductCode(), 0x30924);
    BOOST_CHECK_EQUAL(identifier.getSerialNumber(), 16282079);
}


BOOST_AUTO_TEST_CASE(deviceProfileTest)
{
    std::string sampleXml = R""""(
<Profiles>
    <Device type="Joint" profilename="testprofile">
        <Controller type="TorqueController">
            <Float name="inertia">0.0</Float>
            <Float name="scalePI">0.3</Float>
            <Float name="scaleP">0.0</Float>
            <Float name="Kd">5</Float>
            <Float name="maxTorque">8</Float>
            <Float name="actuatorType">1</Float>
        </Controller>
        <Config>
            <Bool name="tb">true</Bool>
        </Config>
    </Device>
</Profiles>
<Device type="Joint" name="ArmL1_Cla1" profile="testprofile">
    <Slave type="SensorBoard">
        <Identifier>
            <VendorID>0x7BC</VendorID>
            <ProductID>0x103</ProductID>
            <Serial>0</Serial>
        </Identifier>
    </Slave>
    <Config>
        <Bool name="tb">false</Bool>
    </Config>
    <Controller type="TorqueController">
        <Float name="Kd">3</Float>
    </Controller>
    <Controller type="ZeroTorqueController">
        <Int name="actuatorType">1</Int>
    </Controller>
    <Controller type="PositionContoller"/>
    <Controller type="VelocityController"/>
</Device>
        )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("ArmL1_Cla1");
    auto& torque = deviceConfig.getControllerConfig("TorqueController");

    BOOST_CHECK_CLOSE(torque.getFloat("Kd"), 3, 0.00001);
    BOOST_CHECK_CLOSE(torque.getFloat("scalePI"), 0.3, 0.00001);
    BOOST_CHECK_EQUAL(deviceConfig.getBool("tb"), false);
}

BOOST_AUTO_TEST_CASE(listParserTest)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x103</ProductID>
                <Serial>0</Serial>
            </Identifier>
        </Slave>
        <Config>
            <BoolList name="blist">false true false false true true true false</BoolList>
            <FloatList name="flist">3.5 6.8 4.2e-10 5 1.0</FloatList>
            <IntList name="ilist">1 2 3 4 5 6 -1 -2 -3</IntList>
            <UIntList name="ulist">1 2 3 4 5 6 7 8</UIntList>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("hmmm");

    auto bList = deviceConfig.getBoolList("blist");
    auto bComp = std::list<bool>{false, true, false, false, true, true, true, false};
    BOOST_CHECK_EQUAL_COLLECTIONS(bList.begin(), bList.end(), bComp.begin(), bComp.end());

    auto fList = deviceConfig.getFloatList("flist");
    auto fComp = std::list<float>{3.5, 6.8, 4.2e-10, 5, 1};
    BOOST_CHECK_EQUAL_COLLECTIONS(fList.begin(), fList.end(), fComp.begin(), fComp.end());

    auto iList = deviceConfig.getIntList("ilist");
    auto iComp = std::list<std::int32_t>{1, 2, 3, 4, 5, 6, -1, -2, -3};
    BOOST_CHECK_EQUAL_COLLECTIONS(iList.begin(), iList.end(), iComp.begin(), iComp.end());

    auto uList = deviceConfig.getUintList("ulist");
    auto uComp = std::list<std::uint32_t>{1, 2, 3, 4, 5, 6, 7, 8};
    BOOST_CHECK_EQUAL_COLLECTIONS(uList.begin(), uList.end(), uComp.begin(), uComp.end());
}

BOOST_AUTO_TEST_CASE(matrixParserTest)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x103</ProductID>
                <Serial>0</Serial>
            </Identifier>
        </Slave>
        <Config>
            <FloatMatrix name="fmat" rows="2" columns="3">3.5 6.8 4.2e-10 5 1.0 4</FloatMatrix>
            <IntMatrix name="imat" rows="9" columns="1">1 2 3 4 5 6 -1 -2 -3</IntMatrix>
            <UIntMatrix name="umat" rows="1" columns="8">1 2 3 4 5 6 7 8</UIntMatrix>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("hmmm");

    auto fMat = deviceConfig.getMatrix<float, 2, 3>("fmat");
    Eigen::MatrixXf fComp(2, 3);
    fComp(0, 0) = 3.5;
    fComp(0, 1) = 6.8;
    fComp(0, 2) = 4.2e-10;
    fComp(1, 0) = 5;
    fComp(1, 1) = 1;
    fComp(1, 2) = 4;
    BOOST_CHECK_CLOSE(fMat(0, 0), 3.5, 0.00001);
    BOOST_CHECK_CLOSE(fMat(0, 1), 6.8, 0.00001);
    BOOST_CHECK_CLOSE(fMat(0, 2), 4.2e-10, 0.00001);
    BOOST_CHECK_CLOSE(fMat(1, 0), 5, 0.00001);
    BOOST_CHECK_CLOSE(fMat(1, 1), 1, 0.00001);
    BOOST_CHECK_CLOSE(fMat(1, 2), 4, 0.00001);

    auto iMat = deviceConfig.getMatrix<std::int32_t, 9, 1>("imat");
    BOOST_CHECK_EQUAL(iMat(0, 0), 1);
    BOOST_CHECK_EQUAL(iMat(1, 0), 2);
    BOOST_CHECK_EQUAL(iMat(2, 0), 3);
    BOOST_CHECK_EQUAL(iMat(3, 0), 4);
    BOOST_CHECK_EQUAL(iMat(4, 0), 5);
    BOOST_CHECK_EQUAL(iMat(5, 0), 6);
    BOOST_CHECK_EQUAL(iMat(6, 0), -1);
    BOOST_CHECK_EQUAL(iMat(7, 0), -2);
    BOOST_CHECK_EQUAL(iMat(8, 0), -3);

    auto uMat = deviceConfig.getMatrix<std::uint32_t, 1, 8>("umat");
    BOOST_CHECK_EQUAL(uMat(0, 0), 1);
    BOOST_CHECK_EQUAL(uMat(0, 1), 2);
    BOOST_CHECK_EQUAL(uMat(0, 2), 3);
    BOOST_CHECK_EQUAL(uMat(0, 3), 4);
    BOOST_CHECK_EQUAL(uMat(0, 4), 5);
    BOOST_CHECK_EQUAL(uMat(0, 5), 6);
    BOOST_CHECK_EQUAL(uMat(0, 6), 7);
    BOOST_CHECK_EQUAL(uMat(0, 7), 8);
}

BOOST_AUTO_TEST_CASE(matrixParserTestIncorrectMatrices)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x103</ProductID>
                <Serial>0</Serial>
            </Identifier>
        </Slave>
        <Config>
            <FloatMatrix name="fmat" rows="2" columns="3">3.5 6.8 4.2e-10 5 1.0 4</FloatMatrix>
            <IntMatrix name="imat" rows="9" columns="1">1 2 3 4 5 6 -1 -2</IntMatrix>
            <UIntMatrix name="umat" rows="1" columns="8">1 2 3 4 5 6 7 8 9</UIntMatrix>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);


    BOOST_REQUIRE_EXCEPTION(parser.parse(), ParserError, [](ParserError e) { return true; });

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("hmmm");

    try
    {
        deviceConfig.getMatrix<float, 2, 2>("fmat");
        BOOST_FAIL("ConfigElementNotFoundError expected but not thrown");
    }
    catch (ConfigElementNotFoundError& e)
    {
        // Desired case
    }

    try
    {
        deviceConfig.getMatrix<std::int32_t, 2, 2>("imat");
        BOOST_FAIL("ConfigElementNotFoundError expected but not thrown");
    }
    catch (ConfigElementNotFoundError& e)
    {
        // Desired case
    }

    try
    {
        deviceConfig.getMatrix<std::uint32_t, 2, 2>("umat");
        BOOST_FAIL("ConfigElementNotFoundError expected but not thrown");
    }
    catch (ConfigElementNotFoundError& e)
    {
        // Desired case
    }
}

BOOST_AUTO_TEST_CASE(allReadCheck)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x103</ProductID>
                <Serial>0</Serial>
            </Identifier>
        </Slave>
        <Config>
            <Float name="it2">3.7</Float>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();
    DeviceConfig& device = config.getDeviceConfig("hmmm");

    std::vector<std::string> errors;
    bool allItemsRead = device.checkAllItemsRead(errors);
    BOOST_CHECK_EQUAL(allItemsRead, false);
    BOOST_CHECK_EQUAL(errors.size(), 2);
    // example output of error messages
    /*for (auto& s : errors)
        std::cout << s << std::endl;*/

    device.getFloat("it2");

    errors.clear();
    allItemsRead = device.checkAllItemsRead(errors);
    BOOST_CHECK_EQUAL(allItemsRead, false);
    BOOST_CHECK_GT(errors.size(), 0);

    device.getSlaveConfig("MySlave").getIdentifier();

    errors.clear();
    allItemsRead = device.checkAllItemsRead(errors);
    BOOST_CHECK_EQUAL(allItemsRead, true);
    BOOST_CHECK_EQUAL(errors.size(), 0);
}

BOOST_AUTO_TEST_CASE(ambiguityWithoutProfiles)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x103</ProductID>
                <ProductID>0x056</ProductID>
                <Serial>0</Serial>
            </Identifier>
        </Slave>
        <Config>
            <Float name="it2">3.7</Float>
            <Float name="it2">4.5</Float>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("it2") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for it2");
        }
        if (m.find("ProductID") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for ProductID");
        }
    }
}

BOOST_AUTO_TEST_CASE(ambiguityWithProfiles)
{
    std::string sampleXml = R""""(
    <Profiles>
        <Device type="Simple" profilename="a">
            <Config>
                <Float name="it2">3.7</Float>
                <Float name="it2">4.5</Float>
            </Config>
        </Device>
        <Slave type="MySlave" profilename="b">
            <Identifier>
                <Serial>34</Serial>
                <ProductID>54</ProductID>
                <Serial>78</Serial>
            </Identifier>
            <Config>
                <Int name="abc">56</Int>
                <Int name="abc">89</Int>
            </Config>
        </Slave>
    </Profiles>
    <Device type="Simple" name="hmmm" profile="a">
        <Slave type="MySlave" profile="b">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
            </Identifier>
        </Slave>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("it2") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for it2");
        }
        if (m.find("abc") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for abc");
        }
        if (m.find("Serial") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for ProductID");
        }
    }
}

BOOST_AUTO_TEST_CASE(identifierCompletenessProductID)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <Serial>342</Serial>
            </Identifier>
        </Slave>
        <Config>
            <Float name="it2">3.7</Float>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("Identifier") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for ProductID");
        }
    }
}

BOOST_AUTO_TEST_CASE(identifierCompletenessVendor)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm">
        <Slave type="MySlave">
            <Identifier>
                <ProductID>23</ProductID>
                <Serial>342</Serial>
            </Identifier>
        </Slave>
        <Config>
            <Float name="it2">3.7</Float>
        </Config>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("Identifier") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for VendorID");
        }
    }
}

BOOST_AUTO_TEST_CASE(profileNameHandling)
{
    std::string sampleXml = R""""(
    <Profiles>
        <Device type="Simple" profilename="a">
            <Config>
                <Int name="abc">10</Float>
            </Config>
        </Device>
        <Slave type="MySlave" profilename="a">
            <Config>
                <Int name="abc">20</Int>
            </Config>
        </Slave>
        <Slave type="Simple" profilename="a">
            <Config>
                <Int name="abc">30</Int>
            </Config>
        </Slave>
        <Device type="test" profilename="a">
            <Config>
                <Int name="abc">40</Float>
            </Config>
        </Device>
    </Profiles>
    <Device type="Simple" name="hmmm" profile="a">
        <Slave type="MySlave" profile="a">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>78</Serial>
            </Identifier>
        </Slave>
    </Device>
    <Device type="test" name="ha" profile="a">
        <Slave type="Simple" profile="a">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>79</Serial>
            </Identifier>
        </Slave>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();
    auto& config = parser.getHardwareConfig();

    auto& hmmm = config.getDeviceConfig("hmmm");
    BOOST_CHECK_EQUAL(hmmm.getInt("abc"), 10);
    BOOST_CHECK_EQUAL(hmmm.getOnlySlaveConfig().getInt("abc"), 20);

    auto& ha = config.getDeviceConfig("ha");
    BOOST_CHECK_EQUAL(ha.getInt("abc"), 40);
    BOOST_CHECK_EQUAL(ha.getOnlySlaveConfig().getInt("abc"), 30);
}

BOOST_AUTO_TEST_CASE(profileDefinedMultipleTimes)
{
    std::string sampleXml = R""""(
    <Profiles>
        <Device type="Simple" profilename="a">
            <Config>
                <Int name="abc">10</Float>
            </Config>
        </Device>
        <Slave type="Simple" profilename="a">
            <Config>
                <Int name="abc">20</Int>
            </Config>
        </Slave>
        <Slave type="Simple" profilename="a">
            <Config>
                <Int name="abc">30</Int>
            </Config>
        </Slave>
        <Device type="Simple" profilename="a">
            <Config>
                <Int name="abc">40</Float>
            </Config>
        </Device>
    </Profiles>
    <Device type="Simple" name="hmmm" profile="a">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>78</Serial>
            </Identifier>
        </Slave>
    </Device>
    <Device type="test" name="ha">
        <Slave type="Simple" profile="a">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>79</Serial>
            </Identifier>
        </Slave>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("Device") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for Device");
        }
        if (m.find("Slave") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for Slave");
        }
    }
}

BOOST_AUTO_TEST_CASE(testEqualSlaveIdentifier)
{
    std::string sampleXml = R""""(
    <Device type="Simple" name="hmmm" profile="a">
        <Slave type="MySlave">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>78</Serial>
            </Identifier>
        </Slave>
    </Device>
    <Device type="test" name="ha">
        <Slave type="Simple" profile="a">
            <Identifier>
                <VendorID>0x7BC</VendorID>
                <ProductID>0x056</ProductID>
                <Serial>78</Serial>
            </Identifier>
        </Slave>
    </Device>
    )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    try
    {
        parser.parse();

        BOOST_FAIL("Expected ParserError");
    }
    catch (const ParserError& e)
    {
        std::string m = e.what();

        if (m.find("Identifier") == std::string::npos && m.find("identifier") == std::string::npos)
        {
            BOOST_FAIL("Error message did not contain error for non-unique slave identifier");
        }
    }
}

BOOST_AUTO_TEST_CASE(subdeviceProfileTest)
{
    std::string sampleXml = R""""(
<Profiles>
    <Device type="abc" profilename="testprofile">
        <Controller type="TorqueController">
            <Float name="inertia">0.0</Float>
            <Float name="scalePI">0.3</Float>
            <Float name="scaleP">0.0</Float>
            <Float name="Kd">5</Float>
            <Float name="maxTorque">8</Float>
            <Float name="actuatorType">1</Float>
        </Controller>
        <Config>
            <Bool name="tb">true</Bool>
        </Config>
    </Device>
</Profiles>
<Device type="qq" name="ArmL1_Cla1">
    <Slave type="SensorBoard">
        <Identifier>
            <VendorID>0x7BC</VendorID>
            <ProductID>0x103</ProductID>
            <Serial>0</Serial>
        </Identifier>
    </Slave>
    <Subdevice name="sub" type="abc" profile="testprofile">
        <Config>
            <Bool name="tb">false</Bool>
        </Config>
        <Controller type="TorqueController">
            <Float name="Kd">3</Float>
        </Controller>
        <Controller type="ZeroTorqueController">
            <Int name="actuatorType">1</Int>
        </Controller>
        <Controller type="PositionContoller"/>
        <Controller type="VelocityController"/>
    </Subdevice>
</Device>
        )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("ArmL1_Cla1").getSubdeviceConfig("sub");
    auto& torque = deviceConfig.getControllerConfig("TorqueController");

    BOOST_CHECK_CLOSE(torque.getFloat("Kd"), 3, 0.00001);
    BOOST_CHECK_CLOSE(torque.getFloat("scalePI"), 0.3, 0.00001);
    BOOST_CHECK_EQUAL(deviceConfig.getBool("tb"), false);
}

BOOST_AUTO_TEST_CASE(linearConfigOverrideTest)
{
    std::string sampleXml = R""""(
<Profiles>
    <Device type="Joint" profilename="testprofile">
        <Config>
            <LinearConvertedValue name="test1" factor="-3.0" offset="0.2"/>
            <LinearConvertedValue name="test2" factor="3.0" offset="0.2"/>
            <LinearConvertedValue name="test3" factor="-3.0"/>
            <LinearConvertedValue name="test4" offset="0.2"/>
            <LinearConvertedValue name="test5" factor="5.0" offset="0.2"/>
        </Config>
    </Device>
</Profiles>
<Device type="Joint" name="ArmL1_Cla1" profile="testprofile">
    <Slave type="SensorBoard">
        <Identifier>
            <VendorID>0x7BC</VendorID>
            <ProductID>0x103</ProductID>
            <Serial>0</Serial>
        </Identifier>
    </Slave>
    <Config>
        <LinearConvertedValue name="test1" factorSign="+"/>
        <LinearConvertedValue name="test2" factorSign="negative" offset="-0.9"/>
        <LinearConvertedValue name="test3" factorAbs="4.0"/>
        <LinearConvertedValue name="test4" factorSign="-"/>
        <LinearConvertedValue name="test5" factor="-2.4"/>
    </Config>
</Device>
        )"""";

    armarx::RapidXmlReaderPtr reader(armarx::RapidXmlReader::FromXmlString(sampleXml));
    armarx::RapidXmlReaderNode documentNode = reader->getDocument();
    ConfigParser parser(documentNode);

    parser.parse();

    HardwareConfig& config = parser.getHardwareConfig();

    auto& deviceConfig = config.getDeviceConfig("ArmL1_Cla1");
    auto test1 = deviceConfig.getLinearConfig("test1");
    auto test2 = deviceConfig.getLinearConfig("test2");
    auto test3 = deviceConfig.getLinearConfig("test3");
    auto test4 = deviceConfig.getLinearConfig("test4");
    auto test5 = deviceConfig.getLinearConfig("test5");

    BOOST_CHECK_CLOSE(test1.getFactor(), 3, 0.00001);
    BOOST_CHECK_CLOSE(test1.getOffset(), 0.2, 0.00001);
    BOOST_CHECK_CLOSE(test2.getFactor(), -3, 0.00001);
    BOOST_CHECK_CLOSE(test2.getOffset(), -0.9, 0.00001);
    BOOST_CHECK_CLOSE(test3.getFactor(), -4, 0.00001);
    BOOST_CHECK_CLOSE(test3.getOffset(), 0.0, 0.00001);
    BOOST_CHECK_CLOSE(test4.getFactor(), -1, 0.00001);
    BOOST_CHECK_CLOSE(test4.getOffset(), 0.2, 0.00001);
    BOOST_CHECK_CLOSE(test5.getFactor(), -2.4, 0.00001);
    BOOST_CHECK_CLOSE(test5.getOffset(), 0.2, 0.00001);
}
