#pragma once

#include <stdexcept>
#include <string>

namespace armarx::control::hardware_config
{
    /**
     * @brief The ConfigInsertError class represents an error that is thrown if an attempt
     * is mode to set a config value but the value is already defined with the same tag.
     */
    class ConfigInsertError : public std::runtime_error
    {
    public:
        ConfigInsertError(const std::string& message) : std::runtime_error(message)
        {
        }
    };

    /**
     * @brief The ConfigElementNotFoundError class represents an error that is thrown when
     * trying to get a config value that is not defined in the config.
     */
    class ConfigElementNotFoundError : public std::runtime_error
    {
    public:
        ConfigElementNotFoundError(const std::string& message) : std::runtime_error(message)
        {
        }
    };

    /**
     * @brief The ParserError class represents a generic error that occurs during parsing
     */
    class ParserError : public std::runtime_error
    {
    public:
        ParserError(const std::string& message) : std::runtime_error(message)
        {
        }
    };
} // namespace armarx::control::hardware_config
