#include "SlaveConfig.h"

namespace armarx::control::hardware_config
{

    SlaveConfig::SlaveConfig(SlaveIdentifierConfig identifier,
                             std::optional<std::string> type,
                             std::optional<std::string> name) :
        identifier(identifier), type(type), name(name)
    {
    }

    SlaveConfig::SlaveConfig(SlaveIdentifierConfig identifier,
                             std::optional<std::string> type,
                             std::optional<std::string> name,
                             std::shared_ptr<SlaveProfile> profile) :
        identifier(identifier), type(type), name(name)
    {
        this->items = profile->items;
    }

    SlaveIdentifierConfig&
    SlaveConfig::getIdentifier()
    {
        identifierRead = true;
        return identifier;
    }

    std::optional<std::string>
    SlaveConfig::getType() const
    {
        return type;
    }

    std::optional<std::string>
    SlaveConfig::getName() const
    {
        return name;
    }

    bool
    SlaveConfig::checkAllItemsRead(std::vector<std::string>& errors) const
    {
        bool succ = true;

        if (!identifierRead)
        {
            succ = false;
            std::stringstream s;
            s << "Identifier of slave not read. ";
            s << "Identifier details: " << identifier;
            errors.push_back(s.str());
        }
        succ &= Config::checkAllItemsRead(errors);
        return succ;
    }

    void
    SlaveConfig::print(std::stringstream& s, int indention) const
    {
        for (int i = 0; i < indention; i++)
        {
            s << "\t";
        }
        s << "Slave:" << std::endl;
        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Type: " << getType().value_or("<no type>") << std::endl;
        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Name: " << getName().value_or("<no name>") << std::endl;
        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Identifier: " << identifier << std::endl;

        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Config:" << std::endl;
        Config::print(s, indention + 2);
    }

    void
    SlaveConfig::onParsingFinished()
    {
        identifierRead = false;
    }

    std::string
    SlaveProfile::getName()
    {
        return name;
    }

    std::string
    SlaveProfile::getType()
    {
        return type;
    }

    SlaveIdentifierConfig&
    SlaveProfile::getIdentifier()
    {
        return identifier;
    }

} // namespace armarx::control::hardware_config
