#include "Types.h"

namespace armarx::control::hardware_config::types
{
    LinearConfig::LinearConfig(float factor, float offset) :
        factorAbs(std::abs(factor)), offset(offset), negativeFactor(factor < 0)
    {
        ;
    }

    LinearConfig
    LinearConfig::overrideWith(LinearConfig other)
    {
        if (other.factorAbs.has_value())
        {
            factorAbs = other.factorAbs;
        }

        if (other.negativeFactor.has_value())
        {
            negativeFactor = other.negativeFactor;
        }

        if (other.offset.has_value())
        {
            offset = other.offset;
        }
        return *this;
    }

    void
    LinearConfig::setFactorAbsoluteValue(float factorAbs)
    {
        this->factorAbs = std::make_optional(factorAbs);
    }

    void
    LinearConfig::setOffset(float offset)
    {
        this->offset = std::make_optional(offset);
    }

    void
    LinearConfig::setFactorIsNegative(bool isNegative)
    {
        this->negativeFactor = isNegative;
    }

    float
    LinearConfig::getFactor() const
    {
        return (negativeFactor.value_or(false) ? -1 : 1) * factorAbs.value_or(1.f);
    }

    float
    LinearConfig::getOffset() const
    {
        return offset.value_or(0.f);
    }

    ModularConvertedValueConfig::ModularConvertedValueConfig(float factor, float zeroOffset, float discontinuityOffset, float maxValue) :
        factorAbs(std::abs(factor)),
        negativeFactor(factor < 0),
        zeroOffset(zeroOffset),
        discontinuityOffset(discontinuityOffset),
        maxValue(maxValue)
    {
        ;
    }

    ModularConvertedValueConfig
    ModularConvertedValueConfig::overrideWith(const ModularConvertedValueConfig& other)
    {
        if (other.factorAbs.has_value())
        {
            factorAbs = other.factorAbs;
        }

        if (other.negativeFactor.has_value())
        {
            negativeFactor = other.negativeFactor;
        }

        if (other.zeroOffset.has_value())
        {
            zeroOffset = other.zeroOffset;
        }

        if (other.discontinuityOffset.has_value())
        {
            discontinuityOffset = other.discontinuityOffset;
        }

        if (other.maxValue.has_value())
        {
            maxValue = other.maxValue;
        }

        return *this;
    }

    void
    ModularConvertedValueConfig::setFactorAbsoluteValue(float factorAbs)
    {
        this->factorAbs = std::make_optional(factorAbs);
    }

    void
    ModularConvertedValueConfig::setZeroOffset(float zeroOffset)
    {
        this->zeroOffset = zeroOffset;
    }

    void
    ModularConvertedValueConfig::setFactorIsNegative(bool isNegative)
    {
        this->negativeFactor = isNegative;
    }

    float
    ModularConvertedValueConfig::getFactor() const
    {
        return (negativeFactor.value_or(false) ? -1 : 1) * factorAbs.value_or(1.f);
    }

    float
    ModularConvertedValueConfig::getZeroOffset() const
    {
        return zeroOffset.value_or(0.f);
    }

    float
    ModularConvertedValueConfig::getDiscontinuityOffset() const
    {
        return discontinuityOffset.value_or(0.f);
    }

    float
    ModularConvertedValueConfig::getMaxValue() const
    {
        return maxValue.value_or(0.f);
    }

} // namespace armarx::control::hardware_config::types
