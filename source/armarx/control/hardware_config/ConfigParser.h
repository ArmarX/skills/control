#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include <armarx/control/hardware_config/HardwareConfig.h>
#include <armarx/control/hardware_config/SlaveIdentifierConfig.h>

namespace armarx::control::hardware_config
{
    std::string createErrorMessageWithContext(std::string message, RapidXmlReaderNode& context);

    class ConfigParser
    {
    public:
        ConfigParser(MultiNodeRapidXMLReader nodes);
        ConfigParser(RapidXmlReaderNode& hardwareConfigNode);

        /**
         * @brief Parse the input nodes given in constructor
         * @throws ParserError if some kind of error occured
         */
        void parse();

        /**
         * @brief Get warnings that occured during parsing
         * These are additional messages that the parser generated and are meant to be useful
         * find logic issues within the file.
         * @return vector of warning strings
         */
        std::vector<std::string>& getWarnings();

        /**
         * @brief Get the parsed Config object hierarchy.
         * If the ConfigParser::Parse did throw an exception the best appoximation is returned.
         * If ConfigParser::Parse was not called before then the HardwareConfig object is empty.
         */
        HardwareConfig& getHardwareConfig();

    private:
        // These are the internal parser functions. They take a reference to a shared ptr to
        // their config object and create the config within it.
        // The ...node attribute should point to the xml node that contains all needed other nodes.
        // For example parseSlave needs the '<Slave ...' node and parseProfiles needs the
        // '<Profiles>' node.
        bool parseDevice(std::shared_ptr<DeviceConfig>& deviceConfig,
                         RapidXmlReaderNode& deviceNode);

        bool parseDeviceProfile(std::shared_ptr<DeviceConfigBase>& deviceConfig,
                                RapidXmlReaderNode& deviceNode);

        bool parseSubDevice(std::shared_ptr<DeviceConfigBase>& subDeviceConfig,
                            RapidXmlReaderNode& subDeviceNode);

        bool parseSlave(std::shared_ptr<SlaveConfig>& slaveConfig,
                        RapidXmlReaderNode& slaveNode,
                        SlaveType type);

        bool parseSlaveProfile(std::shared_ptr<SlaveProfile>& slaveConfig,
                               RapidXmlReaderNode& slaveNode);

        void parseConfigNode(Config& config, RapidXmlReaderNode& configNode, ConfigTag tag);

        void parseController(ControllerConfig& config,
                             RapidXmlReaderNode& controllerNode,
                             ConfigTag tag);

        void parseProfiles(RapidXmlReaderNode& profilesNode);

        void parseSlaveIdentifier(SlaveIdentifierConfig& identifier,
                                  RapidXmlReaderNode& identifierNode,
                                  std::optional<std::string> name,
                                  ConfigTag tag,
                                  bool checkIdentifierValid);

        /**
         * @brief   Search for a Profile with given name and type.
         * @param ref           The result (if something was found)
         * @param type          device / slave type
         * @param profileName   profilename
         * @param map           The map in which the search should take place
         * @return              If a profile was found
         */
        template <typename T>
        bool findProfile(std::shared_ptr<T>& ref,
                         std::string type,
                         std::string profileName,
                         std::string debugName,
                         std::map<std::string, std::map<std::string, std::shared_ptr<T>>>& map);

        /**
         * @brief Parse a list type
         * @param str   The content of the list node
         * @param func  conversion function
         * @return parsed list
         */
        template <typename T>
        std::list<T> parseList(std::string str, std::function<T(std::string)> func);

        /**
         * @brief Parse a matrix
         * @param str   The content of the matrix node
         * @param func  conversion function
         * @return parsed matrix
         */
        template <typename T>
        Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>
        parseMatrix(RapidXmlReaderNode& node, std::function<T(std::string)> func);

        bool strToBool(std::string s);

        void testSlaveIdentifierUnique(SlaveIdentifierConfig& identifier);

        // Store the document node
        MultiNodeRapidXMLReader hardwareConfigNode;

        // Store parsed data
        HardwareConfig config;

        // Profiles for SlaveConfig and DeviceConfig construction with a profile
        // Type -> Name -> Profile
        std::map<std::string, std::map<std::string, std::shared_ptr<DeviceConfigBase>>>
            deviceProfiles;
        std::map<std::string, std::map<std::string, std::shared_ptr<SlaveProfile>>> slaveProfiles;

        // References to all SlaveIdentifiers that have been parsed (for testing SlaveIdentifier uniqueness)
        std::vector<SlaveIdentifierConfig> identifiers;

        // Store errors from parser
        std::vector<std::string> errors;

        // Store warnings from parser
        std::vector<std::string> warnings;
    };
} // namespace armarx::control::hardware_config
