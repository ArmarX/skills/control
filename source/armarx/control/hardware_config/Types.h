#pragma once


#include <cmath>
#include <optional>

#include <armarx/control/hardware_config/Errors.h>


namespace armarx::control::hardware_config::types
{

    /**
     * @brief The LinearConfig class represents a linear conversion and has a factor and offset.
     * Partial overrides are possible for the offset, factor, factor sign and factor absolute value.
     */
    class LinearConfig
    {
    public:
        /**
         * @brief Create LinearConfig with factor and offset.
         * Convenience constructor for this tag layout:
         * <LinearConvertedValue name="..." factor="..." offset="..."/>
         * @param factor
         * @param offset
         */
        LinearConfig(float factor, float offset);
        LinearConfig() = default;

        /**
         * @brief Perform partial override
         * @return resulting object (=*this)
         */
        LinearConfig overrideWith(LinearConfig);

        /**
         * @brief Set the absolute value for factor, keep sign unaffected
         * @param factorAbs new absolute value
         */
        void setFactorAbsoluteValue(float factorAbs);

        /**
         * @brief Set the offset
         * @param offset new offset
         */
        void setOffset(float offset);

        /**
         * @brief Set the new factor sign, keep the factor absolute value unaffected
         * @param isNegative
         */
        void setFactorIsNegative(bool isNegative);

        /**
         * @brief Get the factor
         * @return Get the offset
         */
        float getFactor() const;
        float getOffset() const;

    private:
        std::optional<float> factorAbs;
        std::optional<float> offset;
        std::optional<bool> negativeFactor;
    };


    class ModularConvertedValueConfig
    {
    public:
        ModularConvertedValueConfig(float factor, float zeroOffset, float discontinuityOffset, float maxValue);
        ModularConvertedValueConfig() = default;

        ModularConvertedValueConfig overrideWith(const ModularConvertedValueConfig&);

        void setFactorAbsoluteValue(float factorAbs);
        void setZeroOffset(float zeroOffset);
        void setFactorIsNegative(bool isNegative);

        float getFactor() const;
        float getZeroOffset() const;
        float getDiscontinuityOffset() const;
        float getMaxValue() const;

    private:
        std::optional<float> factorAbs;
        std::optional<bool> negativeFactor;
        std::optional<float> zeroOffset;
        std::optional<float> discontinuityOffset;
        std::optional<float> maxValue;
    };


    /*
     * Here additional complex types can be declared if they are needed.
     * Do not forget to add the parser code.
     * If partial overrides should be possible for a new type they can be handled in Config::set
     */


} // namespace armarx::control::hardware_config::types
