#pragma once

#include <map>

#include <armarx/control/hardware_config/Config.h>
#include <armarx/control/hardware_config/SlaveConfig.h>
#include <armarx/control/hardware_config/SlaveIdentifierConfig.h>

namespace armarx::control::hardware_config
{
    class DeviceConfig;

    /**
     * @brief A base class that does not have Subdevices or Slaves but can be used for Subdevices
     * or DeviceProfiles
     */
    class DeviceConfigBase : public Config, public ControllerConfig
    {
        friend class ConfigParser;
        friend class DeviceConfig;

    public:
        DeviceConfigBase(std::string type, std::string name);

        DeviceConfigBase(std::string type,
                         std::string name,
                         std::shared_ptr<DeviceConfigBase> profile);

        ~DeviceConfigBase() = default;

        std::string getType() const;

        std::string getName() const;

        void print(std::stringstream& s, int indention) const override;

    private:
        std::string type;
        std::string name;
    };

    class DeviceConfig : public DeviceConfigBase
    {
        friend class ConfigParser;

    public:
        DeviceConfig(std::string type, std::string name);

        DeviceConfig(std::string type, std::string name, std::shared_ptr<DeviceConfigBase> profile);

        ~DeviceConfig() = default;

        SlaveConfig& getSlaveConfig(const std::uint32_t vendorID,
                                    const std::uint32_t productCode,
                                    const std::uint32_t serialNumber);

        SlaveConfig& getSlaveConfig(const std::string type, const std::string name = "");

        SlaveConfig& getOnlySlaveConfig();

        DeviceConfigBase& getSubdeviceConfig(const std::string& subDeviceName);

        std::list<std::reference_wrapper<DeviceConfigBase>>
        getSubDeviceConfigsWithType(const std::string subDeviceType);

        bool checkAllItemsRead(std::vector<std::string>& errors) const override;

        void print(std::stringstream& s, int indention) const override;

        void onParsingFinished() override;

    private:
        void addSlaveConfig(std::shared_ptr<SlaveConfig> slaveConfig);

        void addSubDeviceConfig(const std::string name,
                                std::shared_ptr<DeviceConfigBase> subDeviceConfig);

        std::list<std::shared_ptr<SlaveConfig>> slaveConfigs;
        std::map<std::string, std::shared_ptr<DeviceConfigBase>> subDeviceConfigs;
    };
} // namespace armarx::control::hardware_config
