#include "SlaveIdentifierConfig.h"

namespace armarx::control::hardware_config
{
    std::uint32_t
    SlaveIdentifierConfig::getVendorID()
    {
        return vendorID;
    }

    std::uint32_t
    SlaveIdentifierConfig::getProductCode()
    {
        return productCode;
    }

    std::uint32_t
    SlaveIdentifierConfig::getSerialNumber()
    {
        return serialNumber;
    }

    std::string
    SlaveIdentifierConfig::getName()
    {
        return name;
    }

    bool
    SlaveIdentifierConfig::isValid(std::vector<std::string>& errorMessages)
    {
        bool isValid = true;

        if (vendorTag == ConfigTag::Invalid)
        {
            isValid = false;
            errorMessages.push_back("Identifier is missing a VendorID node");
        }
        if (productTag == ConfigTag::Invalid)
        {
            isValid = false;
            errorMessages.push_back("Identifier is missing a ProductID node");
        }
        if (serialTag == ConfigTag::Invalid)
        {
            isValid = false;
            errorMessages.push_back("Identifier is missing a Serial node");
        }
        return isValid;
    }

    void
    SlaveIdentifierConfig::setVendorID(std::uint32_t vendorID, ConfigTag tag)
    {
        set(vendorID, tag, this->vendorID, this->vendorTag);
    }

    void
    SlaveIdentifierConfig::setProductCode(std::uint32_t productCode, ConfigTag tag)
    {
        set(productCode, tag, this->productCode, this->productTag);
    }

    void
    SlaveIdentifierConfig::setSerialNumber(std::uint32_t serial, ConfigTag tag)
    {
        set(serial, tag, this->serialNumber, this->serialTag);
    }

    void
    SlaveIdentifierConfig::setName(std::string name, ConfigTag tag)
    {
        set(name, tag, this->name, this->nameTag);
    }
} // namespace armarx::control::hardware_config
