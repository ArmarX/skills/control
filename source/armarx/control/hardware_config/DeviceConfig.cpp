#include "DeviceConfig.h"

namespace armarx::control::hardware_config
{
    DeviceConfig::DeviceConfig(std::string type, std::string name) : DeviceConfigBase(type, name)
    {
    }

    DeviceConfig::DeviceConfig(std::string type,
                               std::string name,
                               std::shared_ptr<DeviceConfigBase> profile) :
        DeviceConfigBase(type, name, profile)
    {
    }

    DeviceConfigBase::DeviceConfigBase(std::string type, std::string name) : type(type), name(name)
    {
    }

    DeviceConfigBase::DeviceConfigBase(std::string type,
                                       std::string name,
                                       std::shared_ptr<DeviceConfigBase> profile) :
        ControllerConfig(*profile), type(type), name(name)
    {
        this->items = profile->items;
    }

    SlaveConfig&
    DeviceConfig::getSlaveConfig(const std::uint32_t vendorID,
                                 const std::uint32_t productCode,
                                 const std::uint32_t serialNumber)
    {
        for (auto& slaveConfig : slaveConfigs)
        {
            auto id = slaveConfig->getIdentifier();
            if (id.getVendorID() == vendorID && id.getProductCode() == productCode &&
                id.getSerialNumber() == serialNumber)
            {
                return *slaveConfig;
            }
        }
        std::stringstream ss;
        ss << "Slave Config with vendorID: " << vendorID << " , productCode: " << productCode
           << " , serialNumber: " << serialNumber << " not found";
        throw ConfigElementNotFoundError(ss.str());
    }

    SlaveConfig&
    DeviceConfig::getSlaveConfig(const std::string type, const std::string name)
    {
        for (auto& slaveConfig : slaveConfigs)
        {
            if (slaveConfig->getType() == type)
            {
                if (name == "" || slaveConfig->getName() == name)
                {
                    return *slaveConfig;
                }
            }
        }
        throw ConfigElementNotFoundError("Config Element not found: Type: " + type + ", name: " + name);
    }

    SlaveConfig&
    DeviceConfig::getOnlySlaveConfig()
    {
        if (slaveConfigs.size() == 1)
        {
            return **slaveConfigs.begin();
        }
        throw ConfigElementNotFoundError(
            "There are more than one SlaveConfigs in this DeviceConfig");
    }

    DeviceConfigBase&
    DeviceConfig::getSubdeviceConfig(const std::string& subDeviceName)
    {
        auto it = subDeviceConfigs.find(subDeviceName);
        if (it != subDeviceConfigs.end())
        {
            return *it->second;
        }
        throw ConfigElementNotFoundError("SubDevice with name: " + subDeviceName);
    }

    std::list<std::reference_wrapper<DeviceConfigBase>>
    DeviceConfig::getSubDeviceConfigsWithType(const std::string subDeviceType)
    {
        std::list<std::reference_wrapper<DeviceConfigBase>> list;
        for (auto& subDevice : subDeviceConfigs)
        {
            if (subDevice.second->getType() == subDeviceType)
            {
                list.push_back(*subDevice.second);
            }
        }
        return list;
    }


    std::string
    DeviceConfigBase::getType() const
    {
        return type;
    }

    std::string
    DeviceConfigBase::getName() const
    {
        return name;
    }

    bool
    DeviceConfig::checkAllItemsRead(std::vector<std::string>& errors) const
    {
        bool succ = true;
        for (auto& slave : slaveConfigs)
        {
            succ &= slave->checkAllItemsRead(errors);
        }
        for (auto& controller : controllerConfigs)
        {
            succ &= controller.second->checkAllItemsRead(errors);
        }
        for (auto& sub : subDeviceConfigs)
        {
            succ &= sub.second->checkAllItemsRead(errors);
        }
        succ &= Config::checkAllItemsRead(errors);
        return succ;
    }

    void
    DeviceConfig::print(std::stringstream& s, int indention) const
    {
        for (int i = 0; i < indention; i++)
        {
            s << "\t";
        }
        s << "Device type=" << getType() << " name=" << getName() << ":" << std::endl;

        for (auto& slave : slaveConfigs)
        {
            slave->print(s, indention + 1);
        }

        for (auto& controller : controllerConfigs)
        {
            for (int i = 0; i < indention + 1; i++)
            {
                s << "\t";
            }
            s << "Controller: " << controller.first << ":" << std::endl;
            controller.second->print(s, indention + 2);
        }

        for (auto& sub : subDeviceConfigs)
        {
            sub.second->print(s, indention + 1);
        }

        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Config:" << std::endl;
        Config::print(s, indention + 2);
    }

    void
    DeviceConfig::addSlaveConfig(std::shared_ptr<SlaveConfig> slaveConfig)
    {
        slaveConfigs.push_back(slaveConfig);
    }

    void
    DeviceConfig::addSubDeviceConfig(const std::string name,
                                     std::shared_ptr<DeviceConfigBase> subDeviceConfig)
    {
        subDeviceConfigs[name] = subDeviceConfig;
    }

    void
    DeviceConfig::onParsingFinished()
    {
        Config::onParsingFinished();
        for (auto& i : subDeviceConfigs)
        {
            i.second->onParsingFinished();
        }
        for (auto& i : slaveConfigs)
        {
            i->onParsingFinished();
        }
        for (auto& i : controllerConfigs)
        {
            i.second->onParsingFinished();
        }
    }

    void
    DeviceConfigBase::print(std::stringstream& s, int indention) const
    {
        for (int i = 0; i < indention; i++)
        {
            s << "\t";
        }
        s << "SubDevice name=" << getName() << ":" << std::endl;

        for (auto& controller : controllerConfigs)
        {
            for (int i = 0; i < indention + 1; i++)
            {
                s << "\t";
            }
            s << "Controller: " << controller.first << ":" << std::endl;
            controller.second->print(s, indention + 2);
        }

        for (int i = 0; i < indention + 1; i++)
        {
            s << "\t";
        }
        s << "Config:" << std::endl;
        Config::print(s, indention + 2);
    }

} // namespace armarx::control::hardware_config
