#include "HardwareConfig.h"

namespace armarx::control::hardware_config
{
    HardwareConfig::HardwareConfig()
    {
    }

    DeviceConfig&
    HardwareConfig::getDeviceConfig(std::string name)
    {
        auto it = deviceConfigs.find(name);
        if (it == deviceConfigs.end())
        {
            throw ConfigElementNotFoundError("Device with Name: " + name);
        }
        return *it->second;
    }


    void
    HardwareConfig::onParsingFinished()
    {
        for (auto& i : deviceConfigs)
        {
            i.second->onParsingFinished();
        }
    }

    void
    HardwareConfig::addDeviceConfig(const std::string name, std::shared_ptr<DeviceConfig> config)
    {
        if (deviceConfigs.find(name) != deviceConfigs.end())
        {
            throw ConfigElementNotFoundError("Parser error: Device with name " + name +
                                             " is defined multiple times");
        }
        deviceConfigs[name] = config;
    }

    bool
    HardwareConfig::checkAllItemsRead(std::vector<std::string>& errors) const
    {
        bool succ = true;
        for (auto& dev : deviceConfigs)
        {
            succ &= dev.second->checkAllItemsRead(errors);
        }
        return succ;
    }
} // namespace armarx::control::hardware_config
