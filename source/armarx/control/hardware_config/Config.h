#pragma once

#include <array>
#include <cstdlib>
#include <list>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <armarx/control/hardware_config/Errors.h>
#include <armarx/control/hardware_config/Types.h>

#include <cxxabi.h>

namespace armarx::control::hardware_config
{
    template <typename T>
    std::string
    type_name()
    {
        int status;
        std::string tname = typeid(T).name();
        char* demangled_name = abi::__cxa_demangle(tname.c_str(), NULL, NULL, &status);
        if (status == 0)
        {
            tname = demangled_name;
            std::free(demangled_name);
        }
        return tname;
    }

    /// The variant type of the config map
    using var_t = std::variant<bool,
                               float,
                               std::uint32_t,
                               std::int32_t,
                               std::list<bool>,
                               std::list<float>,
                               std::list<std::uint32_t>,
                               std::list<std::int32_t>,
                               types::LinearConfig,
                               types::ModularConvertedValueConfig,
                               std::string,
                               Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>,
                               Eigen::Matrix<std::uint32_t, Eigen::Dynamic, Eigen::Dynamic>,
                               Eigen::Matrix<std::int32_t, Eigen::Dynamic, Eigen::Dynamic>>;

    /**
     * @brief The ConfigTag is used when setting a config value.
     * It allows for checks whether the same config value is defined multiple times on the
     * same hierarchy.
     * The hierarchy is: Custom overwrites Profiles
     */
    enum ConfigTag : int
    {
        Invalid = -1,
        TagProfile = 1,
        TagCustom = 2
    };

    /**
     * @brief The ConfigItemWithMetadata struct packs the data together with the
     * tag and a isRead flag.
     */
    struct ConfigItemWithMetadata
    {
        ConfigItemWithMetadata() = default;
        ConfigItemWithMetadata(var_t data, ConfigTag tag) : data(data), tag(tag), isRead(false)
        {
        }
        var_t data;
        ConfigTag tag;
        bool isRead;
    };

    std::string tagName(ConfigTag tag);

    /**
     * @brief The Config class is the base class of all specialized configurations that have a
     * direct key -> value mapping.
     * Direct instantiation is always possible, if this mapping is all that is needed.
     * A Config in read-only and only ConfigParser can change the contents.
     */
    class Config
    {
        friend class ConfigParser; // For setters
        friend class ControllerConfig; // Needed to copy controller configurations

    public:
        Config();

        virtual ~Config() = default;

        /**
         * @brief Get a Bool typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no Bool value to this key
         * @return the value
         */
        bool getBool(const std::string name);

        /**
         * @brief Get a Float typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no Float value to this key
         * @return the value
         */
        float getFloat(const std::string name);

        /**
         * @brief Get a Uint (std::uint32_t) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no Uint value to this key
         * @return the value
         */
        std::uint32_t getUint(const std::string name);

        /**
         * @brief Get a Int (std::int32_t) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no Int value to this key
         * @return the value
         */
        std::int32_t getInt(const std::string name);

        /**
         * @brief Get a LinearConvertedValue typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no LinearConvertedValue to this key
         * @return the value
         */
        types::LinearConfig getLinearConfig(const std::string name);

        /**
         * @brief Get a ModularConvertedValue typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no ModularConvertedValue to this key
         * @return the value
         */
        types::ModularConvertedValueConfig getModularConvertedValueConfig(const std::string& name);

        /**
         * @brief Get a String typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no String value to this key
         * @return the value
         */
        std::string getString(const std::string name);

        /**
         * @brief Get a BoolList (std::list<bool>) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no BoolList value to this key
         * @return the value
         */
        std::list<bool> getBoolList(const std::string name);

        /**
         * @brief Get a FloatList (std::list<float>) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no FloatList value to this key
         * @return the value
         */
        std::list<float> getFloatList(const std::string name);

        /**
         * @brief Get a UIntList (std::list<std::uint32_t>) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no UIntList value to this key
         * @return the value
         */
        std::list<std::uint32_t> getUintList(const std::string name);

        /**
         * @brief Get a IntList (std::list<std::int32_t>) typed Config attribute by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError if there is no IntList value to this key
         * @return the value
         */
        std::list<std::int32_t> getIntList(const std::string name);

        /**
         * @brief Get a Matrix by name
         * @param name  the name of this Config attribute
         * @throws ConfigElementNotFoundError   if there is no matrix to this key with
         *                                      the correct type, width and height
         * @return the value
         */
        template <typename Type, int Rows, int Columns>
        Eigen::Matrix<Type, Rows, Columns>
        getMatrix(const std::string name)
        {
            Eigen::Matrix<Type, Rows, Columns> mat;

            auto mVal = get<Eigen::Matrix<Type, Eigen::Dynamic, Eigen::Dynamic>>(name);

            if (Rows != mVal.rows())
            {
                std::stringstream ss;
                ss << "The matrix has the wrong dimension: rows in config: " << mVal.rows()
                   << " , rows in getter " << Rows;
                throw ConfigElementNotFoundError(ss.str());
            }
            if (Columns != mVal.cols())
            {
                std::stringstream ss;
                ss << "The matrix has the wrong dimension: columns in config: " << mVal.cols()
                   << " , columns in getter " << Columns;
                throw ConfigElementNotFoundError(ss.str());
            }
            mat = Eigen::Matrix<Type, Rows, Columns>(mVal);

            return {mat};
        }

        /**
         * @brief This method is called once the device has read the configuration data it needs.
         * In this method it should be checked, whether there are entries in this Config
         * (including config objects that are stored in the Config).
         * @return true iff all items in this config have been read
         */
        virtual bool checkAllItemsRead(std::vector<std::string>& errors) const;

        /**
         * @brief Print this configuration. Works recursively.
         * @param s the stream to append the output to
         * @param indention the indentation (=level of recursion)
         */
        virtual void print(std::stringstream& s, int indention) const;

        /**
         * @brief This method is called when the config is completely read form the HardwareConfig file.
         * It starts the tracking of which item is read from this Config object.
         */
        virtual void onParsingFinished();

    private:
        // declare copy constructor and assignment operator as public, so that copying is not allowed
        Config(const Config&) = default;
        Config& operator=(const Config&) = default;

    protected:
        std::map<std::string, ConfigItemWithMetadata> items;

        // Can only be accessed by ConfigParser
        void setBool(const std::string name, bool value, ConfigTag tag);
        void setFloat(const std::string name, float value, ConfigTag tag);
        void setUint(const std::string name, std::uint32_t value, ConfigTag tag);
        void setInt(const std::string name, std::int32_t value, ConfigTag tag);
        void setLinearConfig(const std::string name, types::LinearConfig&& value, ConfigTag tag);
        void setModularConvertedValueConfig(const std::string name, types::ModularConvertedValueConfig&& value, ConfigTag tag);
        void setString(const std::string name, std::string value, ConfigTag tag);

        void setBoolList(const std::string name, std::list<bool> value, ConfigTag tag);
        void setFloatList(const std::string name, std::list<float> value, ConfigTag tag);
        void setUintList(const std::string name, std::list<std::uint32_t> value, ConfigTag tag);
        void setIntList(const std::string name, std::list<std::int32_t> value, ConfigTag tag);

        template <class T>
        void
        set(const std::string name, T value, ConfigTag tag)
        {
            auto it = items.find(name);
            if (it != items.end())
            {
                ConfigItemWithMetadata item = it->second;
                if (item.tag == tag)
                {
                    throw ConfigInsertError("Config Node with name " + name +
                                            " is defined multiple times in " + tagName(tag) +
                                            " nodes");
                }
                else if (item.tag > tag)
                {
                    // The config nodes have been parsed in the wrong order.
                    // But the new node should not overwrite the old one
                    // So we just return and don't change the entry
                    return;
                }
            }

            // Handle partial overrides in types that support it
            if constexpr (std::is_same_v<types::LinearConfig, T>)
            {
                auto oldItem = get_nothrow<T>(name);
                if (oldItem.has_value())
                {
                    items[name] =
                        ConfigItemWithMetadata(var_t(oldItem.value().overrideWith(value)), tag);
                }
                else
                {
                    items[name] = ConfigItemWithMetadata(var_t(value), tag);
                }
            }
            else // Base case: Full override
            {
                items[name] = ConfigItemWithMetadata(var_t(value), tag);
            }
        }

        template <typename T>
        std::optional<T>
        get_nothrow(const std::string name)
        {
            auto it = items.find(name);
            if (it == items.end())
            {
                return {};
            }
            it->second.isRead = true;
            if (const T* val = std::get_if<T>(&(it->second.data)))
            {
                return {*val};
            }
            return {};
        }

        template <typename T>
        T
        get(const std::string name)
        {
            auto it = items.find(name);
            if (it == items.end())
            {
                throw ConfigElementNotFoundError("Config item with name " + name + " and type " +
                                                 type_name<T>() + " not found (name not found)");
            }
            it->second.isRead = true;
            if (const T* val = std::get_if<T>(&(it->second.data)))
            {
                return *val;
            }
            throw ConfigElementNotFoundError("Config item with name " + name + " and type " +
                                             type_name<T>() + " not found (types do not match)");
        }
    };

    /**
     * @brief Config with additional Config objects for controllers
     */
    class ControllerConfig
    {
        friend class ConfigParser;
        friend class DeviceConfig;

    public:
        ControllerConfig()
        {
        }

        Config& getControllerConfig(const std::string controllerName);

    protected:
        ControllerConfig(const ControllerConfig& orig);

        /**
         * @brief Add a Config for a controller with name
         * @param controllerName    name of the controller
         * @param controllerConfig  Config object
         */
        void addControllerConfig(const std::string controllerName,
                                 std::shared_ptr<Config> controllerConfig);

        std::map<std::string, std::shared_ptr<Config>> controllerConfigs;
    };

    /**
     * @brief Create an error message that prints part of the current Config object to make
     *          it easiier to find the place with the erro rin the xml file
     * @param message   The message that should be printed
     * @param context   The Config object that should serve as context
     * @return
     */
    std::string createErrorMessageWithContext(std::string message, const Config& context);

} // namespace armarx::control::hardware_config
