hardware_config documentation
=============================

The hardware_config package contains data structures to represent hardware configuration and a parser to fill these data structures from a xml configuration file. The file structure has been redesigned to better fit the driver structures und allow for type safety.
A goal is to enforce compliance of xml file and driver meaning attributes defined in xml but not read by the driver or the other way round lead to an exception.

The most important class is `Config`. It represents a key value store for all supported types. There are getters for all supported types like `getUint` for UInt.
Lists and matrices are also supported. A getter for a 2 by 3 Float matrix is `getMatrix<2,3,float>`, a getter for a Int list is getIntList.

Config objects are passed as a reference to Device constructors (in the device driver) and have to be read in the constructor. After the constructor returns it is checked, if all keys have been read in the Config object.

xml file structure (informal description):

```xml
<HardwareConfig robotName="...">
    Profiles...
    Device...
</HardwareConfig>
```

Profiles:

```xml
<Profiles>
    SlaveProfile...
    DeviceProfile...
</Profiles>
```

SlaveProfile:

```xml
<Slave type="..." profilename="...">
    (Identifier)
    (Config)
</Slave>

DeviceProfile:

```xml
<Device type="..." profilename="...">
    (Config)
    Controller...
</Device>
```

Device:

```xml
<Device type="..." name="..." (profile="...")>
    (Config)
    Controller...
    Subdevice...
</Device>
```

Identifier (only needs to be complete after identifiers of profile and slave have been merged):

```xml
<Identifier>
    <VendorID>...</VendorID>
    <ProductID>...</ProductID>
    <Serial>...</Serial>
</Identifier>
```

Config:

```xml
<Config>
    ConfigContent
</Config>
```

Controller:

```xml
<Controller type="...">
    ConfigContent
</Controller>
```

Subdevice:

```xml
<Subdevice name="..." type="..." (profile="...")>
    (Config)
    Controller...
</Subdevice>
```

ConfigContent:

```xml
Float...
Int...
UInt...
LinearConvertedValue...
String...
FloatList...
IntList...
UIntList...
FloatMatrix...
IntMatrix...
UIntMatrix...
```

Float:

```xml
<Float name="...">...</Float>
```

Int:

```xml
<Int name="...">...</Int>
```

UInt:

```xml
<UInt name="...">...</UInt>
```

LinearConvertedValue:

```xml
<LinearConvertedValue name="..." (factor="...") (factorAbs="...") (factorSign="+/-") (offset="...")/>
```

String:

```xml
<String name="...">...</String>
```

FloatList:

```xml
<FloatList name="...">... ... ... ... ...</FloatList>
```

IntList:

```xml
<IntList name="...">... ... ... ... ...</IntList>
```

UIntList:

```xml
<UIntList name="...">... ... ... ... ...</UIntList>
```

FloatMatrix:

```xml
<FloatMatrix rows="..." columns="...">... ... ... ... ...</FloatMatrix>
```

IntMatrix:

```xml
<IntMatrix rows="..." columns="...">... ... ... ... ...</IntMatrix>
```

UIntMatrix:

```xml
<UIntMatrix rows="..." columns="...">... ... ... ... ...</UIntMatrix>
```

Some explanations:

- `...`: a floating point, integer or string value (depending on context)
- `SomeName...`: 0..n instances of SomeName
- `(SomeName)`: 0..1 instances of SomeName
- Device profiles can be used by Devices or Subdevices, Slave profiles can be used by Slaves.
