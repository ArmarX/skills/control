#pragma once

#include <inttypes.h>

#include <string>

#include <armarx/control/hardware_config/Config.h>

namespace armarx::control::hardware_config
{
    /**
     * @brief Data structure holding the information necessary to create a SlaveIdentifier.
     * Can be printed to streams and compared for equality.
     */
    class SlaveIdentifierConfig
    {
        friend class ConfigParser;

    public:
        SlaveIdentifierConfig() = default;

        std::uint32_t getVendorID();
        std::uint32_t getProductCode();
        std::uint32_t getSerialNumber();
        std::string getName();

        /**
         * @brief   Check if the SlaveIdentifierConfig is complete and valid.
         * @param   errorMessages vector that receives the messages describing the cause if invalid.
         *          If true is returned then no message is appended to the vector.
         * @return  iff this SlaveIdentifierConfig is valid
         */
        bool isValid(std::vector<std::string>& errorMessages);

        friend std::ostream&
        operator<<(std::ostream& stream, const SlaveIdentifierConfig& rhs)
        {
            stream << "Name: " << rhs.name;

            if (rhs.vendorTag != ConfigTag::Invalid)
            {
                stream << ", VendorID: 0x" << std::hex << rhs.vendorID << std::dec << " ("
                       << rhs.vendorID << ")";
            }
            if (rhs.productTag != ConfigTag::Invalid)
            {
                stream << ", ProductCode: 0x" << std::hex << rhs.productCode << std::dec << " ("
                       << rhs.productCode << ")";
            }
            if (rhs.serialTag != ConfigTag::Invalid)
            {
                stream << ", SerialNumber: 0x" << std::hex << rhs.serialNumber << std::dec << " ("
                       << rhs.serialNumber << ")";
            }
            return stream;
        }

        bool
        operator==(const SlaveIdentifierConfig& other) const
        {
            return vendorID == other.vendorID && productCode == other.productCode &&
                   serialNumber == other.serialNumber;
        }

    private:
        std::uint32_t vendorID;
        std::uint32_t productCode;
        std::uint32_t serialNumber;
        std::string name = "unknown";
        ConfigTag vendorTag = ConfigTag::Invalid;
        ConfigTag productTag = ConfigTag::Invalid;
        ConfigTag serialTag = ConfigTag::Invalid;
        ConfigTag nameTag = ConfigTag::Invalid;

    private:
        void setVendorID(std::uint32_t vendorID, ConfigTag tag);
        void setProductCode(std::uint32_t productCode, ConfigTag tag);
        void setSerialNumber(std::uint32_t serial, ConfigTag tag);
        void setName(std::string name, ConfigTag tag);

        template <typename T>
        void
        set(T value, ConfigTag tag, T& field, ConfigTag& tagField)
        {
            if (tagField == tag)
            {
                throw ConfigInsertError("Config Node with name " + name +
                                        " is defined multiple times in " + tagName(tag) + " nodes");
            }
            else if (tagField > tag)
            {
                // The config nodes have been parsed in the wrong order.
                // But the new node should not overwrite the old one
                // So we just return and don't change the entry
                return;
            }

            field = value;
            tagField = tag;
        }
    };

} // namespace armarx::control::hardware_config
