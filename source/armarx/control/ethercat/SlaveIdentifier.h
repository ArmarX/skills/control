/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <cstdint>
#include <iostream>
#include <string>


namespace armarx
{
    class RapidXmlReaderNode;
}

#include <armarx/control/hardware_config/SlaveIdentifierConfig.h>

namespace armarx::control::ethercat
{

    /**
     * @class SlaveIdentifier
     * @ingroup Library-ethercat
     *
     * @brief The SlaveIdentifier class is a POD-type representing a unique set of values
     * identifying an EtherCAT slave.
     *
     * The requirement of being a POD-type requires a pre-defined length of the name-variable.
     * Therefore it might happen, that the name of a SlaveIdentifier is cut after
     * a pre-defined amount of characters.
     */
    class SlaveIdentifier
    {
        static constexpr std::uint8_t MAX_NAME_LENGTH = 128;

    public:
        SlaveIdentifier() = default;
        /**
         * @brief Construct a SlaveIdentifier from a RapidXmlReaderNode by parsing the node for
         * following entries:
         * \li VendorID
         * \li ProductCode
         * \li SerialNumber
         *
         * @param node The RapidXmlReaderNode which is used as input.
         */
        SlaveIdentifier(hardware_config::SlaveIdentifierConfig& config);

        /** The index of the slave on the bus */
        std::int16_t slaveIndex = -1;
        /** The unique id of the vendor of the slave hardware */
        std::uint32_t vendorID = 0;
        /** The product code of the slave. The same product code means the same salve hardware */
        std::uint32_t productCode = 0;
        /** The serial number of a slave with a certain productCode */
        std::uint32_t serialNumber = 0;
        /** The revision number of the slave hard- or firmware (is not used for identifying a
            slave on the bus) */
        std::uint32_t revisionNumber = 0;

        friend std::ostream&
        operator<<(std::ostream& stream, const SlaveIdentifier& rhs)
        {
            stream << rhs.toString();
            return stream;
        }

        /**
         * @brief Returns the combination of slave name and name of the parent device of a
         * slave identifier as a string object.
         *
         * This creates a new std::string obejct and therefore might allocate memory.
         *
         * @return The name as a std::string
         */
        std::string getName() const;

        /**
         * @brief Returns the combination of slave name and name of the parent device of a
         * slave identifier as char array.
         *
         * This function does \b not allocate memory and is safe to be used in a rt-thread.
         *
         * @return The name as a const pointer to a char array
         */
        const char* getNameAsCStr() const;

        /**
         * @brief Sets the slave name of a SlaveIdentifier and returns whether the new name fits
         * together with the parent device name into the name-variable.
         * The name of the parent device will be kept, unless the combined name is too long for the
         * size of the name-variable and some parts were lost.
         *
         * @param name the new slave name
         * @return true if the provided name could be fully set as the salve name,
         * false if parts of the combined name needed to be cut
         */
        bool setName(const std::string& name);

        /**
         * @brief Sets the name of the parent device of a slave identifier.
         * @see setName for conditions on possible cutting of the provided name.
         *
         * @param parentDeviceName the new name of the parent device
         * @return true if the provided name could be set without losing data, false if not.
         */
        bool setParentDeviceName(const std::string& parentDeviceName);

        /**
         * @brief Returns a full string representation of a SlaveIdentifier.
         *
         * This representation contains all members seperated by newlines.
         * Each line will be prefixed by the provided linePrefix.
         *
         * @param linePrefix the string which will be put in front of every line
         * @return A full string representation.
         */
        std::string toString(const std::string& linePrefix = "") const;

        /**
         * @brief Returns a minimal string representation of a SlaveIdentifier.
         *
         * This representation contains only the combined name and the slaveIndex.
         * Each printed member will be prefixed by the provided linePrefix and printed on its own
         * line.
         *
         * @param linePrefix the string which will be put in front of every line
         * @return A minimal string representation.
         */
        std::string toMinimalString(const std::string& linePrefix = "") const;

    private:
        char m_name[MAX_NAME_LENGTH] = "unknown (unknown)";
        std::uint16_t m_start_pos_device_name{7};
    };

} // namespace armarx::control::ethercat
