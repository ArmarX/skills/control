#pragma once

#include <cstdint>
#include <memory>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include "ErrorReporting.h"
#include "SlaveIdentifier.h"


namespace armarx::control::ethercat
{

    class Bus;

    class SlaveErrorRegistersDevice;
    using SlaveErrorRegistersDevicePtr = std::shared_ptr<SlaveErrorRegistersDevice>;


    /**
     * @class SlaveInterface
     * @ingroup Library-ethercat
     * @brief Brief description of class SlaveInterface.
     *
     * Detailed description of class SlaveInterface.
     */
    class SlaveInterface : public armarx::Logging
    {

    public:
        SlaveInterface(const SlaveIdentifier& slaveIdentifier);
        virtual ~SlaveInterface() override
        {
        }

        /**
         * This is called after EtherCAT Bus is PreOp Mode. This is where the PDO Mapping can be configured for the slave.
         */
        virtual void doMappings() = 0;

        /**
         * This gets triggered by the bus controller before it will start the control loop.
         * If a slave needs more preparation than just getting in EtherCAT Op-Mode this should be done here.
         * So slaves can assume that the EtherCAT state machine is in Op-Mode so PDO's are available.
         * Attention!!! This needs to be implemented cooperative
         * @return true if the prepare is finished an don't needs to be called again
         */
        virtual bool prepareForRun() = 0;

        /**
         * This method gets triggered by the Bus Controller, this function hast to be implemented cooperative.
         * The Bus controller will guarantee that the process data will be update before each call.
         */
        virtual void execute() = 0;

        /**
         * This gets triggered by the bus Controller before it will close the EtherCAT bus.
         * So if the device need to do something before to get in a safe state, this can be done here.
         * Attention!!! This needs to be implemented cooperative
         * @return if slave is shut down
         */
        virtual bool shutdown() = 0;

        virtual void setInputPDO(void* ptr) = 0;

        virtual void setOutputPDO(void* ptr) = 0;

        /**
         * This gets called between the SafeOp an the Op state of the bus at the initizialisation.
         * The slave can assume that the PDO is already mapped
         */
        virtual void prepareForOp() = 0;

        /**
         * @brief This gets called after prepareForOp() was called. This is useful if prepareForOp()
         * executes a long running initialization and needs to be done before the slave goes into op
         */
        virtual void
        finishPreparingForOp()
        {
        }

        virtual void
        prepareForSafeOp()
        {
        }

        virtual void
        finishPreparingForSafeOp()
        {
        }

        /**
         * This function indicates if there is a error or Problem with this slave. It should not
         * @return true if there is an error/problem with this slave otherwise false;
         */
        virtual bool hasError() = 0;

        virtual bool
        isEmergencyStopActive() const
        {
            return false;
        }

        virtual bool
        recoverFromEmergencyStop()
        {
            return true;
        }

        /**
         * This tries to clear oder fix the errors or problems of the slave or just gives detailed information about the problem.
         * If hasError == false this function does nothing.
         * @return true if the function is trying to recover the slave or there is no error, false is send if this just reports the info
         */
        virtual bool handleErrors();


        std::uint16_t getSlaveNumber() const;

        const SlaveIdentifier& getSlaveIdentifier() const;

        virtual void setName(const std::string& name);
        void setParentDeviceName(const std::string& name);
        void setErrorRegistersDevice(SlaveErrorRegistersDevicePtr errorRegistersDevice);
        SlaveErrorRegistersDevicePtr getErrorRegistersDevice() const;

        virtual bool
        hasPDOMapping() const
        {
            return false;
        }

    protected:
        SlaveIdentifier slaveIdentifier;
        SlaveErrorRegistersDevicePtr errorRegistersDevice;
    };


    template <class InputT, class OutputT>
    class SlaveInterfaceWithIO : public SlaveInterface
    {

    public:
        using SlaveInterface::SlaveInterface;

        void
        setInputPDO(void* ptr) override
        {
            const auto ptrAsInt = reinterpret_cast<std::uint64_t>(ptr);
            ARMARX_CHECK_EXPRESSION((ptrAsInt % alignof(InputT)) == 0)
                << "\nThe alignment is wrong!\nIt has to be " << alignof(InputT)
                << ", but the data is aligned with " << ptrAsInt % alignof(std::max_align_t)
                << "!\nThis is an offset of " << (ptrAsInt % alignof(InputT))
                << " bytes!\nThe datatype is " << GetTypeString<InputT>() << "\nIts size is "
                << sizeof(InputT) << " bytes";
            inputs = static_cast<InputT*>(ptr);
        }

        void
        setOutputPDO(void* ptr) override
        {
            const auto ptrAsInt = reinterpret_cast<std::uint64_t>(ptr);
            ARMARX_CHECK_EXPRESSION((ptrAsInt % alignof(OutputT)) == 0)
                << "\nThe alignment is wrong!\nIt has to be " << alignof(OutputT)
                << ", but the data is aligned with " << ptrAsInt % alignof(std::max_align_t)
                << "!\nThis is an offset of " << (ptrAsInt % alignof(OutputT))
                << " bytes!\nThe datatype is " << GetTypeString<OutputT>() << "\nIts size is "
                << sizeof(OutputT) << " bytes";
            outputs = static_cast<OutputT*>(ptr);
        }

        bool
        hasPDOMapping() const final override
        {
            return true;
        }

        OutputT*
        getOutputsPtr()
        {
            return outputs;
        }

        InputT*
        getInputsPtr()
        {
            return inputs;
        }

    protected:
        InputT* inputs{nullptr};
        OutputT* outputs;
    };


    template <typename Slave>
    std::unique_ptr<armarx::control::ethercat::SlaveInterface>
    createSlave(const SlaveIdentifier& sid)
    {
        SlaveIdentifier sidWithDefaultName(sid);
        sidWithDefaultName.setName(Slave::getDefaultName());

        if (Slave::isSlaveIdentifierAccepted(sidWithDefaultName))
        {
            return std::make_unique<Slave>(sidWithDefaultName);
        }

        return nullptr;
    }

} // namespace armarx::control::ethercat
