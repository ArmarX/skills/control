#pragma once

#include <cstdint>
#include <ostream>

#include <ethercattype.h>

namespace armarx::control::ethercat
{

    /**
     * @class EtherCATState
     * @ingroup Library-ethercat
     *
     * @brief This class is a wrapper around an enum containing the different EtherCAT states.
     *
     * The EtherCAT states have been defined by Beckhoff here:
     * https://infosys.beckhoff.com/english.php?content=../content/1033/ax2000-b110/html/bt_ecbasics_ecstatemachine.htm
     *
     * This wrapper allows for easy, well defined construction from std::uint16_t
     * and for converting the state into a string representation.
     * The underlying numerical values are the same as defined by the SOEM library here:
     * https://github.com/OpenEtherCATsociety/SOEM/blob/master/soem/ethercattype.h
     */
    class EtherCATState
    {
    public:
        /**
         * @brief The EtherCAT state enum
         */
        enum Value : std::uint16_t
        {
            /// State is not valid, e.g. if a request for reading the actual bus state failed.
            invalid = EC_STATE_NONE,
            /// Initial state after switch a EtherCAT slave on.
            /// No communication is possible.
            init = EC_STATE_INIT,
            /// Pre-operational state.
            /// SDO communication is possible, but no PDOs are updated.
            preOp = EC_STATE_PRE_OP,
            /// Boot state. The firmware could be updated in this state, but this functionality
            /// is not supported in theís framework.
            boot = EC_STATE_BOOT,
            /// Safe-operational state. PDO inputs (data from the slaves) can be updated, but
            /// no data can be sent vie PDOs to the slaves.
            safeOp = EC_STATE_SAFE_OP,
            /// Operational state.
            /// Both PDO inputs and outputs can be updated.
            op = EC_STATE_OPERATIONAL,
            /// Safe-operational state after an error has happend.
            safeOpError = EC_STATE_SAFE_OP + EC_STATE_ERROR,
        };

        EtherCATState() = default;
        /**
         * @brief Construct a new EtherCATState from another EtherCATState.
         */
        constexpr EtherCATState(Value state) : value(state)
        {
        }
        /**
         * @brief Construct a new EtherCATState from a std::uint16_t (e.g. a state value provided
         * by SOEM).
         */
        constexpr EtherCATState(std::uint16_t state) : value(fromUInt16(state))
        {
        }

        /**
         * @brief Returns the wrapped enum value.
         */
        operator Value() const
        {
            return value;
        }

        /**
         * @brief Delete bool-operator to prevent usages like if(state) which does not make sense
         * in this context and might lead to unexpected behaviour if being allowed to use.
         */
        explicit operator bool() = delete;

        /**
         * @returns A string representation of this EtherCATState.
         */
        constexpr const char*
        c_str() const
        {
            switch (value)
            {
                case Value::invalid:
                    return "EC_STATE_NONE";
                case Value::init:
                    return "EC_STATE_INIT";
                case Value::preOp:
                    return "EC_STATE_PRE_OP";
                case Value::boot:
                    return "EC_STATE_BOOT";
                case Value::safeOp:
                    return "EC_STATE_SAFE_OP";
                case Value::op:
                    return "EC_STATE_OPERATIONAL";
                case Value::safeOpError:
                    return "EC_STATE_SAFE_OP + EC_STATE_ERROR";
            }
            return "UNKNOWN_STATE";
        }

        friend std::ostream&
        operator<<(std::ostream& stream, const EtherCATState& rhs)
        {
            stream << rhs.c_str();
            return stream;
        }

    private:
        Value value;

        constexpr Value
        fromUInt16(std::uint16_t state)
        {
            switch (state)
            {
                case EC_STATE_INIT:
                    return Value::init;
                case EC_STATE_PRE_OP:
                    return Value::preOp;
                case EC_STATE_BOOT:
                    return Value::boot;
                case EC_STATE_SAFE_OP:
                    return Value::safeOp;
                case EC_STATE_OPERATIONAL:
                    return Value::op;
                case EC_STATE_SAFE_OP + EC_STATE_ERROR:
                    return Value::safeOpError;
                default:
                    return Value::invalid;
            }
        }
    };

} // namespace armarx::control::ethercat
