/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Stefan Reither( stefan.reither at kit dot edu)
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <bitset>
#include <cstdint>
#include <optional>
#include <vector>

namespace armarx::control::ethercat
{

    constexpr std::uint16_t ESI_Header_StartAdress = 0x0000;
    constexpr std::uint16_t ESI_Header_EndAdress = 0x0040;

    /**
     * @class ESIHandler
     * @ingroup Library-ethercat
     * @brief Brief description of class ESIHandler.
     *
     * Detailed description of class ESIHandler.
     */
    class ESIHandler
    {
    protected:
        // 2 Byte access, data at end address not included in blob
        std::optional<std::vector<std::byte>> readESIBinaryBlob(std::uint16_t slaveIndex,
                                                                std::uint16_t startAddress,
                                                                std::uint16_t endAddress) const;

    private:
        std::vector<std::byte> readFromEEPROM(std::uint16_t slaveIndex,
                                              std::uint16_t startAddress,
                                              std::uint16_t endAddress,
                                              bool has64BitPackets) const;
    };


    // TODO
    // ------------------------------------------
    // Following code has been copied from etherkitten and should be replaced by the original
    // project code once etherkitten has been made a dependecy of armarx
    // ------------------------------------------

    struct ESIHeader
    {
        std::uint16_t pdiControl;
        std::uint16_t pdiConfiguration;
        std::uint16_t syncImpulseLen;
        std::uint16_t pdiConfiguration2;
        std::uint16_t stationAlias;
        std::uint16_t checkSum;
        std::uint32_t vendorID;
        std::uint32_t productCode;
        std::uint32_t revisionNumber;
        std::uint32_t serialNumber;
        std::uint16_t bootstrapReceiveMailboxOffset;
        std::uint16_t bootstrapReceiveMailboxSize;
        std::uint16_t bootstrapSendMailboxOffset;
        std::uint16_t bootstrapSendMailboxSize;
        std::uint16_t standardReceiveMailboxOffset;
        std::uint16_t standardReceiveMailboxSize;
        std::uint16_t standardSendMailboxOffset;
        std::uint16_t standardSendMailboxSize;
        std::uint16_t mailboxProtocol;
        std::uint16_t eepromSize;
        std::uint16_t version;
    };

    enum class MailboxProtocols
    {
        AOE = 0x1,
        EOE = 0x2,
        COE = 0x4,
        FOE = 0x8,
        SOE = 0x10,
        VOE = 0x20,
    };

    using ESIStrings = std::vector<std::string>;

    struct ESIGeneral
    {
        std::uint8_t groupIdx;
        std::uint8_t imgIdx;
        std::uint8_t orderIdx;
        std::uint8_t nameIdx;
        std::uint8_t coEDetails;
        std::uint8_t foEDetails;
        std::uint8_t eoEDetails;
        std::uint8_t soEChannels;
        std::uint8_t dS402Channels;
        std::uint8_t sysmanClass;
        std::uint8_t flags;
        std::int16_t currentOnEBus;
        std::uint16_t physicalPort;
        std::uint16_t physicalMemoryAddress;
        std::bitset<3> identALStatus;
        std::bitset<4> identPhysicalMemoryAddress;
    };

    using ESIFMMU = std::vector<std::uint8_t>;

    struct ESISyncMElement
    {
        std::uint16_t physicalStartAddress;
        std::uint16_t length;
        std::uint8_t controlRegister;
        std::uint8_t statusRegister;
        std::uint8_t enableSynchManager;
        std::uint8_t syncManagerType;
    };

    using ESISyncM = std::vector<ESISyncMElement>;

    struct ESIPDOEntry
    {
        std::uint16_t index;
        std::uint8_t subIndex;
        std::uint8_t nameIdx;
        std::uint8_t dataType;
        std::uint8_t bitLength;
        std::uint16_t flags;
    };

    struct ESIPDOObject
    {
        std::uint16_t pdoIndex;
        std::uint8_t entryCount;
        std::uint8_t syncManager;
        std::uint8_t synchronization;
        std::uint8_t nameIdx;
        std::uint16_t flags;
        std::vector<ESIPDOEntry> entries;
    };

    using ESITxPDO = std::vector<ESIPDOObject>;
    using ESIRxPDO = std::vector<ESIPDOObject>;

    /*!
     * \brief Holds ESI data that can be read from slaves via SII.
     *
     * For details regarding contents and structure, see ETG1000.6.
     */
    struct ESIData
    {
        ESIHeader header{};
        ESIStrings strings;
        std::optional<ESIGeneral> general;
        ESIFMMU fmmu;
        ESISyncM syncM;
        ESITxPDO txPDO;
        ESIRxPDO rxPDO;
    };


    class ESIParser
    {
    public:
        ESIHeader parseHeader(const std::vector<std::byte>& esiBinary);

        std::vector<std::string> parseStrings(const std::vector<std::byte>& esiBinary,
                                              std::uint16_t wordOffset);

        ESIGeneral parseGeneral(const std::vector<std::byte>& esiBinary, std::uint16_t wordOffset);

        ESIFMMU parseFMMU(const std::vector<std::byte>& esiBinary,
                          std::uint16_t wordOffset,
                          std::uint16_t len);

        ESISyncM parseSyncM(const std::vector<std::byte>& esiBinary,
                            std::uint16_t wordOffset,
                            std::uint16_t len);

        std::vector<ESIPDOObject> parsePDOs(const std::vector<std::byte>& esiBinary,
                                            std::uint16_t wordOffset,
                                            std::uint16_t len);

        /*!
         * \brief Parse a standard-conformant binary in SII format to an ESI structure.
         *
         * See ETG1000.6 for the SII specification.
         * \param esiBinary the binary SII to parse
         * \return the parsed ESI data structure
         * \exception ParseException iff esiBinary is not standard-conformant
         */
        ESIData parseESI(const std::vector<std::byte>& esiBinary);
    };

} // namespace armarx::control::ethercat
