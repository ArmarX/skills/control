/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Stefan Reither( stefan dot reither at kit dot edu)
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <condition_variable>
#include <memory>
#include <vector>

#include <ArmarXCore/core/util/PropagateConst.h>

namespace armarx::control::ethercat
{
    class RequestBase;
    class SDOUpdateRequest;

    struct SDOIdentifier;
    class EtherCATState;

    struct RegisterDataList;
    struct EtherCATFrameList;

    /**
     * @class RequestQueue
     * @ingroup Library-ethercat
     * @brief Brief description of class RequestQueue.
     *
     * Detailed description of class RequestQueue.
     */
    class RequestQueue
    {
    public:
        RequestQueue();
        ~RequestQueue();
        RequestQueue& operator=(RequestQueue rhs);

        std::shared_ptr<RequestBase> getNextRequest();
        void postReply(std::shared_ptr<RequestBase>&& request);


        bool postSDOReadRequest(SDOIdentifier sdoIdentifier,
                                std::uint16_t buflen,
                                unsigned char* buf,
                                bool completeAccess);
        bool postSDOWriteRequest(SDOIdentifier sdoIdentifier,
                                 std::uint16_t buflen,
                                 unsigned char* buf,
                                 bool completeAccess);


        bool postChangeStateRequest(std::uint16_t slaveIndex,
                                    EtherCATState state,
                                    bool validate,
                                    EtherCATState* actualState);


        bool postReadStatesRequest(EtherCATState* state);

        bool postRegisterResetRequest(std::uint16_t slaveIndex);

        bool postRegisterReadRequest(std::vector<RegisterDataList>& registerData);

        bool postRegisterReadRequest(EtherCATFrameList* frames, std::uint16_t amountFramesToRead);


    private:
        bool postRequest(const std::shared_ptr<RequestBase>& request);
        bool postSDOUpdateRequest(const std::shared_ptr<SDOUpdateRequest>& request);

        struct QueueImpl;
        PropagateConst<std::unique_ptr<QueueImpl>> pimpl;

        std::condition_variable requestCV;
    };

} // namespace armarx::control::ethercat
