#pragma once

namespace armarx::control::ethercat
{
    /**
     * @class RequestBase
     * @ingroup Library-ethercat
     * @brief Brief description of class RequestBase.
     *
     * Detailed description of class RequestBase.
     */
    class RequestBase
    {
    public:
        virtual ~RequestBase() = default;
        /*!
         * \brief Check whether this request has been processed.
         * \retval true if this request has been processed
         * \retval false if this request is yet to be processed
         */
        bool
        isProcessed() const
        {
            return requestProcessed;
        }

        /*!
         * \brief Signal that this request has been processed.
         *
         * To be used by the request handler.
         */
        void
        setProcessed()
        {
            requestProcessed = true;
        }

        /*!
         * \brief Check whether this request has been processed.
         * \retval true if this request has been processed
         * \retval false if this request is yet to be processed
         */
        bool
        hasFailed() const
        {
            return requestFailed;
        }

        /*!
         * \brief Signal that this request has failed.
         *
         * To be used by the request handler.
         */
        void
        setFailed()
        {
            requestFailed = true;
        }

    private:
        bool requestProcessed = false;
        bool requestFailed = false;
    };
} // namespace armarx::control::ethercat
