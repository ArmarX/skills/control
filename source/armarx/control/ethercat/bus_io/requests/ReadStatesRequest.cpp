#include "ReadStatesRequest.h"

namespace armarx::control::ethercat
{

    ReadStatesRequest::ReadStatesRequest(EtherCATState* state) : state(state)
    {
    }

    void
    ReadStatesRequest::setState(EtherCATState state)
    {
        if (this->state)
        {
            *this->state = state;
        }
    }
} // namespace armarx::control::ethercat
