#include "RegisterReadRequest.h"

#include <sstream>

#include <ArmarXCore/core/logging/Logging.h>
#include <armarx/control/ethercat/bus_io/EtherCATFrame.h>
#include <armarx/control/ethercat/bus_io/SlaveRegisterReadingScheduler.h>

namespace armarx::control::ethercat
{

    RegisterReadRequest::RegisterReadRequest(std::vector<RegisterDataList>* requestedRegisters) :
        requestedRegisters(requestedRegisters)
    {
        frames = SlaveRegisterReadingScheduler::createEtherCATFrameListFromRegisterDataList(
            requestedRegisters);
        this->amountFramesToRead = static_cast<std::uint16_t>(frames->list.size());
    }

    RegisterReadRequest::RegisterReadRequest(EtherCATFrameList* preallocatedFrames,
                                             std::uint16_t amountFramesToRead)
    {
        if (preallocatedFrames)
        {
            frames = preallocatedFrames;
            if (amountFramesToRead > 0)
            {
                this->amountFramesToRead = amountFramesToRead;
            }
            else
            {
                this->amountFramesToRead = static_cast<std::uint16_t>(frames->list.size());
            }
        }
    }


    std::pair<EtherCATFrameList*, std::uint16_t>
    RegisterReadRequest::getFrames()
    {
        if (frames)
        {
            return {frames, amountFramesToRead};
        }
        else
        {
            throw std::logic_error("RegisterReadRequest: No EtherCATFrameList available");
        }
    }

    void
    RegisterReadRequest::updateRequestedRegisters()
    {
        if (requestedRegisters)
        {
            SlaveRegisterReadingScheduler::updateRegisterDataFromEtherCATFrameList(
                frames, requestedRegisters);
        }
    }
} // namespace armarx::control::ethercat
