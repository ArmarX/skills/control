
#include "SDOUpdateRequest.h"

namespace armarx::control::ethercat
{

    SDOUpdateRequest::SDOUpdateRequest(SDOIdentifier sdoIdentifier,
                                       uint16_t buflen,
                                       unsigned char* buf,
                                       bool readRequest,
                                       bool completeAccess) :
        sdoIdentifier(sdoIdentifier),
        buflen(buflen),
        buf(buf),
        readRequest(readRequest),
        completeAccess(completeAccess)
    {
    }
} // namespace armarx::control::ethercat
