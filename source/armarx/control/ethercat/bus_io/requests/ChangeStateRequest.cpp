#include "ChangeStateRequest.h"

namespace armarx::control::ethercat
{
    ChangeStateRequest::ChangeStateRequest(std::uint16_t slaveIndex,
                                           EtherCATState state,
                                           bool validate,
                                           EtherCATState* actualState) :
        slaveIndex(slaveIndex), state(state), validate(validate), actualState(actualState)
    {
    }

    void
    ChangeStateRequest::setActualState(EtherCATState state)
    {
        if (actualState)
        {
            *actualState = state;
        }
    }
} // namespace armarx::control::ethercat
