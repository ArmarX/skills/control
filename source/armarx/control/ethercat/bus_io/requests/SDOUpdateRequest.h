#pragma once

#include <cstdint>

#include "RequestBase.h"

namespace armarx::control::ethercat
{
    struct SDOIdentifier
    {
        std::uint16_t slaveIndex;
        std::uint16_t index;
        std::uint8_t subIndex;
    };

    /**
     * @class SDOUpdateRequest
     * @ingroup Library-ethercat
     * @brief Brief description of class SDOUpdateRequest.
     *
     * Detailed description of class SDOUpdateRequest.
     */
    class SDOUpdateRequest : public virtual RequestBase
    {
    public:
        SDOUpdateRequest() = default;

        SDOUpdateRequest(SDOIdentifier sdoIdentifier,
                         std::uint16_t buflen,
                         unsigned char* buf,
                         bool readRequest,
                         bool completeAccess = false);

        SDOIdentifier sdoIdentifier{};
        std::uint16_t buflen = 0;
        unsigned char* buf = nullptr;

        bool readRequest = false;
        bool completeAccess = false;
    };
} // namespace armarx::control::ethercat
