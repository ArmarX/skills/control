#pragma once

#include <unordered_map>

#include <armarx/control/ethercat/bus_io/SlaveRegisters.h>

#include "RequestBase.h"

namespace armarx::control::ethercat
{
    struct EtherCATFrameList;

    /**
     * @class RegisterReadRequest
     * @ingroup Library-ethercat
     * @brief Brief description of class RegisterReadRequest.
     *
     * Detailed description of class RegisterReadRequest.
     */
    class RegisterReadRequest : public virtual RequestBase
    {
    public:
        RegisterReadRequest() = default;
        RegisterReadRequest(std::vector<RegisterDataList>* requestedRegisters);
        RegisterReadRequest(EtherCATFrameList* preallocatedFrames,
                            std::uint16_t amountFramesToRead = 0);

        std::vector<RegisterDataList>* requestedRegisters = nullptr;

        std::pair<EtherCATFrameList*, std::uint16_t> getFrames();

        void updateRequestedRegisters();

    private:
        EtherCATFrameList* frames;
        std::uint16_t amountFramesToRead;
    };

} // namespace armarx::control::ethercat
