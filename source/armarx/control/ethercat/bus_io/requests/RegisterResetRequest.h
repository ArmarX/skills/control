#pragma once

#include <cstdint>

#include "RequestBase.h"

namespace armarx::control::ethercat
{
    /**
     * @class RegisterResetRequest
     * @ingroup Library-ethercat
     * @brief Brief description of class RegisterResetRequest.
     *
     * Detailed description of class RegisterResetRequest.
     */
    class RegisterResetRequest : public virtual RequestBase
    {
    public:
        RegisterResetRequest() = default;
        RegisterResetRequest(std::uint16_t slaveIndex);

        std::uint16_t slaveIndex;
    };

} // namespace armarx::control::ethercat
