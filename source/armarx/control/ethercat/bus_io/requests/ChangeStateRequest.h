#pragma once

#include <cstdint>

#include <armarx/control/ethercat/EtherCATState.h>

#include "RequestBase.h"

namespace armarx::control::ethercat
{
    /**
     * @class ChangeStateRequest
     * @ingroup Library-ethercat
     * @brief Brief description of class ChangeStateRequest.
     *
     * Detailed description of class ChangeStateRequest.
     */
    class ChangeStateRequest : public virtual RequestBase
    {
    public:
        ChangeStateRequest() = default;
        ChangeStateRequest(std::uint16_t slaveIndex,
                           EtherCATState state,
                           bool validate,
                           EtherCATState* actualState);

        std::uint16_t slaveIndex = 0;
        EtherCATState state = EtherCATState::invalid;
        bool validate = false;

        void setActualState(EtherCATState state);

    private:
        EtherCATState* actualState = nullptr;
    };

} // namespace armarx::control::ethercat
