#include "RegisterResetRequest.h"


namespace armarx::control::ethercat
{
    RegisterResetRequest::RegisterResetRequest(uint16_t slaveIndex) : slaveIndex(slaveIndex)
    {
    }

} // namespace armarx::control::ethercat
