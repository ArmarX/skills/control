#pragma once

#include <armarx/control/ethercat/EtherCATState.h>

#include "RequestBase.h"

namespace armarx::control::ethercat
{
    /**
     * @class ReadStatesRequest
     * @ingroup Library-ethercat
     * @brief Brief description of class ReadStatesRequest.
     *
     * Detailed description of class ReadStatesRequest.
     */
    class ReadStatesRequest : public virtual RequestBase
    {
    public:
        ReadStatesRequest() = default;
        ReadStatesRequest(EtherCATState* state);

        void setState(EtherCATState state);

    private:
        EtherCATState* state = nullptr;
    };
} // namespace armarx::control::ethercat
