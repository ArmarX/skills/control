#include "SlaveRegisters.h"

namespace armarx::control::ethercat
{
    std::size_t
    getRegisterByteLength(datatypes::RegisterEnum reg)
    {
        static constexpr std::size_t byteSize = 8;
        std::size_t bitLength = datatypes::registerMap.at(reg).bitLength;
        if (bitLength < byteSize)
        {
            bitLength = byteSize;
        }
        return bitLength / byteSize;
    }

} // namespace armarx::control::ethercat
