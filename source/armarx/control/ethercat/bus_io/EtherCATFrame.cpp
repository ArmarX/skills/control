#include "EtherCATFrame.h"

namespace armarx::control::ethercat
{
    EtherCATFrameIterator::EtherCATFrameIterator(EtherCATFrameList* list,
                                                 size_t startIndex,
                                                 size_t count) :
        list(list), nextIndex(startIndex), remaining(count)
    {
    }

    std::pair<EtherCATFrame*, EtherCATFrameMetaData*>
    EtherCATFrameIterator::operator*() const
    {
        return {&list->list[nextIndex].first, &list->list[nextIndex].second};
    }

    bool
    EtherCATFrameIterator::hasCompletedLoop() const
    {
        return nextIndex == 0;
    }

    EtherCATFrameIterator&
    EtherCATFrameIterator::operator++()
    {
        if (remaining > 0)
        {
            nextIndex = (nextIndex + 1) % list->list.size();
            --remaining;
        }
        return *this;
    }

    bool
    EtherCATFrameIterator::atEnd() const
    {
        return remaining == 0;
    }
} // namespace armarx::control::ethercat
