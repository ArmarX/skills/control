#pragma once

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "EtherCATFrame.h"
#include "SlaveRegisters.h"

namespace armarx::control::ethercat
{
    /**
     * @class SlaveRegisterReadingScheduler
     * @ingroup Library-ethercat
     * @brief Brief description of class SlaveRegisterReadingScheduler.
     *
     * Detailed description of class SlaveRegisterReadingScheduler.
     */
    class SlaveRegisterReadingScheduler
    {
    public:
        static EtherCATFrameList* createEtherCATFrameListFromRegisterDataList(
            const std::vector<RegisterDataList>* requestedRegisters);

        static void
        updateRegisterDataFromEtherCATFrameList(const EtherCATFrameList* frameList,
                                                std::vector<RegisterDataList>* requestedRegisters);


        SlaveRegisterReadingScheduler(std::uint16_t slaveCount,
                                      unsigned int updatePeriodInMS,
                                      std::vector<datatypes::RegisterEnum> registerList);
        ~SlaveRegisterReadingScheduler();

        const std::vector<RegisterDataList>& getRegisterData();

        void startReadingNextRegisters();
        bool allRegistersUpdated() const;

    private:
        std::vector<RegisterDataList> registerData;

        EtherCATFrameList* frameList;

        void updateLoop();
        unsigned int updatePeriodInMS{1};
        std::thread errorRegisterUpdateThread;
        std::atomic_bool errorRegisterUpdateThreadRunning{true};

        std::condition_variable cv_readNextRegisters;
        std::mutex mutex_readNextRegisters;
        std::atomic_bool readyToReadNextRegisters{true};
        std::atomic_bool areAllRegistersUpdated{false};

    private:
        /*!
         * \brief Turn a map of which registers to read into a vector of addresses that must be read.
         *
         * Multi-byte registers are accounted for - every byte is added to the list individually.
         * The returned list is sorted.
         * \param toRead the register read map to turn into a list of addresses
         * \return the sorted list of addresses
         */
        static std::vector<int> registerReadMapToAddressList(const RegisterDataList& toRead);

        /*!
         * \brief Calculate the optimal separation of registers into intervals for PDUs.
         *
         * Note that the intervals may not be optimal once you pack them into EtherCAT frames
         * because their size might lead to suboptimal fittings. Usually, the intervals
         * shouldn't be that large though (an Ethernet frame fits 1500 bytes of payload, after all).
         *
         * The interval ends are exclusive (i.e., the interval is [start, end)).
         * \param toRead the registers to fit into intervals
         * \return a list of intervals that cover the registers optimally
         */
        static std::vector<std::pair<int, int>>
        createFrameIntervals(const RegisterDataList& toRead);

        /*!
         * \brief Create the PDUMetaData for a PDU in an EtherCAT frame.
         * \param pduInterval the PDU interval (associated with a slave) to generate metadata for
         * \param pduOffset the offset of the PDU relative to the start of the EtherCATFrame
         * \return the PDU metadata
         */
        static PDUMetaData createPDUMetaData(std::tuple<uint16_t, int, int> pduInterval,
                                             size_t pduOffset);

        /*!
         * \brief Create an EtherCATFrame that reads all the given address intervals via FPRD
         * if sent on the EtherCAT bus.
         * \param slaveAssignedPDUIntervals the PDU intervals to fit in an EtherCATFrame
         * \return the EtherCATFrame along with its metadata
         * \exception std::length_error iff the PDUs are too long to fit in an EtherCAT frame
         */
        static std::pair<EtherCATFrame, EtherCATFrameMetaData>
        createEtherCATFrame(std::vector<std::tuple<uint16_t, int, int>> slaveAssignedPDUIntervals);

        /*!
         * \brief Create an EtherCATFrameList from the given address intervals.
         *
         * The frames will be generated to contain every interval for every slave.
         *
         * This method will try to fit the PDUs into the frames in order,
         * starting a new frame if the next PDU doesn't fit. That is not optimal.
         * If it bugs you, write something smarter.
         * \param pduIntervals The address intervals to fit into EtherCAT frames
         */
        static EtherCATFrameList* createEtherCATFrameList(
            const std::vector<std::pair<std::uint16_t, std::vector<std::pair<int, int>>>>&
                pduIntervals);
    };

} // namespace armarx::control::ethercat
