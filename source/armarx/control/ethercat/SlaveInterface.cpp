#include "SlaveInterface.h"

#include "Bus.h"
#include "SlaveErrorRegistersDevice.h"


namespace armarx::control::ethercat
{

    SlaveInterface::SlaveInterface(const SlaveIdentifier& slaveIdentifier) :
        slaveIdentifier(slaveIdentifier)
    {
    }

    /**
     * This returns the slave number of the slave on the bus +1 because slave 0 is the master
     * @return index in ec_slave array
     */
    std::uint16_t
    SlaveInterface::getSlaveNumber() const
    {
        return static_cast<std::uint16_t>(slaveIdentifier.slaveIndex);
    }


    bool
    SlaveInterface::handleErrors()
    {
        bool retVal;
        retVal = !hasError();
        return retVal;
    }


    const SlaveIdentifier&
    SlaveInterface::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    void
    SlaveInterface::setName(const std::string& name)
    {
        slaveIdentifier.setName(name);
    }

    void
    SlaveInterface::setParentDeviceName(const std::string& name)
    {
        slaveIdentifier.setParentDeviceName(name);
    }

    void
    SlaveInterface::setErrorRegistersDevice(SlaveErrorRegistersDevicePtr errorRegistersDevice)
    {
        ARMARX_CHECK_IS_NULL(this->errorRegistersDevice);
        ARMARX_CHECK_NOT_NULL(errorRegistersDevice);
        this->errorRegistersDevice = errorRegistersDevice;
    }

    SlaveErrorRegistersDevicePtr
    SlaveInterface::getErrorRegistersDevice() const
    {
        ARMARX_CHECK_NOT_NULL(this->errorRegistersDevice);
        return this->errorRegistersDevice;
    }

} // namespace armarx::control::ethercat
