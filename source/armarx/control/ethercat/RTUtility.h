#pragma once

#include <cstdint>

namespace armarx::control::ethercat
{
    /**
     * @class RTUtility
     * @ingroup Library-ethercat
     *
     * @brief The RTUtility class contains functions for improving the realtime capabilities
     * of a thread or of the whole system.
     */
    class RTUtility
    {

    public:
        /**
         * The thread priority for rt-threads.
         *
         * 49 is the highest value that can be safely used, because PRREMPT_RT uses 50 for kernel
         * tasklets and interrupt handler by default.
         */
        static constexpr int RT_THREAD_PRIORITY = 49;

        /**
         * @brief Elevate the thread priority of the calling thread to the given priority.
         *
         * The owning user of the calling thread must be allowed to set its priority to the given
         * value.
         * Each user has a maximum value it can sets its thread priorities to.
         * This maximum value can be configured by editing the file /etc/security/limits.conf
         * according to https://linux.die.net/man/5/limits.conf and setting the entry \b rtprio for
         * the executing users.
         * This function will only work if the operating system is build with a PRREMPT_RT
         * patch (https://wiki.linuxfoundation.org/realtime/documentation/technical_details/start).
         *
         * @param priority the new priority of the calling thread
         * @return true if the requested priority could be set, false if not.
         */
        static bool elevateThreadPriority(int priority);

        /**
         * @brief Pins the calling thread to the CPU with the given id.
         *
         * The thread can only be pinned to a cpu which actually exists on the system.
         *
         * @param cpu The id of the CPU the calling thread should be pinned to
         * @return tue if the pinning was successful, false if not
         */
        static bool pinThreadToCPU(unsigned int cpu);

        /**
         * @brief Activate low latency mode of the system.
         *
         * If the file /dev/cpu_dma_latency exists, open it and write a zero into it. This will
         * tell the power management system not to transition to a high cstate
         * (in fact, the system acts like idle=poll).
         * When the fd to /dev/cpu_dma_latency is closed, the behavior goes back to the system
         * default.
         *
         * Taken from https://wiki.linuxfoundation.org/realtime/documentation/howto/tools/rt-tests.
         *
         * @return true if the low latency mode could be activated, false if not.
         */
        static bool startLowLatencyMode();

        /**
         * @brief Deactivate low latency mode of the system.
         * @return true if low latency mode was active, false if not and nothng needs to be done
         */
        static bool stopLowLatencyMode();
    };
} // namespace armarx::control::ethercat
