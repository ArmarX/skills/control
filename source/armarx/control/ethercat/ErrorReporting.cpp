#include "ErrorReporting.h"

#include <iomanip>

#include <boost/lockfree/policies.hpp>
#include <boost/lockfree/queue.hpp>

#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>

namespace armarx::control::ethercat::error
{
    std::ostream&
    operator<<(std::ostream& stream, const Type& rhs)
    {
        switch (rhs)
        {
            case Type::BusError:
                stream << "BusError";
                break;
            case Type::SlaveError:
                stream << "SlaveError";
                break;
            case Type::DeviceError:
                stream << "DeviceError";
                break;
        }
        return stream;
    }

    std::ostream&
    operator<<(std::ostream& stream, const Severity& rhs)
    {
        switch (rhs)
        {
            case Severity::None:
                stream << "None";
                break;
            case Severity::Warning:
                stream << "WARNING";
                break;
            case Severity::Error:
                stream << "ERROR";
                break;
            case Severity::Fatal:
                stream << "FATAL";
                break;
        }
        return stream;
    }


    struct Reporting::QueueImpl
    {
        static constexpr size_t queueSize = 20;

        boost::lockfree::queue<Entry, boost::lockfree::capacity<queueSize>> queue;
    };

    Reporting::Reporting(std::size_t maxErrorHistorySize) :
        pimpl(std::make_unique<QueueImpl>()), m_maxErrorHistorySize(maxErrorHistorySize)
    {
        setTag("ethercat::ErrorReporting");

        m_timestampReportingStart = IceUtil::Time::now();
        m_errorProcessingThread = std::thread(&Reporting::errorProcessingLoop, this);
    }

    Reporting::~Reporting()
    {
        m_errorProcessingRunning.store(false);
        m_errorProcessingCondition.notify_all();
        m_errorProcessingThread.join();
    }

    Reporting&
    Reporting::getErrorReporting()
    {
        static Reporting _instance;
        return _instance;
    }

    Reporter
    Reporting::getErrorReporter()
    {
        return Reporter(this);
    }

    Reporter&
    Reporting::getGlobalErrorReporter()
    {
        static Reporter _instance = Reporter(&getErrorReporting());
        return _instance;
    }

    void
    Reporting::report(Entry&& error)
    {
        error.m_timestamp = IceUtil::Time::now();
        pimpl->queue.push(error);
        m_errorProcessingCondition.notify_all();
    }

    void
    Reporting::setMaxErrorHistorySize(std::size_t maxErrorHistorySize)
    {
        m_maxErrorHistorySize = maxErrorHistorySize;
    }

    std::optional<std::filesystem::path>
    Reporting::dumpToFile(std::filesystem::path file, std::uint64_t lastNSeconds)
    {
        IceUtil::Time endTime = IceUtil::Time::now();
        IceUtil::Time startTime =
            endTime - IceUtil::Time::seconds(static_cast<std::int64_t>(lastNSeconds));
        if (startTime < m_timestampReportingStart)
        {
            startTime = m_timestampReportingStart;
        }
        std::string yearString = startTime.toString("%y-%m-%d");
        std::string startTimeString = startTime.toString("%H-%M-%S");
        std::string endTimeString = endTime.toString("%H-%M-%S");

        file.replace_filename(std::string(file.stem().c_str()) + "_" + yearString + "__" +
                              startTimeString + "_" + endTimeString);
        file.replace_extension(".errors");

        if (std::filesystem::exists(file))
        {
            file.replace_filename(file.filename().string() + ".new");
        }

        auto history = m_errorHistory;
        auto it = history.begin();
        for (const auto& [key, value] : history)
        {
            if (key.timestamp < startTime.toMicroSeconds())
            {
                it++;
            }
        }
        history.erase(history.begin(), it);

        if (history.size() > 0)
        {
            ARMARX_INFO << history.size() << " errors from " << startTimeString << " to "
                        << endTimeString << " will be saved to file " << file.string();
        }
        else
        {
            ARMARX_VERBOSE << "No errors happend from " << startTimeString << " to "
                           << endTimeString;
            return std::nullopt;
        }


        if (!std::filesystem::exists(file.parent_path()) &&
            !std::filesystem::create_directories(file.parent_path()))
        {
            throw std::runtime_error{"Could not create directories for '" + file.string() + "'"};
        }

        std::ofstream outFile{file.string(), std::ofstream::out};
        if (!outFile)
        {
            throw std::runtime_error{"dumpToFile could not open filestream for '" + file.string() +
                                     "'"};
        }

        for (const auto& [key, value] : history)
        {

            std::string timeStr = value.m_timestamp.toDateTime();
            timeStr = timeStr.substr(timeStr.find(' ') + 1);

            outFile << "[" << timeStr << "]";
            outFile << "[" << value.m_type << "]";
            outFile << "[" << value.m_severity << "] ";
            outFile << std::string(value.m_message, value.m_message_size) << "\n";
            if (value.m_sid.slaveIndex != -1)
            {
                outFile << value.m_sid.toString("          ") << "\n";
            }
        }

        return std::move(file);
    }

    void
    Reporting::errorProcessingLoop()
    {
        Entry e;
        auto handleEntries = [&, this]()
        {
            while (pimpl->queue.pop(e))
            {
                std::int64_t timestamp = e.m_timestamp.toMicroSeconds();
                std::uint16_t i = 0;

                // Check for max size of history
                while (m_errorHistory.size() >= m_maxErrorHistorySize)
                {
                    m_errorHistory.erase(m_errorHistory.begin());
                }

                // Check if there is already an entry with the same timestamp and find correct subindex
                if (m_errorHistory.begin()->first.timestamp == timestamp)
                {
                    i = m_errorHistory.begin()->first.subid + 1;
                }
                while (m_errorHistory.count({timestamp, i}))
                {
                    i++;
                }

                m_errorHistory.insert({{timestamp, i}, e});

                auto logpointer =
                    (loghelper(e.m_metaInfo.file, e.m_metaInfo.line, e.m_metaInfo.function));
                switch (e.m_severity)
                {
                    case error::Severity::None:
                        *logpointer->setBacktrace(false)
                            << ::armarx::MessageTypeT::INFO << deactivateSpam(1.f, e.toString())
                            << e.toString();
                        return;
                    case error::Severity::Warning:
                        *logpointer->setBacktrace(false)
                            << ::armarx::MessageTypeT::WARN
                            << deactivateSpam(e.m_deactivate_spam_seconds, e.toString())
                            << e.toString();
                        return;
                    case error::Severity::Error:
                        *logpointer->setBacktrace(false)
                            << ::armarx::MessageTypeT::ERROR
                            << deactivateSpam(e.m_deactivate_spam_seconds, e.toString())
                            << e.toString();
                        return;
                    case error::Severity::Fatal:
                        *logpointer << ::armarx::MessageTypeT::FATAL
                                    << deactivateSpam(e.m_deactivate_spam_seconds, e.toString())
                                    << e.toString();
                        return;
                }
            }
        };

        while (m_errorProcessingRunning.load() == true)
        {
            std::unique_lock<std::mutex> lock{m_errorProcessingConditionMutex};
            m_errorProcessingCondition.wait(
                lock, [&]() { return !pimpl->queue.empty() || !m_errorProcessingRunning.load(); });
            handleEntries();
        }

        // Also handle leftover entries
        handleEntries();
    }

    Reporter::ReportWrapper
    Reporter::reportWarning(Entry& error)
    {
        error.m_severity = Severity::Warning;
        m_errorCount++;
        return Reporter::ReportWrapper{error, this};
    }

    Reporter::ReportWrapper
    Reporter::reportError(Entry& error)
    {
        error.m_severity = Severity::Error;
        m_errorCount++;
        return Reporter::ReportWrapper{error, this};
    }

    void
    Reporter::reportErrorAndThrow(Entry& error)
    {
        error.m_severity = Severity::Fatal;
        m_errorCount++;
        m_errorReporting->report(std::move(error));
        throw std::runtime_error(error.toString());
    }

    bool
    Reporter::hasErrors() const
    {
        return m_errorCount > 0;
    }

    Reporter::Reporter(Reporting* errorReporting) : m_errorReporting(errorReporting)
    {
    }

    Entry&
    Error::slaveIdentifier(SlaveIdentifier sid)
    {
        m_sid = sid;
        return *this;
    }

    Entry&
    Error::errorType(Type type)
    {
        m_type = type;
        return *this;
    }

    Entry&
    Entry::metaInfo(const char* file, int line, const char* function)
    {
        snprintf(m_metaInfo.file, sizeof(m_metaInfo.file), "%s", file);
        m_metaInfo.line = line;
        snprintf(m_metaInfo.function, sizeof(m_metaInfo.function), "%s", function);
        return *this;
    }

    Entry&
    Entry::deviceName(const char* deviceName)
    {
        snprintf(m_device_name, sizeof(m_device_name), "%s", deviceName);
        return *this;
    }

    std::string
    Entry::toString() const
    {
        std::stringstream ss;
        ss << "[" << m_type << "] ";
        ss << std::string(m_message, m_message_size) << "\n";
        if (m_sid.slaveIndex != -1)
        {
            ss << m_sid.toMinimalString("    ");
        }
        if (m_device_name[0] != '\0')
        {
            ss << "    " << std::left << std::setw(16) << "Name: " << std::string(m_device_name);
        }
        return ss.str();
    }

    Reporter::ReportWrapper::ReportWrapper(Entry& error, Reporter* reporter) :
        m_error(error), m_reporter(reporter)
    {
    }

    Reporter::ReportWrapper::~ReportWrapper()
    {
        m_reporter->m_errorReporting->report(std::move(m_error));
    }

    Reporter::ReportWrapper&
    Reporter::ReportWrapper::deactivateSpam(float seconds)
    {
        m_error.m_deactivate_spam_seconds = std::fmax(0.f, seconds);
        return *this;
    }

} // namespace armarx::control::ethercat::error
