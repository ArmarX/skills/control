/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SlaveIdentifier.h"

#include <iomanip>
#include <iostream>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>


namespace armarx::control::ethercat
{
    SlaveIdentifier::SlaveIdentifier(hardware_config::SlaveIdentifierConfig& config)
    {
        setName(config.getName());
        vendorID = config.getVendorID();
        productCode = config.getProductCode();
        serialNumber = config.getSerialNumber();
    }

    std::string
    SlaveIdentifier::getName() const
    {
        return std::string(m_name);
    }

    const char*
    SlaveIdentifier::getNameAsCStr() const
    {
        return &m_name[0];
    }

    bool
    SlaveIdentifier::setName(const std::string& name)
    {
        std::string dname = std::string(&m_name[m_start_pos_device_name]);
        std::size_t old_len = strlen(m_name);
        int written_chars =
            std::snprintf(m_name, sizeof(m_name), "%s%s", name.c_str(), dname.c_str());
        m_start_pos_device_name += static_cast<std::size_t>(written_chars) - old_len;
        return written_chars > static_cast<int>(MAX_NAME_LENGTH);
    }

    bool
    SlaveIdentifier::setParentDeviceName(const std::string& parentDeviceName)
    {
        return std::snprintf(&m_name[m_start_pos_device_name],
                             sizeof(m_name) - m_start_pos_device_name,
                             " (%s)",
                             parentDeviceName.c_str()) > static_cast<int>(MAX_NAME_LENGTH);
    }

    std::string
    SlaveIdentifier::toString(const std::string& linePrefix) const
    {
        std::stringstream ss;
        constexpr std::uint16_t fw = 16;
        // clang-format off
        using namespace std;
        ss << linePrefix << left << setw(fw) << "Name:"
            << getName() << "\n";
        ss << linePrefix << left << setw(fw) << "VendorID:"
            << hex << "0x" << vendorID << dec << " (" << vendorID << ")\n";
        ss << linePrefix << left << setw(fw) << "ProductCode:"
            << hex << "0x" << productCode << dec << " (" << productCode << ")\n";
        ss << linePrefix << left << setw(fw) << "RevisionNumber:"
            << hex << "0x" << revisionNumber << dec << " (" << revisionNumber << ")\n";
        ss << linePrefix << left << setw(fw) << "SerialNumber:"
            << hex << "0x" << serialNumber << dec << " (" << serialNumber << ")\n";
        ss << linePrefix << left << setw(fw) << "SlaveIndex:"
            << hex << "0x" << slaveIndex << dec << " (" << slaveIndex << ")";
        // clang-format on

        return ss.str();
    }

    std::string
    SlaveIdentifier::toMinimalString(const std::string& linePrefix) const
    {
        std::stringstream ss;
        constexpr std::uint16_t fw = 16;
        // clang-format off
        using namespace std;
        ss << linePrefix << left << setw(fw) << "Name:"
            << getName() << "\n";
        ss << linePrefix << left << setw(fw) << "SlaveIndex:"
            << hex << "0x" << slaveIndex << dec << " (" << slaveIndex << ")";
        // clang-format on

        return ss.str();
    }
} // namespace armarx::control::ethercat
