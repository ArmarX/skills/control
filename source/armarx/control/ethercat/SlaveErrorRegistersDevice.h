#pragma once


#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueBase.h>

#include "bus_io/SlaveRegisters.h"

namespace armarx::control::ethercat
{
    class SlaveErrorRegistersSensorValue : public armarx::SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

            std::uint8_t linkLostCounterPort0{0};
        std::uint8_t linkLostCounterPort1{0};
        //        std::uint8_t linkLostCounterPort2{0};
        //        std::uint8_t linkLostCounterPort3{0};

        std::uint8_t frameErrorCounterPort0{0};
        std::uint8_t frameErrorCounterPort1{0};
        //        std::uint8_t frameErrorCounterPort2{0};
        //        std::uint8_t frameErrorCounterPort3{0};

        std::uint8_t rxErrorCounterPort0{0};
        std::uint8_t rxErrorCounterPort1{0};
        //        std::uint8_t rxErrorCounterPort2{0};
        //        std::uint8_t rxErrorCounterPort3{0};

        std::uint8_t forwardedRxErrorCounterPort0{0};
        std::uint8_t forwardedRxErrorCounterPort1{0};
        //        std::uint8_t forwardedRxErrorCounterPort2{0};
        //        std::uint8_t forwardedRxErrorCounterPort3{0};

        static SensorValueInfo<SlaveErrorRegistersSensorValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<SlaveErrorRegistersSensorValue> svi;

            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::linkLostCounterPort0,
                                  "LinkLostCounter.Port0");
            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::linkLostCounterPort1,
                                  "LinkLostCounter.Port1");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::linkLostCounterPort2,
            //                                  "LinkLostCounter.Port2");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::linkLostCounterPort3,
            //                                  "LinkLostCounter.Port3");

            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::frameErrorCounterPort0,
                                  "FrameErrorCounter.Port0");
            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::frameErrorCounterPort1,
                                  "FrameErrorCounter.Port1");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::frameErrorCounterPort2,
            //                                  "FrameErrorCounter.Port2");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::frameErrorCounterPort3,
            //                                  "FrameErrorCounter.Port3");

            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::rxErrorCounterPort0,
                                  "RxErrorCounter.Port0");
            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::rxErrorCounterPort1,
                                  "RxErrorCounter.Port1");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::rxErrorCounterPort2,
            //                                  "RxErrorCounter.Port2");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::rxErrorCounterPort3,
            //                                  "RxErrorCounter.Port3");

            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::forwardedRxErrorCounterPort0,
                                  "ForwardedRxErrorCounter.Port0");
            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::forwardedRxErrorCounterPort1,
                                  "ForwardedRxErrorCounter.Port1");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::forwardedRxErrorCounterPort2,
            //                                  "ForwardedRxErrorCounter.Port2");
            //            svi.addMemberVariable(&SlaveErrorRegistersSensorValue::forwardedRxErrorCounterPort3,
            //                                  "ForwardedRxErrorCounter.Port3");

            return svi;
        }
    };

    TYPEDEF_PTRS_SHARED(SlaveErrorRegistersDevice);

    /**
     * @class SlaveErrorRegistersDevice
     * @ingroup Library-ethercat
     * @brief Brief description of class SlaveErrorRegistersDevice.
     *
     * Detailed description of class SlaveErrorRegistersDevice.
     */
    class SlaveErrorRegistersDevice : virtual public SensorDevice
    {
    public:
        SlaveErrorRegistersDevice(const std::string& name, std::uint16_t slaveIndex) :
            DeviceBase(name), SensorDevice(name), slaveIndex(slaveIndex)
        {
        }

        /**
         *  @see RtUtility:startLowLatencyMode
         *  @see armarx::control::ethercat::SlaveIdentifier::vendorID
         */
        const SensorValueBase*
        getSensorValue() const override
        {
            return &value;
        }

        SlaveErrorRegistersSensorValue value;

        void updateValues(const RegisterDataList* data);

    private:
        std::uint16_t slaveIndex;
    };

} // namespace armarx::control::ethercat
