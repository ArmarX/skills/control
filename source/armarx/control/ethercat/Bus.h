#pragma once

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>

#include <armarx/control/ethercat/SlaveIdentifier.h>
#include <armarx/control/hardware_config/ConfigParser.h>

#include "BusErrorHandler.h"
#include "ErrorReporting.h"
#include "EtherCATState.h"
#include "SlaveIdentifier.h"
#include "bus_io/BusIO.h"

namespace VirtualRobot
{
    using RobotPtr = std::shared_ptr<class Robot>;
} // namespace VirtualRobot

namespace armarx
{
    class RapidXmlReaderNode;
    class DefaultRapidXmlReaderNode;
} // namespace armarx

namespace armarx::control::ethercat
{
    class SlaveInterface;
    class DeviceInterface;
    class SlaveRegisterReadingScheduler;

    enum class PDOValidity
    {
        None,
        OnlyInputs, // slaves can "send" updates, but master cannot "send" updates
        Both
    };

    enum class BusFunctionalState
    {
        Running, // normal operational mode, all slaves are working as intended
        Reinitializing, // one or more slaves were lost and are either still lost or in the process of reinitializing
        EmergencyStop, // one or more slaves report that their hardware emergency stop signal is active
        Shutdown // bus is shutdown or it is in the process of initializing
    };


    /**
     * @class Bus
     * @ingroup Library-ethercat
     * @brief Brief description of class Bus.
     *
     * Detailed description of class Bus.
     */
    class Bus : public BusIO, virtual public Logging
    {
        friend BusErrorHandler;

    public:
        /**
         * This returns the one and only Bus object.
         * An implements the singelton pattern
         * @return The Bus instance
         */
        static Bus& getBus();

        std::string logErrorsToFile(std::filesystem::path path, unsigned int lastSeconds);

        void setSocketFileDescriptor(int socketFileDescriptor);

        void setIfName(const std::string& ifname);

        void setAndParseHardwareConfig(const MultiNodeRapidXMLReader& hwconfig);

        void setRobot(const VirtualRobot::RobotPtr& robot);

        /**
         * @brief configureErrorCountersReading configure stuff
         *
         * more configure stuff explanation
         *
         * @param enable sadösdm
         * @param periodInMS asdsads
         */
        void configureErrorCountersReading(bool enable, unsigned int periodInMS);

        PDOValidity getPDOValidity() const;

        /**
         *
         * @return false if the bus has not been switched to .
         */
        /**
         * @brief Updates all information on the bus, so all commands will be send to the Bus and all
         * sensor and other monitored values will be recived from the bus.
         * @param doErrorHandling
         * @return asdojis \ref EtherCATState::op
         */
        bool rtUpdateBus(bool doErrorHandling = true);

        /**
         * Shuts the bus down by executing the shutdown-hook for each slave and afterwards switch
         * all slaves to EC_STATE_INIT. If the bus state is already EC_STATE_INIT these steps are skipped.
         * Afterwards the socket will be closed and no communication with the bus is possible anymore.
         */
        void shutdown();

        SlaveInterface* getSlaveAtIndex(std::uint16_t slaveIndex) const;

        /**
         * Returns all identifiied slaves on the bus
         * @return
         */
        std::vector<SlaveInterface*> getSlaves() const;

        /**
         * Returns all initialized devices.
         * @return
         */
        const std::vector<std::shared_ptr<DeviceInterface>>& getDevices() const;

        EtherCATState rtGetEtherCATState() const;

        BusFunctionalState rtGetFunctionalState() const;

        bool rtIsEmergencyStopActive() const;

        bool rtHasError() const;

        using SlaveFactory = std::function<std::unique_ptr<SlaveInterface>(SlaveIdentifier sid)>;

        void registerSlaveFactory(const SlaveFactory& factory);

        using DeviceFactory =
            std::function<std::shared_ptr<DeviceInterface>(hardware_config::DeviceConfig& config,
                                                           const VirtualRobot::RobotPtr& robot)>;

        void registerDeviceFactory(const std::string& name, const DeviceFactory& factory);

        // Stuff for switching state of slaves on bus
        bool switchBusToInit();
        bool switchBusToPreOp();
        bool switchBusToSafeOp();
        bool switchBusToOp();

    private:
        bool _initToPreOp();
        bool _initToSafeOp();
        bool _initToOp();
        bool _preOpToInit();
        bool _preOpToSafeOp();
        bool _preOpToOp();
        bool _safeOpToInit();
        bool _safeOpToPreOp();
        bool _safeOpToOp();
        bool _opToInit();
        bool _opToPreOp();
        bool _opToSafeOp();

        bool changeBusState(EtherCATState state);

        bool validateBus() const;

        bool socketInitialized = false;
        bool initSocket();

    private:
        /**
         * Default constructor private to enforce singleton.
         * @see Bus::getBus()
         */
        Bus();
        ~Bus() override;

        //avoid coping the object (singelton pattern)
        Bus(const Bus&);
        Bus& operator=(const Bus&);

        /**
         * Creates the slaves and devices and adds them to the corresponding list.
         * @see Bus::slaves
         * @see Bus::devices
         */
        bool createDevices();

        /**
         * This sets the pointers of the PDO mappings for the slaves
         */
        bool setPDOMappings();

        void readAllErrorCounters();

        std::string ifname;
        /** Socketfiledescriptor on which the ethercat connection is running */
        int socketFileDescriptor = -1;

        /** The current ethercat-state of the whole bus */
        EtherCATState busState;

        /** The current functional state of the bus. This state describes the highlevel state. */
        BusFunctionalState functionalState = BusFunctionalState::Shutdown;

        /** current Bus group we are working on */
        std::uint8_t currentGroup;

        /** List of all slaves */
        std::vector<std::unique_ptr<SlaveInterface>> slaves;

        /** List of all devices */
        std::vector<std::shared_ptr<DeviceInterface>> devices;

        /** List of all registered slave factories */
        std::vector<SlaveFactory> slaveFactories;

        /** Map of all registered device factories */
        std::map<std::string, DeviceFactory> deviceFactories;


        std::unique_ptr<SlaveRegisterReadingScheduler> errorRegisterReadingScheduler;

        PDOValidity pdoValidity = PDOValidity::None;


        bool emergencyStopWasActivated = false;
        IceUtil::Time busUpdateLastUpdateTime;

        hardware_config::HardwareConfig hwconfig;

        VirtualRobot::RobotPtr robot; // TODO: Remove if possible.

        BusErrorHandler errorHandler;
    };
} // namespace armarx::control::ethercat
