/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <cmath>
#include <memory>
#include <type_traits>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>

// Just so that these functionalities are available
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>

#include <armarx/control/hardware_config/Types.h>


namespace armarx::control::ethercat
{

    /**
     * @class LinearConvertedValue
     * @ingroup Library-ethercat
     * @brief Brief description of class LinearConvertedValue.
     *
     * Detailed description of class LinearConvertedValue.
     */
    template <typename T>
    class LinearConvertedValue
    {

    public:
        LinearConvertedValue()
        {
            raw = nullptr;
            offset = factor = 0;
            offsetBeforeFactor = true;
        }

        /**
         * @brief init
         * @param raw
         * @param node
         * @param defaultValue
         * @param offsetBeforeFactor if true the offset is added before multiplying with factor. If false: the other way around.
         * @param nameForDebugging This name is printend in case an error is encountered (Its only purpose is to ease debugging)
         */
        void
        init(T* raw,
             const hardware_config::types::LinearConfig linearConfig,
             float defaultValue = std::nanf("1"),
             bool offsetBeforeFactor = true,
             const char* nameForDebugging = "")
        {
            init(raw,
                 linearConfig.getFactor(),
                 linearConfig.getOffset(),
                 defaultValue,
                 offsetBeforeFactor,
                 nameForDebugging);
        }

        void
        init(T* raw,
             float factor,
             float offset,
             float defaultValue = std::nanf("1"),
             bool offsetBeforeFactor = true,
             const char* nameForDebugging = "")
        {
            const auto rawAsInt = reinterpret_cast<std::uint64_t>(raw);
            ARMARX_CHECK_EXPRESSION((rawAsInt % alignof(T)) == 0)
                << "\nThe alignment is wrong!\nIt has to be " << alignof(T)
                << ", but the data is aligned with " << rawAsInt % alignof(std::max_align_t)
                << "!\nThis is an offset of " << (rawAsInt % alignof(T))
                << " bytes!\nThe datatype is " << GetTypeString<T>() << "\nIts size is "
                << sizeof(T) << "\nraw = " << raw << " bytes\nThe name is " << nameForDebugging;
            this->offsetBeforeFactor = offsetBeforeFactor;
            this->factor = factor;
            this->offset = offset;
            this->raw = raw;
            if (!std::isnan(defaultValue))
            {
                value = defaultValue;
                write();
            }
            else
            {
                value = 0;
                read();
            }
        }

        void
        read()
        {
            if (offsetBeforeFactor)
            {
                value = ((*raw) + offset) * factor;
            }
            else
            {
                value = (*raw) * factor + offset;
            }
        }

        void
        write()
        {
            if (offsetBeforeFactor)
            {
                *raw = static_cast<T>((value / factor) - offset);
            }
            else
            {
                *raw = static_cast<T>((value)-offset) / factor;
            }
        }

        float value;

        T
        getRaw() const
        {
            return *raw;
        }

        float
        getFactor() const
        {
            return factor;
        }

        float
        getOffset() const
        {
            return offset;
        }

        bool
        getOffsetBeforeFactor() const
        {
            return offsetBeforeFactor;
        }

    private:
        T* raw;
        float offset, factor;
        bool offsetBeforeFactor;
    };


    template <typename T, typename SignedT = T>
    class ModularConvertedValue
    {

    public:
        ModularConvertedValue()
        {
            rawValue = nullptr;
            zeroOffset = 0;
            discontinuityOffset = 0;
            factor = 0;
            maxValue = 0;
        }

        void
        init(T* raw,
             const hardware_config::types::ModularConvertedValueConfig modularConfig,
             const char* nameForDebugging = "")
        {
            init(raw,
                 modularConfig.getFactor(),
                 modularConfig.getZeroOffset(),
                 modularConfig.getDiscontinuityOffset(),
                 modularConfig.getMaxValue(),
                 nameForDebugging);
        }

        void
        init(T* raw,
             float factor,
             float zeroOffset,
             float discontinuityOffset,
             float maxValue,
             const char* nameForDebugging = "")
        {
            const auto rawAsInt = reinterpret_cast<std::uint64_t>(raw);
            ARMARX_CHECK_EXPRESSION((rawAsInt % alignof(T)) == 0)
                << "\nThe alignment is wrong!\nIt has to be " << alignof(T)
                << ", but the data is aligned with " << rawAsInt % alignof(std::max_align_t)
                << "!\nThis is an offset of " << (rawAsInt % alignof(T))
                << " bytes!\nThe datatype is " << GetTypeString<T>() << "\nIts size is "
                << sizeof(T) << "\nraw = " << raw << " bytes\nThe name is " << nameForDebugging;

            this->factor = factor;
            this->zeroOffset = static_cast<SignedT>(zeroOffset);
            this->discontinuityOffset = static_cast<SignedT>(discontinuityOffset);
            this->maxValue = static_cast<T>(maxValue);
            this->rawValue = raw;

            read();
        }

        void
        read()
        {
            value = ((((*rawValue) + discontinuityOffset - zeroOffset) % maxValue) - discontinuityOffset) * factor;
        }

        void
        write()
        {
            //*rawValue = static_cast<T>((value / factor) - offset);
        }

        float value;

        T
        getRaw() const
        {
            return *rawValue;
        }

    private:
        T* rawValue;
        SignedT zeroOffset;
        SignedT discontinuityOffset;
        float factor;
        T maxValue;
    };


    /**
     * @class DataInterface
     * @ingroup Library-ethercat
     * @brief Brief description of class DataInterface.
     *
     * Detailed description of class DataInterface.
     */
    class DataInterface
    {

    public:
        DataInterface();
        virtual ~DataInterface();
        virtual void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                        const IceUtil::Time& timeSinceLastIteration) = 0;
        virtual void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                         const IceUtil::Time& timeSinceLastIteration) = 0;
    };

} // namespace armarx::control::ethercat
