#pragma once

#include <atomic>
#include <condition_variable>
#include <filesystem>
#include <fstream>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <thread>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/util/PropagateConst.h>

#include "SlaveIdentifier.h"

/**
 * @defgroup Namespace-error error
 * @ingroup Library-ethercat
 * A description of the namespace error.
 */
namespace armarx::control::ethercat::error
{
    /**
     * @enum Type
     * @ingroup Namespace-error
     *
     * @brief The Type enum
     */
    enum class Type
    {
        BusError, //!< Bussdfnödf
        SlaveError, //!< sdfluieb,dj
        DeviceError
    };

    std::ostream& operator<<(std::ostream& stream, const Type& rhs);

    /**
     * @enum Severity
     * @ingroup Namespace-error
     *
     * @brief The Severity enum
     */
    enum class Severity
    {
        None,
        Warning,
        Error,
        Fatal
    };

    std::ostream& operator<<(std::ostream& stream, const Severity& rhs);

    class Reporter;
    class Reporting;

    /**
     * @class Entry
     * @ingroup Namespace-error
     * @brief Brief description of class Entry.
     *
     * Detailed description of class Entry.
     */
    class Entry
    {
        static constexpr std::uint16_t DEVICE_NAME_BUFFER_SIZE = 64;

        static constexpr std::uint16_t MESSAGE_BUFFER_SIZE = 512;
        static constexpr char OVERFLOW_MESSAGE[] = {"[ERROR MESSAGE TOO LONG]"};
        static_assert(MESSAGE_BUFFER_SIZE > sizeof(OVERFLOW_MESSAGE) + 1);

        static constexpr std::uint16_t FILE_STRING_BUFFER_SIZE = 256;
        static constexpr std::uint16_t FUNCTION_STRING_BUFFER_SIZE = 256;

        friend Reporter;
        friend Reporting;

        char m_message[MESSAGE_BUFFER_SIZE];
        std::size_t m_message_size = 0;
        Type m_type;
        SlaveIdentifier m_sid;
        char m_device_name[DEVICE_NAME_BUFFER_SIZE];
        Severity m_severity;
        IceUtil::Time m_timestamp;
        float m_deactivate_spam_seconds = 0;

        struct MetaInfo
        {
            char file[FILE_STRING_BUFFER_SIZE];
            int line;
            char function[FUNCTION_STRING_BUFFER_SIZE];
        };
        MetaInfo m_metaInfo;

    public:
        Entry() = default;
        ~Entry() = default;

        template <typename... T>
        Entry&
        message(const char* fmt, T... args)
        {
#pragma GCC diagnostic ignored "-Wformat-security"
            int sz = std::snprintf(m_message, sizeof(m_message), fmt, args...);
            if (sz >= static_cast<int>(sizeof(m_message)))
            {
                std::snprintf(m_message + MESSAGE_BUFFER_SIZE - sizeof(OVERFLOW_MESSAGE),
                              sizeof(OVERFLOW_MESSAGE),
                              OVERFLOW_MESSAGE);
                m_message_size = MESSAGE_BUFFER_SIZE;
            }
            else
            {
                m_message_size = static_cast<std::size_t>(sz);
            }
#pragma GCC diagnostic pop
            return *this;
        }

        Entry& slaveIdentifier(SlaveIdentifier sid);
        Entry& errorType(Type type);
        Entry& metaInfo(const char* file, int line, const char* function);
        Entry& deviceName(const char* deviceName);

        std::string toString() const;
    };

    /**
     * @class Reporter
     * @ingroup Namespace-error
     * @brief Brief description of class Reporter.
     *
     * Detailed description of class Reporter.
     */
    class Reporter
    {
        friend Reporting;

        class ReportWrapper
        {
        public:
            ReportWrapper(Entry& error, Reporter* reporter);
            ~ReportWrapper();
            ReportWrapper(const ReportWrapper&) = delete; // Copying not allowed
            ReportWrapper& operator=(const ReportWrapper&) = delete; // Assignment not allowed

            ReportWrapper& deactivateSpam(float seconds);

        private:
            Entry& m_error;
            Reporter* m_reporter;
        };

    public:
        ReportWrapper reportWarning(Entry& error);
        ReportWrapper reportError(Entry& error);
        [[noreturn]] void reportErrorAndThrow(Entry& error);
        bool hasErrors() const;

    private:
        Reporter() = delete;
        Reporter(Reporting* errorReporting);
        Reporting* m_errorReporting;
        unsigned int m_errorCount = 0;
    };

    /**
     * @class Reporting
     * @ingroup Namespace-error
     * @brief Brief description of class Reporting.
     *
     * Detailed description of class Reporting.
     */
    class Reporting : virtual public Logging
    {
    public:
        static Reporting& getErrorReporting();

        Reporter getErrorReporter();
        static Reporter& getGlobalErrorReporter();
        void report(Entry&& error);

        void setMaxErrorHistorySize(std::size_t maxErrorHistorySize);

        std::optional<std::filesystem::path> dumpToFile(std::filesystem::path file,
                                                        std::uint64_t lastNSeconds = 1000000);

    private:
        Reporting(std::size_t maxErrorHistorySize = 1000);
        ~Reporting() override;

        std::vector<Entry> m_errors;

        struct QueueImpl;
        PropagateConst<std::unique_ptr<QueueImpl>> pimpl;

        struct ErrorTimestamp
        {
            std::int64_t timestamp;
            std::uint16_t subid;
            bool
            operator<(const ErrorTimestamp& x) const
            {
                return std::tie(this->timestamp, this->subid) < std::tie(x.timestamp, x.subid);
            }
        };

        std::map<ErrorTimestamp, Entry> m_errorHistory;
        std::size_t m_maxErrorHistorySize;

        std::thread m_errorProcessingThread;
        std::atomic_bool m_errorProcessingRunning{true};
        void errorProcessingLoop();
        std::condition_variable m_errorProcessingCondition;
        std::mutex m_errorProcessingConditionMutex;

        IceUtil::Time m_timestampReportingStart;
    };
} // namespace armarx::control::ethercat::error

namespace armarx::control::ethercat
{
    using Error = error::Entry;
    using ErrorType = error::Type;


#define _detail_BUS_ERROR_CONSTRUCTION(...)                                                        \
    armarx::control::ethercat::Error()                                                             \
        .errorType(armarx::control::ethercat::ErrorType::BusError)                                 \
        .metaInfo(__FILE__, __LINE__, ARMARX_FUNCTION)                                             \
        .message(__VA_ARGS__)

#define BUS_WARNING(...)                                                                           \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportWarning(           \
        _detail_BUS_ERROR_CONSTRUCTION(__VA_ARGS__))
#define BUS_ERROR(...)                                                                             \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportError(             \
        _detail_BUS_ERROR_CONSTRUCTION(__VA_ARGS__))
#define BUS_FATAL_AND_THROW(...)                                                                   \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportErrorAndThrow(     \
        _detail_BUS_ERROR_CONSTRUCTION(__VA_ARGS__))

#define BUS_WARNING_LOCAL(reporter, ...)                                                           \
    (reporter).reportWarning(_detail_BUS_ERROR_CONSTRUCTION(__VA_ARGS__))
#define BUS_ERROR_LOCAL(reporter, ...)                                                             \
    (reporter).reportError(_detail_BUS_ERROR_CONSTRUCTION(__VA_ARGS__))


#define _detail_SLAVE_ERROR_CONSTRUCTION(_sid, ...)                                                \
    armarx::control::ethercat::Error()                                                             \
        .errorType(armarx::control::ethercat::ErrorType::SlaveError)                               \
        .metaInfo(__FILE__, __LINE__, ARMARX_FUNCTION)                                             \
        .message(__VA_ARGS__)                                                                      \
        .slaveIdentifier(_sid)

#define SLAVE_WARNING(sid, ...)                                                                    \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportWarning(           \
        _detail_SLAVE_ERROR_CONSTRUCTION(sid, __VA_ARGS__))
#define SLAVE_ERROR(sid, ...)                                                                      \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportError(             \
        _detail_SLAVE_ERROR_CONSTRUCTION(sid, __VA_ARGS__))
#define SLAVE_FATAL_AND_THROW(sid, ...)                                                            \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportErrorAndThrow(     \
        _detail_SLAVE_ERROR_CONSTRUCTION(sid, __VA_ARGS__))

#define SLAVE_WARNING_LOCAL(reporter, sid, ...)                                                    \
    (reporter).reportWarning(_detail_SLAVE_ERROR_CONSTRUCTION(sid, __VA_ARGS__))
#define SLAVE_ERROR_LOCAL(reporter, sid, ...)                                                      \
    (reporter).reportError(_detail_SLAVE_ERROR_CONSTRUCTION(sid, __VA_ARGS__))


#define _detail_DEVICE_ERROR_CONSTRUCTION(devName, ...)                                            \
    armarx::control::ethercat::Error()                                                             \
        .errorType(armarx::control::ethercat::ErrorType::DeviceError)                              \
        .metaInfo(__FILE__, __LINE__, ARMARX_FUNCTION)                                             \
        .message(__VA_ARGS__)                                                                      \
        .deviceName(devName)

#define DEVICE_WARNING(deviceName, ...)                                                            \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportWarning(           \
        _detail_DEVICE_ERROR_CONSTRUCTION(deviceName, __VA_ARGS__))
#define DEVICE_ERROR(deviceName, ...)                                                              \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportError(             \
        _detail_DEVICE_ERROR_CONSTRUCTION(deviceName, __VA_ARGS__))
#define DEVICE_FATAL_AND_THROW(deviceName, ...)                                                    \
    armarx::control::ethercat::error::Reporting::getGlobalErrorReporter().reportErrorAndThrow(     \
        _detail_DEVICE_ERROR_CONSTRUCTION(deviceName, __VA_ARGS__))

#define DEVICE_WARNING_LOCAL(reporter, deviceName, ...)                                            \
    (reporter).reportWarning(_detail_DEVICE_ERROR_CONSTRUCTION(deviceName, __VA_ARGS__))
#define DEVICE_ERROR_LOCAL(reporter, deviceName, ...)                                              \
    (reporter).reportError(_detail_DEVICE_ERROR_CONSTRUCTION(deviceName, __VA_ARGS__))


} // namespace armarx::control::ethercat
