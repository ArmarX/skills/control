/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Stefan Reither( stefan dot reither at kit dot edu)
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <atomic>
#include <thread>

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::control::ethercat
{
    class SlaveInterface;
    using SlaveList = std::vector<SlaveInterface*>;

    class Bus;

    /**
     * @class BusErrorHandler
     * @ingroup Library-ethercat
     * @brief Brief description of class BusErrorHandler.
     *
     * Detailed description of class BusErrorHandler.
     */
    class BusErrorHandler : public armarx::Logging
    {
    public:
        BusErrorHandler(Bus* bus);
        ~BusErrorHandler() override;

        void rtHandleBusErrors();
        void rtHandleSlaveErrors();

        bool hasError() const;
        bool isReinitializationActive() const;

        bool isSlaveLostOrDuringReinitialization(SlaveInterface* slave) const;

        void init(int wkc);


    private:
        Bus* bus;

        enum class SlaveState
        {
            Operational,
            Lost,
            Reinitializating
        };

        // This member must be updated whenever ...
        // - the workCounter changes
        // - foundAgainSlaves are fully recovered
        // - ?
        std::map<SlaveInterface*, SlaveState> slaveStates;
        std::atomic_bool allSlavesReinitialized{true};

        int secondLastUnexpectedWorkCounter = 0;
        int expectedWorkCounter = -1;

        bool busErrorFound = false;
        bool slaveErrorFound = false;

        void rtUpdateSlaveStates();

        void rtMarkLostSlavesInSOEMStruct() const;

        void reinitializeSlaves();

        std::thread slaveReinitializingThread;
        std::atomic_bool slaveReinitializingThreadRunning{true};
        void slaveReinitializingLoop();

        bool areAllSlavesReinitialized() const;

        /**
         * @brief
         */
        class Watchdog
        {
        public:
            Watchdog(std::uint32_t minDurationUS);
            bool expired() const;
            void reset();

        private:
            std::uint32_t minDuration;
            IceUtil::Time lastTime;
        };

        Watchdog workerCounterWatchdog;
    };
} // namespace armarx::control::ethercat
