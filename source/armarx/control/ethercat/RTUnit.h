/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <array>
#include <fstream>
#include <memory>
#include <sstream>
#include <thread>

#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

/**
 * @defgroup Library-ethercat ethercat
 * @ingroup armarx_control
 * A description of the library ethercat.
 */
namespace armarx::control::ethercat
{

    /**
     * @class RTUnit
     * @ingroup Library-ethercat
     * @brief Brief description of class RTUnit.
     *
     * Detailed description of class RTUnit.
     */
    class RTUnit : virtual public Logging, virtual public RobotUnit
    {

    public:
        RTUnit();
        ~RTUnit() override;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string
        getDefaultName() const override
        {
            return "EtherCATRTUnit";
        }

    protected:
        void onInitRobotUnit() override;
        void onConnectRobotUnit() override;
        void onDisconnectRobotUnit() override;
        void onExitRobotUnit() override;

        void initializeKinematicUnit() override;

        void joinControlThread() override;

        void publish(armarx::StringVariantBaseMap debugObserverMap = {},
                     armarx::StringVariantBaseMap timingMap = {}) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    protected:
        static MultiNodeRapidXMLReader ReadHardwareConfigFile(std::string hardwareConfigFilePath,
                                                              std::string rootNodeName);

        //all the stuff to run the rt Thread
        void startRTThread();

        //        void stopRTThread();

        /** the run method of the rt thread */
        virtual void rtRun();

        bool initBusRTThread();

        void controlLoopRTThread();

        enum class CalibrationStatus
        {
            Calibrating,
            Done
        };

        /**
         * @brief Allows to switch controllers while calibrating
         *
         * use
         * rtSetJointController(JointController* c, std::size_t index)
         * to switch controllers
         */
        virtual void
        rtCalibrateActivateControlers()
        {
        }

        /**
         * @brief Hook to add calibration code
         *
         * This function is called in the rt loop! So you should not take too long!
         *
         * read sensors and write targets
         * while calibrating return CalibrationStatus::Calibrating
         * if done return CalibrationStatus::Done
         *
         * @return Whether done or still calibrating
         */
        virtual CalibrationStatus
        rtCalibrate()
        {
            return CalibrationStatus::Done;
        }

        bool
        rtIsCalibrating() const
        {
            return _calibrationStatus == CalibrationStatus::Calibrating;
        }

        std::uintmax_t
        getIterationCount()
        {
            return _iterationCount;
        }

    private:
        CalibrationStatus _calibrationStatus = CalibrationStatus::Calibrating;
        std::atomic_uintmax_t _iterationCount = 0;

    protected:
        void computeInertiaTorque();

        std::thread rtTask;
        std::atomic_bool taskRunning{false};

        //timestamps for the pdo updates
        IceUtil::Time currentPDOUpdateTimestamp;

        VirtualRobot::RobotPtr robot;

        VirtualRobot::RobotNodeSetPtr rtRobotJointSet, rtRobotBodySet;
        std::vector<std::pair<VirtualRobot::RobotNodePtr, SensorValue1DoFGravityTorque*>>
            nodeJointDataVec;

        IceUtil::Time
        getControlThreadTargetPeriod() const override
        {
            return IceUtil::Time::microSeconds(properties.rtLoopTimeInUS);
        }

        void startBackupLogging();
        void periodicBackupLoggingRun();
        std::thread backupLoggingTask;
        std::atomic_bool backupLoggingRunning{false};

        enum class PeriodicLoggingMode
        {
            None,
            OnlyBusErrors,
            OnlyRtLogging,
            Both
        };

        struct
        {
            std::string busConfigFilePath;
            int socketFileDescriptor = 777;
            std::string ethercatInterfaceName;
            bool useTorqueVelocityModeAsDefault = false;
            int rtLoopTimeInUS = 1000;
            double rTLoopTimingCheckToleranceFactor = 1.0f;

            PeriodicLoggingMode periodicLoggingMode = PeriodicLoggingMode::Both;
            unsigned int periodicLoggingIntervalInSeconds = 300;
            unsigned int periodicLoggingHistorySize = 2;
            std::string periodicLoggingDirectory = "/tmp/periodic_rt_logging/";

            bool enableErrorCountersReading = false;
            unsigned int errorCountersReadingPeriodInMS = 100;
            bool resetErrorRegistersAtStartup = false;
        } properties;
    };

} // namespace armarx::control::ethercat
