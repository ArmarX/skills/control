#include "SlaveErrorRegistersDevice.h"

namespace armarx::control::ethercat
{
    void
    SlaveErrorRegistersDevice::updateValues(const RegisterDataList* data)
    {
        ARMARX_CHECK_EQUAL(data->slaveIndex, this->slaveIndex);

        auto update = [&](std::uint8_t* member, datatypes::RegisterEnum registertype)
        {
            if (data->registerData.count(registertype))
            {
                *member = std::get<datatypes::EtherCATDataType::UNSIGNED8>(
                    data->registerData.at(registertype));
            }
        };

        update(&value.linkLostCounterPort0, datatypes::RegisterEnum::LOST_LINK_COUNTER_PORT_0);
        update(&value.linkLostCounterPort1, datatypes::RegisterEnum::LOST_LINK_COUNTER_PORT_1);
        //        update(&value.linkLostCounterPort2, datatypes::RegisterEnum::LOST_LINK_COUNTER_PORT_2);
        //        update(&value.linkLostCounterPort3, datatypes::RegisterEnum::LOST_LINK_COUNTER_PORT_3);

        update(&value.frameErrorCounterPort0, datatypes::RegisterEnum::FRAME_ERROR_COUNTER_PORT_0);
        update(&value.frameErrorCounterPort1, datatypes::RegisterEnum::FRAME_ERROR_COUNTER_PORT_1);
        //        update(&value.frameErrorCounterPort2, datatypes::RegisterEnum::FRAME_ERROR_COUNTER_PORT_2);
        //        update(&value.frameErrorCounterPort3, datatypes::RegisterEnum::FRAME_ERROR_COUNTER_PORT_3);

        update(&value.rxErrorCounterPort0, datatypes::RegisterEnum::PHYSICAL_ERROR_COUNTER_PORT_0);
        update(&value.rxErrorCounterPort1, datatypes::RegisterEnum::PHYSICAL_ERROR_COUNTER_PORT_1);
        //        update(&value.rxErrorCounterPort2, datatypes::RegisterEnum::PHYSICAL_ERROR_COUNTER_PORT_2);
        //        update(&value.rxErrorCounterPort3, datatypes::RegisterEnum::PHYSICAL_ERROR_COUNTER_PORT_3);

        update(&value.forwardedRxErrorCounterPort0,
               datatypes::RegisterEnum::PREVIOUS_ERROR_COUNTER_PORT_0);
        update(&value.forwardedRxErrorCounterPort1,
               datatypes::RegisterEnum::PREVIOUS_ERROR_COUNTER_PORT_1);
        //        update(&value.forwardedRxErrorCounterPort2,
        //               datatypes::RegisterEnum::PREVIOUS_ERROR_COUNTER_PORT_2);
        //        update(&value.forwardedRxErrorCounterPort3,
        //               datatypes::RegisterEnum::PREVIOUS_ERROR_COUNTER_PORT_3);
    }
} // namespace armarx::control::ethercat
