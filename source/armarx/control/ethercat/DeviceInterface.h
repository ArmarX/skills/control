#pragma once


// STD/STL
#include <memory>
#include <string>

// Simox
#include <VirtualRobot/Robot.h>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/components/units/RobotUnit/Devices/DeviceBase.h>

#include <armarx/control/hardware_config/DeviceConfig.h>

#include "ErrorReporting.h"


namespace armarx::control::ethercat
{

    class SlaveInterface;


    /**
     * @class DeviceInterface
     * @ingroup Library-ethercat
     * @brief Brief description of class DeviceInterface.
     *
     * Detailed description of class DeviceInterface.
     */
    class DeviceInterface : public virtual DeviceBase
    {

    public:
        class SubDeviceInterface : public virtual DeviceBase
        {
        public:
            SubDeviceInterface(const std::string& name) : DeviceBase{name}
            {
            }

            virtual ~SubDeviceInterface()
            {
            }
        };


        DeviceInterface(const std::string& name);

        virtual ~DeviceInterface()
        {
        }

        /**
         * @brief Hook for executing code after the bus has successfully switched
         * into the SafeOp-State.
         * SDO of the slaves can be read and written.
         * PDO data from the slaves have been read after switching to SafeOp,
         * but no new updates to the slaves can bus until the bus in Op-state
         */
        virtual void
        postSwitchToSafeOp()
        {
        }

        /**
         * @brief Hook for executing code after the bus has successfully switched
         * into the Op-State.
         * Before executing this hook, the PDO of all slaves have been updated at least once.
         * Updates can be sent to slaves via PDO.
         */
        virtual void
        postSwitchToOp()
        {
        }

        virtual void
        execute()
        {
        }

        enum class TryAssignResult
        {
            /// Used if the slave was adpoted by this device. (Expected)
            assigned,
            /// Used if the slave is unknown to this device. (Expected)
            unknown,
            /// Used if the slave was already adopted before. (Error)
            alreadyAssigned
        };

        /**
         * @brief Method called by the EtherCAT bus to try to assign slaves
         * to functional devices.
         *
         * @see TryAssignResult
         * @param slave
         *
         * @return TryAssignResult holding the outcome of the method.
         */
        virtual TryAssignResult tryAssign(SlaveInterface& slave) = 0;

        enum class AllAssignedResult
        {
            /// Used if assignments were successful. (Expected)
            ok,
            /// Used if slaves are missing. (Error)
            slavesMissing
        };

        /**
         * @brief Method called by the EtherCAT bus after all slaves have
         * been assigned.
         *
         * This method allows further initialization of assigned slaves and
         * allows indicating missing slaves. When this method was called,
         * all slaves have already been assigned, so if this device is still missing slaves,
         * it should indicate it by returning `AllAssignedResult::slavesMissing`.
         * `AllAssignedResult::ok` indicates that all expected slaves were assigned.
         *
         * @return `ok` if all expected slaves were assigned, `slavesMissing` otherwise.
         */
        virtual AllAssignedResult onAllAssigned() = 0;

        virtual std::string getClassName() const = 0;

        virtual const std::vector<std::shared_ptr<SubDeviceInterface>> getSubDevices() const;

    protected:
        std::vector<std::shared_ptr<SubDeviceInterface>> subDevices;
    };


    template <typename Device>
    std::shared_ptr<DeviceInterface>
    createDevice(hardware_config::DeviceConfig& config, const VirtualRobot::RobotPtr& robot)
    {
        return std::make_shared<Device>(config, robot);
    }


    template <typename SlaveI, typename Slave>
    [[nodiscard]] DeviceInterface::TryAssignResult
    tryAssignUsingSerialNo(SlaveI& slave, Slave*& concreteSlave, std::uint32_t serialNo)
    {
        static_assert(std::is_base_of<SlaveI, Slave>::value);

        using Result = DeviceInterface::TryAssignResult;


        if (Slave::isSlaveIdentifierAccepted(slave.getSlaveIdentifier()))
        {
            Slave* upcastedSlave = dynamic_cast<Slave*>(&slave);

            if (upcastedSlave and upcastedSlave->getSlaveIdentifier().serialNumber == serialNo)
            {
                // Ensures that concreteSlave was not already assigned. Dynamic cast prevents errors with
                // uninitialized pointers which are not nullptr.
                if (concreteSlave == nullptr or dynamic_cast<SlaveI*>(concreteSlave) == nullptr)
                {
                    concreteSlave = upcastedSlave;
                    return Result::assigned;
                }

                return Result::alreadyAssigned;
            }
        }

        // Unknown slave.
        return Result::unknown;
    }

} // namespace armarx::control::ethercat
