armarx_add_library(memory
  HEADERS
    constants.h
    config/Reader.h
    config/Writer.h
  SOURCES
    config/Reader.cpp
    config/Writer.cpp
  DEPENDENCIES
#    armarx_control::core #ocommon
     aron
     armem
)
