/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Writer.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/Clock.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/aron/core/codegeneration/cpp/AronGeneratedClass.h>

#include <armarx/control/common/type.h>
#include <armarx/control/memory/constants.h>

namespace armarx::control::memory::config
{

    Writer::Writer(armem::client::MemoryNameSystem& mns) :
        memoryWriter(
            mns.useWriter(armarx::armem::MemoryID().withMemoryName(memory::constants::MemoryName)))
    {
    }

    bool
    Writer::storeConfig(const common::ControllerType controllerType,
                        const std::string& controllerName,
                        const aron::data::DictPtr& cfg)
    {
        return storeConfig(
            common::ControllerTypeNames.to_name(controllerType), controllerName, cfg);
    }

    bool
    Writer::storeConfig(const std::string& controllerType,
                        const std::string& controllerName,
                        const aron::data::DictPtr& cfg)
    {
        ARMARX_VERBOSE << "Storing result";

        const armem::Time timestamp(Clock::Now());

        armem::EntityUpdate update;
        update.entityID = armem::MemoryID()
                              .withMemoryName(constants::MemoryName)
                              .withCoreSegmentName(constants::ParameterizationCoreSegmentName)
                              .withProviderSegmentName(controllerType)
                              .withEntityName(controllerName)
                              .withTimestamp(timestamp);
        update.timeCreated = timestamp;
        update.instancesData = {cfg};

        armem::Commit commit;
        commit.add(update);

        // std::lock_guard g{memoryWriterMutex()};
        const armem::CommitResult updateResult = memoryWriter.commit(commit);

        ARMARX_VERBOSE << updateResult;

        if (not updateResult.allSuccess())
        {
            ARMARX_ERROR << updateResult.allErrorMessages();
        }
        return updateResult.allSuccess();
    }
    
    bool Writer::storeDefaultConfig(const std::string& controllerType,
                                    const armarx::aron::data::DictPtr& cfg,
                                    const std::string& name)
    {
        ARMARX_VERBOSE << "Storing result";

        const armem::Time timestamp(Clock::Now());

        armem::EntityUpdate update;
        update.entityID = armem::MemoryID()
                              .withMemoryName(constants::MemoryName)
                              .withCoreSegmentName(constants::DefaultParameterizationCoreSegmentName)
                              .withProviderSegmentName(controllerType)
                              .withEntityName(name)
                              .withTimestamp(timestamp);
        update.timeCreated = timestamp;
        update.instancesData = {cfg};

        armem::Commit commit;
        commit.add(update);

        // std::lock_guard g{memoryWriterMutex()};
        const armem::CommitResult updateResult = memoryWriter.commit(commit);

        ARMARX_VERBOSE << updateResult;

        if (not updateResult.allSuccess())
        {
            ARMARX_ERROR << updateResult.allErrorMessages();
        }
        return updateResult.allSuccess();
    }


} // namespace armarx::control::memory::config
