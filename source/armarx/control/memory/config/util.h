/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <filesystem>
#include <fstream>
#include <string_view>

#include <SimoxUtility/json/json.hpp>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/data/rw/reader/nlohmannJSON/NlohmannJSONReaderWithoutTypeCheck.h>

#include <armarx/control/client/ControllerDescription.h>
#include <armarx/control/memory/config/Writer.h>

namespace armarx::control::memory::config
{

    template <auto T>
    bool
    parseAndStoreDefaultConfig(const nlohmann::json& json,
                               Writer& writer,
                               const std::string& configName)
    {
        using ControllerDescriptionType = client::ControllerDescription<T>;
        using AronDTO = typename ControllerDescriptionType::AronDTO;

        const std::string controllerTypeName =
            "PlatformTrajectory"; //ControllerDescriptionType::name;
        ARMARX_CHECK_NOT_EMPTY(controllerTypeName);

        // convert to aron
        AronDTO configDto;

        armarx::aron::data::reader::NlohmannJSONReaderWithoutTypeCheck reader;
        configDto.read(reader, json);

        return writer.storeDefaultConfig(controllerTypeName, configDto.toAron(), configName);
    }


    template <auto T>
    bool
    parseAndStoreDefaultConfigs(const std::string& configBasePath, Writer& writer)
    {

        // using ControllerDescriptionType = client::ControllerDescription<T>;
        const std::string controllerTypeName =
            "PlatformTrajectory"; //  ControllerDescriptionType::name;
        ARMARX_CHECK_NOT_EMPTY(controllerTypeName);

        //  std::filesystem::path(configBasePath

        // std::filesystem::path thisControllerConfigPath = configBasePath;
        // thisControllerConfigPath /= controllerTypeName;

        const std::string thisControllerConfigPath = "/home/fabi/workspace/armarx/skills/navigation/data/armarx_navigation/controller_config/" + controllerTypeName;

        ARMARX_INFO << "Searching for configs in directory " << thisControllerConfigPath;
        ARMARX_CHECK_NOT_EMPTY(thisControllerConfigPath);

        for (const auto& dir_entry : std::filesystem::directory_iterator{thisControllerConfigPath})
        {
            if (not dir_entry.is_regular_file())
            {
                continue;
            }

            const std::string filename = dir_entry.path();

            // the name of the directory
            const std::string configName = dir_entry.path().stem();

            ARMARX_INFO << "Loading config from file `" << filename << "` as default config`"
                        << configName << "`";
            std::ifstream ifs{filename};

            nlohmann::json jsonConfig;
            ifs >> jsonConfig;

            if (not parseAndStoreDefaultConfig<T>(jsonConfig, writer, configName))
            {
                ARMARX_WARNING << "Failed to store config from file `" << filename
                               << "` as default config`" << configName << "`";
            }
        }
    }


} // namespace armarx::control::memory::config
