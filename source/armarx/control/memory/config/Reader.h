/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>

#include <armarx/control/common/type.h>
#include <armarx/control/memory/constants.h>


namespace armarx::control::memory::config
{

    class Reader
    {
    public:
        Reader(armem::client::MemoryNameSystem& mns);

        armarx::aron::data::DictPtr
        getDefaultConfig(common::ControllerType ct,
                         const std::string& name = constants::DEFAULT_NAME) const;


        armarx::aron::data::DictPtr
        getConfig(common::ControllerType ct,
                  const std::string& name = constants::DEFAULT_NAME) const;

        armarx::aron::data::DictPtr getConfig(const armarx::armem::MemoryID& memoryId) const;

        armarx::aron::data::DictPtr
        getDefaultConfig(const std::string& controllerName,
                         const std::string& name = constants::DEFAULT_NAME) const;

        armarx::aron::data::DictPtr
        getConfig(const std::string& controllerName,
                  const std::string& name = constants::DEFAULT_NAME) const;

    private:
        armarx::armem::client::Reader memoryReaderClient;
    };

} // namespace armarx::control::memory::config
