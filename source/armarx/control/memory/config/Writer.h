/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Writer.h>

#include <armarx/control/common/type.h>
#include <armarx/control/memory/constants.h>

namespace armarx::control::memory::config
{

    class Writer
    {
    public:
        Writer(armem::client::MemoryNameSystem& mns);


        bool storeConfig(common::ControllerType controllerType,
                         const std::string& controllerName,
                         const armarx::aron::data::DictPtr& cfg);

        bool storeConfig(const std::string& controllerType,
                         const std::string& controllerName,
                         const armarx::aron::data::DictPtr& cfg);

        bool storeDefaultConfig(const std::string& controllerType,
                                const armarx::aron::data::DictPtr& cfg,
                                const std::string& name = constants::DEFAULT_NAME);


    private:
        armarx::armem::client::Writer memoryWriter;
    };


} // namespace armarx::control::memory::config
