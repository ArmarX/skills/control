/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Reader.h"

namespace armarx::control::memory::config
{

    Reader::Reader(armem::client::MemoryNameSystem& mns) :
        memoryReaderClient(mns.useReader(memory::constants::MemoryName))
    {
    }

    armarx::aron::data::DictPtr
    Reader::getDefaultConfig(const common::ControllerType ct, const std::string& name) const
    {
        const auto controllerName = common::ControllerTypeNames.to_name(ct);
        return getDefaultConfig(controllerName, name);
    }

    armarx::aron::data::DictPtr
    Reader::getDefaultConfig(const std::string& controllerName, const std::string& name) const
    {
        return getConfig(
            armarx::armem::MemoryID(memory::constants::MemoryName,
                                    memory::constants::DefaultParameterizationCoreSegmentName,
                                    controllerName,
                                    name));
    }

    armarx::aron::data::DictPtr
    Reader::getConfig(const common::ControllerType ct, const std::string& name) const
    {
        const auto controllerName = common::ControllerTypeNames.to_name(ct);
        return getConfig(controllerName, name);
    }

    armarx::aron::data::DictPtr
    Reader::getConfig(const std::string& controllerName, const std::string& name) const
    {
        return getConfig(armarx::armem::MemoryID(memory::constants::MemoryName,
                                                 memory::constants::ParameterizationCoreSegmentName,
                                                 controllerName,
                                                 name));
    }

    armarx::aron::data::DictPtr
    Reader::getConfig(const armem::MemoryID& memoryId) const
    {
        ARMARX_CHECK(memoryId.hasMemoryName());
        ARMARX_CHECK(memoryId.hasCoreSegmentName());
        ARMARX_CHECK(memoryId.hasProviderSegmentName());
        ARMARX_CHECK(memoryId.hasEntityName());

        const auto result = memoryReaderClient.queryMemoryIDs({memoryId});

        if (not result)
        {
            ARMARX_WARNING << "No config matches " << memoryId;
            return nullptr;
        }

        if (not result.memory.hasEntity(memoryId))
        {
            ARMARX_WARNING << "No config matches " << memoryId;
            return nullptr;
        }

        const auto entity = result.memory.getEntity(memoryId);

        const auto& snapshot = entity.getLatestSnapshot();

        if (not snapshot.hasInstance(0))
        {
            ARMARX_WARNING << "No config matches (empty snapshot) " << memoryId;
            return nullptr;
        }

        return snapshot.getInstance(0).data();
    }


} // namespace armarx::control::memory::config
