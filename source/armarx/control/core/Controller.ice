/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
 
#pragma once


#include <ArmarXCore/interface/serialization/Eigen.ice>

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>
#include <RobotAPI/interface/aron.ice>
#include <RobotAPI/interface/core/PoseBase.ice>

[["python:pkgdir:armarx"]]
module armarx
{
    module control
    {

          interface ControllerInterface {//extends armem::client::MemoryListenerInterface {

                void updateConfig(armarx::aron::data::dto::Dict config);

                // void moveTo(Eigen::Matrix4fSeq waypoints, string navigationFrame, string callerId);

                // void moveTo2(detail::Waypoints waypoints, string navigationFrame, string callerId);

                // void
                // moveTowards(Eigen::Vector3f direction, string navigationFrame, string callerId);

                // idempotent void pause(string callerId);
                // idempotent void resume(string callerId);
                // idempotent void stop(string callerId);
                // idempotent void stopAll();

                // bool isPaused(string callerId);
                // bool isStopped(string callerId);
            };

            interface ControllerCreator
            {
              ControllerInterface create(armarx::aron::data::dto::Dict config, string callerId);
            };
        
            
    };
};
