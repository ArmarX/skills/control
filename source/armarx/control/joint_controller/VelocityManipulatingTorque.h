/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <cmath>

// armarx
#include <ArmarXCore/observers/filters/rtfilters/GaussianFilter.h>

#include <RobotAPI/libraries/core/PIDController.h>

// control
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>


namespace armarx::control::joint_controller
{
    using VelocityManipulatingTorqueControllerConfigurationPtr =
        std::shared_ptr<class VelocityManipulatingTorqueControllerConfiguration>;

    class VelocityManipulatingTorqueControllerConfiguration
    {
    public:
        VelocityManipulatingTorqueControllerConfiguration();
        static VelocityManipulatingTorqueControllerConfigurationPtr
        CreateTorqueConfigData(hardware_config::Config& config,
                               bool limitless,
                               float jointLimitLow,
                               float jointLimitHigh);
        double pid_proportional_gain = 0;
        double pid_integral_gain = 0;
        double pid_derivative_gain = 0;
        double pid_max_value = 1.0;
        double accelerationGain = 0.1;
        double deadZone = 0.1;
        double decay = 0.99;
        double maxVelocity = 3.0;
        double maxAcceleration = 10.0;
        double maxJerk = 500.0;
        double torqueToCurrent = 0.0;
        double maxTargetTorque = 10.0;
        bool limitless = true;
        double jointLimitHigh = 0.0, jointLimitLow = 0.0;
        double pushbackMargin = 5.0 / 180. * M_PI;
        double pushBackTorque = 100;
        std::vector<float> firFilterImpulseResponse;
    };

    class VelocityManipulatingTorqueController
    {
    public:
        VelocityManipulatingTorqueController(
            const VelocityManipulatingTorqueControllerConfigurationPtr& torqueConfigData);
        float update(const IceUtil::Time& sensorValuesTimestamp,
                     const IceUtil::Time& timeSinceLastIteration,
                     float gravity,
                     float actualTorque,
                     float targetTorque,
                     float actualVelocity,
                     float actualPosition);
        void reset();


        double getCurrentTargetVelocity() const;

        const VelocityManipulatingTorqueControllerConfigurationPtr& getTorqueConfigData() const;
        const PIDController& getPid();
        double getLastAcceleration() const;

        double getLastJerk() const;

    protected:
        double calcJointLimitProtectionTorque(float actualPosition);

    private:
        PIDController pid;
        PIDController pidTargetTorque;
        armarx::rtfilters::GaussianFilter torqueGaussianFilter;
        armarx::control::rt_filters::RtMedianFilter torqueMedianFilter, actualVelocityFilter;
        VelocityManipulatingTorqueControllerConfigurationPtr torqueConfigData;
        double currentTargetVelocity = 0.0;
        double lastAcceleration = 0.0;
        double lastJerk = 0.0;
    };

} // namespace armarx::control::joint_controller
