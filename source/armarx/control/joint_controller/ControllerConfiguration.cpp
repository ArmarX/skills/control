#include "ControllerConfiguration.h"


namespace armarx::control::joint_controller
{

    PositionControllerConfigurationPtr
    PositionControllerConfiguration::CreatePositionControllerConfigData(
        hardware_config::Config& config)
    {
        PositionControllerConfiguration configData;

        configData.maxVelocityRad = config.getFloat("maxVelocityRad");
        configData.maxAccelerationRad = config.getFloat("maxAccelerationRad");
        configData.maxDecelerationRad = config.getFloat("maxDecelerationRad");
        configData.p = config.getFloat("p");
        configData.accuracy = config.getFloat("accuracy");

        return std::make_shared<PositionControllerConfiguration>(configData);
    }

    VelocityControllerConfigurationPtr
    VelocityControllerConfiguration::CreateVelocityControllerConfigData(
        hardware_config::Config& config)
    {
        VelocityControllerConfiguration configData;

        configData.maxVelocityRad = config.getFloat("maxVelocityRad");
        configData.maxAccelerationRad = config.getFloat("maxAccelerationRad");
        configData.maxDecelerationRad = config.getFloat("maxDecelerationRad");

        return std::make_shared<VelocityControllerConfiguration>(configData);
    }

    HolonomicPlatformControllerConfigurationPtr
    HolonomicPlatformControllerConfiguration::CreateHolonomicPlatformControllerConfigData(
        hardware_config::Config& config)
    {
        HolonomicPlatformControllerConfiguration configData;

        configData.maxVelocity = config.getFloat("maxVelocity");
        configData.maxAcceleration = config.getFloat("maxAcceleration");
        configData.maxDeceleration = config.getFloat("maxDeceleration");
        configData.maxAngularVelocity = config.getFloat("maxAngularVelocity");
        configData.maxAngularAcceleration = config.getFloat("maxAngularAcceleration");
        configData.maxAngularDeceleration = config.getFloat("maxAngularDeceleration");

        return std::make_shared<HolonomicPlatformControllerConfiguration>(configData);
    }

} // namespace armarx::control::joint_controller
