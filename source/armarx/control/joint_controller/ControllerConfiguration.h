#pragma once


// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>

#include <armarx/control/hardware_config/Config.h>


namespace armarx::control::joint_controller
{

    class PositionControllerConfiguration;
    using PositionControllerConfigurationPtr = std::shared_ptr<PositionControllerConfiguration>;


    class PositionControllerConfiguration
    {
    public:
        PositionControllerConfiguration()
        {
        }
        static PositionControllerConfigurationPtr
        CreatePositionControllerConfigData(hardware_config::Config& config);
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
        float p;
        float accuracy;
    };


    class VelocityControllerConfiguration;
    using VelocityControllerConfigurationPtr = std::shared_ptr<VelocityControllerConfiguration>;


    class VelocityControllerConfiguration
    {
    public:
        VelocityControllerConfiguration()
        {
        }
        static VelocityControllerConfigurationPtr
        CreateVelocityControllerConfigData(hardware_config::Config& config);
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
    };


    class HolonomicPlatformControllerConfiguration;
    using HolonomicPlatformControllerConfigurationPtr =
        std::shared_ptr<HolonomicPlatformControllerConfiguration>;


    class HolonomicPlatformControllerConfiguration
    {
    public:
        HolonomicPlatformControllerConfiguration()
        {
        }
        static HolonomicPlatformControllerConfigurationPtr
        CreateHolonomicPlatformControllerConfigData(hardware_config::Config& config);
        float maxVelocity;
        float maxAcceleration;
        float maxDeceleration;
        float maxAngularVelocity;
        float maxAngularAcceleration;
        float maxAngularDeceleration;
    };

} // namespace armarx::control::joint_controller
