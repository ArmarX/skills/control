/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::BimanualCartesianAdmittanceControllerGuiWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualCartesianAdmittanceControllerGuiWidgetController.h"

#include <string>

#include <SimoxUtility/math/convert/mat4f_to_rpy.h>
#include <SimoxUtility/math/convert/pos_rpy_to_mat4f.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

namespace armarx
{
    void
    clearLayout(QLayout* layout)
    {
        QLayoutItem* item;
        while ((item = layout->takeAt(0)))
        {
            if (item->layout())
            {
                clearLayout(item->layout());
                delete item->layout();
            }
            if (item->widget())
            {
                delete item->widget();
            }
            delete item;
        }
    }

    BimanualCartesianAdmittanceControllerGuiWidgetController::
        BimanualCartesianAdmittanceControllerGuiWidgetController() :
        NJointControllerGuiPluginBase("NJointBimanualCartesianAdmittanceController")
    {
        _ui.setupUi(getWidget());
        connectCreateAcivateDeactivateDelete(_ui.pushButtonCtrlCreate,
                                             _ui.pushButtonCtrlActivate,
                                             _ui.pushButtonCtrlDeactivate,
                                             _ui.pushButtonCtrlDelete);
        using T = BimanualCartesianAdmittanceControllerGuiWidgetController;
        connect(_ui.pushButtonReadCurrentPose,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonReadCurrentPose_clicked);
        connect(
            _ui.pushButtonTargAdd, &QPushButton::clicked, this, &T::on_pushButtonTargAdd_clicked);
        connect(_ui.pushButtonCfgSendDefaultPose,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendDefaultPose_clicked);
        connect(_ui.pushButtonCfgSendNullspace,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendNullspace_clicked);
        connect(_ui.pushButtonCfgSendImpedance,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendImpedance_clicked);
        connect(_ui.pushButtonCfgSendAdmittance,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendAdmittance_clicked);
        connect(_ui.pushButtonCfgSendForce,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendForce_clicked);
        connect(_ui.pushButtonCfgSendAll,
                &QPushButton::clicked,
                this,
                &T::on_pushButtonCfgSendAll_clicked);
        connect(
            _ui.pushButtonTargSend, &QPushButton::clicked, this, &T::on_pushButtonTargSend_clicked);
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::setupGuiAfterConnect()
    {
        //fill rns combo box
        {
            const auto fill = [&](auto rnsname, auto& lay, auto& sp)
            {
                static const std::map<std::string, double> defaults{
                    ///TODO
                };

                clearLayout(lay);
                int i = 0;
                for (const auto& rn : _robot->getRobotNodeSet(rnsname)->getAllRobotNodes())
                {
                    const auto&& n = rn->getName();
                    lay->addWidget(new QLabel{QString::fromStdString(n)}, i, 0);
                    auto b = new QDoubleSpinBox;
                    sp.addWidget(b);
                    lay->addWidget(b, i, 1);
                    const auto lo = rn->getJointLimitLow();
                    const auto hi = rn->getJointLimitHigh();
                    b->setMinimum(lo);
                    b->setMaximum(hi);
                    b->setValue(defaults.count(n) ? defaults.at(n) : (lo + hi) / 2);
                    ++i;
                }
            };
            fill("LeftArm", _ui.gridLayoutDefaultPoseL, _desiredJointValuesLeft);
            fill("RightArm", _ui.gridLayoutDefaultPoseR, _desiredJointValuesRight);
        }
    }
} // namespace armarx
//read config
namespace armarx
{
    std::array<Ice::FloatSeq, 2>
    BimanualCartesianAdmittanceControllerGuiWidgetController::readDesiredJointCFG() const
    {
        return {_desiredJointValuesLeft.get<std::vector<float>>(),
                _desiredJointValuesRight.get<std::vector<float>>()};
    }
    detail::NJBmanCartAdmCtrl::Nullspace
    BimanualCartesianAdmittanceControllerGuiWidgetController::readNullspaceCFG() const
    {
        detail::NJBmanCartAdmCtrl::Nullspace c;
        c.k = _ui.doubleSpinBoxNK->value();
        c.d = _ui.doubleSpinBoxND->value();
        const auto arms = readDesiredJointCFG();
        c.desiredJointValuesLeft = arms.at(0);
        c.desiredJointValuesRight = arms.at(1);
        return c;
    }
    std::array<detail::NJBmanCartAdmCtrl::Impedance, 2>
    BimanualCartesianAdmittanceControllerGuiWidgetController::readImpedanceCFG() const
    {
        detail::NJBmanCartAdmCtrl::Impedance l;
        detail::NJBmanCartAdmCtrl::Impedance r;

        l.KpXYZ(0) = _ui.doubleSpinBoxIPTXL->value();
        l.KpXYZ(1) = _ui.doubleSpinBoxIPTYL->value();
        l.KpXYZ(2) = _ui.doubleSpinBoxIPTZL->value();

        l.KpRPY(0) = _ui.doubleSpinBoxIPRXL->value();
        l.KpRPY(1) = _ui.doubleSpinBoxIPRYL->value();
        l.KpRPY(2) = _ui.doubleSpinBoxIPRZL->value();

        l.KdXYZ(0) = _ui.doubleSpinBoxIDTXL->value();
        l.KdXYZ(1) = _ui.doubleSpinBoxIDTYL->value();
        l.KdXYZ(2) = _ui.doubleSpinBoxIDTZL->value();

        l.KdRPY(0) = _ui.doubleSpinBoxIDRXL->value();
        l.KdRPY(1) = _ui.doubleSpinBoxIDRYL->value();
        l.KdRPY(2) = _ui.doubleSpinBoxIDRZL->value();

        r.KpXYZ(0) = _ui.doubleSpinBoxIPTXR->value();
        r.KpXYZ(1) = _ui.doubleSpinBoxIPTYR->value();
        r.KpXYZ(2) = _ui.doubleSpinBoxIPTZR->value();

        r.KpRPY(0) = _ui.doubleSpinBoxIPRXR->value();
        r.KpRPY(1) = _ui.doubleSpinBoxIPRYR->value();
        r.KpRPY(2) = _ui.doubleSpinBoxIPRZR->value();

        r.KdXYZ(0) = _ui.doubleSpinBoxIDTXR->value();
        r.KdXYZ(1) = _ui.doubleSpinBoxIDTYR->value();
        r.KdXYZ(2) = _ui.doubleSpinBoxIDTZR->value();

        r.KdRPY(0) = _ui.doubleSpinBoxIDRXR->value();
        r.KdRPY(1) = _ui.doubleSpinBoxIDRYR->value();
        r.KdRPY(2) = _ui.doubleSpinBoxIDRZR->value();

        return {l, r};
    }
    std::array<detail::NJBmanCartAdmCtrl::Force, 2>
    BimanualCartesianAdmittanceControllerGuiWidgetController::readForceCFG() const
    {
        detail::NJBmanCartAdmCtrl::Force l;
        detail::NJBmanCartAdmCtrl::Force r;

        l.wrenchXYZ(0) = _ui.doubleSpinBoxFWTXL->value();
        l.wrenchXYZ(1) = _ui.doubleSpinBoxFWTYL->value();
        l.wrenchXYZ(2) = _ui.doubleSpinBoxFWTZL->value();

        l.wrenchRPY(0) = _ui.doubleSpinBoxFWRXL->value();
        l.wrenchRPY(1) = _ui.doubleSpinBoxFWRYL->value();
        l.wrenchRPY(2) = _ui.doubleSpinBoxFWRZL->value();

        l.mass = _ui.doubleSpinBoxFML->value();

        l.offsetForce(0) = _ui.doubleSpinBoxFOFXL->value();
        l.offsetForce(1) = _ui.doubleSpinBoxFOFYL->value();
        l.offsetForce(2) = _ui.doubleSpinBoxFOFZL->value();

        l.offsetTorque(0) = _ui.doubleSpinBoxFOTXL->value();
        l.offsetTorque(1) = _ui.doubleSpinBoxFOTYL->value();
        l.offsetTorque(2) = _ui.doubleSpinBoxFOTZL->value();

        l.forceThreshold(0) = _ui.doubleSpinBoxFFTXL->value();
        l.forceThreshold(1) = _ui.doubleSpinBoxFFTYL->value();
        l.forceThreshold(2) = _ui.doubleSpinBoxFFTZL->value();

        r.wrenchXYZ(0) = _ui.doubleSpinBoxFWTXR->value();
        r.wrenchXYZ(1) = _ui.doubleSpinBoxFWTYR->value();
        r.wrenchXYZ(2) = _ui.doubleSpinBoxFWTZR->value();

        r.wrenchRPY(0) = _ui.doubleSpinBoxFWRXR->value();
        r.wrenchRPY(1) = _ui.doubleSpinBoxFWRYR->value();
        r.wrenchRPY(2) = _ui.doubleSpinBoxFWRZR->value();

        r.mass = _ui.doubleSpinBoxFMR->value();

        r.offsetForce(0) = _ui.doubleSpinBoxFOFXR->value();
        r.offsetForce(1) = _ui.doubleSpinBoxFOFYR->value();
        r.offsetForce(2) = _ui.doubleSpinBoxFOFZR->value();

        r.offsetTorque(0) = _ui.doubleSpinBoxFOTXR->value();
        r.offsetTorque(1) = _ui.doubleSpinBoxFOTYR->value();
        r.offsetTorque(2) = _ui.doubleSpinBoxFOTZR->value();

        r.forceThreshold(0) = _ui.doubleSpinBoxFFTXR->value();
        r.forceThreshold(1) = _ui.doubleSpinBoxFFTYR->value();
        r.forceThreshold(2) = _ui.doubleSpinBoxFFTZR->value();
        return {l, r};
    }
    detail::NJBmanCartAdmCtrl::Admittance
    BimanualCartesianAdmittanceControllerGuiWidgetController::readAdmittanceCFG() const
    {
        detail::NJBmanCartAdmCtrl::Admittance c;

        c.KpXYZ(0) = _ui.doubleSpinBoxAPTX->value();
        c.KpXYZ(1) = _ui.doubleSpinBoxAPTY->value();
        c.KpXYZ(2) = _ui.doubleSpinBoxAPTZ->value();

        c.KpRPY(0) = _ui.doubleSpinBoxAPRX->value();
        c.KpRPY(1) = _ui.doubleSpinBoxAPRY->value();
        c.KpRPY(2) = _ui.doubleSpinBoxAPRZ->value();

        c.KdXYZ(0) = _ui.doubleSpinBoxADTX->value();
        c.KdXYZ(1) = _ui.doubleSpinBoxADTY->value();
        c.KdXYZ(2) = _ui.doubleSpinBoxADTZ->value();

        c.KdRPY(0) = _ui.doubleSpinBoxADRX->value();
        c.KdRPY(1) = _ui.doubleSpinBoxADRY->value();
        c.KdRPY(2) = _ui.doubleSpinBoxADRZ->value();

        c.KmXYZ(0) = _ui.doubleSpinBoxAMTX->value();
        c.KmXYZ(1) = _ui.doubleSpinBoxAMTY->value();
        c.KmXYZ(2) = _ui.doubleSpinBoxAMTZ->value();

        c.KmRPY(0) = _ui.doubleSpinBoxAMRX->value();
        c.KmRPY(1) = _ui.doubleSpinBoxAMRY->value();
        c.KmRPY(2) = _ui.doubleSpinBoxAMRZ->value();

        return c;
    }
    NJointControllerConfigPtr
    BimanualCartesianAdmittanceControllerGuiWidgetController::readFullCFG() const
    {
        NJointBimanualCartesianAdmittanceControllerConfigPtr c =
            new NJointBimanualCartesianAdmittanceControllerConfig;
        c->kinematicChainRight = "RightArm";
        c->kinematicChainLeft = "LeftArm";
        c->ftSensorRight = "FT R";
        c->ftSensorLeft = "FT L";
        c->ftSensorRightFrame = "ArmR8_Wri2";
        c->ftSensorLeftFrame = "ArmL8_Wri2";
        c->box.width = _ui.doubleSpinBoxOBoxw->value();
        c->filterCoeff = _ui.doubleSpinBoxOFiltCoeff->value();
        c->torqueLimit = _ui.doubleSpinBoxOTorqueLim->value();
        c->ftCalibrationTime = _ui.doubleSpinBoxOCalibTime->value();
        c->nullspace = readNullspaceCFG();
        c->admittanceObject = readAdmittanceCFG();
        const auto f = readForceCFG();
        c->forceLeft = f.at(0);
        c->forceRight = f.at(1);
        const auto i = readImpedanceCFG();
        c->impedanceLeft = i.at(0);
        c->impedanceRight = i.at(1);
        return NJointControllerConfigPtr::dynamicCast(c);
    }
    std::array<Eigen::Vector3f, 2>
    BimanualCartesianAdmittanceControllerGuiWidgetController::readPosTarg() const
    {
        Eigen::Vector3f xyz;
        Eigen::Vector3f rpy;

        xyz(0) = _ui.doubleSpinBoxTargTX->value();
        xyz(1) = _ui.doubleSpinBoxTargTY->value();
        xyz(2) = _ui.doubleSpinBoxTargTZ->value();

        rpy(0) = _ui.doubleSpinBoxTargRX->value();
        rpy(1) = _ui.doubleSpinBoxTargRY->value();
        rpy(2) = _ui.doubleSpinBoxTargRZ->value();

        return {xyz, rpy};
    }
    std::array<Eigen::Vector3f, 2>
    BimanualCartesianAdmittanceControllerGuiWidgetController::readVelTarg() const
    {
        Eigen::Vector3f xyz;
        Eigen::Vector3f rpy;

        xyz(0) = _ui.doubleSpinBoxTargVTX->value();
        xyz(1) = _ui.doubleSpinBoxTargVTY->value();
        xyz(2) = _ui.doubleSpinBoxTargVTZ->value();

        rpy(0) = _ui.doubleSpinBoxTargVRX->value();
        rpy(1) = _ui.doubleSpinBoxTargVRY->value();
        rpy(2) = _ui.doubleSpinBoxTargVRZ->value();

        return {xyz, rpy};
    }
} // namespace armarx
//push buttons
namespace armarx
{
    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::on_pushButtonCfgSendAll_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        _controller->setConfig(
            NJointBimanualCartesianAdmittanceControllerConfigPtr::dynamicCast(readFullCFG()));
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::on_pushButtonCfgSendForce_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const auto c = readForceCFG();
        _controller->setForceConfig(c.at(0), c.at(1));
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::
        on_pushButtonCfgSendAdmittance_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        _controller->setAdmittanceConfig(readAdmittanceCFG());
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::
        on_pushButtonCfgSendImpedance_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const auto c = readImpedanceCFG();
        _controller->setImpedanceConfig(c.at(0), c.at(1));
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::
        on_pushButtonCfgSendNullspace_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        _controller->setNullspaceConfig(readNullspaceCFG());
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::
        on_pushButtonCfgSendDefaultPose_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const auto c = readDesiredJointCFG();
        _controller->setDesiredJointValuesLeft(c.at(0));
        _controller->setDesiredJointValuesRight(c.at(1));
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::on_pushButtonTargSend_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const auto t = readPosTarg();
        const auto v = readVelTarg();
        const Eigen::Matrix4f m = simox::math::pos_rpy_to_mat4f(t.at(0), t.at(1));
        _controller->setBoxPoseAndVelocity(m, v.at(0), v.at(1));
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::on_pushButtonTargAdd_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const auto t = readPosTarg();
        const Eigen::Matrix4f m = simox::math::pos_rpy_to_mat4f(t.at(0), t.at(1));
        _controller->moveBoxPose(m);
    }

    void
    BimanualCartesianAdmittanceControllerGuiWidgetController::on_pushButtonReadCurrentPose_clicked()
    {
        std::lock_guard g{_allMutex};
        if (!_controller)
        {
            return;
        }
        const Eigen::Matrix4f m = _controller->getBoxPose();
        const auto rpy = simox::math::mat4f_to_rpy(m);
        _ui.doubleSpinBoxTargTX->setValue(m(0, 3));
        _ui.doubleSpinBoxTargTY->setValue(m(1, 3));
        _ui.doubleSpinBoxTargTZ->setValue(m(2, 3));
        _ui.doubleSpinBoxTargRX->setValue(rpy(0));
        _ui.doubleSpinBoxTargRY->setValue(rpy(1));
        _ui.doubleSpinBoxTargRZ->setValue(rpy(2));
    }
} // namespace armarx
