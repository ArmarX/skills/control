armarx_set_target("CartesianImpedanceControllerGuiPlugin")
armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES 
    CartesianImpedanceControllerGuiPlugin.cpp
    CartesianImpedanceControllerWidgetController.cpp
    CartesianImpedanceControllerConfigWidget.cpp
)
set(HEADERS 
    CartesianImpedanceControllerGuiPlugin.h
    CartesianImpedanceControllerWidgetController.h
    CartesianImpedanceControllerConfigWidget.h
)

set(GUI_MOC_HDRS ${HEADERS})
set(GUI_UIS CartesianImpedanceControllerWidget.ui CartesianImpedanceControllerConfigWidget.ui)
set(COMPONENT_LIBS NJointControllerGuiPluginUtility RobotAPINJointControllerWidgets RobotUnit)

if(ArmarXGui_FOUND)
    armarx_gui_library(CartesianImpedanceControllerGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
