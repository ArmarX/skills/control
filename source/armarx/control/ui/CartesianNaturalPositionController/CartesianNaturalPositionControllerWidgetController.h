/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::CartesianNaturalPositionControllerWidgetController
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <mutex>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/gui-plugins/CartesianNaturalPositionController/ui_CartesianNaturalPositionControllerWidget.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/natik/CartesianNaturalPositionControllerProxy.h>

namespace armarx
{
    /**
    \page armarx_control-GuiPlugins-CartesianNaturalPositionController CartesianNaturalPositionController
    \brief The CartesianNaturalPositionController allows visualizing ...

    \image html CartesianNaturalPositionController.png
    The user can

    API Documentation \ref CartesianNaturalPositionControllerWidgetController

    \see CartesianNaturalPositionControllerGuiPlugin
    */

    /**
     * \class CartesianNaturalPositionControllerWidgetController
     * \brief CartesianNaturalPositionControllerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT CartesianNaturalPositionControllerWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<
            CartesianNaturalPositionControllerWidgetController>
    {
        Q_OBJECT

    public:
        struct NullspaceTarget
        {
            QCheckBox* checkBox;
            QSlider* slider;
            QLabel* label;
            size_t i;
        };

        /**
         * Controller Constructor
         */
        explicit CartesianNaturalPositionControllerWidgetController();

        /**
         * Controller destructor
         */
        virtual ~CartesianNaturalPositionControllerWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString
        GetWidgetName()
        {
            return "RobotControl.NJointControllers.CartesianNaturalPositionController";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

    public slots:
        /* QT slot declarations */
        void onConnectComponentQt();
        void on_pushButtonCreateController_clicked();
        void on_anyDeltaPushButton_clicked();
        void on_sliders_valueChanged(int);
        void on_checkBoxAutoKp_stateChanged(int);
        void on_checkBoxSetOri_stateChanged(int);
        void on_anyNullspaceCheckbox_stateChanged(int);
        void on_anyNullspaceSlider_valueChanged(int);
        void on_horizontalSliderPosVel_valueChanged(int);

    signals:
        /* QT signal declarations */
        void invokeConnectComponentQt();
        void invokeDisconnectComponentQt();

    private:
        void deleteOldController();
        void readRunCfgFromUi(CartesianNaturalPositionControllerConfig& runCfg);
        void timerEvent(QTimerEvent*) override;
        void updateTarget(const Eigen::Matrix4f& newTarget);
        void updateKpSliders(const CartesianNaturalPositionControllerConfig& runCfg);
        void updateKpSliderLabels(const CartesianNaturalPositionControllerConfig& runCfg);
        void updateNullspaceTargets();


        std::string _robotStateComponentName;
        std::string _robotUnitName;
        RobotStateComponentInterfacePrx _robotStateComponent;
        RobotUnitInterfacePrx _robotUnit;
        Ui::CartesianNaturalPositionControllerWidget _ui;
        QPointer<SimpleConfigDialog> _dialog;
        CartesianNaturalPositionControllerProxyPtr _controller;
        std::string _controllerName;
        VirtualRobot::RobotPtr _robot;
        std::vector<Eigen::Matrix4f> _lastParsedWPs;
        bool _supportsFT{false};
        bool _setOri = true;
        mutable std::recursive_mutex _allMutex;
        int _timer;
        Eigen::Matrix4f _tcpTarget;

        std::map<QObject*, Eigen::Vector3f> _deltaMapPos;
        std::map<QObject*, Eigen::Vector3f> _deltaMapOri;
        //CartesianNaturalPositionControllerConfig        _runCfg;

        std::vector<NullspaceTarget> _nullspaceTargets;
    };
} // namespace armarx
