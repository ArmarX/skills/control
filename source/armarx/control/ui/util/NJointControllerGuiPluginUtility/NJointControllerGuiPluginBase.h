/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointControllerGuiPluginUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "detail/NJointControllerGuiPluginBase.h"

namespace armarx
{
    template <class Derived, class Proxy>
    class NJointControllerGuiPluginBase : public detail::_NJointControllerGuiPluginBase::Base
    {
    public:
        NJointControllerGuiPluginBase(const std::string& ctrlClass) :
            detail::_NJointControllerGuiPluginBase::Base(), _ctrlClass{ctrlClass}
        {
        }
        ~NJointControllerGuiPluginBase()
        {
            deleteController();
        }

    public:
        QString
        getWidgetName() const final override
        {
            return Derived::GetWidgetName();
        }
        QIcon
        getWidgetIcon() const final override
        {
            return Derived::GetWidgetIcon();
        }
        QIcon
        getWidgetCategoryIcon() const final override
        {
            return Derived::GetWidgetCategoryIcon();
        }

    public:
        void
        createController() override
        {
            ARMARX_TRACE;
            std::lock_guard g{_allMutex};
            if (_controller || !getRobotUnit())
            {
                ARMARX_IMPORTANT << "No RobotUnit or controller already created";
                return;
            }
            ARMARX_IMPORTANT << "Creating " << _ctrlClass << " '" << getControllerName() << "'";
            _controller = Proxy::checkedCast(getRobotUnit()->createNJointController(
                _ctrlClass, getControllerName(), readFullCFG()));
            ARMARX_CHECK_NOT_NULL(_controller);
        }
        void
        activateController() override
        {
            ARMARX_TRACE;
            std::lock_guard g{_allMutex};
            if (!_controller)
            {
                ARMARX_IMPORTANT << "No controller";
                return;
            }
            ARMARX_IMPORTANT << "activating controller";
            _controller->activateController();
        }
        void
        deactivateController() override
        {
            ARMARX_TRACE;
            std::lock_guard g{_allMutex};
            if (!_controller)
            {
                ARMARX_IMPORTANT << "No controller";
                return;
            }
            ARMARX_IMPORTANT << "deactivating controller";
            _controller->deactivateController();
        }
        void
        deleteController() override
        {
            ARMARX_TRACE;
            std::lock_guard g{_allMutex};
            if (!_controller || !getRobotUnit())
            {
                ARMARX_IMPORTANT << "No controller";
                return;
            }
            ARMARX_IMPORTANT << "deleting old controller '" << getControllerName() << "'";
            try
            {
                _controller->deactivateAndDeleteController();
            }
            catch (...)
            {
            }
            _controller = nullptr;
        }

    protected:
        Proxy _controller;
        std::string _ctrlClass;
    };
} // namespace armarx
