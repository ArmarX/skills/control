#include "NJointControllerGuiPluginBase.h"

#include <QPushButton>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

namespace armarx::detail::_NJointControllerGuiPluginBase
{
    std::string
    Base::getControllerName() const
    {
        return getName() + "_controller";
    }

    void
    Base::createController()
    {
    }
    void
    Base::activateController()
    {
    }
    void
    Base::deactivateController()
    {
    }
    void
    Base::deleteController()
    {
    }

    Base::Base()
    {
    }

    Base::~Base()
    {
    }

    void
    Base::connectCreateAcivateDeactivateDelete(QPushButton* cr,
                                               QPushButton* ac,
                                               QPushButton* dc,
                                               QPushButton* de)
    {
        if (cr)
        {
            connect(cr, &QPushButton::clicked, this, &Base::createController);
        }
        if (ac)
        {
            connect(ac, &QPushButton::clicked, this, &Base::activateController);
        }
        if (dc)
        {
            connect(dc, &QPushButton::clicked, this, &Base::deactivateController);
        }
        if (de)
        {
            connect(de, &QPushButton::clicked, this, &Base::deleteController);
        }
    }

    void
    Base::loadSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        getRobotStateComponentPlugin().setRobotStateComponentName(
            settings->value("rsc", "Armar6StateComponent").toString().toStdString());
        getRobotUnitComponentPlugin().setRobotUnitName(
            settings->value("ru", "Armar6Unit").toString().toStdString());
    }

    void
    Base::saveSettings(QSettings* settings)
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        settings->setValue(
            "rsc",
            QString::fromStdString(getRobotStateComponentPlugin().getRobotStateComponentName()));
        settings->setValue(
            "ru", QString::fromStdString(getRobotUnitComponentPlugin().getRobotUnitName()));
    }

    QPointer<QDialog>
    Base::getConfigDialog(QWidget* parent)
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<RobotUnitInterfacePrx>({"ru", "Robot Unit", "*Unit"});
            _dialog->addProxyFinder<RobotStateComponentInterfacePrx>(
                {"rsc", "Robot State Component", "*Component"});
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }

    void
    Base::configured()
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        getRobotStateComponentPlugin().setRobotStateComponentName(_dialog->getProxyName("rsc"));
        getRobotUnitComponentPlugin().setRobotUnitName(_dialog->getProxyName("ru"));
    }

    void
    Base::preOnConnectComponent()
    {
        ARMARX_TRACE;
        ArmarXComponentWidgetController::preOnConnectComponent();
        RobotUnitComponentPluginUser::preOnConnectComponent();
        RobotStateComponentPluginUser::preOnConnectComponent();
        std::lock_guard g{_allMutex};
        _robot = addRobot("state robot", VirtualRobot::RobotIO::eStructure);
    }
    void
    Base::postOnConnectComponent()
    {
        ARMARX_TRACE;
        ArmarXComponentWidgetController::postOnConnectComponent();
        RobotUnitComponentPluginUser::postOnConnectComponent();
        RobotStateComponentPluginUser::postOnConnectComponent();
        QMetaObject::invokeMethod(this, "doSetupGuiAfterConnect", Qt::QueuedConnection);
    }
    void
    Base::postOnDisconnectComponent()
    {
        ARMARX_TRACE;
        ArmarXComponentWidgetController::postOnDisconnectComponent();
        RobotUnitComponentPluginUser::postOnDisconnectComponent();
        RobotStateComponentPluginUser::postOnDisconnectComponent();
        std::lock_guard g{_allMutex};
        deleteController();
    }

    void
    Base::doSetupGuiAfterConnect()
    {
        ARMARX_TRACE;
        std::lock_guard g{_allMutex};
        if (_robot)
        {
            synchronizeLocalClone(_robot);
        }
        ARMARX_IMPORTANT << "call setupGuiAfterConnect";
        setupGuiAfterConnect();
    }

    void
    Base::setupGuiAfterConnect()
    {
    }
} // namespace armarx::detail::_NJointControllerGuiPluginBase
