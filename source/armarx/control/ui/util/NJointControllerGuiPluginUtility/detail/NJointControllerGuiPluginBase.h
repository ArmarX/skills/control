/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointControllerGuiPluginUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>


namespace armarx::detail::_NJointControllerGuiPluginBase
{
    class Base :
        public ArmarXComponentWidgetController,
        public virtual RobotUnitComponentPluginUser,
        public virtual RobotStateComponentPluginUser
    {
        Q_OBJECT
    public:
        virtual NJointControllerConfigPtr readFullCFG() const = 0;
        std::string getControllerName() const;
    public Q_SLOTS: //for some reason using slots does not work -> use Q_SLOTS
        virtual void createController();
        virtual void activateController();
        virtual void deactivateController();
        virtual void deleteController();
    private Q_SLOTS:
        void doSetupGuiAfterConnect();

    public:
        Base();
        ~Base() override;

        void connectCreateAcivateDeactivateDelete(QPushButton* cr,
                                                  QPushButton* ac,
                                                  QPushButton* dc,
                                                  QPushButton* de);

    public:
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

    public:
        void
        onInitComponent() override
        {
        }
        void
        onConnectComponent() override
        {
        }
        void
        onDisconnectComponent() override
        {
        }
        void
        onExitComponent() override
        {
        }

    public:
        virtual void setupGuiAfterConnect();

    protected:
        void preOnConnectComponent() override;
        void postOnConnectComponent() override;
        void postOnDisconnectComponent() override;

    protected:
        QPointer<SimpleConfigDialog> _dialog;

        mutable std::recursive_mutex _allMutex;
        VirtualRobot::RobotPtr _robot;
    };
} // namespace armarx::detail::_NJointControllerGuiPluginBase
