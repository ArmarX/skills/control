/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::CartesianWaypointControlGuiWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/gui-plugins/CartesianWaypointControlGui/ui_CartesianWaypointControlGuiWidget.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianWaypointController.h>
#include <RobotAPI/libraries/NJointControllerGuiPluginUtility/NJointControllerGuiPluginBase.h>

namespace armarx
{
    /**
    \page armarx_control-GuiPlugins-CartesianWaypointControlGui CartesianWaypointControlGui
    \brief The CartesianWaypointControlGui allows visualizing ...

    \image html CartesianWaypointControlGui.png
    The user can

    API Documentation \ref CartesianWaypointControlGuiWidgetController

    \see CartesianWaypointControlGuiGuiPlugin
    */

    /**
     * \class CartesianWaypointControlGuiWidgetController
     * \brief CartesianWaypointControlGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT CartesianWaypointControlGuiWidgetController :
        public NJointControllerGuiPluginBase<CartesianWaypointControlGuiWidgetController,
                                             NJointCartesianWaypointControllerInterfacePrx>
    {
        Q_OBJECT
    public:
        /// Controller Constructor
        explicit CartesianWaypointControlGuiWidgetController();
        ~CartesianWaypointControlGuiWidgetController();

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString
        GetWidgetName()
        {
            return "RobotControl.NJointControllers.CartesianWaypointControlGui";
        }

        void onConnectComponent() override;
        NJointControllerConfigPtr readFullCFG() const override;
        void setupGuiAfterConnect() override;

    private slots:
        void on_pushButtonExecute_clicked();
        void on_pushButtonZeroFT_clicked();
        void on_pushButtonSendSettings_clicked();
        void createController() override;
        void copyCurrentPose();

        void triggerParsing();

    private:
        NJointCartesianWaypointControllerRuntimeConfig readRunCfg() const;
        void timerEvent(QTimerEvent*) override;

    private:
        Ui::CartesianWaypointControlGuiWidget _ui;
        std::vector<Eigen::Matrix4f> _lastParsedWPs;
        bool _supportsFT{false};
        int _timer{0};
    };
} // namespace armarx
