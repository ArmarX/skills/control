#include "ComponentPlugin.h"

#include <memory>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <RobotAPI/components/armem/MemoryNameSystem/MemoryNameSystem.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>

// ComponentPlugin

namespace armarx::control::client
{

    ComponentPlugin::ComponentPlugin(armarx::ManagedIceObject& parent, const std::string& prefix) :
        armarx::ComponentPlugin(parent, prefix)
    {
        addPlugin(armemPlugin);
        addPlugin(robotUnitPlugin);
    }


    ComponentPlugin::~ComponentPlugin() = default;

    void
    ComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        armemPlugin->postCreatePropertyDefinitions(properties);
        robotUnitPlugin->postCreatePropertyDefinitions(properties);
    }

    void
    ComponentPlugin::preOnInitComponent()
    {
        armemPlugin->preOnInitComponent();
        robotUnitPlugin->preOnInitComponent();
    }

    void
    ComponentPlugin::preOnConnectComponent()
    {
        armemPlugin->preOnConnectComponent();
        robotUnitPlugin->preOnConnectComponent();

        ARMARX_INFO << "Reader: connecting to memory";
        configReader = memory::config::Reader(armemPlugin->getMemoryNameSystemClient());

        ARMARX_INFO << "Writer: connecting to memory";
        configWriter = memory::config::Writer(armemPlugin->getMemoryNameSystemClient());
    }


    // ComponentPluginUser

    ComponentPluginUser::ComponentPluginUser()
    {
        ARMARX_TRACE;
        addPlugin(plugin);
    }

    ComponentPlugin&
    ComponentPluginUser::getControlComponentPlugin()
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        return *plugin;
    }


    ComponentPluginUser::~ComponentPluginUser() = default;

} // namespace armarx::control::client
