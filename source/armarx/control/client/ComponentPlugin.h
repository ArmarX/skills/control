/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <filesystem>
#include <optional>
#include <string>
#include <type_traits>

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>

#include <armarx/control/client/ControllerBuilder.h>
#include <armarx/control/client/ControllerDescription.h>
#include <armarx/control/client/ControllerWrapper.h>
#include <armarx/control/common/type.h>
#include <armarx/control/core/Controller.h>
#include <armarx/control/memory/config/Reader.h>
#include <armarx/control/memory/config/Writer.h>
#include <armarx/control/memory/constants.h>
#include <armarx/control/interface/ConfigurableNJointControllerInterface.h>


namespace armarx::armem::client
{
    class MemoryNameSystem;
}

namespace armarx::armem::client::plugins
{
    class Plugin;
}

namespace armarx::plugins
{
    class RobotUnitComponentPlugin;
}

namespace armarx::control::client
{

    class ComponentPlugin : public armarx::ComponentPlugin
    {
    public:
        ComponentPlugin(ManagedIceObject& parent, const std::string& prefix);
        ~ComponentPlugin() override;

        void postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties) override;

        void preOnInitComponent() override;
        void preOnConnectComponent() override;


        template <auto T, typename... Args>
        auto
        createControllerBuilder(Args... args)
        {
            // Probe if a proper builder is returned ...
            using BuilderT = decltype(concreteControllerBuilder<T>(args...));

            // ... otherwise, print a meaningful message.
            static_assert(not std::is_void<BuilderT>::value,
                          "This controller type is not supported at the moment!");

            return concreteControllerBuilder<T>(args...);
        }

        template <auto T>
        auto
        createControllerBuilder()
        {
            return createControllerBuilder<T>(robotUnitPlugin->getRobotUnit(),
                                              configReader.value(),
                                              configWriter.value(),
                                              parent().getName());
        }

        armarx::plugins::RobotUnitComponentPlugin&
        getRobotUnitPlugin()
        {
            return *robotUnitPlugin;
        }
        armarx::armem::client::plugins::Plugin&
        getArmemPlugin()
        {
            return *armemPlugin;
        }

        memory::config::Reader&
        configMemoryReader()
        {
            ARMARX_CHECK(configReader.has_value());
            return configReader.value();
        }

        memory::config::Writer&
        configMemoryWriter()
        {
            ARMARX_CHECK(configWriter.has_value());
            return configWriter.value();
        }

    private:
        template <auto T, typename... Args>
        constexpr auto
        concreteControllerBuilder(Args... args)
        {
            return ControllerBuilder<T>(args...);
        }
        // static constexpr const char* PROPERTY_NAME = "nav.NavigatorName";

        std::optional<memory::config::Reader> configReader;
        std::optional<memory::config::Writer> configWriter;

        armarx::armem::client::plugins::Plugin* armemPlugin = nullptr;
        armarx::plugins::RobotUnitComponentPlugin* robotUnitPlugin = nullptr;
    };


    class ComponentPluginUser : virtual public ManagedIceObject
    {

    public:
        ComponentPluginUser();


        // Non-API
        ~ComponentPluginUser() override;

        template <auto T>
        auto
        controllerBuilder()
        {
            ARMARX_CHECK_NOT_NULL(plugin);
            return plugin->createControllerBuilder<T>();
        }

        ComponentPlugin& getControlComponentPlugin();

    private:
        ComponentPlugin* plugin = nullptr;
    };


} // namespace armarx::control::client
