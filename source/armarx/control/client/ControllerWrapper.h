/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>
#include <type_traits>

#include <RobotAPI/libraries/aron/core/codegeneration/cpp/AronGeneratedClass.h>

#include <armarx/control/client/ControllerDescription.h>
#include <armarx/control/common/type.h>
#include <armarx/control/memory/config/Writer.h>
#include <armarx/control/memory/constants.h>
#include <armarx/control/interface/ConfigurableNJointControllerInterface.h>

namespace armarx::control::client
{


    template <auto T>
    class ControllerWrapper
    {
    public:
        using ControllerPrx = armarx::control::
            ConfigurableNJointControllerInterfacePrx; // ::armarx::control::ControllerInterfacePtr;

        using ControllerDescriptionType = ControllerDescription<T>;
        using AronConfigT = typename ControllerDescriptionType::AronDTO;


        static_assert(not std::is_void<typename ControllerDescriptionType::AronDTO>::value,
                      "You must provide a ControllerDescription!");

        ControllerWrapper() = default;

        ControllerWrapper(const ControllerPrx& controller,
                          const std::string& controllerName,
                          const memory::config::Writer& configWriter,
                          AronConfigT config) :
            config(config),
            controller(controller),
            controllerName(controllerName),
            configWriter(configWriter)
        {
        }

        AronConfigT config;
        static_assert(std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass,
                                      AronConfigT>::value,
                      "asdf");

        void
        updateConfig()
        {
            ARMARX_INFO << "Updating config for controller `" << controllerName << "`";

            ARMARX_CHECK_NOT_NULL(controller);
            controller->updateConfig(config.toAronDTO());

            // store the config also in the memory (logging only)
            configWriter.storeConfig(
                std::string(ControllerDescriptionType::name), controllerName, config.toAron());
        }

        ~ControllerWrapper()
        {

            // deactivate the controller
            if (not daemonized)
            {
                ARMARX_INFO << "Deactivating and deleting controller `" << controllerName << "`";
                // FIXME does not work atm:  controller->deactivateAndDeleteController();
            }

            // => switch to velocity mode ??

            // delete controll
        }

        void
        activate()
        {
            ARMARX_INFO << "Activating controller `" << controllerName << "`";
            controller->activateController();
        }

        void
        deactivate()
        {
            ARMARX_INFO << "Deactivating controller `" << controllerName << "`";
            controller->deactivateController();
        }

        /**
         * @brief 
         * 
         */
        void
        daemonize(const bool daemonize = true)
        {
            ARMARX_INFO << "Daemonizing controller `" << controllerName << "`";
            daemonized = daemonize;
        }

        ControllerPrx&
        ctrl()
        {
            return controller;
        }

    private:
        ControllerPrx controller;
        const std::string controllerName;

        memory::config::Writer configWriter;

        // Indicates whether this controller will be deactivated and deleted from the RobotUnit
        // upon deletion of this object.
        bool daemonized = false;
    };

} // namespace armarx::control::client
